<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Sample FirebaseUI App</title>
        <script src="https://www.gstatic.com/firebasejs/5.3.0/firebase.js"></script>
        <script src="../../assets/js/Oauth/firebase-config.js" type="text/javascript"></script>
        <script src="../../assets/js/Oauth/firebase-account.js"></script>
    </head>
    <body>
        <h1>Welcome</h1>
        <div id="sign-in-status"></div>
        <div id="sign-in"></div>
        <div id="account-details"></div>
    </body>
</html>