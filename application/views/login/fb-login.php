<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Sample FirebaseUI App</title>
    <link type="text/css" rel="stylesheet" href="https://cdn.firebase.com/libs/firebaseui/3.2.0/firebaseui.css" />
</head>
<body>
	<h1>Welcome</h1>
    <div id="firebaseui-auth-container"></div>
</body>
	<script src="https://www.gstatic.com/firebasejs/5.3.0/firebase.js"></script>
	<script src="https://cdn.firebase.com/libs/firebaseui/3.2.0/firebaseui.js"></script>
	<script src="../../assets/js/Oauth/firebase-config.js" type="text/javascript"></script>
	<script src="../../assets/js/Oauth/firebase-config-ui.js" type="text/javascript"></script>
</html>