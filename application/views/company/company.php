<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <?php echo create_breadcrumb(); ?>
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    Ganti Password
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <!--begin: Search Form -->
                        <div class="m-form m-form--label-align-right m--margin-bottom-30">
                            <div class="row align-items-center">
                                <div class="col-xl-8 order-2 order-xl-1"> </div>
                                <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                                    <a href="#" id="add_new" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                                        <span>
                                            <i class="la la-plus-circle"></i>
                                            <span>
                                                Tambah Data
                                            </span>
                                        </span>
                                    </a>
                                    <div class="m-separator m-separator--dashed d-xl-none"></div>
                                </div>
                            </div>
                        </div>
                        <!--end: Search Form -->
                        <table id="tb-perusahaan" style="width:100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>ID</th>
                                    <th>Perusahaan</th>
                                    <th>Email</th>
                                    <th>Telepon</th>
                                    <th>Provinsi</th>
                                    <th>Kota</th>
                                    <th>Alamat</th>
                                    <th>Foto</th>
                                    <th>Deskripsi</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <!--end::Portlet-->
            </div>
        </div>
    </div>
</div>

<!--begin::Modal-->
<div class="modal fade" id="m_modal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    Form Perusahaan
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <form id="form-data">
                <div class="modal-body">
                    <input type="hidden" name="id" id="id">
                    <div class="form-group">
                        <label class="form-control-label">
                            Nama Perusahaan: <span class="m--font-danger">*</span>
                        </label>
                        <input type="text" class="form-control" id="company" name="company" required="required">
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">
                            Email: <span class="m--font-danger">*</span>
                        </label>
                        <input type="email" class="form-control" id="email" name="email" required="required">
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">
                            Telepon: <span class="m--font-danger">*</span>
                        </label>
                        <input type="text" class="form-control" id="telpon" name="telpon" required="required">
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">
                            Jumlah Karyawan: <span class="m--font-danger">*</span>
                        </label>
                        <input type="number" class="form-control" id="jumlah" name="jumlah" value="0" required="required">
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">
                            Tanggal Mulai: <span class="m--font-danger">*</span>
                        </label>
                        <input type="text" class="form-control" id="mulai" name="mulai" required="required">
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">
                            Tgl Selesai: <span class="m--font-danger">*</span>
                        </label>
                        <input type="text" class="form-control" id="selesai" name="selesai" required="required">
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">
                            Provinsi: <span class="m--font-danger">*</span>
                        </label>
                        <?php echo form_dropdown('provinsi', $provinsi, '', 'id="provinsi" class="form-control m-select2"  style="width:100%;" required="required"'); ?>
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">
                            Kota: <span class="m--font-danger">*</span>
                        </label>
                        <select name="kota" id="kota" class="form-control m-select2"  style="width:100%;" required="required"> </select>
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">
                            Alamat: <span class="m--font-danger">*</span>
                        </label>
                        <textarea class="form-control" name="alamat" id="alamat" rows="5" required="required"></textarea>
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">
                            Logo Perusahaan: 
                        </label>
                        <input type="file" name="foto" id="foto" class="form-control">
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">
                            Deskripsi: 
                        </label>
                        <textarea class="form-control" name="deskripsi" id="deskripsi" rows="5"></textarea>
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">
                            Setting Gaji
                        </label>
                        <select class="form-control" name="salary" id="salary" required="required">
                            <option value="0">Fix Salary</option>
                            <option value="1">Upload Excel</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">
                            Username: <span class="m--font-danger">*</span>
                        </label>
                        <input type="text" class="form-control" id="username" name="username" required="required">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        Close
                    </button>
                    <button type="submit" id="simpan" class="btn btn-primary">
                        Submit
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<!--end::Modal-->
