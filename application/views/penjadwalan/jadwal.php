<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <?php echo create_breadcrumb(); ?>
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    <?php echo web_title(); ?>
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <!--begin: Search Form -->
                        <div class="m-form m-form--label-align-right m--margin-bottom-30">
                            <div class="row align-items-center">
                                <div class="col-xl-6 order-2 order-xl-1"> </div>
                                <div class="col-xl-3 order-1 order-xl-2 m--align-right" style="margin-bottom: 20px;">
                                    <a href="#" id="copy_jadwal" class="btn btn-focus m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                                        <span>
                                            <i class="la la-copy"></i>
                                            <span>
                                                Copy Jadwal
                                            </span>
                                        </span>
                                    </a>
                                    <div class="m-separator m-separator--dashed d-xl-none"></div>
                                </div>
                                <div class="col-xl-3 order-1 order-xl-2 m--align-right" style="margin-bottom: 20px;">
                                    <a href="#" id="add_new" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                                        <span>
                                            <i class="la la-plus-circle"></i>
                                            <span>
                                                Tambah Data
                                            </span>
                                        </span>
                                    </a>
                                    <div class="m-separator m-separator--dashed d-xl-none"></div>
                                </div>
                                <div class="col-xl-3 order-1 order-xl-2">
                                    <div class="input-group">
                                        <input type="text" class="form-control m-input" id="search" placeholder="Select time">
                                    </div>
                                </div>
                                <div class="col-xl-3 order-1 order-xl-2">
                                    <?php echo form_dropdown('company-search', $company, '', 'id="company-search" class="form-control"'); ?>
                                </div>
                                <div class="col-xl-3 order-1 order-xl-2">
                                    <div class="input-group">
                                        <select name="search-karyawan" id="search-karyawan" class="form-control"> </select>
                                    </div>
                                </div>
                                <div class="col-xl-3 order-1 order-xl-2">
                                    <a href="javascript:;" id="search-btn" class="btn btn-primary m-btn m-btn--icon">
                                        <span>
                                            <i class="fa fa-search"></i>
                                            <span>
                                                Cari
                                            </span>
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!--end: Search Form -->
                        <table id="tb-jenis" style="width:100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Perusahaan</th>
                                    <th>Nama</th>
                                    <th>Tanggal</th>
                                    <th>Shift</th>
                                    <th>Jam Masuk</th>
                                    <th>Jam Pulang</th>
                                    <th>Keterangan</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <!--end::Portlet-->
            </div>
        </div>
    </div>
</div>

<!--begin::Modal-->
<div class="modal fade" id="m_modal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    Form Shift
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <form id="form-data">
                <div class="modal-body">
                    <input type="hidden" name="id" id="id">
                    <div class="form-group">
                        <label class="form-control-label">
                            Perusahaan: <span class="m--font-danger">*</span>
                        </label>
                        <?php echo form_dropdown('company', $company, '', 'id="company" class="form-control m-select2" required="required" style="width:100%;"'); ?>
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">
                            Karyawan: <span class="m--font-danger">*</span>
                        </label>
                        <?php echo form_dropdown('karyawan', [], '', 'id="karyawan" class="form-control m-select2" required="required" style="width:100%;"'); ?>
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">
                            Shift: <span class="m--font-danger">*</span>
                        </label>
                        <?php echo form_dropdown('shift', [], '', 'id="shift" class="form-control m-select2" required="required" style="width:100%;"'); ?>
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">
                            Tanggal Mulai: <span class="m--font-danger">*</span>
                        </label>
                        <input type="text" class="form-control" id="tgl_mulai" name="tgl_mulai" required="required">
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">
                            Tanggal Selesai: <span class="m--font-danger">*</span>
                        </label>
                        <input type="text" class="form-control" id="tgl_selesai" name="tgl_selesai" required="required">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        Close
                    </button>
                    <button type="submit" id="simpan" class="btn btn-primary">
                        Submit
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="m_copy" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    Copy Jadwal
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <form id="form-data-copy">
                <div class="modal-body">
                    <div class="m-form__heading">
                        <h3 class="m-form__heading-title">
                            Copy Dari:
                        </h3>
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">
                            Perusahaan: <span class="m--font-danger">*</span>
                        </label>
                        <?php echo form_dropdown('company-copy', $company, '', 'id="company-copy" class="form-control m-select2" required="required" style="width:100%;"'); ?>
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">
                            Karyawan: <span class="m--font-danger">*</span>
                        </label>
                        <?php echo form_dropdown('karyawan-copy', [], '', 'id="karyawan-copy" class="form-control m-select2" required="required" style="width:100%;"'); ?>
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">
                            Tanggal Mulai: <span class="m--font-danger">*</span>
                        </label>
                        <input type="text" class="form-control" id="tgl_mulai-copy" name="tgl_mulai-copy" required="required">
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">
                            Tanggal Selesai: <span class="m--font-danger">*</span>
                        </label>
                        <input type="text" class="form-control" id="tgl_selesai-copy" name="tgl_selesai-copy" required="required">
                    </div>
                    <div class="m-form__heading">
                        <h3 class="m-form__heading-title">
                            Copy Ke:
                        </h3>
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">
                            Mode: <span class="m--font-danger">*</span>
                        </label>
                        <?php echo form_dropdown('mode-copy', $mode, '', 'id="mode-copy" class="form-control" required="required" style="width:100%;"'); ?>
                    </div>
                    <div class="form-group" id="divisi" style="display: none;">
                        <label class="form-control-label">
                            Divisi:
                        </label>
                        <?php echo form_dropdown('divisi-copy', [], '', 'id="divisi-copy" class="form-control" style="width:100%;"'); ?>
                    </div>
                    <div class="form-group" id="target" style="display: none;">
                        <label class="form-control-label">
                            Karyawan: <span class="m--font-danger">*</span>
                        </label>
                        <?php echo form_dropdown('target-copy', [], '', 'id="target-copy" class="form-control m-select2" style="width:100%;"'); ?>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        Close
                    </button>
                    <button type="submit" id="simpan" class="btn btn-primary">
                        Submit
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>