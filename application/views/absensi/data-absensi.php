<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <?php echo create_breadcrumb(); ?>
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    <?php echo web_title(); ?>
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <!--begin: Search Form -->
                        <div class="m-form m-form--label-align-right m--margin-bottom-30">
                            <div class="row align-items-center">
                                <div class="col-xl-3 order-1 order-xl-2">
                                    <div class="input-group">
                                        <input type="text" class="form-control m-input" id="search" placeholder="Select time">
                                    </div>
                                </div>
                                <div class="col-xl-3 order-1 order-xl-2">
                                    <?php echo form_dropdown('company-search', $company, '', 'id="company-search" class="form-control"'); ?>
                                </div>
                                <div class="col-xl-3 order-1 order-xl-2">
                                    <a href="javascript:;" id="search-btn" class="btn btn-primary m-btn m-btn--icon">
                                        <span>
                                            <i class="fa fa-search"></i>
                                            <span>
                                                Cari
                                            </span>
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!--end: Search Form -->
                        <table id="tb-absensi" style="width:100%" class="table-striped table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Perusahaan</th>
                                    <th>NIK</th>
                                    <th>Nama</th>
                                    <th>Tanggal</th>
                                    <th>Status</th>
                                    <th>Keterangan</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <!--end::Portlet-->
            </div>
        </div>
    </div>
</div>

<!--begin::Modal-->
<div class="modal fade" id="m_modal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    Form Absensi
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <form id="form-data">
                <div class="modal-body">
                    <input type="hidden" name="id" id="id">
                    <input type="hidden" name="id_karyawan" id="id_karyawan">
                    <div class="form-group">
                        <label class="form-control-label">
                            Perusahaan: <span class="m--font-danger">*</span>
                        </label>
                        <?php echo form_dropdown('company', $company, '', 'id="company" class="form-control m-select2 dropdown-select2" required="required" style="width:100%;"'); ?>
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">
                            Nama Karyawan: <span class="m--font-danger">*</span>
                        </label>
                        <select name="nama" id="nama" class="form-control m-select2 dropdown-select2" style="width: 100%;" required="required"></select>
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">
                            Tanggal: <span class="m--font-danger">*</span>
                        </label>
                        <input type="text" class="form-control" id="tgl" name="tgl" required="required" readonly="readonly">
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">
                            Status: <span class="m--font-danger">*</span>
                        </label>
                        <?php echo form_dropdown('absensi', $absensi, '', 'id="absensi" class="form-control m-select2 dropdown-select2" required="required" style="width:100%;"'); ?>
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">
                            Keterangan:
                        </label>
                        <textarea name="keterangan" id="keterangan" cols="30" rows="5" class="form-control"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        Close
                    </button>
                    <button type="submit" id="simpan" class="btn btn-primary">
                        Submit
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<!--end::Modal-->
