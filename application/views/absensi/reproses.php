<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <?php echo create_breadcrumb(); ?>
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    <?php echo web_title(); ?>
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <form id="form-reproses">
                            <div class="form-group m-form__group">
                                <label>
                                    Tanggal
                                </label>
                                <input type="text" class="form-control m-input col-md-5" id="search" placeholder="Select time">
                            </div>
                            <div class="form-group m-form__group">
                                <label>
                                    Perusahaan
                                </label>
                                <div class="clearfix"></div>
                                <?php echo form_dropdown('company-search', $company, '', 'id="company-search" class="form-control dropdown-select2 col-md-5"'); ?>
                            </div>
                            <div class="form-group m-form__group">
                                <label>
                                    Karyawan
                                </label>
                                <div class="clearfix"></div>
                                <?php echo form_dropdown('karyawan-search', [], '', 'id="karyawan-search" class="form-control dropdown-select2 col-md-5"'); ?>
                            </div>
                            <div class="form-group m-form__group">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-refresh"></i> Proses Absensi</button>
                            </div>
                        </form>
                    </div>
                </div>
                <!--end::Portlet-->
            </div>
        </div>
    </div>
</div>
