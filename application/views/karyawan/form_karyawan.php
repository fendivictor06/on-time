<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Karyawan
                            </span>
                        </a>
                    </li>
                    <li class="m-nav__separator">
                        -
                    </li>
                    <li class="m-nav__item">
                        <a href="" class="m-nav__link">
                            <span class="m-nav__link-text">
                                Form Karyawan
                            </span>
                        </a>
                    </li>
                </ul>       
            </div>
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet-->
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Form Karyawan
                                </h3>
                            </div>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" id="form-data">
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group row">
                                <input type="hidden" name="id" id="id" value="<?php echo isset($edit->id) ? $edit->id : ''; ?>">
                                <div class="col-lg-6">
                                    <label>
                                        Perusahaan: <span class="m--font-danger">*</span>
                                    </label>
                                    <?php echo form_dropdown('company', $company, isset($edit->id_company) ? $edit->id_company : '', 'id="company" class="form-control m-select2" required="required" style="width:100%;"'); ?>
                                </div>
                                <div class="col-lg-6">
                                    <label>
                                        NIK: <span class="m--font-danger">*</span>
                                    </label>
                                    <input type="text" name="nik" id="nik" class="form-control m-input" placeholder="Nomor Induk Karyawan" required="required" value="<?php echo isset($edit->nik) ? $edit->nik : ''; ?>" <?php $read_nik = (isset($edit->nik)) ? 'readonly' : '';
                                    echo $read_nik; ?>>
                                </div>
                                <div class="col-lg-6" style="margin-top: 20px;">
                                    <label>
                                        Nama Lengkap: <span class="m--font-danger">*</span>
                                    </label>
                                    <input type="text" name="nama" id="nama" class="form-control m-input" placeholder="Nama Lengkap" required="required" value="<?php echo isset($edit->nama) ? $edit->nama : ''; ?>">
                                </div>
                                <div class="col-lg-6" style="margin-top: 20px;">
                                    <label>
                                        Nomor E-KTP: <span class="m--font-danger">*</span>
                                    </label>
                                    <input type="text" name="ktp" id="ktp" class="form-control m-input" placeholder="Nomor E-KTP" required="required" value="<?php echo isset($edit->ktp) ? $edit->ktp : ''; ?>">
                                </div>
                                <div class="col-lg-6" style="margin-top: 20px;">
                                    <label>
                                        Jenis Kelamin: <span class="m--font-danger">*</span>
                                    </label>
                                    <?php echo form_dropdown('jenkel', $jenkel, isset($edit->jenis_kelamin) ? $edit->jenis_kelamin : '', 'id="jenkel" class="form-control m-select2" required="required" style="width:100%;"'); ?>
                                </div>
                                <div class="col-lg-6" style="margin-top: 20px;">
                                    <label>
                                        Tempat Lahir: <span class="m--font-danger">*</span>
                                    </label>
                                    <input type="text" name="tempat_lahir" id="tempat_lahir" class="form-control m-input" placeholder="Tempat Lahir" required="required" value="<?php echo isset($edit->tempat_lahir) ? $edit->tempat_lahir : ''; ?>">
                                </div>
                                <div class="col-lg-6" style="margin-top: 20px;">
                                    <label>
                                        Tanggal Lahir: <span class="m--font-danger">*</span>
                                    </label>
                                    <input type="text" name="tgl_lahir" id="tgl_lahir" class="form-control m-input" placeholder="Tanggal Lahir" required="required" value="<?php echo isset($edit->tgl_lahir) ? tanggal($edit->tgl_lahir) : ''; ?>">
                                </div>
                                <div class="col-lg-6" style="margin-top: 20px;">
                                    <label>
                                        Status Nikah: <span class="m--font-danger">*</span>
                                    </label>
                                    <?php echo form_dropdown('status_nikah', $status_nikah, isset($edit->status_nikah) ? $edit->status_nikah : '', 'id="status_nikah" class="form-control m-select2" required="required" style="width:100%;"'); ?>
                                </div>
                                <div class="col-lg-6" style="margin-top: 20px;">
                                    <label>
                                        Agama: <span class="m--font-danger">*</span>
                                    </label>
                                    <?php echo form_dropdown('agama', $agama, isset($edit->agama) ? $edit->agama : '', 'id="agama" class="form-control m-select2" required="required" style="width:100%;"'); ?>
                                </div>
                                <div class="col-lg-6" style="margin-top: 20px;">
                                    <label>
                                        Golongan Darah: <span class="m--font-danger">*</span>
                                    </label>
                                    <?php echo form_dropdown('golongan_darah', $golongan_darah, isset($edit->golongan_darah) ? $edit->golongan_darah : '', 'id="golongan_darah" class="form-control m-select2" required="required" style="width:100%;"'); ?>
                                </div>
                                <div class="col-lg-6" style="margin-top: 20px;">
                                    <label>
                                        Foto:
                                    </label>
                                    <input type="file" name="foto" id="foto" class="form-control">
                                    <?php
                                        $file = isset($edit->foto) ? $edit->foto : '';

                                    if ($file != '') {
                                        $path = path_foto();

                                        echo '<a href="'.base_url('assets/uploads/images/foto/'.$file).'" target="_blank"><i class="fa fa-download"></i> Download</a>';
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <div class="col-lg-6">
                                    <label>
                                        Alamat: <span class="m--font-danger">*</span>
                                    </label>
                                    <textarea class="form-control" name="alamat" id="alamat" rows="5" required="required"><?php echo isset($edit->alamat) ? $edit->alamat : ''; ?></textarea>
                                </div>
                                <div class="col-lg-6"> </div>
                                <!-- <div class="col-lg-6" style="margin-top: 20px;">
                                    <label>
                                        Provinsi: <span class="m--font-danger">*</span>
                                    </label>
                                    <?php echo form_dropdown('provinsi', $provinsi, isset($edit->provinsi) ? $edit->provinsi : '', 'id="provinsi" class="form-control m-select2" required="required" style="width:100%;"'); ?>
                                </div>
                                <div class="col-lg-6" style="margin-top: 20px;">
                                    <label>
                                        Kota: <span class="m--font-danger">*</span>
                                    </label>
                                    <?php echo form_dropdown('kota', [], '', 'id="kota" class="form-control m-select2" required="required" style="width:100%;"'); ?>
                                </div> -->
                                <div class="col-lg-6" style="margin-top: 20px;">
                                    <label>
                                        No Telepon: <span class="m--font-danger">*</span>
                                    </label>
                                    <input type="text" name="telp" id="telp" class="form-control m-input" placeholder="No Telepon" required="required" value="<?php echo isset($edit->telp) ? $edit->telp : ''; ?>">
                                </div>
                                <div class="col-lg-6" style="margin-top: 20px;">
                                    <label>
                                        Email: <span class="m--font-danger">*</span>
                                    </label>
                                    <input type="email" name="email" id="email" class="form-control m-input" placeholder="Email" required="required" value="<?php echo isset($edit->email) ? $edit->email : ''; ?>">
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <div class="col-lg-6">
                                    <label>
                                        Pendidikan: <span class="m--font-danger">*</span>
                                    </label>
                                    <?php echo form_dropdown('pendidikan', $pendidikan, isset($edit->pendidikan) ? $edit->pendidikan : '', 'id="pendidikan" class="form-control m-select2" required="required" style="width:100%;"'); ?>
                                </div>
                                <!-- <div class="col-lg-6">
                                    <label>
                                        Institusi: <span class="m--font-danger">*</span>
                                    </label>
                                    <input type="text" name="institusi" id="institusi" class="form-control m-input" placeholder="Institusi" required="required" value="<?php echo isset($edit->institusi) ? $edit->institusi : ''; ?>">
                                </div>
                                <div class="col-lg-6" style="margin-top: 20px;">
                                    <label>
                                        Jurusan: <span class="m--font-danger">*</span>
                                    </label>
                                    <input type="text" name="jurusan" id="jurusan" class="form-control m-input" placeholder="Jurusan" required="required" value="<?php echo isset($edit->jurusan) ? $edit->jurusan : ''; ?>">
                                </div>
                                <div class="col-lg-6" style="margin-top: 20px;">
                                    <label>
                                        Tahun Lulus: <span class="m--font-danger">*</span>
                                    </label>
                                    <input type="text" name="tahun_lulus" id="tahun_lulus" class="form-control m-input" placeholder="Tahun Lulus" required="required" value="<?php echo isset($edit->tahun_lulus) ? $edit->tahun_lulus : ''; ?>">
                                </div> -->
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <div class="col-lg-6">
                                <label>
                                    Divisi / Departemen: <span class="m--font-danger">*</span>
                                </label>
                                <?php echo form_dropdown('divisi', [], '', 'id="divisi" class="form-control m-select2" required="required" style="width:100%;"'); ?>
                            </div>
                            <div class="col-lg-6">
                                <label>
                                    Jabatan: <span class="m--font-danger">*</span>
                                </label>
                                <?php echo form_dropdown('jabatan', [], '', 'id="jabatan" class="form-control m-select2" required="required" style="width:100%;"'); ?>
                            </div>
                            <div class="col-lg-6" style="margin-top: 20px;">
                                <label>
                                    Tanggal Masuk: <span class="m--font-danger">*</span>
                                </label>
                                <input type="text" name="tgl_masuk" id="tgl_masuk" class="form-control m-input" placeholder="Tanggal Masuk" required="required" value="<?php echo isset($edit->tgl_masuk) ? tanggal($edit->tgl_masuk) : ''; ?>">
                            </div>
                            <div class="col-lg-6" style="margin-top: 20px;">
                                <label>
                                    Posisi: <span class="m--font-danger">*</span>
                                </label>
                                <input type="text" name="posisi" id="posisi" class="form-control m-input" placeholder="Posisi" required="required" value="<?php echo isset($edit->posisi) ? $edit->posisi : ''; ?>">
                            </div>
                            <div class="col-lg-6" style="margin-top: 20px;">
                                <label>
                                    Keterangan: 
                                </label>
                                <textarea class="form-control" name="keterangan" id="keterangan" rows="5"><?php echo isset($edit->keterangan) ? $edit->keterangan : ''; ?></textarea>
                            </div>
                            <?php if (isset($flag_gaji)) {
                                if ($flag_gaji == 0) {
                                    $nominal = isset($edit->salary) ? $edit->salary : '';
                                    echo '
                                        <div class="col-lg-6" style="margin-top: 20px;">
                                            <label>
                                                Gaji: <span class="m--font-danger">*</span>
                                            </label>
                                            <input type="number" name="salary" id="salary" class="form-control m-input" placeholder="Gaji" required="required" value="'.$nominal.'">
                                        </div>';
                                } 
                            } ?>
                        </div>
                        <div class="form-group m-form__group row">
                            <div class="col-lg-6">
                                <label>
                                    Username: <span class="m--font-danger">*</span>
                                </label>
                                <input type="text" name="username" id="username" class="form-control m-input" placeholder="Username" required="required" value="<?php echo isset($username) ? $username : ''; ?>" <?php $read_user = (isset($username)) ? 'readonly' : '';
                                echo $read_user; ?>>
                                <small>Default password sementara '1234'</small>
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                            <div class="m-form__actions m-form__actions--solid">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <button type="submit" class="btn btn-primary">
                                            Save
                                        </button>
                                        <button type="reset" class="btn btn-secondary">
                                            Cancel
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
            </div>
        </div>
    </div>
</div>
