<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <?php echo create_breadcrumb(); ?>
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    <?php echo web_title(); ?>
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <p>Download Format Karyawan <a href="<?php echo $url; ?>" target="_blank"> <i class="fa fa-download"></i> disini </a> </p>
                        <form id="upload-karyawan">
                            <div class="form-group">
                                <input type="file" name="karyawan" id="karyawan" class="form-control" required="required">
                                <button type="submit" class="btn btn-primary" style="margin-top: 10px;">
                                    Upload
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
                <!--end::Portlet-->
            </div>
        </div>
    </div>
</div>
