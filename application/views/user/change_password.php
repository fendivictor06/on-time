<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <?php echo create_breadcrumb(); ?>
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    Ganti Password
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <form id="form-data">
                            <div class="form-group m-form__group">
                                <label>
                                    Password Lama: <span class="m--font-danger">*</span>
                                </label>
                                <input type="password" class="form-control m-input col-md-5" id="password-lama" name="password-lama" placeholder="Password Lama" required="required">
                            </div>
                            <div class="form-group m-form__group">
                                <label>
                                    Password Baru: <span class="m--font-danger">*</span>
                                </label>
                                <input type="password" class="form-control m-input col-md-5" id="password-baru" name="password-baru" placeholder="Password Baru" required="required">
                            </div>
                            <div class="form-group m-form__group">
                                <label>
                                    Ulangi Password: <span class="m--font-danger">*</span>
                                </label>
                                <input type="password" class="form-control m-input col-md-5" id="re-password" name="re-password" placeholder="Ulangi Password" required="required">
                            </div>
                            <div class="form-group m-form__group">
                                <button type="submit" class="btn btn-primary">
                                    Ganti Password
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
                <!--end::Portlet-->
            </div>
        </div>
    </div>
</div>
