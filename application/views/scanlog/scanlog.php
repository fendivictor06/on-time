<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <?php echo create_breadcrumb(); ?>
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    <?php echo web_title(); ?>
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <!--begin: Search Form -->
                        <div class="m-form m-form--label-align-right m--margin-bottom-30">
                            <div class="row align-items-center">
                                <div class="col-xl-3 order-1 order-xl-2">
                                    <div class="input-group">
                                        <input type="text" class="form-control m-input" id="search" placeholder="Select time">
                                    </div>
                                </div>
                                <div class="col-xl-3 order-1 order-xl-2">
                                    <?php echo form_dropdown('company-search', $company, '', 'id="company-search" class="form-control"'); ?>
                                </div>
                                <div class="col-xl-3 order-1 order-xl-2">
                                    <div class="input-group">
                                        <select name="search-karyawan" id="search-karyawan" class="form-control"> </select>
                                    </div>
                                </div>
                                <div class="col-xl-3 order-1 order-xl-2">
                                    <a href="javascript:;" id="search-btn" class="btn btn-primary m-btn m-btn--icon">
                                        <span>
                                            <i class="fa fa-search"></i>
                                            <span>
                                                Cari
                                            </span>
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!--end: Search Form -->
                        <table id="tb-scanlog" style="width:100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Perusahaan</th>
                                    <th>Nama</th>
                                    <th>Scanlog</th>
                                    <th>Keterangan</th>
                                    <th>Foto</th>
                                    <th>Lokasi</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <!--end::Portlet-->
            </div>
        </div>
    </div>
</div>
