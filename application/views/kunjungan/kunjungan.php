<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <?php echo create_breadcrumb(); ?>
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    <?php echo web_title(); ?>
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <ul class="nav nav-tabs  m-tabs-line" role="tablist">
                            <li class="nav-item m-tabs__item">
                                <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_tabs_6_1" role="tab">
                                    Tanggal
                                </a>
                            </li>
                            <li class="nav-item m-tabs__item">
                                <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_tabs_6_2" role="tab">
                                    Harian
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="m_tabs_6_1" role="tabpanel">
                                <!--begin: Search Form -->
                                <div class="m-form m-form--label-align-right m--margin-bottom-30">
                                    <div class="row align-items-center">
                                        <div class="col-xl-8 order-2 order-xl-1"> </div>
                                        <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                                            <a href="#" id="add-tgl" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                                                <span>
                                                    <i class="la la-plus-circle"></i>
                                                    <span>
                                                        Tambah Data
                                                    </span>
                                                </span>
                                            </a>
                                            <div class="m-separator m-separator--dashed d-xl-none"></div>
                                        </div>
                                        <div class="col-xl-3 order-1 order-xl-2">
                                            <?php echo form_dropdown('company-search-tgl', $company, '', 'id="company-search-tgl" class="form-control dropdown-select2" style="width:100%"'); ?>
                                        </div>
                                        <div class="col-xl-3 order-1 order-xl-2">
                                            <div class="input-group">
                                                <input type="text" class="form-control m-input" id="search-tgl" placeholder="Select time">
                                            </div>
                                        </div>
                                        <div class="col-xl-3 order-1 order-xl-2">
                                            <a href="javascript:;" id="search-btn-tgl" class="btn btn-primary m-btn m-btn--icon">
                                                <span>
                                                    <i class="fa fa-search"></i>
                                                    <span>
                                                        Cari
                                                    </span>
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <!--end: Search Form -->
                                <table id="tb-tgl" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Perusahaan</th>
                                            <th>Nama</th>
                                            <th>Tanggal</th>
                                            <th>Lokasi</th>
                                            <th>Keterangan</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            <div class="tab-pane" id="m_tabs_6_2" role="tabpanel">
                                <!--begin: Search Form -->
                                <div class="m-form m-form--label-align-right m--margin-bottom-30">
                                    <div class="row align-items-center">
                                        <div class="col-xl-8 order-2 order-xl-1"> </div>
                                        <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                                            <a href="#" id="add-hari" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                                                <span>
                                                    <i class="la la-plus-circle"></i>
                                                    <span>
                                                        Tambah Data
                                                    </span>
                                                </span>
                                            </a>
                                            <div class="m-separator m-separator--dashed d-xl-none"></div>
                                        </div>
                                        <div class="col-xl-3 order-1 order-xl-2">
                                            <?php echo form_dropdown('company-search-hari', $company, '', 'id="company-search-hari" class="form-control dropdown-select2" style="width:100%"'); ?>
                                        </div>
                                        <div class="col-xl-3 order-1 order-xl-2">
                                            <?php echo form_dropdown('search-hari', $hari, '', 'id="search-hari" class="form-control dropdown-select2" style="width:100%"'); ?>
                                        </div>
                                        <div class="col-xl-3 order-1 order-xl-2">
                                            <a href="javascript:;" id="search-btn-hari" class="btn btn-primary m-btn m-btn--icon">
                                                <span>
                                                    <i class="fa fa-search"></i>
                                                    <span>
                                                        Cari
                                                    </span>
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <!--end: Search Form -->
                                <table id="tb-harian" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Perusahaan</th>
                                            <th>Nama</th>
                                            <th>Hari</th>
                                            <th>Lokasi</th>
                                            <th>Keterangan</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Portlet-->
            </div>
        </div>
    </div>
</div>


<!--begin::Modal-->
<div class="modal fade" id="modal-tgl" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    Form Kunjungan
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <form id="form-data-tgl">
                <div class="modal-body">
                    <input type="hidden" name="id-tgl" id="id-tgl">
                    <div class="form-group">
                        <label class="form-control-label">
                            Perusahaan: <span class="m--font-danger">*</span>
                        </label>
                        <?php echo form_dropdown('company-tgl', $company, '', 'id="company-tgl" class="form-control m-select2" required="required" style="width:100%;"'); ?>
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">
                            Nama Karyawan: <span class="m--font-danger">*</span>
                        </label>
                        <select name="nama-tgl" id="nama-tgl" class="form-control m-select2" style="width: 100%;" required="required"></select>
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">
                            Lokasi: <span class="m--font-danger">*</span>
                        </label>
                        <select name="lokasi-tgl" id="lokasi-tgl" class="form-control m-select2" style="width: 100%;" required="required"></select>
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">
                            Tanggal: <span class="m--font-danger">*</span>
                        </label>
                        <input type="text" class="form-control" id="tgl" name="tgl" required="required" readonly="readonly">
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">
                            Keterangan:
                        </label>
                        <textarea name="keterangan-tgl" id="keterangan-tgl" cols="30" rows="5" class="form-control"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        Close
                    </button>
                    <button type="submit" id="simpan-tgl" class="btn btn-primary">
                        Submit
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<!--end::Modal-->

<!--begin::Modal-->
<div class="modal fade" id="modal-hari" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    Form Kunjungan
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <form id="form-data-hari">
                <div class="modal-body">
                    <input type="hidden" name="id-hari" id="id-hari">
                    <div class="form-group">
                        <label class="form-control-label">
                            Perusahaan: <span class="m--font-danger">*</span>
                        </label>
                        <?php echo form_dropdown('company-hari', $company, '', 'id="company-hari" class="form-control m-select2" required="required" style="width:100%;"'); ?>
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">
                            Nama Karyawan: <span class="m--font-danger">*</span>
                        </label>
                        <select name="nama-hari" id="nama-hari" class="form-control m-select2" style="width: 100%;" required="required"></select>
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">
                            Lokasi: <span class="m--font-danger">*</span>
                        </label>
                        <select name="lokasi-hari" id="lokasi-hari" class="form-control m-select2" style="width: 100%;" required="required"></select>
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">
                            Hari: <span class="m--font-danger">*</span>
                        </label>
                        <?php echo form_dropdown('hari', $hari, '', 'id="hari" class="form-control m-select2" required="required" style="width:100%;"'); ?>
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">
                            Keterangan:
                        </label>
                        <textarea name="keterangan-hari" id="keterangan-hari" cols="30" rows="5" class="form-control"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        Close
                    </button>
                    <button type="submit" id="simpan-hari" class="btn btn-primary">
                        Submit
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<!--end::Modal-->