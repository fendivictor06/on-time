            </div>
            <!-- end:: Body -->
            <!-- begin::Footer -->
            <footer class="m-grid__item m-footer ">
                <div class="m-container m-container--fluid m-container--full-height m-page__container">
                    <div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
                        <div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
                            <span class="m-footer__copyright">
                                2018 &copy; 
                                <a href="#" class="m-link">
                                    Gmedia
                                </a>
                            </span>
                        </div>
                    </div>
                </div>
            </footer>
            <!-- end::Footer -->
        </div>
        <!-- end:: Page -->
        <!-- begin::Scroll Top -->
        <div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300">
            <i class="la la-arrow-up"></i>
        </div>
        <!-- end::Scroll Top -->
        <!--begin::Base Scripts -->
        <script src="<?php echo base_url(); ?>assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/vendors/custom/sweet-alert/sweet-alert.js"></script>
        <script src="<?php echo base_url(); ?>assets/plugins/axios/axios.js"></script>
        <script src="<?php echo base_url(); ?>assets/plugins/pace/pace.min.js"></script>
        <script> const base_url = '<?php echo base_url(); ?>'; </script>
        <?php echo isset($script) ? $script : ''; ?>
        <script src="<?php echo base_url(); ?>assets/app/js/core.min.js"></script>
        <?php echo isset($app) ? '<script src="'.base_url().'assets/app/js/'.$app.'"></script>' : ''; ?>
        <!--end::Base Scripts -->
    </body>
    <!-- end::Body -->
</html>
