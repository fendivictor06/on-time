<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <?php echo create_breadcrumb(); ?>
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    <?php echo web_title(); ?>
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="m-checkbox-inline">
                            <label class="m-checkbox">
                                <input type="checkbox" id="imei" name="imei" value="1">
                                <b>Lock Imei</b> <small>(User hanya bisa absen dengan imei handphone yang telah didaftarkan saja)</small>
                                <span></span>
                            </label>
                            <label class="m-checkbox">
                                <input type="checkbox" id="macaddress" name="macaddress" value="1">
                                <b>Lock Akses Point</b> <small>(User hanya bisa adsen di jaringan / akses point tertenu)</small>
                                <span></span>
                            </label>
                            <div class="form-group" style="margin-top: 20px;">
                                <label class="form-control-label">
                                    Approval Cuti: <span class="m--font-danger">*</span>
                                </label>
                                <?php echo form_dropdown('cuti', [], '', 'id="cuti" class="form-control m-select2" required="required" style="width:100%;"'); ?>
                            </div>
                            <div class="form-group">
                                <label class="form-control-label">
                                    Approval Ijin: <span class="m--font-danger">*</span>
                                </label>
                                <?php echo form_dropdown('ijin', [], '', 'id="ijin" class="form-control m-select2" required="required" style="width:100%;"'); ?>
                            </div>
                            <button type="button" id="simpan" class="btn btn-primary">
                                Simpan
                            </button>
                        </div>
                    </div>
                </div>
                <!--end::Portlet-->
            </div>
        </div>
    </div>
</div>
