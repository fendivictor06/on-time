<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Rest_Scan extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Rest_Scan_Model');
    }

    function type_scan()
    {
        $auth = $this->token->auth('GET');
        if ($auth == true) {
            $data = $this->Rest_Scan_Model->view_tipe_scan();

            if ($data) {
                $status = 200;
                $message = 'Berhasil';
            } else {
                $status = 404;
                $message = 'Data tidak ditemukan';
            }

            print_json($status, $message, $data);
        }
    }

    function scan()
    {
        $auth = $this->token->auth('POST');
        if ($auth == true) {
            $params = get_params();
            $foto = isset($params['foto']) ? $params['foto'] : '';
            $latitude = isset($params['latitude']) ? $params['latitude'] : '';
            $longitude = isset($params['longitude']) ? $params['longitude'] : '';
            $tipe_scan = isset($params['tipe_scan']) ? $params['tipe_scan'] : '';
            $imei = isset($params['imei']) ? $params['imei'] : '';
            $macaddress = isset($params['mac_address']) ? $params['mac_address'] : '';
            $ippublic = isset($params['ip_public']) ? $params['ip_public'] : '';
            $today = date('Y-m-d');
            $id_karyawan = id_karyawan();
            $id_company = id_company();

            # cari tipe scan
            $ms_scan = $this->customdb->view_by_id('ms_tipe_scan', ['kode' => $tipe_scan], 'row');
            $keterangan = isset($ms_scan->keterangan) ? $ms_scan->keterangan : '';
            $rule_imei = isset($ms_scan->imei) ? $ms_scan->imei : 0;
            $rule_jaringan = isset($ms_scan->jaringan) ? $ms_scan->jaringan : 0;

            $status_imei = 1;
            $status_jaringan = 1;

            # validasi imei
            if ($rule_imei == 1) {
                $validasi_imei = $this->Rest_Scan_Model->validasi_imei($imei);

                if ($validasi_imei == true) {
                    $status_imei = 1;
                } else {
                    $status_imei = 0;
                }
            }

            # validasi jaringan
            if ($rule_jaringan == 1) {
                $validasi_jaringan = $this->Rest_Scan_Model->validasi_jaringan($macaddress, $ippublic);

                if ($validasi_jaringan == true) {
                    $status_jaringan = 1;
                } else {
                    $status_jaringan = 0;
                }
            }

            # validasi kunjungan
            $status_kunjungan = 1;
            $status_jarak = 1;
            $id_lokasi = 0;
            $loc_latitude = 0;
            $loc_longitude = 0;
            $toleransi = 0;

            if ($tipe_scan == 7 || $tipe_scan == 8) {
                # cek kunjungan tgl
                $kunjungan = $this->Rest_Scan_Model->kunjungan_tgl($id_karyawan, $id_company, $today);
                if ($kunjungan) {
                    $id_lokasi = isset($kunjungan->id_lokasi) ? $kunjungan->id_lokasi : '';
                    $loc_latitude = isset($kunjungan->latitude) ? $kunjungan->latitude : '';
                    $loc_longitude = isset($kunjungan->longitude) ? $kunjungan->longitude : '';
                    $toleransi = isset($kunjungan->toleransi) ? $kunjungan->toleransi : '';
                } else {
                    $kunjungan = $this->Rest_Scan_Model->kunjungan_hari($id_karyawan, $id_company, $today);
                    if ($kunjungan) {
                        $id_lokasi = isset($kunjungan->id_lokasi) ? $kunjungan->id_lokasi : '';
                        $loc_latitude = isset($kunjungan->latitude) ? $kunjungan->latitude : '';
                        $loc_longitude = isset($kunjungan->longitude) ? $kunjungan->longitude : '';
                        $toleransi = isset($kunjungan->toleransi) ? $kunjungan->toleransi : '';
                    } else {
                        $status_kunjungan = 0;
                    }
                }
            }

            # load haversine library
            $this->load->library('Haversine');
            $jarak = $this->haversine->hitung_jarak($latitude, $longitude, $loc_latitude, $loc_longitude);
            $jarak = $jarak * 1000;

            if ($jarak > $toleransi) {
                $status_jarak = 0;
            }

            if ($status_imei == 0 || $status_jaringan == 0 || $status_kunjungan == 0 || $status_jarak == 0) {
                $status = 400;
                $message = '';
                if ($status_imei == 0) {
                    $message = 'Device anda tidak terdaftar';
                }

                if ($status_jaringan == 0) {
                    $message = 'Scan '.$keterangan.' gagal, Silahkan gunakan jaringan yang telah ditentukan.';
                }

                if ($status_kunjungan == 0) {
                    $message = 'Anda tidak terjadwal dalam kunjungan hari ini.';
                }

                if ($status_jarak == 0) {
                    $message = 'Lokasi anda tidak berada di radius lokasi kunjungan';
                }
            } else {
                if ($foto != '') {
                    $nama_foto = 'scan_'.user_id().time().'.jpg';
                    $path_foto = './assets/uploads/images/scanlog/'.$nama_foto;

                    decode_image($foto, $path_foto);

                    $foto = $nama_foto;
                }

                $simpan = $this->Rest_Scan_Model->add_scanlog($tipe_scan, $foto, $latitude, $longitude, $id_lokasi);
                if ($simpan > 0) {
                    $status = 200;

                    $message = 'Scan '.$keterangan.' Berhasil, Terima Kasih.';
                } else {
                    $status = 500;
                    $message = 'Scan Gagal, Silahkan ulangi beberapa saat lagi.';
                }
            }

            print_json($status, $message, []);
            log_api($params, $status, $message, []);
        }
    }

    function view_scan()
    {
        $auth = $this->token->auth('POST');
        if ($auth == true) {
            $params = get_params();
            $date = isset($params['date']) ? $params['date'] : '';

            $data = $this->Rest_Scan_Model->view_scanlog($date);

            $response = [];
            if ($data) {
                $path = link_scan();

                foreach ($data as $row => $val) {
                    $response[$row] = array(
                        'id' => $val->id,
                        'id_company' => $val->id_company,
                        'company' => $val->company,
                        'id_karyawan' => $val->id_karyawan,
                        'nama' => $val->nama,
                        'scan_date' => $val->scan_date,
                        'keterangan' => $val->keterangan,
                        'foto' => ($val->foto == '') ? '' : $path.$val->foto,
                        'latitude' => $val->latitude,
                        'longitude' => $val->longitude
                    );
                }

                $status = 200;
                $message = 'Berhasil';
            } else {
                $status = 404;
                $message = 'Data tidak ditemukan';
            }

            print_json($status, $message, $response);
        }
    }
}

/* End of file Rest_Scan.php */
/* Location: ./application/controllers/Rest_Scan.php */
