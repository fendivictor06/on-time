<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Rest_News extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Rest_News_Model');
    }

    function index()
    {
        $auth = $this->token->auth('POST');
        if ($auth == true) {
            $params = get_params();
            $id = filter_params($params, 'id');

            $response = [];
            $path = link_news();

            $data = $this->Rest_News_Model->view_news($id);

            if ($id == '') {
                if ($data) {
                    foreach ($data as $row => $val) {
                        $image = ($val->gambar != '') ? $path.$val->gambar : '';

                        $response[$row] = array(
                            'id' => $val->id,
                            'judul' => $val->judul,
                            'tgl' => $val->tgl,
                            'teks' => $val->teks,
                            'gambar' => $image
                        );
                    }

                    $status = 200;
                    $message = 'Berhasil';
                } else {
                    $status = 404;
                    $message = 'Data tidak ditemukan';
                }
            } else {
                if ($data) {
                    $image = isset($data->gambar) ? $data->gambar : '';
                    $image = ($image != '') ? $path.$image : '';

                    $response = array(
                        'id' => isset($data->id) ? $data->id : '',
                        'judul' => isset($data->judul) ? $data->judul : '',
                        'tgl' => isset($data->tgl) ? $data->tgl : '',
                        'teks' => isset($data->teks) ? $data->teks : '',
                        'gambar' => $image
                    );

                    $status = 200;
                    $message = 'Berhasil';
                } else {
                    $status = 404;
                    $message = 'Data tidak ditemukan';
                }
            }

            print_json($status, $message, $response);
        }
    }
}

/* End of file Rest_News.php */
/* Location: ./application/controllers/Rest_News.php */
