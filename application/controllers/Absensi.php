<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Absensi extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Absensi_Model');
    }

    function index()
    {
        $this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css')
        );

        $company = $this->Main_Model->list_company();
        $data = array(
            'company' => $company
        );

        $footer = array(
            'script' => datatable('js'),
            'app' => 'absensi/absensi.min.js'
        );

        $this->load->view('template/header', $header);
        $this->load->view('absensi/absensi', $data);
        $this->load->view('template/footer', $footer);
    }

    function data_absensi()
    {
        $this->Auth_Model->is_login();
        $datestart = $this->input->get('datestart');
        $dateend = $this->input->get('dateend');
        $company = $this->input->get('company');

        if ($this->input->is_ajax_request()) {
            $data = $this->Absensi_Model->rekap_absensi($datestart, $dateend, $company);

            $response = ['data' => []];
            $no = 1;
            if ($data) {
                foreach ($data as $row) {
                    $response['data'][] = array(
                        $no++,
                        $row->nik,
                        $row->nama,
                        $row->hari_kerja,
                        $row->mangkir,
                        $row->sakit,
                        $row->ijin,
                        $row->terlambat,
                        $row->cuti,
                        $row->lembur
                    );
                }
            }

            echo json_encode($response);
        } else {
            show_404();
        }
    }

    function data()
    {
        $this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css')
        );

        $company = $this->Main_Model->list_company();

        $absensi = array(
            '1' => 'Mangkir',
            '2' => 'Sakit',
            '3' => 'Ijin',
            '4' => 'Cuti',
            '5' => 'Masuk'
        );

        $data = array(
            'company' => $company,
            'absensi' => $absensi
        );

        $footer = array(
            'script' => datatable('js'),
            'app' => 'absensi/data-absensi.min.js'
        );

        $this->load->view('template/header', $header);
        $this->load->view('absensi/data-absensi', $data);
        $this->load->view('template/footer', $footer);
    }

    function data_presensi()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $draw = $this->input->get('draw');
            $start = $this->input->get('start');
            $length = $this->input->get('length');
            $search = $this->input->get('search');
            $order = $this->input->get('order');
            $company = $this->input->get('company');
            $datestart = $this->input->get('datestart');
            $dateend = $this->input->get('dateend');

            $json = $this->Absensi_Model->json_presensi($draw, $start, $length, $search['value'], $order[0]['column'], $order[0]['dir'], $company, $datestart, $dateend);

            echo json_encode($json);
        } else {
            show_404();
        }
    }

    function proses($tgl = '', $id_company = '', $id_karyawan = '')
    {
        if ($tgl == '') {
            $tgl = date('Y-m-d');
        }

        # karyawan yang absensinya akan diproses
        $karyawan = $this->Absensi_Model->view_jadwal($id_company, $tgl, $id_karyawan);

        if ($karyawan) {
            foreach ($karyawan as $row) {
                $id_karyawan = $row->id;
                $shift = $row->shift;
                $jam_masuk = $row->jam_masuk;
                $jam_pulang = $row->jam_pulang;

                # check if absensi sudah pernah diproses
                $exists = $this->customdb->view_by_id('tb_absensi', ['tgl' => $tgl, 'id_karyawan' => $id_karyawan, 'flag' => 1], 'row');

                if (empty($exists)) {
                    # delete absensi if exist
                    $delete_absensi = $this->customdb->delete_data('tb_absensi', ['tgl' => $tgl, 'id_karyawan' => $id_karyawan]);

                    # delete presensi if exists
                    $delete_presensi = $this->customdb->delete_data('tb_presensi', ['tgl' => $tgl, 'id_karyawan' => $id_karyawan]);

                    # delete terlambat
                    $delete_terlambat = $this->customdb->delete_data('tb_terlambat', ['tgl' => $tgl, 'id_karyawan' => $id_karyawan]);

                    $waktu_masuk = '';
                    $waktu_pulang = '';
                    $is_telat = 0;

                    if ($jam_masuk == '' && $jam_pulang == '') {
                        $libur = $this->Absensi_Model->holiday($tgl, $id_company);
                        $status = 5;

                        # cek apakah libur ?
                        if ($libur) {
                            $keterangan = isset($libur->keterangan) ? $libur->keterangan : '';
                        } else {
                            $keterangan = 'Libur / tidak ada jadwal';
                        }
                    } else {
                        # scan masuk
                        $scan_masuk = $this->Absensi_Model->scan_masuk($id_karyawan, $tgl);
                        $waktu_masuk = isset($scan_masuk->scan_date) ? $scan_masuk->scan_date : '';
                        $format_jam = isset($scan_masuk->format_jam) ? $scan_masuk->format_jam : '';

                        # flag terlambat
                        $is_telat = isset($scan_masuk->terlambat) ? $scan_masuk->terlambat : 0;

                        # scan pulang
                        $scan_pulang = $this->Absensi_Model->scan_pulang($id_karyawan, $tgl);
                        $waktu_pulang = isset($scan_pulang->scan_date) ? $scan_pulang->scan_date : '';

                        # kode status
                        # 1. Clear tidak mangkir
                        # 2. Tidak Absen Masuk
                        # 3. Tidak Absen Pulang
                        # 4. Tidak Masuk
                        # 5. Libur / tidak ada jadwal

                        # logic status
                        if ($waktu_masuk == '' && $waktu_pulang != '') {
                            $status = 2;
                            $keterangan = 'Tidak Absen Masuk';
                        } elseif ($waktu_masuk != '' && $waktu_pulang == '') {
                            $status = 3;
                            $keterangan = 'Tidak Absen Pulang';
                        } elseif ($waktu_masuk == '' && $waktu_pulang == '') {
                            $status = 4;
                            $keterangan = 'Tidak Masuk';
                        } else {
                            $status = 1;
                            $keterangan = '';
                        }
                    }

                    $data = array(
                        'id_company' => $id_company,
                        'id_karyawan' => $id_karyawan,
                        'shift' => $shift,
                        'jam_masuk' => $jam_masuk,
                        'jam_pulang' => $jam_pulang,
                        'scan_masuk' => $waktu_masuk,
                        'scan_pulang' => $waktu_pulang,
                        'tgl' => $tgl,
                        'status' => $status,
                        'keterangan' => $keterangan,
                        'flag' => 0,
                        'user_insert' => 'GENERATE'
                    );

                    # simpan absensi
                    $simpan_absensi = $this->customdb->process_data('tb_absensi', $data);

                    if ($simpan_absensi > 0) {
                        # jika status pelanggaran
                        if ($status != 1 && $status != 5) {
                            $data_presensi = array(
                                'id_company' => $id_company,
                                'id_karyawan' => $id_karyawan,
                                'tgl' => $tgl,
                                'status' => 1,
                                'keterangan' => $keterangan,
                                'user_insert' => 'GENERATE'
                            );

                            # insert presensi
                            $simpan_presensi = $this->customdb->process_data('tb_presensi', $data_presensi);
                        }

                        # jika terlambat
                        if ($is_telat == 1) {
                            $data_telat = array(
                                'id_company' => $id_company,
                                'id_karyawan' => $id_karyawan,
                                'tgl' => $tgl,
                                'scan_masuk' => $waktu_masuk,
                                'jam_masuk' => $jam_masuk,
                                'total_menit' => hitung_menit($jam_masuk, $format_jam),
                                'shift' => $shift,
                                'user_insert' => 'GENERATE'
                            );

                            # insert telat
                            $simpan_telat = $this->customdb->process_data('tb_terlambat', $data_telat);
                        }
                    }
                }
            }
        }

        echo json_encode(['message' => 'Proses complete']);
    }

    function proses_absensi()
    {
        $login = $this->Auth_Model->is_login_ajax();
        if ($login == true) {
            $company = $this->input->get('company');
            $karyawan = $this->input->get('karyawan');
            $datestart = $this->input->get('datestart');
            $dateend = $this->input->get('dateend');

            $datestart = convert_tgl($datestart);
            $dateend = convert_tgl($dateend);

            $arr = range_to_date($datestart, $dateend);
            if ($arr) {
                for ($i = 0; $i < count($arr); $i++) {
                    $tgl = isset($arr[$i]) ? $arr[$i] : '';
                    $url = base_url('Absensi/proses/'.$tgl.'/'.$company);

                    if ($karyawan != '' && $karyawan != 'null' && $karyawan != null) {
                        $url .= '/'.$karyawan;
                    }

                    request_url($url, [], 'GET');
                }
            }

            echo json_encode(['message' => 'Absensi Telah diproses']);
        }
    }

    function reproses()
    {
        $this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css')
        );

        $company = $this->Main_Model->list_company();
        $data = array(
            'company' => $company
        );

        $footer = array(
            'script' => datatable('js'),
            'app' => 'absensi/reproses.min.js'
        );

        $this->load->view('template/header', $header);
        $this->load->view('absensi/reproses', $data);
        $this->load->view('template/footer', $footer);
    }

    function absensi_id()
    {
        $login = $this->Auth_Model->is_login_ajax();
        if ($login == true) {
            $id = $this->input->get('id');

            $data = $this->customdb->view_by_id('view_presensi', ['id' => $id], 'row');

            echo json_encode($data);
        }
    }

    function klarifikasi()
    {
        $login = $this->Auth_Model->is_login_ajax();
        if ($login == true) {
            $id = $this->input->post('id');
            $id_karyawan = $this->input->post('id_karyawan');
            $company = $this->input->post('company');
            $tgl = $this->input->post('tgl');
            $absensi = $this->input->post('absensi');
            $keterangan = $this->input->post('keterangan');

            $this->form_validation->set_rules('id_karyawan', 'Karyawan', 'required', ['required' => 'Masukkan %s']);
            $this->form_validation->set_rules('company', 'Perusahaan', 'required', ['required' => 'Masukkan %s']);
            $this->form_validation->set_rules('tgl', 'Tanggal', 'required', ['required' => 'Masukkan %s']);

            if ($this->form_validation->run() == false) {
                $status = 0;
                $message = validation_errors();
            } else {
                $tgl = convert_tgl($tgl);
                $result = 0;

                if ($absensi == 1 || $absensi == 2 || $absensi == 3) {
                    $update = array(
                        'status' => $absensi,
                        'keterangan' => $keterangan,
                        'update_at' => now(),
                        'user_update' => username()
                    );

                    $simpan = $this->Absensi_Model->update_presensi($update, $id, $tgl, $id_karyawan);
                    $result = ($simpan == true) ? 1 : 0;
                } elseif ($absensi == 4) {
                    $exists = $this->customdb->view_by_id('detail_cuti', ['id_karyawan' => $id_karyawan, 'tgl' => $tgl], 'row');
                    if ($exists) {
                        $status = 0;
                        $message = 'Input Cuti gagal, Sudah ada pengajuan di tanggal '.tanggal($tgl);
                    } else {
                        $simpan = $this->Absensi_Model->insert_cuti($id, $id_karyawan, $company, $keterangan, $tgl);
                        $result = ($simpan == true) ? 1 : 0;
                    }
                } elseif ($absensi == 5) {
                    $simpan = $this->Absensi_Model->clear_presensi($id, $id_karyawan, $keterangan, $tgl);
                    $result = ($simpan == true) ? 1 : 0;
                } else {
                    $result = 0;
                }

                if ($result == 1) {
                    $status = 1;
                    $message = 'Data berhasil disimpan';
                } else {
                    $status = 0;
                    $message = 'Gagal menyimpan data';
                }
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        }
    }
}

/* End of file Absensi.php */
/* Location: ./application/controllers/Absensi.php */
