<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Validasi extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('User_Model');
    }

    function perusahaan($kode = '', $id = '')
    {
        $check = $this->customdb->view_by_id('log_verifikasi', ['id_company' => $id, 'unik' => $kode], 'row');
        if ($check) {
            $ms_company = $this->customdb->view_by_id('ms_company', ['id' => $id], 'row');
            $company = isset($ms_company->company) ? $ms_company->company : '';
            $to = isset($ms_company->email) ? $ms_company->email : '';
            $kode_perusahaan = isset($ms_company->kode_perusahaan) ? $ms_company->kode_perusahaan : '';
            $ms_user = $this->User_Model->ms_user($id);
            $username = isset($ms_user->username) ? $ms_user->username : '';
            $password = isset($ms_user->pwd) ? $ms_user->pwd : '';

            $bcc = ['victor.fendi@gmedia.co.id'];
            $from = 'no-reply@ontime.co.id';

            $update = $this->customdb->process_data('ms_company', ['verify' => 1], ['id' => $id]);
            if ($update > 0) {
                # send email to company
                $this->load->library('Sendmail');
                $judul = 'Akun On Time';
                $pesan = 'Hai, '.$company.' berikut kami kirimkan akun untuk login ke website On Time <br>';
                $pesan .= 'Username : '.$username.'<br>';
                $pesan .= 'Password : '.$password.'<br>';
                $pesan .= 'Kode Perusahaan : '.$kode_perusahaan.'<br>';
                $pesan .= 'Url : <a href="'.base_url().'"> klik disini </a> <br>';
                $pesan .= 'App : <a href="'.base_url().'"> klik disini </a>';
                $kirim = $this->sendmail->sending([], $from, $to, $judul, $judul, $pesan, [], $bcc);

                log_api(['to' => $to, 'company' => $company, 'id' => $id], 200, 'log-email', $kirim);

                $message = 'Verifikasi Berhasil, Username & password anda telah dikirim ke email anda.';
            } else {
                $message = 'Verifikasi Gagal.';
            }

            echo $message;
        } else {
            show_404();
        }
    }
}

/* End of file Validasi.php */
/* Location: ./application/controllers/Validasi.php */
