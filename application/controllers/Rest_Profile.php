<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Rest_Profile extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Rest_Profile_Model');
    }

    function index()
    {
        $auth = $this->token->auth('POST');
        if ($auth == true) {
            $params = get_params();
            $fcm_id = isset($params['fcm_id']) ? $params['fcm_id'] : '';
            $data = $this->Rest_Profile_Model->view_profile();
            $id_karyawan = id_karyawan();

            if ($fcm_id != '') {
                $this->customdb->process_data('ms_karyawan', ['fcm_id' => $fcm_id], ['id' => $id_karyawan]);
            }

            $response = [];
            if ($data) {
                $path = link_foto();

                $id = isset($data->id) ? $data->id : '';
                $nik = isset($data->nik) ? $data->nik : '';
                $nama = isset($data->nama) ? $data->nama : '';
                $ktp = isset($data->ktp) ? $data->ktp : '';
                $jenis_kelamin = isset($data->jenis_kelamin) ? $data->jenis_kelamin : '';
                $tempat_lahir = isset($data->tempat_lahir) ? $data->tempat_lahir : '';
                $tgl_lahir = isset($data->tgl_lahir) ? $data->tgl_lahir : '';
                $telp = isset($data->telp) ? $data->telp : '';
                $provinsi = isset($data->provinsi) ? $data->provinsi : '';
                $kota = isset($data->kota) ? $data->kota : '';
                $alamat = isset($data->alamat) ? $data->alamat : '';
                $email = isset($data->email) ? $data->email : '';
                $tgl_masuk = isset($data->tgl_masuk) ? $data->tgl_masuk : '';
                $status_nikah = isset($data->status_nikah) ? $data->status_nikah : '';
                $pendidikan = isset($data->pendidikan) ? $data->pendidikan : '';
                $institusi = isset($data->institusi) ? $data->institusi : '';
                $jurusan = isset($data->jurusan) ? $data->jurusan : '';
                $tahun_lulus = isset($data->tahun_lulus) ? $data->tahun_lulus : '';
                $agama = isset($data->agama) ? $data->agama : '';
                $golongan_darah = isset($data->golongan_darah) ? $data->golongan_darah : '';
                $foto = isset($data->foto) ? $path.$data->foto : '';
                $tgl_resign = isset($data->tgl_resign) ? $data->tgl_resign : '';
                $alasan_resign = isset($data->alasan_resign) ? $data->alasan_resign : '';
                $company = isset($data->company) ? $data->company : '';
                $jabatan = isset($data->jabatan) ? $data->jabatan : '';
                $divisi = isset($data->divisi) ? $data->divisi : '';
                $posisi = isset($data->posisi) ? $data->posisi : '';
                $keterangan = isset($data->keterangan) ? $data->keterangan : '';

                $response = array(
                    'id' => $id,
                    'nik' => $nik,
                    'nama' => $nama,
                    'ktp' => $ktp,
                    'jenis_kelamin' => $jenis_kelamin,
                    'tempat_lahir' => $tempat_lahir,
                    'tgl_lahir' => $tgl_lahir,
                    'telp' => $telp,
                    'provinsi' => $provinsi,
                    'kota' => $kota,
                    'alamat' => $alamat,
                    'email' => $email,
                    'tgl_masuk' => $tgl_masuk,
                    'status_nikah' => $status_nikah,
                    'pendidikan' => $pendidikan,
                    'institusi' => $institusi,
                    'jurusan' => $jurusan,
                    'tahun_lulus' => $tahun_lulus,
                    'agama' => $agama,
                    'golongan_darah' => $golongan_darah,
                    'foto' => $foto,
                    'tgl_resign' => $tgl_resign,
                    'alasan_resign' => $alasan_resign,
                    'company' => $company,
                    'jabatan' => $jabatan,
                    'divisi' => $divisi,
                    'posisi' => $posisi,
                    'keterangan' => $keterangan
                );

                $status = 200;
                $message = 'Berhasil';
            } else {
                $status = 404;
                $message = 'Data tidak ditemukan';
            }

            print_json($status, $message, $response);
        }
    }
}

/* End of file Rest_Profile.php */
/* Location: ./application/controllers/Rest_Profile.php */
