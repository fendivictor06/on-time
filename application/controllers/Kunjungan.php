<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Kunjungan extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Kunjungan_Model');
    }

    function index()
    {
        $this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css')
        );

        $company = $this->Main_Model->list_company();
        $hari = array(
            '0' => 'Senin',
            '1' => 'Selasa',
            '2' => 'Rabu',
            '3' => 'Kamis',
            '4' => 'Jumat',
            '5' => 'Sabtu',
            '6' => 'Minggu'
        );

        $data = array(
            'company' => $company,
            'hari' => $hari
        );

        $footer = array(
            'script' => datatable('js'),
            'app' => 'kunjungan/kunjungan.min.js'
        );

        $this->load->view('template/header', $header);
        $this->load->view('kunjungan/kunjungan', $data);
        $this->load->view('template/footer', $footer);
    }

    function data_lokasi()
    {
        $login = $this->Auth_Model->is_login_ajax();
        if ($login == true) {
            $search = $this->input->get('q');
            $data = $this->Kunjungan_Model->view_lokasi($search);

            echo json_encode($data);
        }
    }

    function data_karyawan()
    {
        $login = $this->Auth_Model->is_login_ajax();
        if ($login == true) {
            $search = $this->input->get('q');
            $company = $this->input->get('company');
            $data = $this->Kunjungan_Model->view_karyawan($search, $company);

            echo json_encode($data);
        }
    }

    function add_kunjungan_tgl()
    {
        $login = $this->Auth_Model->is_login_ajax();
        if ($login == true) {
            $id = $this->input->post('id-tgl');
            $company = $this->input->post('company-tgl');
            $nama = $this->input->post('nama-tgl');
            $lokasi = $this->input->post('lokasi-tgl');
            $tgl = $this->input->post('tgl');
            $keterangan = $this->input->post('keterangan-tgl');

            $this->form_validation->set_rules('company-tgl', 'Perusahaan', 'required', ['required' => 'Masukkan %s']);
            $this->form_validation->set_rules('nama-tgl', 'Nama Karyawan', 'required', ['required' => 'Masukkan %s']);
            $this->form_validation->set_rules('lokasi-tgl', 'Lokasi', 'required', ['required' => 'Masukkan %s']);
            $this->form_validation->set_rules('tgl', 'Tanggal', 'required', ['required' => 'Masukkan %s']);

            if ($this->form_validation->run() == false) {
                $status = 0;
                $message = validation_errors();
            } else {
                $user = ($id != '') ? 'user_update' : 'user_insert';
                $time = ($id != '') ? 'update_at' : 'insert_at';
                $condition = ($id != '') ? ['id' => $id] : [];

                # ms_lokasi
                $ms_lokasi = $this->customdb->view_by_id('ms_lokasi', ['id' => $lokasi], 'row');
                $latitude = isset($ms_lokasi->latitude) ? $ms_lokasi->latitude : '';
                $longitude = isset($ms_lokasi->longitude) ? $ms_lokasi->longitude : '';

                $tgl = convert_tgl($tgl);
                $data = array(
                    'id_company' => $company,
                    'id_karyawan' => $nama,
                    'tgl' => $tgl,
                    'id_lokasi' => $lokasi,
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                    'keterangan' => $keterangan,
                    $time => now(),
                    $user => username()
                );

                $simpan = $this->customdb->process_data('ms_kunjungan', $data, $condition);
                if ($id == '') {
                    if ($simpan > 0) {
                        $status = 1;
                        $message = 'Data berhasil disimpan';
                    } else {
                        $status = 0;
                        $message = 'Gagal menyimpan data';
                    }
                } else {
                    $status = 1;
                    $message = 'Data berhasil disimpan';
                }
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        }
    }

    function add_kunjungan_hari()
    {
        $login = $this->Auth_Model->is_login_ajax();
        if ($login == true) {
            $id = $this->input->post('id-hari');
            $company = $this->input->post('company-hari');
            $nama = $this->input->post('nama-hari');
            $lokasi = $this->input->post('lokasi-hari');
            $hari = $this->input->post('hari');
            $keterangan = $this->input->post('keterangan-hari');

            $this->form_validation->set_rules('company-hari', 'Perusahaan', 'required', ['required' => 'Masukkan %s']);
            $this->form_validation->set_rules('nama-hari', 'Nama Karyawan', 'required', ['required' => 'Masukkan %s']);
            $this->form_validation->set_rules('lokasi-hari', 'Lokasi', 'required', ['required' => 'Masukkan %s']);
            $this->form_validation->set_rules('hari', 'Hari', 'required', ['required' => 'Masukkan %s']);

            if ($this->form_validation->run() == false) {
                $status = 0;
                $message = validation_errors();
            } else {
                $user = ($id != '') ? 'user_update' : 'user_insert';
                $time = ($id != '') ? 'update_at' : 'insert_at';
                $condition = ($id != '') ? ['id' => $id] : [];

                # ms_lokasi
                $ms_lokasi = $this->customdb->view_by_id('ms_lokasi', ['id' => $lokasi], 'row');
                $latitude = isset($ms_lokasi->latitude) ? $ms_lokasi->latitude : '';
                $longitude = isset($ms_lokasi->longitude) ? $ms_lokasi->longitude : '';

                $data = array(
                    'id_company' => $company,
                    'id_karyawan' => $nama,
                    'hari' => $hari,
                    'id_lokasi' => $lokasi,
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                    'keterangan' => $keterangan,
                    $time => now(),
                    $user => username()
                );

                $simpan = $this->customdb->process_data('ms_kunjungan_hari', $data, $condition);
                if ($id == '') {
                    if ($simpan > 0) {
                        $status = 1;
                        $message = 'Data berhasil disimpan';
                    } else {
                        $status = 0;
                        $message = 'Gagal menyimpan data';
                    }
                } else {
                    $status = 1;
                    $message = 'Data berhasil disimpan';
                }
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        }
    }

    function data_kunjungan_tgl()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $draw = $this->input->get('draw');
            $start = $this->input->get('start');
            $length = $this->input->get('length');
            $search = $this->input->get('search');
            $order = $this->input->get('order');
            $company = $this->input->get('company');
            $datestart = $this->input->get('datestart');
            $dateend = $this->input->get('dateend');

            $json = $this->Kunjungan_Model->json_kunjungan_tgl($draw, $start, $length, $search['value'], $order[0]['column'], $order[0]['dir'], $company, $datestart, $dateend);

            echo json_encode($json);
        } else {
            show_404();
        }
    }

    function data_kunjungan_hari()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $draw = $this->input->get('draw');
            $start = $this->input->get('start');
            $length = $this->input->get('length');
            $search = $this->input->get('search');
            $order = $this->input->get('order');
            $company = $this->input->get('company');
            $hari = $this->input->get('hari');

            $json = $this->Kunjungan_Model->json_kunjungan_hari($draw, $start, $length, $search['value'], $order[0]['column'], $order[0]['dir'], $company, $hari);

            echo json_encode($json);
        } else {
            show_404();
        }
    }

    function id_kunjungan_tgl()
    {
        $login = $this->Auth_Model->is_login_ajax();
        if ($login == true) {
            $id = $this->input->get('id');

            $data = $this->customdb->view_by_id('ms_kunjungan', ['id' => $id], 'row');
            echo json_encode($data);
        }
    }

    function id_kunjungan_hari()
    {
        $login = $this->Auth_Model->is_login_ajax();
        if ($login == true) {
            $id = $this->input->get('id');

            $data = $this->customdb->view_by_id('ms_kunjungan_hari', ['id' => $id], 'row');
            echo json_encode($data);
        }
    }

    function hapus_tgl()
    {
        $login = $this->Auth_Model->is_login_ajax();
        if ($login == true) {
            $id = $this->input->get('id');

            $delete = $this->customdb->delete_data('ms_kunjungan', ['id' => $id]);
            if ($delete > 0) {
                $status = 1;
                $message = 'Data berhasil dihapus';
            } else {
                $status = 0;
                $message = 'Gagal menghapus data';
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        }
    }

    function hapus_hari()
    {
        $login = $this->Auth_Model->is_login_ajax();
        if ($login == true) {
            $id = $this->input->get('id');

            $delete = $this->customdb->delete_data('ms_kunjungan_hari', ['id' => $id]);
            if ($delete > 0) {
                $status = 1;
                $message = 'Data berhasil dihapus';
            } else {
                $status = 0;
                $message = 'Gagal menghapus data';
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        }
    }
}

/* End of file Kunjungan.php */
/* Location: ./application/controllers/Kunjungan.php */
