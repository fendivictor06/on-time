<?php
if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Main extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        $this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css')
        );

        $widget = '';
        $level = level_user();
        if ($level == 1) {
            $jumlah_perusahaan = $this->Main_Model->jumlah_perusahaan();
            $widget .= $this->create_element('Perusahaan', 'Jumlah Perusahaan', $jumlah_perusahaan);
        } else {
            $karyawan_aktif = $this->Main_Model->karyawan_aktif();
            $widget .= $this->create_element('Karyawan Aktif', 'Jumlah Karyawan Aktif', $karyawan_aktif);
            $karyawan_baru = $this->Main_Model->karyawan_baru();
            $widget .= $this->create_element('Karyawan Baru', 'Jumlah Karyawan Baru bulan ini', $karyawan_baru);
            $karyawan_resign = $this->Main_Model->karyawan_resign();
            $widget .= $this->create_element('Karyawan Resign', 'Jumlah Karyawan Resign bulan ini', $karyawan_resign);
        }

        $footer = array(
            'script' => datatable('js'),
            'app' => 'main-dashboard.min.js'
        );

        $data = array(
            'widget' => $widget
        );

        $this->load->view('template/header', $header);
        $this->load->view('dashboard/dashboard', $data);
        $this->load->view('template/footer', $footer);
    }

    function create_element($title = '', $subtitle = '', $total = 0)
    {
        return '<div class="col-md-12 col-lg-6 col-xl-6">
                    <!--begin::Total Profit-->
                    <div class="m-widget24">
                        <div class="m-widget24__item">
                            <h4 class="m-widget24__title">
                                '.$title.'
                            </h4>
                            <br> 
                            <span class="m-widget24__desc">
                                '.$subtitle.'
                            </span>
                          
                            <span class="m-widget24__stats m--font-brand">
                                '.$total.'
                            </span>
                            <div class="m--space-10"></div>
                            <div class="progress m-progress--sm">
                                <div class="progress-bar m--bg-brand" role="progressbar" style="width: 100%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                    <!--end::Total Profit-->
                </div>';
    }
}

/* End of file Main.php */
/* Location: ./application/controllers/Main.php */
