<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Rest_Jadwal extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Rest_Jadwal_Model');
    }

    function index()
    {
        $auth = $this->token->auth('POST');
        if ($auth == true) {
            $params = get_params();
            $datestart = filter_params($params, 'datestart');
            $dateend = filter_params($params, 'dateend');
            $start = filter_params($params, 'start');
            $count = filter_params($params, 'count');

            $data = $this->Rest_Jadwal_Model->view_jadwal($datestart, $dateend, $start, $count);
            $response = [];
            if ($data) {
                foreach ($data as $row => $val) {
                    $response[$row] = array(
                        'hari' => hari($val->tgl),
                        'tgl' => $val->tgl,
                        'shift' => $val->shift,
                        'jam_masuk' => jam($val->jam_masuk),
                        'jam_pulang' => jam($val->jam_pulang)
                    );
                }

                $status = 200;
                $message = 'Berhasil';
            } else {
                $status = 404;
                $message = 'Data tidak ditemukan';
            }

            print_json($status, $message, $response);
        }
    }
}

/* End of file Rest_Jadwal.php */
/* Location: ./application/controllers/Rest_Jadwal.php */
