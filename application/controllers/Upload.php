<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Upload extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Upload_Model');
    }

    function index()
    {
    }

    function gaji()
    {
        $this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css')
        );

        $id_company = company_id();
        $ms_company = $this->customdb->view_by_id('ms_company', ['id' => $id_company], 'row');
        $flag_gaji = isset($ms_company->flag_gaji) ? $ms_company->flag_gaji : 0;

        $footer = array(
            'script' => datatable('js')
        );

        if ($flag_gaji == 1) {
            $footer['app'] = 'gaji/upload_gaji.js';
        }

        $this->load->view('template/header', $header);
        if ($flag_gaji == 1) {
            $this->load->view('gaji/upload_gaji');
        } else {
            $this->load->view('gaji/oops.php');
        }
        $this->load->view('template/footer', $footer);
    }

    function format_gaji()
    {
        $this->Auth_Model->is_login();
        $id_company = company_id();
        $title = 'Format_Gaji'.date('Ymd');

        $ambildata  = $this->Upload_Model->karyawan();

        ini_set('display_errors', true);
        ini_set('display_startup_errors', true);
        define('EOL', (PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
        require_once(APPPATH.'libraries/PHPExcel.php');
        require_once(APPPATH.'libraries/PHPExcel/IOFactory.php');

        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("Attendance Management System");
        $objPHPExcel->getProperties()->setLastModifiedBy("Attendance Management System");
        $objPHPExcel->getProperties()->setTitle("Download Format Upload Gaji");
        $objPHPExcel->getProperties()->setSubject("Download Format Upload Gaji");
        $objPHPExcel->getProperties()->setDescription("Download Format Upload Gaji");
        $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
        $objget = $objPHPExcel->getActiveSheet();  //inisiasi get object

        $objget->setTitle('Format Upload Gaji'); //sheet title

        $styleArray = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN
                  )
              )
        );

        //table header
        $cols = array("A","B","C","D","E","F");
        $val = array("#","NIK","NAMA","BULAN (1 - 12)","TAHUN","NOMINAL");
             
        for ($a = 0; $a < count($cols); $a++) {
            $objset->setCellValue($cols[$a].'2', $val[$a]);
                 
            //Setting lebar cell
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
             
            $style = array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle($cols[$a].'1')->applyFromArray($style);
        }
             
        $baris  = 3;
        $i = 1;
        foreach ($ambildata as $frow) {
            //pemanggilan sesuaikan dengan nama kolom tabel
            $objset->setCellValue('A'.$baris, $i++);
            $objset->setCellValue('B'.$baris, $frow->nik);
            $objset->setCellValue('C'.$baris, $frow->nama);
            $objset->setCellValue('D'.$baris, date('m'));
            $objset->setCellValue('E'.$baris, date('Y'));
            $objset->setCellValue('F'.$baris, '0');
                 
            $baris++;
        }
        $b = $baris - 1;
        $objPHPExcel->getActiveSheet()->getStyle('A2:F'.$b)->applyFromArray($styleArray);
        
        $objPHPExcel->getActiveSheet()->setTitle('Format Upload Gaji');


        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename='.$title.'.xls');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }

    function add_gaji()
    {
        $this->Auth_Model->is_login();
        $company = company_id();
        $ms_company = $this->customdb->view_by_id('ms_company', ['id' => $company], 'row');
        $kode_perusahaan = isset($ms_company->kode_perusahaan) ? $ms_company->kode_perusahaan : '';

        $this->load->library('upload');
        $path = path_gaji();
        $file_name = 'GAJI_'.$kode_perusahaan.date('YmdHisu');
        $config = array(
            'upload_path' => $path,
            'allowed_types' => 'xls|xlsx',
            'file_name' => $file_name,
            'overwrite' => false,
            'max_size' => 0
        );
        $this->upload->initialize($config);

        $upload_foto = $this->upload->do_upload('gaji');
        $upload_data = $this->upload->data();
        $file_upload = isset($upload_data['file_name']) ? $upload_data['file_name'] : '';

        if (file_exists($path.$file_upload) == true) {
            $simpan = $this->Upload_Model->simpan_gaji($file_upload);

            if ($simpan > 0) {
                $status = 1;
                $message = 'File berhasil diupload';
            } else {
                $status = 0;
                $message = 'File gagal diupload';
            }
        } else {
            $status = 0;
            $message = 'Upload file gagal';
        }

        $result = array(
            'status' => $status,
            'message' => $message
        );

        echo json_encode($result);
    }

    function karyawan()
    {
        $this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css')
        );

        $id_company = company_id();
        $ms_company = $this->customdb->view_by_id('ms_company', ['id' => $id_company], 'row');
        $flag_gaji = isset($ms_company->flag_gaji) ? $ms_company->flag_gaji : 0;

        $url_download = ($flag_gaji == 1) ? base_url('assets/download/karyawan/dynamic/Format Upload Karyawan.xls') : base_url('assets/download/karyawan/static/Format Upload Karyawan.xls');

        $footer = array(
            'script' => datatable('js'),
            'app' => 'karyawan/upload_karyawan.js'
        );

        $data = array(
            'url' => $url_download
        );

        $this->load->view('template/header', $header);
        $this->load->view('karyawan/upload_karyawan', $data);
        $this->load->view('template/footer', $footer);
    }

    function add_karyawan()
    {
        $login = $this->Auth_Model->is_login_ajax();
        if ($login == true) {
            $company = company_id();
            $ms_company = $this->customdb->view_by_id('ms_company', ['id' => $company], 'row');
            $kode_perusahaan = isset($ms_company->kode_perusahaan) ? $ms_company->kode_perusahaan : '';

            $this->load->library('upload');
            $path = path_karyawan();
            $file_name = 'KARYAWAN_'.$kode_perusahaan.date('YmdHisu');
            $config = array(
                'upload_path' => $path,
                'allowed_types' => 'xls|xlsx',
                'file_name' => $file_name,
                'overwrite' => false,
                'max_size' => 0
            );
            $this->upload->initialize($config);

            $upload_foto = $this->upload->do_upload('karyawan');
            $upload_data = $this->upload->data();
            $file_upload = isset($upload_data['file_name']) ? $upload_data['file_name'] : '';

            if (file_exists($path.$file_upload) == true) {
                $simpan = $this->Upload_Model->simpan_karyawan($file_upload);

                if ($simpan > 0) {
                    $status = 1;
                    $message = 'File berhasil diupload';
                } else {
                    $status = 0;
                    $message = 'File gagal diupload';
                }
            } else {
                $status = 0;
                $message = 'Upload file gagal';
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        }
    }
}

/* End of file Upload.php */
/* Location: ./application/controllers/Upload.php */
