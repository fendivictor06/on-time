<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Shift extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Shift_Model');
    }

    function index()
    {
        $this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css')
        );

        $footer = array(
            'script' => datatable('js'),
            'app' => 'penjadwalan/shift.js'
        );

        $company = $this->Main_Model->list_company();
        $data = array(
            'company' => $company
        );

        $this->load->view('template/header', $header);
        $this->load->view('penjadwalan/shift', $data);
        $this->load->view('template/footer', $footer);
    }

    function data_shift()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $draw = $this->input->get('draw');
            $start = $this->input->get('start');
            $length = $this->input->get('length');
            $search = $this->input->get('search');
            $order = $this->input->get('order');

            $json = $this->Shift_Model->json_shift($draw, $start, $length, $search['value'], $order[0]['column'], $order[0]['dir']);

            echo json_encode($json);
        } else {
            show_404();
        }
    }

    function add_shift()
    {
        // checking auth
        $this->Auth_Model->is_login();
        // check request ajax jika bukan dari ajax show_404();
        if ($this->input->is_ajax_request()) {
            // declare all input
            $id = $this->input->post('id');
            $company = $this->input->post('company');
            $shift = $this->input->post('shift');
            $jam_masuk = $this->input->post('jam_masuk');
            $jam_pulang = $this->input->post('jam_pulang');
            $keterangan = $this->input->post('keterangan');
            $mulai_istirahat = $this->input->post('mulai_istirahat');
            $selesai_istirahat = $this->input->post('selesai_istirahat');

            // check mandatory field
            if ($company == '' || $shift == '' || $jam_masuk == '' || $jam_pulang == '' || $keterangan == '' || $mulai_istirahat == '' || $selesai_istirahat == '') {
                $message = '';
                $status = 0;

                $message .= ($company == '') ? 'Field Perusahaan wajib diisi. <br>' : '';
                $message .= ($shift == '') ? 'Field Shift wajib diisi. <br>' : '';
                $message .= ($jam_masuk == '') ? 'Field Jam Masuk wajib diisi. <br>' : '';
                $message .= ($jam_pulang == '') ? 'Field Jam Pulang wajib diisi. <br>' : '';
                $message .= ($keterangan == '') ? 'Field Keterangan wajib diisi. <br>' : '';
                $message .= ($mulai_istirahat == '') ? 'Field Mulai Istirahat wajib diisi <br>' : '';
                $message .= ($selesai_istirahat == '') ? 'Field Selesai Istirahat wajib diisi <br>' : '';
            } else {
                // user create
                $time = ($id != '') ? 'update_at' : 'insert_at';
                $user = ($id != '') ? 'user_update' : 'user_insert';

                // buat data utk disimpan
                $data = array(
                    'id_company' => $company,
                    'shift' => $shift,
                    'jam_masuk' => $jam_masuk,
                    'jam_pulang' => $jam_pulang,
                    'mulai_istirahat' => $mulai_istirahat,
                    'selesai_istirahat' => $selesai_istirahat,
                    'keterangan' => $keterangan,
                    $time => now(),
                    $user => username()
                );

                // if id exists or not
                $condition = ($id != '') ? ['id' => $id] : [];
                // simpan action
                $simpan = $this->customdb->process_data('ms_shift', $data, $condition);
                // message ketika insert
                if ($id == '') {
                    if ($simpan > 0) {
                        $status = 1;
                        $message = 'Berhasil menyimpan data.';
                    } else {
                        $status = 0;
                        $message = 'Gagal menyimpan data, ulangi beberapa saat lagi.';
                    }
                } else {
                    // message ketika update
                    $status = 1;
                    $message = 'Berhasil mengupdate data.';
                }
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        } else {
            show_404();
        }
    }

    function id_shift()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');

            $data = $this->customdb->view_by_id('ms_shift', ['id' => $id], 'row');
            echo json_encode($data);
        } else {
            show_404();
        }
    }

    function hapus_shift()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');

            $hapus = $this->customdb->process_data('ms_shift', ['status' => 0], ['id' => $id]);
            if ($hapus > 0) {
                $status = 1;
                $message = 'Data berhasil dihapus';
            } else {
                $status = 0;
                $message = 'Gagal menghapus data';
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        } else {
            show_404();
        }
    }

    function hari_libur()
    {
        $this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css')
        );

        $footer = array(
            'script' => datatable('js'),
            'app' => 'penjadwalan/hari_libur.js'
        );

        $company = $this->Main_Model->list_company();
        $data = array(
            'company' => $company
        );

        $this->load->view('template/header', $header);
        $this->load->view('penjadwalan/hari_libur', $data);
        $this->load->view('template/footer', $footer);
    }

    function data_libur()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $draw = $this->input->get('draw');
            $start = $this->input->get('start');
            $length = $this->input->get('length');
            $search = $this->input->get('search');
            $order = $this->input->get('order');

            $json = $this->Shift_Model->json_libur($draw, $start, $length, $search['value'], $order[0]['column'], $order[0]['dir']);

            echo json_encode($json);
        } else {
            show_404();
        }
    }

    function add_libur()
    {
        // checking auth
        $this->Auth_Model->is_login();
        // check request ajax jika bukan dari ajax show_404();
        if ($this->input->is_ajax_request()) {
            // declare all input
            $id = $this->input->post('id');
            $company = $this->input->post('company');
            $tgl = $this->input->post('tgl');
            $keterangan = $this->input->post('keterangan');

            $level = level_user();
            if ($level != 2) {
                $company = 0;
            } else {
                $company = company_id();
            }

            // check mandatory field
            if ($tgl == '' || $keterangan == '') {
                $message = '';
                $status = 0;

                // $message .= ($company == '') ? 'Field Perusahaan wajib diisi. <br>' : '';
                $message .= ($tgl == '') ? 'Field Tanggal wajib diisi. <br>' : '';
                $message .= ($keterangan == '') ? 'Field Keterangan wajib diisi. <br>' : '';
            } else {
                $tgl = convert_tgl($tgl);

                // user create
                $time = ($id != '') ? 'update_at' : 'insert_at';
                $user = ($id != '') ? 'user_update' : 'user_insert';

                // buat data utk disimpan
                $data = array(
                    'id_company' => $company,
                    'tgl' => $tgl,
                    'keterangan' => $keterangan,
                    $time => now(),
                    $user => username()
                );

                // if id exists or not
                $condition = ($id != '') ? ['id' => $id] : [];
                // simpan action
                $simpan = $this->customdb->process_data('ms_libur', $data, $condition);
                // message ketika insert
                if ($id == '') {
                    if ($simpan > 0) {
                        $status = 1;
                        $message = 'Berhasil menyimpan data.';
                    } else {
                        $status = 0;
                        $message = 'Gagal menyimpan data, ulangi beberapa saat lagi.';
                    }
                } else {
                    // message ketika update
                    $status = 1;
                    $message = 'Berhasil mengupdate data.';
                }
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        } else {
            show_404();
        }
    }

    function id_libur()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');
            $response = [];

            $data = $this->customdb->view_by_id('ms_libur', ['id' => $id], 'row');

            if ($data) {
                $response = array(
                    'id' => isset($data->id) ? $data->id : '',
                    'tgl' => isset($data->tgl) ? tanggal($data->tgl) : '',
                    'keterangan' => isset($data->keterangan) ? $data->keterangan : ''
                );
            }

            echo json_encode($response);
        } else {
            show_404();
        }
    }

    function hapus_libur()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');

            $hapus = $this->customdb->delete_data('ms_libur', ['id' => $id]);
            if ($hapus > 0) {
                $status = 1;
                $message = 'Data berhasil dihapus';
            } else {
                $status = 0;
                $message = 'Gagal menghapus data';
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        } else {
            show_404();
        }
    }

    function penjadwalan()
    {
        $this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css')
        );

        $footer = array(
            'script' => datatable('js'),
            'app' => 'penjadwalan/jadwal.js'
        );

        $company = $this->Main_Model->list_company();
        $mode = array(
            '1' => 'Per-orangan',
            '2' => 'Divisi'
        );
        $data = array(
            'company' => $company,
            'mode' => $mode
        );

        $this->load->view('template/header', $header);
        $this->load->view('penjadwalan/jadwal', $data);
        $this->load->view('template/footer', $footer);
    }

    function ajax_shift($id_company = '')
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $shift = $this->customdb->view_by_id('ms_shift', ['id_company' => $id_company, 'status' => 1], 'result');
            $result = [];

            if ($shift) {
                foreach ($shift as $row => $val) {
                    $result[$row] = array(
                        'id' => $val->id,
                        'text' => $val->shift.' ('.$val->jam_masuk.' - '.$val->jam_pulang.')'
                    );
                }
            }

            echo json_encode($result);
        } else {
            show_404();
        }
    }

    function ajax_karyawan($id_company = '')
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            if (level_user() == 2 && $id_company == '') {
                $id_company = company_id();
            }
            $karyawan = $this->customdb->view_by_id('ms_karyawan', ['id_company' => $id_company], 'result');
            $result = [];

            if ($karyawan) {
                foreach ($karyawan as $row => $val) {
                    $result[$row] = array(
                        'id' => $val->id,
                        'text' => $val->nama
                    );
                }
            }

            echo json_encode($result);
        } else {
            show_404();
        }
    }

    function ajax_divisi($id_company = '')
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $divisi = $this->customdb->view_by_id('ms_divisi', ['id_company' => $id_company, 'status' => 1], 'result');
            $result = [];

            if ($divisi) {
                foreach ($divisi as $row => $val) {
                    $result[$row] = array(
                        'id' => $val->id,
                        'text' => $val->divisi
                    );
                }
            }

            echo json_encode($result);
        } else {
            show_404();
        }
    }

    function add_penjadwalan()
    {
        // checking auth
        $this->Auth_Model->is_login();
        // check request ajax jika bukan dari ajax show_404();
        if ($this->input->is_ajax_request()) {
            // declare all input
            $id = $this->input->post('id');
            $company = $this->input->post('company');
            $karyawan = $this->input->post('karyawan');
            $shift = $this->input->post('shift');
            $tgl_mulai = $this->input->post('tgl_mulai');
            $tgl_selesai = $this->input->post('tgl_selesai');

            // check mandatory field
            if ($company == '' || $tgl_mulai == '' || $karyawan == '' || $shift == '' || $tgl_selesai == '') {
                $message = '';
                $status = 0;

                $message .= ($company == '') ? 'Field Perusahaan wajib diisi. <br>' : '';
                $message .= ($tgl_mulai == '') ? 'Field Tanggal Mulai wajib diisi. <br>' : '';
                $message .= ($karyawan == '') ? 'Field Karyawan wajib diisi. <br>' : '';
                $message .= ($shift == '') ? 'Field Shift wajib diisi. <br>' : '';
                $message .= ($tgl_selesai == '') ? 'Field Tanggal Selesai wajib diisi <br>' : '';
            } else {
                $query = [];
                $tgl_mulai = convert_tgl($tgl_mulai);
                $tgl_selesai = convert_tgl($tgl_selesai);

                $arr_tgl = range_to_date($tgl_mulai, $tgl_selesai);
                if ($arr_tgl) {
                    $string_query = '';
                    for ($i = 0; $i < count($arr_tgl); $i++) {
                        $tgl = isset($arr_tgl[$i]) ? $arr_tgl[$i] : '';
                        # get data master shift
                        $ms_shift = $this->customdb->view_by_id('ms_shift', ['id' => $shift], 'row');
                        $jam_masuk = isset($ms_shift->jam_masuk) ? $ms_shift->jam_masuk : '';
                        $jam_pulang = isset($ms_shift->jam_pulang) ? $ms_shift->jam_pulang : '';
                        $keterangan = isset($ms_shift->keterangan) ? $ms_shift->keterangan : '';
                        $nama_shift = isset($ms_shift->shift) ? $ms_shift->shift : '';
                        $mulai_istirahat = isset($ms_shift->mulai_istirahat) ? $ms_shift->mulai_istirahat : '';
                        $selesai_istirahat = isset($ms_shift->selesai_istirahat) ? $ms_shift->selesai_istirahat : '';

                        $query[] = [$company, $shift, $karyawan, $tgl, $jam_masuk, $jam_pulang, $nama_shift, $keterangan, username(), $mulai_istirahat, $selesai_istirahat];
                        $string_query = build($query);
                    }

                    $simpan = $this->Shift_Model->simpan_jadwal($string_query, $tgl_mulai, $tgl_selesai, $karyawan);
                    if ($simpan > 0) {
                        $status = 1;
                        $message = 'Jadwal berhasil dibuat';
                    } else {
                        $status = 0;
                        $message = 'Jadwal gagal dibuat';
                    }
                } else {
                    $status = 0;
                    $message = 'Tanggal tidak ditemukan / invalid';
                }
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        } else {
            show_404();
        }
    }

    function add_penjadwalan20181129()
    {
        // checking auth
        $this->Auth_Model->is_login();
        // check request ajax jika bukan dari ajax show_404();
        if ($this->input->is_ajax_request()) {
            // declare all input
            $id = $this->input->post('id');
            $company = $this->input->post('company');
            $karyawan = $this->input->post('karyawan');
            $shift = $this->input->post('shift');
            $tgl = $this->input->post('tgl');

            // check mandatory field
            if ($company == '' || $tgl == '' || $karyawan == '' || $shift == '') {
                $message = '';
                $status = 0;

                $message .= ($company == '') ? 'Field Perusahaan wajib diisi. <br>' : '';
                $message .= ($tgl == '') ? 'Field Tanggal wajib diisi. <br>' : '';
                $message .= ($karyawan == '') ? 'Field Karyawan wajib diisi. <br>' : '';
                $message .= ($shift == '') ? 'Field Shift wajib diisi. <br>' : '';
            } else {
                $tgl = convert_tgl($tgl);

                # delete dulu bos
                $delete = $this->customdb->delete_data('tb_jadwal', ['id_karyawan' => $karyawan, 'tgl' => $tgl]);

                // user create
                $time = ($id != '') ? 'update_at' : 'insert_at';
                $user = ($id != '') ? 'user_update' : 'user_insert';

                # get data master shift
                $ms_shift = $this->customdb->view_by_id('ms_shift', ['id' => $shift], 'row');
                $jam_masuk = isset($ms_shift->jam_masuk) ? $ms_shift->jam_masuk : '';
                $jam_pulang = isset($ms_shift->jam_pulang) ? $ms_shift->jam_pulang : '';
                $keterangan = isset($ms_shift->keterangan) ? $ms_shift->keterangan : '';
                $nama_shift = isset($ms_shift->shift) ? $ms_shift->shift : '';

                // buat data utk disimpan
                $data = array(
                    'id_company' => $company,
                    'id_shift' => $shift,
                    'id_karyawan' => $karyawan,
                    'shift' => $nama_shift,
                    'jam_masuk' => $jam_masuk,
                    'jam_pulang' => $jam_pulang,
                    'tgl' => $tgl,
                    'keterangan' => $keterangan,
                    $time => now(),
                    $user => username()
                );

                // if id exists or not
                // $condition = ($id != '') ? ['id' => $id] : [];
                // simpan action
                $simpan = $this->customdb->process_data('tb_jadwal', $data);
                // message ketika insert
                if ($id == '') {
                    if ($simpan > 0) {
                        $status = 1;
                        $message = 'Berhasil menyimpan data.';
                    } else {
                        $status = 0;
                        $message = 'Gagal menyimpan data, ulangi beberapa saat lagi.';
                    }
                } else {
                    // message ketika update
                    $status = 1;
                    $message = 'Berhasil mengupdate data.';
                }
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        } else {
            show_404();
        }
    }

    function id_penjadwalan()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');

            $data = $this->Shift_Model->penjadwalan_id($id);
            echo json_encode($data);
        } else {
            show_404();
        }
    }

    function hapus_penjadwalan()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');

            $hapus = $this->customdb->delete_data('tb_jadwal', ['id' => $id]);
            if ($hapus > 0) {
                $status = 1;
                $message = 'Data berhasil dihapus';
            } else {
                $status = 0;
                $message = 'Gagal menghapus data';
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        } else {
            show_404();
        }
    }

    function data_penjadwalan()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $draw = $this->input->get('draw');
            $start = $this->input->get('start');
            $length = $this->input->get('length');
            $search = $this->input->get('search');
            $order = $this->input->get('order');

            $datestart = $this->input->get('datestart');
            $dateend = $this->input->get('dateend');
            $company = $this->input->get('company');
            $karyawan = $this->input->get('karyawan');

            $json = $this->Shift_Model->json_penjadwalan($draw, $start, $length, $search['value'], $order[0]['column'], $order[0]['dir'], $datestart, $dateend, $company, $karyawan);

            echo json_encode($json);
        } else {
            show_404();
        }
    }

    function copy_jadwal()
    {
        $this->Auth_Model->is_login();
        $company = $this->input->post('company-copy');
        $karyawan = $this->input->post('karyawan-copy');
        $tgl_mulai = $this->input->post('tgl_mulai-copy');
        $tgl_selesai = $this->input->post('tgl_selesai-copy');
        $mode = $this->input->post('mode-copy');
        $divisi = $this->input->post('divisi-copy');
        $target = $this->input->post('target-copy');

        if ($company == '' || $karyawan == '' || $tgl_mulai == '' || $tgl_selesai == '') {
            $status = 0;
            $message = '';

            if ($company == '') {
                $message .= 'Field Perusahaan wajib diisi <br>';
            }

            if ($karyawan == '') {
                $message .= 'Field Karyawan wajib diisi <br>';
            }

            if ($tgl_mulai == '') {
                $message .= 'Field Tgl Mulai wajib diisi <br>';
            }

            if ($tgl_selesai == '') {
                $message .= 'Field Tgl Selesai wajib diisi <br>';
            }
        } else {
            $tgl_mulai = convert_tgl($tgl_mulai);
            $tgl_selesai = convert_tgl($tgl_selesai);

            if (strtotime($tgl_selesai) < strtotime($tgl_mulai)) {
                $status = 0;
                $message = 'Format tgl tidak valid, Tgl Selesai harus lebih dari / sama dgn tgl mulai';
            } else {
                $arr_karyawan = [];
                if ($mode == 1) {
                    $arr_karyawan = [$target];

                    $simpan = $this->Shift_Model->salin_jadwal($karyawan, $tgl_mulai, $tgl_selesai, $arr_karyawan);

                    if ($simpan > 0) {
                        $status = 1;
                        $message = 'Data berhasil disimpan';
                    } else {
                        $status = 0;
                        $message = 'Gagal menyimpan data';
                    }
                } elseif ($mode == 2) {
                    $ms_karyawan = $this->customdb->view_by_id('ms_karyawan', ['id_company' => $company, 'id_divisi' => $divisi], 'result');
                    if ($ms_karyawan) {
                        foreach ($ms_karyawan as $row) {
                            if ($karyawan != $row->id) {
                                $arr_karyawan[] = $row->id;
                            }
                        }

                        $simpan = $this->Shift_Model->salin_jadwal($karyawan, $tgl_mulai, $tgl_selesai, $arr_karyawan);

                        if ($simpan > 0) {
                            $status = 1;
                            $message = 'Data berhasil disimpan';
                        } else {
                            $status = 0;
                            $message = 'Gagal menyimpan data';
                        }
                    } else {
                        $status = 0;
                        $message = 'Karyawan tidak ditemukan';
                    }
                } else {
                    $status = 0;
                    $message = 'Mode tidak diketahui';
                }
            }
        }

        $result = array(
            'status' => $status,
            'message' => $message
        );

        echo json_encode($result);
    }
}

/* End of file Shift.php */
/* Location: ./application/controllers/Shift.php */
