<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Rest_Lembur extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Rest_Lembur_Model');
    }

    function index()
    {
        $auth = $this->token->auth('POST');
        if ($auth == true) {
            $params = get_params();
            $datestart = filter_params($params, 'datestart');
            $dateend = filter_params($params, 'dateend');

            $response = [];
            if ($datestart == '' || $dateend == '') {
                $status = 404;
                $message = 'Masukkan tanggal lembur';
            } else {
                if (strtotime($datestart) > strtotime($dateend)) {
                    $status = 400;
                    $message = 'Format tanggal salah';
                } else {
                    $data = $this->Rest_Lembur_Model->view_lembur($datestart, $dateend);

                    if ($data) {
                        foreach ($data as $row => $val) {
                            $response[$row] = array(
                                'id' => $val->id,
                                'nama' => $val->nama,
                                'nominal' => $val->nominal,
                                'keterangan' => $val->keterangan,
                                'mulai' => $val->mulai,
                                'selesai' => $val->selesai,
                                'awal' => $val->awal,
                                'akhir' => $val->akhir
                            );
                        }

                        $status = 200;
                        $message = 'Berhasil';
                    } else {
                        $status = 404;
                        $message = 'Data tidak ditemukan';
                    }
                }
            }

            print_json($status, $message, $response);
        }
    }
}

/* End of file Rest_Lembur.php */
/* Location: ./application/controllers/Rest_Lembur.php */
