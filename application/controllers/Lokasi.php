<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Lokasi extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Master_Model');
    }

    function index()
    {
        $this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css')
        );

        $company = $this->Main_Model->list_company();

        $data = array(
            'company' => $company
        );

        $footer = array(
            'script' => datatable('js').'<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAspaX59masB7qErpq95bFSu-SAWUidh-Q"></script>',
            'app' => 'master/lokasi.min.js'
        );

        $this->load->view('template/header', $header);
        $this->load->view('master/lokasi', $data);
        $this->load->view('template/footer', $footer);
    }

    function data_lokasi()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $draw = $this->input->get('draw');
            $start = $this->input->get('start');
            $length = $this->input->get('length');
            $search = $this->input->get('search');
            $order = $this->input->get('order');
            $company = $this->input->get('company');

            $json = $this->Master_Model->json_lokasi($draw, $start, $length, $search['value'], $order[0]['column'], $order[0]['dir'], $company);

            echo json_encode($json);
        } else {
            show_404();
        }
    }

    function add_lokasi()
    {
        $login = $this->Auth_Model->is_login_ajax();
        if ($login == true) {
            $id = $this->input->post('id');
            $company = $this->input->post('company');
            $nama = $this->input->post('nama');
            $latitude = $this->input->post('latitude');
            $longitude = $this->input->post('longitude');
            $toleransi = $this->input->post('toleransi');

            $this->form_validation->set_rules('company', 'Perusahaan', 'required', ['required' => 'Masukkan %s']);
            $this->form_validation->set_rules('latitude', 'Lokasi', 'required', ['required' => '%s Tidak sesuai']);
            $this->form_validation->set_rules('longitude', 'Lokasi', 'required', ['required' => '%s Tidak sesuai']);
            $this->form_validation->set_rules('toleransi', 'Toleransi', 'required', ['required' => 'Masukkan %s']);

            if ($this->form_validation->run() == false) {
                $status = 0;
                $message = validation_errors();
            } else {
                if ($toleransi <= 600) {
                    $user = ($id) ? 'user_update' : 'user_insert';
                    $time = ($id) ? 'update_at' : 'insert_at';
                    $condition = ($id) ? ['id' => $id] : [];

                    $data = array(
                        'id_company' => $company,
                        'nama' => $nama,
                        'latitude' => $latitude,
                        'longitude' => $longitude,
                        'toleransi' => $toleransi,
                        $user => username(),
                        $time => now()
                    );

                    $simpan = $this->customdb->process_data('ms_lokasi', $data, $condition);
                    if ($id == '') {
                        if ($simpan > 0) {
                            $status = 1;
                            $message = 'Data berhasil disimpan';
                        } else {
                            $status = 0;
                            $message = 'Gagal menyimpan data';
                        }
                    } else {
                        $status = 1;
                        $message = 'Data berhasil disimpan';
                    }
                } else {
                    $status = 0;
                    $message = 'Toleransi Jarak maksimal 600m';
                }
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        }
    }

    function lokasi_id()
    {
        $login = $this->Auth_Model->is_login_ajax();
        if ($login == true) {
            $id = $this->input->get('id');

            $data = $this->customdb->view_by_id('ms_lokasi', ['id' => $id], 'row');
            echo json_encode($data);
        }
    }

    function hapus_lokasi()
    {
        $login = $this->Auth_Model->is_login_ajax();
        if ($login == true) {
            $id = $this->input->get('id');
            $delete = $this->customdb->process_data('ms_lokasi', ['status' => 0], ['id' => $id]);
            if ($delete > 0) {
                $status = 1;
                $message = 'Data berhasil dihapus';
            } else {
                $status = 0;
                $message = 'Gagal menghapus data';
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        }
    }
}

/* End of file Lokasi.php */
/* Location: ./application/controllers/Lokasi.php */
