<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Scanlog extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Scanlog_Model');
    }

    function index()
    {
        $this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css')
        );

        $footer = array(
            'script' => datatable('js'),
            'app' => 'scanlog/scanlog.js'
        );

        $company = $this->Main_Model->list_company();
        $data = array(
            'company' => $company
        );

        $this->load->view('template/header', $header);
        $this->load->view('scanlog/scanlog', $data);
        $this->load->view('template/footer', $footer);
    }

    function data_scanlog()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $draw = $this->input->get('draw');
            $start = $this->input->get('start');
            $length = $this->input->get('length');
            $search = $this->input->get('search');
            $order = $this->input->get('order');

            $datestart = $this->input->get('datestart');
            $dateend = $this->input->get('dateend');
            $company = $this->input->get('company');
            $karyawan = $this->input->get('karyawan');

            $json = $this->Scanlog_Model->json_scanlog($draw, $start, $length, $search['value'], $order[0]['column'], $order[0]['dir'], $datestart, $dateend, $company, $karyawan);

            echo json_encode($json);
        } else {
            show_404();
        }
    }
}

/* End of file Scanlog.php */
/* Location: ./application/controllers/Scanlog.php */
