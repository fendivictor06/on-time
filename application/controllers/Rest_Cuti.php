<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Rest_Cuti extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Rest_Cuti_Model');
    }

    function index()
    {
        $auth = $this->token->auth('POST');
        if ($auth == true) {
            $params = get_params();
            $start = filter_params($params, 'start');
            $count = filter_params($params, 'count');
            $id = filter_params($params, 'id');

            $data = $this->Rest_Cuti_Model->view_cuti($start, $count, $id);
            if ($data) {
                $status = 200;
                $message = 'Berhasil';
            } else {
                $status = 404;
                $message = 'Data tidak ditemukan';
            }

            print_json($status, $message, $data);
        }
    }

    function view_approval()
    {
        $auth = $this->token->auth('POST');
        if ($auth == true) {
            $params = get_params();
            $start = filter_params($params, 'start');
            $count = filter_params($params, 'count');
            $id = filter_params($params, 'id');

            $data = $this->Rest_Cuti_Model->view_approval($start, $count, $id);
            if ($data) {
                $status = 200;
                $message = 'Berhasil';
            } else {
                $status = 404;
                $message = 'Data tidak ditemukan';
            }

            print_json($status, $message, $data);
        }
    }

    function approve_cuti()
    {
        $auth = $this->token->auth('POST');
        if ($auth == true) {
            $params = get_params();
            $id = filter_params($params, 'id');
            $apv = filter_params($params, 'status');
            # status = 2 setuju
            # status = 3 tolak

            $simpan = $this->Rest_Cuti_Model->approve_cuti($id, $apv);
            if ($simpan > 0) {
                $status = 200;
                $message = 'Cuti berhasil ';

                if ($apv == 2) {
                    $message .= 'disetujui';
                } else {
                    $message .= 'ditolak';
                }
            } else {
                $status = 500;
                $message = 'Cuti gagal ';

                if ($apv == 2) {
                    $message .= 'disetujui';
                } else {
                    $message .= 'ditolak';
                }
            }

            print_json($status, $message, []);
        }
    }

    function add_cuti20181130()
    {
        $auth = $this->token->auth('POST');
        if ($auth == true) {
            $params = get_params();
            $id = filter_params($params, 'id');
            $awal = filter_params($params, 'datestart');
            $akhir = filter_params($params, 'dateend');
            $alasan = filter_params($params, 'alasan');

            if ($awal == '' || $akhir == '' || $alasan == '') {
                $status = 404;
                $message = '';

                if ($awal == '') {
                    $message .= 'Masukkan tanggal mulai. ';
                }

                if ($akhir == '') {
                    $message .= 'Masukkan tanggal selesai. ';
                }

                if ($alasan == '') {
                    $message .= 'Masukkan alasan cuti. ';
                }
            } else {
                $id_company = id_company();
                $id_karyawan = id_karyawan();

                if (strtotime($awal) > strtotime($akhir)) {
                    $status = 400;
                    $message = 'Tanggal cuti tidak sesuai';
                } else {
                    # check kuota karyawan
                    $kuota = $this->Rest_Cuti_Model->kuota_cuti($awal, $akhir);

                    # hitung hari cuti
                    $jumlah = $this->Rest_Cuti_Model->hitung_cuti($awal, $akhir);

                    if ($jumlah > $kuota) {
                        $status = 400;
                        $message = 'Kuota cuti anda tidak mencukupi';
                    } else {
                        # validasi tanggal hari ini atau kemarin
                        $today = date('Y-m-d');
                        if (strtotime($awal) <= strtotime($today)) {
                            $status = 400;
                            $message = 'Pengajuan cuti tidak boleh hari ini atau hari kemarin.';
                        } else {
                            # validasi tanggal yang sama
                            $validasi_tgl = $this->Rest_Cuti_Model->validasi_tanggal($awal, $akhir);

                            if (empty($validasi_tgl)) {
                                $condition = ($id != '') ? ['id' => $id] : [];
                                $time = ($id != '') ? 'update_at' : 'insert_at';
                                $user = ($id != '') ? 'user_update' : 'user_insert';

                                $data = array(
                                    'id_company' => $id_company,
                                    'id_karyawan' => $id_karyawan,
                                    'awal' => $awal,
                                    'akhir' => $akhir,
                                    'alasan' => $alasan,
                                    'jumlah' => $jumlah,
                                    $time => now(),
                                    $user => user_id()
                                );

                                $simpan = $this->customdb->process_data('tb_cuti', $data, $condition);
                                if ($simpan > 0) {
                                    $tanggal_cuti = $this->Rest_Cuti_Model->tanggal_cuti($awal, $akhir);
                                    if ($tanggal_cuti) {
                                        # delete if exist
                                        $this->customdb->delete_data('detail_cuti', ['id_cuti' => $simpan]);

                                        for ($i = 0; $i < count($tanggal_cuti); $i++) {
                                            $detail = array(
                                                'id_cuti' => $simpan,
                                                'id_company' => $id_company,
                                                'id_karyawan' => $id_karyawan,
                                                'tgl' => $tanggal_cuti[$i],
                                                'insert_at' => now(),
                                                'user_insert' => user_id()
                                            );
                                            # simpan detail cuti
                                            $this->customdb->process_data('detail_cuti', $detail);
                                        }
                                    }

                                    $status = 200;
                                    $message = 'Data berhasil disimpan';
                                } else {
                                    $status = 500;
                                    $message = 'Gagal menyimpan data';
                                }
                            } else {
                                $status = 400;
                                $message = 'Anda sudah mengajukan cuti di tanggal ';

                                for ($i = 0; $i < count($validasi_tgl); $i++) {
                                    $message .= tgl_indo($validasi_tgl[$i]);
                                    if ((count($validasi_tgl) - 1) != $i) {
                                        $message .= ', ';
                                    }
                                }
                            }
                        }
                    }
                }
            }

            print_json($status, $message, []);
            log_api($params, $status, $message, []);
        }
    }

    function add_cuti()
    {
        $auth = $this->token->auth('POST');
        if ($auth == true) {
            $params = get_params();
            $id = filter_params($params, 'id');
            $awal = filter_params($params, 'datestart');
            $akhir = filter_params($params, 'dateend');
            $alasan = filter_params($params, 'alasan');

            if ($awal == '' || $akhir == '' || $alasan == '') {
                $status = 404;
                $message = '';

                if ($awal == '') {
                    $message .= 'Masukkan tanggal mulai. ';
                }

                if ($akhir == '') {
                    $message .= 'Masukkan tanggal selesai. ';
                }

                if ($alasan == '') {
                    $message .= 'Masukkan alasan cuti. ';
                }
            } else {
                $id_company = id_company();
                $id_karyawan = id_karyawan();

                if (strtotime($awal) > strtotime($akhir)) {
                    $status = 400;
                    $message = 'Tanggal cuti tidak sesuai';
                } else {
                    # validasi tanggal hari ini atau kemarin
                    $today = date('Y-m-d');
                    if (strtotime($awal) <= strtotime($today)) {
                        $status = 400;
                        $message = 'Pengajuan cuti tidak boleh hari ini atau hari kemarin.';
                    } else {
                        # validasi tanggal yang sama
                        $validasi_tgl = $this->Rest_Cuti_Model->validasi_tanggal($awal, $akhir);

                        # hitung hari cuti
                        $jumlah = $this->Rest_Cuti_Model->hitung_cuti($awal, $akhir);

                        if (empty($validasi_tgl)) {
                            $condition = ($id != '') ? ['id' => $id] : [];
                            $time = ($id != '') ? 'update_at' : 'insert_at';
                            $user = ($id != '') ? 'user_update' : 'user_insert';

                            $data = array(
                                'id_company' => $id_company,
                                'id_karyawan' => $id_karyawan,
                                'awal' => $awal,
                                'akhir' => $akhir,
                                'alasan' => $alasan,
                                'jumlah' => $jumlah,
                                $time => now(),
                                $user => user_id()
                            );

                            $simpan = $this->customdb->process_data('tb_cuti', $data, $condition);
                            if ($simpan > 0) {
                                $tanggal_cuti = $this->Rest_Cuti_Model->tanggal_cuti($awal, $akhir);
                                if ($tanggal_cuti) {
                                    # delete if exist
                                    $this->customdb->delete_data('detail_cuti', ['id_cuti' => $simpan]);

                                    for ($i = 0; $i < count($tanggal_cuti); $i++) {
                                        $detail = array(
                                            'id_cuti' => $simpan,
                                            'id_company' => $id_company,
                                            'id_karyawan' => $id_karyawan,
                                            'tgl' => $tanggal_cuti[$i],
                                            'insert_at' => now(),
                                            'user_insert' => user_id()
                                        );
                                        # simpan detail cuti
                                        $this->customdb->process_data('detail_cuti', $detail);
                                    }
                                }

                                $karyawan = $this->customdb->view_by_id('ms_karyawan', ['id' => $id_karyawan], 'row');
                                $nama = isset($karyawan->nama) ? $karyawan->nama : '';

                                $title = 'Pengajuan Cuti';
                                $text = $nama.' Telah mengajukan Cuti.';
                                $fcm = apv_cuti();

                                $notif = $this->firebasenotif->notif($title, $text, $fcm, 'ACT_MAIN', [['jenis' => 'cuti']]);
                                # log notif
                                log_api([$title, $text, $fcm], $notif, '', []);

                                $status = 200;
                                $message = 'Data berhasil disimpan';
                            } else {
                                $status = 500;
                                $message = 'Gagal menyimpan data';
                            }
                        } else {
                            $status = 400;
                            $message = 'Anda sudah mengajukan cuti di tanggal ';

                            for ($i = 0; $i < count($validasi_tgl); $i++) {
                                $message .= tgl_indo($validasi_tgl[$i]);
                                if ((count($validasi_tgl) - 1) != $i) {
                                    $message .= ', ';
                                }
                            }
                        }
                    }
                }
            }

            print_json($status, $message, []);
            log_api($params, $status, $message, []);
        }
    }

    function jumlah_cuti()
    {
        $auth = $this->token->auth('GET');
        if ($auth == true) {
            $data = $this->Rest_Cuti_Model->jumlah_cuti();
            if ($data) {
                $status = 200;
                $message = 'Berhasil';
            } else {
                $status = 404;
                $message = 'Data tidak ditemukan';
            }

            print_json($status, $message, $data);
        }
    }
}

/* End of file Rest_Cuti.php */
/* Location: ./application/controllers/Rest_Cuti.php */
