<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Rest_Pulang_Awal extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Rest_Pulang_Model');
    }

    function index()
    {
        $auth = $this->token->auth('POST');
        if ($auth == true) {
            $params = get_params();
            $start = filter_params($params, 'start');
            $count = filter_params($params, 'count');
            $id = filter_params($params, 'id');

            $data = $this->Rest_Pulang_Model->view_pulang_awal($start, $count, $id);
            if ($data) {
                $status = 200;
                $message = 'Berhasil';
            } else {
                $status = 404;
                $message = 'Data tidak ditemukan';
            }

            print_json($status, $message, $data);
        }
    }

    function add_pulang_awal()
    {
        $auth = $this->token->auth('POST');
        if ($auth == true) {
            $params = get_params();
            $id = filter_params($params, 'id');
            $tgl = filter_params($params, 'tgl');
            $jam = filter_params($params, 'jam');
            $alasan = filter_params($params, 'alasan');

            if ($tgl == '' || $jam == '' || $alasan == '') {
                $status = 404;
                $message = '';

                if ($tgl == '') {
                    $message .= 'Masukkan Tanggal. ';
                }

                if ($jam == '') {
                    $message .= 'Masukkan jam. ';
                }

                if ($alasan == '') {
                    $message .= 'Masukkan Alasan ';
                }
            } else {
                $id_company = id_company();
                $id_karyawan = id_karyawan();

                $condition = ($id != '') ? ['id' => $id] : [];
                $time = ($id != '') ? 'update_at' : 'insert_at';
                $user = ($id != '') ? 'user_update' : 'user_insert';

                $data = array(
                    'id_company' => $id_company,
                    'id_karyawan' => $id_karyawan,
                    'tgl' => $tgl,
                    'jam' => $jam,
                    'alasan' => $alasan,
                    $time => now(),
                    $user => user_id()
                );

                $simpan = $this->customdb->process_data('tb_pulang_awal', $data, $condition);
                if ($simpan > 0) {
                    $status = 200;
                    $message = 'Data berhasil disimpan';
                } else {
                    $status = 500;
                    $message = 'Gagal menyimpan data';
                }
            }

            print_json($status, $message, []);
            log_api($params, $status, $message, []);
        }
    }
}

/* End of file Rest_Pulang_Awal.php */
/* Location: ./application/controllers/Rest_Pulang_Awal.php */
