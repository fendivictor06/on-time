<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Master extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Master_Model');
    }

    function jenis_kelamin()
    {
        $this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css')
        );

        $footer = array(
            'script' => datatable('js'),
            'app' => 'master/jenis_kelamin.js'
        );

        $this->load->view('template/header', $header);
        $this->load->view('master/jenis_kelamin');
        $this->load->view('template/footer', $footer);
    }

    function data_jenis_kelamin()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $draw = $this->input->get('draw');
            $start = $this->input->get('start');
            $length = $this->input->get('length');
            $search = $this->input->get('search');
            $order = $this->input->get('order');

            $json = $this->Master_Model->json_jenis_kelamin($draw, $start, $length, $search['value'], $order[0]['column'], $order[0]['dir']);

            echo json_encode($json);
        } else {
            show_404();
        }
    }

    function add_jenis_kelamin()
    {
        // checking auth
        $this->Auth_Model->is_login();
        // check request ajax jika bukan dari ajax show_404();
        if ($this->input->is_ajax_request()) {
            // declare all input
            $id = $this->input->post('id');
            $jenis_kelamin = $this->input->post('jenis_kelamin');
            $keterangan = $this->input->post('keterangan');

            // check mandatory field
            if ($jenis_kelamin == '') {
                $message = '';
                $status = 0;

                // jika field jenis_kelamin kosong
                $message .= ($jenis_kelamin == '') ? 'Field Jenis Kelamin wajib diisi. <br>' : '';
            } else {
                // buat data utk disimpan
                $data = array(
                    'jenis_kelamin' => $jenis_kelamin,
                    'keterangan' => $keterangan
                );

                // if id exists or not
                $condition = ($id != '') ? ['id' => $id] : [];
                // simpan action
                $simpan = $this->customdb->process_data('ms_jenis_kelamin', $data, $condition);
                // message ketika insert
                if ($id == '') {
                    if ($simpan > 0) {
                        $status = 1;
                        $message = 'Berhasil menyimpan data.';
                    } else {
                        $status = 0;
                        $message = 'Gagal menyimpan data, ulangi beberapa saat lagi.';
                    }
                } else {
                    // message ketika update
                    $status = 1;
                    $message = 'Berhasil mengupdate data.';
                }
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        } else {
            show_404();
        }
    }

    function id_jenis_kelamin()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');

            $data = $this->customdb->view_by_id('ms_jenis_kelamin', ['id' => $id], 'row');
            echo json_encode($data);
        } else {
            show_404();
        }
    }

    function hapus_jenis_kelamin()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');

            $hapus = $this->customdb->process_data('ms_jenis_kelamin', ['status' => 0], ['id' => $id]);
            if ($hapus > 0) {
                $status = 1;
                $message = 'Data berhasil dihapus';
            } else {
                $status = 0;
                $message = 'Gagal menghapus data';
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        } else {
            show_404();
        }
    }

    function golongan_darah()
    {
        $this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css')
        );

        $footer = array(
            'script' => datatable('js'),
            'app' => 'master/golongan_darah.js'
        );

        $this->load->view('template/header', $header);
        $this->load->view('master/golongan_darah');
        $this->load->view('template/footer', $footer);
    }

    function data_golongan_darah()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $draw = $this->input->get('draw');
            $start = $this->input->get('start');
            $length = $this->input->get('length');
            $search = $this->input->get('search');
            $order = $this->input->get('order');

            $json = $this->Master_Model->json_golongan_darah($draw, $start, $length, $search['value'], $order[0]['column'], $order[0]['dir']);

            echo json_encode($json);
        } else {
            show_404();
        }
    }

    function add_golongan_darah()
    {
        // checking auth
        $this->Auth_Model->is_login();
        // check request ajax jika bukan dari ajax show_404();
        if ($this->input->is_ajax_request()) {
            // declare all input
            $id = $this->input->post('id');
            $golongan_darah = $this->input->post('golongan_darah');

            // check mandatory field
            if ($golongan_darah == '') {
                $message = '';
                $status = 0;

                // jika field golongan_darah kosong
                $message .= ($golongan_darah == '') ? 'Field Golongan Darah wajib diisi. <br>' : '';
            } else {
                // buat data utk disimpan
                $data = array(
                    'golongan_darah' => $golongan_darah
                );

                // if id exists or not
                $condition = ($id != '') ? ['id' => $id] : [];
                // simpan action
                $simpan = $this->customdb->process_data('ms_golongan_darah', $data, $condition);
                // message ketika insert
                if ($id == '') {
                    if ($simpan > 0) {
                        $status = 1;
                        $message = 'Berhasil menyimpan data.';
                    } else {
                        $status = 0;
                        $message = 'Gagal menyimpan data, ulangi beberapa saat lagi.';
                    }
                } else {
                    // message ketika update
                    $status = 1;
                    $message = 'Berhasil mengupdate data.';
                }
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        } else {
            show_404();
        }
    }

    function id_golongan_darah()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');

            $data = $this->customdb->view_by_id('ms_golongan_darah', ['id' => $id], 'row');
            echo json_encode($data);
        } else {
            show_404();
        }
    }

    function hapus_golongan_darah()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');

            $hapus = $this->customdb->process_data('ms_golongan_darah', ['status' => 0], ['id' => $id]);
            if ($hapus > 0) {
                $status = 1;
                $message = 'Data berhasil dihapus';
            } else {
                $status = 0;
                $message = 'Gagal menghapus data';
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        } else {
            show_404();
        }
    }

    function agama()
    {
        $this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css')
        );

        $footer = array(
            'script' => datatable('js'),
            'app' => 'master/agama.js'
        );

        $this->load->view('template/header', $header);
        $this->load->view('master/agama');
        $this->load->view('template/footer', $footer);
    }

    function data_agama()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $draw = $this->input->get('draw');
            $start = $this->input->get('start');
            $length = $this->input->get('length');
            $search = $this->input->get('search');
            $order = $this->input->get('order');

            $json = $this->Master_Model->json_agama($draw, $start, $length, $search['value'], $order[0]['column'], $order[0]['dir']);

            echo json_encode($json);
        } else {
            show_404();
        }
    }

    function add_agama()
    {
        // checking auth
        $this->Auth_Model->is_login();
        // check request ajax jika bukan dari ajax show_404();
        if ($this->input->is_ajax_request()) {
            // declare all input
            $id = $this->input->post('id');
            $agama = $this->input->post('agama');

            // check mandatory field
            if ($agama == '') {
                $message = '';
                $status = 0;

                // jika field agama kosong
                $message .= ($agama == '') ? 'Field Agama wajib diisi. <br>' : '';
            } else {
                // buat data utk disimpan
                $data = array(
                    'agama' => $agama
                );

                // if id exists or not
                $condition = ($id != '') ? ['id' => $id] : [];
                // simpan action
                $simpan = $this->customdb->process_data('ms_agama', $data, $condition);
                // message ketika insert
                if ($id == '') {
                    if ($simpan > 0) {
                        $status = 1;
                        $message = 'Berhasil menyimpan data.';
                    } else {
                        $status = 0;
                        $message = 'Gagal menyimpan data, ulangi beberapa saat lagi.';
                    }
                } else {
                    // message ketika update
                    $status = 1;
                    $message = 'Berhasil mengupdate data.';
                }
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        } else {
            show_404();
        }
    }

    function id_agama()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');

            $data = $this->customdb->view_by_id('ms_agama', ['id' => $id], 'row');
            echo json_encode($data);
        } else {
            show_404();
        }
    }

    function hapus_agama()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');

            $hapus = $this->customdb->process_data('ms_agama', ['status' => 0], ['id' => $id]);
            if ($hapus > 0) {
                $status = 1;
                $message = 'Data berhasil dihapus';
            } else {
                $status = 0;
                $message = 'Gagal menghapus data';
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        } else {
            show_404();
        }
    }

    function status_nikah()
    {
        $this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css')
        );

        $footer = array(
            'script' => datatable('js'),
            'app' => 'master/status_nikah.js'
        );

        $this->load->view('template/header', $header);
        $this->load->view('master/status_nikah');
        $this->load->view('template/footer', $footer);
    }

    function data_status_nikah()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $draw = $this->input->get('draw');
            $start = $this->input->get('start');
            $length = $this->input->get('length');
            $search = $this->input->get('search');
            $order = $this->input->get('order');

            $json = $this->Master_Model->json_status_nikah($draw, $start, $length, $search['value'], $order[0]['column'], $order[0]['dir']);

            echo json_encode($json);
        } else {
            show_404();
        }
    }

    function add_status_nikah()
    {
        // checking auth
        $this->Auth_Model->is_login();
        // check request ajax jika bukan dari ajax show_404();
        if ($this->input->is_ajax_request()) {
            // declare all input
            $id = $this->input->post('id');
            $status_nikah = $this->input->post('status_nikah');

            // check mandatory field
            if ($status_nikah == '') {
                $message = '';
                $status = 0;

                // jika field agama kosong
                $message .= ($status_nikah == '') ? 'Field Status Nikah wajib diisi. <br>' : '';
            } else {
                // buat data utk disimpan
                $data = array(
                    'status_nikah' => $status_nikah
                );

                // if id exists or not
                $condition = ($id != '') ? ['id' => $id] : [];
                // simpan action
                $simpan = $this->customdb->process_data('ms_status_nikah', $data, $condition);
                // message ketika insert
                if ($id == '') {
                    if ($simpan > 0) {
                        $status = 1;
                        $message = 'Berhasil menyimpan data.';
                    } else {
                        $status = 0;
                        $message = 'Gagal menyimpan data, ulangi beberapa saat lagi.';
                    }
                } else {
                    // message ketika update
                    $status = 1;
                    $message = 'Berhasil mengupdate data.';
                }
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        } else {
            show_404();
        }
    }

    function id_status_nikah()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');

            $data = $this->customdb->view_by_id('ms_status_nikah', ['id' => $id], 'row');
            echo json_encode($data);
        } else {
            show_404();
        }
    }

    function hapus_status_nikah()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');

            $hapus = $this->customdb->process_data('ms_status_nikah', ['status' => 0], ['id' => $id]);
            if ($hapus > 0) {
                $status = 1;
                $message = 'Data berhasil dihapus';
            } else {
                $status = 0;
                $message = 'Gagal menghapus data';
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        } else {
            show_404();
        }
    }

    function pendidikan()
    {
        $this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css')
        );

        $footer = array(
            'script' => datatable('js'),
            'app' => 'master/pendidikan.js'
        );

        $this->load->view('template/header', $header);
        $this->load->view('master/pendidikan');
        $this->load->view('template/footer', $footer);
    }

    function data_pendidikan()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $draw = $this->input->get('draw');
            $start = $this->input->get('start');
            $length = $this->input->get('length');
            $search = $this->input->get('search');
            $order = $this->input->get('order');

            $json = $this->Master_Model->json_pendidikan($draw, $start, $length, $search['value'], $order[0]['column'], $order[0]['dir']);

            echo json_encode($json);
        } else {
            show_404();
        }
    }

    function add_pendidikan()
    {
        // checking auth
        $this->Auth_Model->is_login();
        // check request ajax jika bukan dari ajax show_404();
        if ($this->input->is_ajax_request()) {
            // declare all input
            $id = $this->input->post('id');
            $pendidikan = $this->input->post('pendidikan');
            $keterangan = $this->input->post('keterangan');

            // check mandatory field
            if ($pendidikan == '') {
                $message = '';
                $status = 0;

                // jika field pendidikan kosong
                $message .= ($pendidikan == '') ? 'Field Pendidikan wajib diisi. <br>' : '';
            } else {
                // buat data utk disimpan
                $data = array(
                    'pendidikan' => $pendidikan,
                    'keterangan' => $keterangan
                );

                // if id exists or not
                $condition = ($id != '') ? ['id' => $id] : [];
                // simpan action
                $simpan = $this->customdb->process_data('ms_pendidikan', $data, $condition);
                // message ketika insert
                if ($id == '') {
                    if ($simpan > 0) {
                        $status = 1;
                        $message = 'Berhasil menyimpan data.';
                    } else {
                        $status = 0;
                        $message = 'Gagal menyimpan data, ulangi beberapa saat lagi.';
                    }
                } else {
                    // message ketika update
                    $status = 1;
                    $message = 'Berhasil mengupdate data.';
                }
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        } else {
            show_404();
        }
    }

    function id_pendidikan()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');

            $data = $this->customdb->view_by_id('ms_pendidikan', ['id' => $id], 'row');
            echo json_encode($data);
        } else {
            show_404();
        }
    }

    function hapus_pendidikan()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');

            $hapus = $this->customdb->process_data('ms_pendidikan', ['status' => 0], ['id' => $id]);
            if ($hapus > 0) {
                $status = 1;
                $message = 'Data berhasil dihapus';
            } else {
                $status = 0;
                $message = 'Gagal menghapus data';
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        } else {
            show_404();
        }
    }

    function divisi()
    {
        $this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css')
        );

        $footer = array(
            'script' => datatable('js'),
            'app' => 'master/divisi.js'
        );

        $company = $this->Main_Model->list_company();

        $data = array(
            'company' => $company
        );

        $this->load->view('template/header', $header);
        $this->load->view('master/divisi', $data);
        $this->load->view('template/footer', $footer);
    }

    function data_divisi()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $draw = $this->input->get('draw');
            $start = $this->input->get('start');
            $length = $this->input->get('length');
            $search = $this->input->get('search');
            $order = $this->input->get('order');

            $json = $this->Master_Model->json_divisi($draw, $start, $length, $search['value'], $order[0]['column'], $order[0]['dir']);

            echo json_encode($json);
        } else {
            show_404();
        }
    }

    function add_divisi()
    {
        // checking auth
        $this->Auth_Model->is_login();
        // check request ajax jika bukan dari ajax show_404();
        if ($this->input->is_ajax_request()) {
            // declare all input
            $id = $this->input->post('id');
            $company = $this->input->post('company');
            $divisi = $this->input->post('divisi');
            $keterangan = $this->input->post('keterangan');

            // check mandatory field
            if ($company == '' || $divisi == '') {
                $message = '';
                $status = 0;

                // jika field company kosong
                $message .= ($company == '') ? 'Field Perusahaan wajib diisi. <br>' : '';
                $message .= ($divisi == '') ? 'Field Divisi wajib diisi. <br>' : '';
            } else {
                // user create
                $time = ($id != '') ? 'update_at' : 'insert_at';
                $user = ($id != '') ? 'user_update' : 'user_insert';

                // buat data utk disimpan
                $data = array(
                    'id_company' => $company,
                    'divisi' => $divisi,
                    'keterangan' => $keterangan,
                    $time => now(),
                    $user => username()
                );

                // if id exists or not
                $condition = ($id != '') ? ['id' => $id] : [];
                // simpan action
                $simpan = $this->customdb->process_data('ms_divisi', $data, $condition);
                // message ketika insert
                if ($id == '') {
                    if ($simpan > 0) {
                        $status = 1;
                        $message = 'Berhasil menyimpan data.';
                    } else {
                        $status = 0;
                        $message = 'Gagal menyimpan data, ulangi beberapa saat lagi.';
                    }
                } else {
                    // message ketika update
                    $status = 1;
                    $message = 'Berhasil mengupdate data.';
                }
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        } else {
            show_404();
        }
    }

    function id_divisi()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');

            $data = $this->customdb->view_by_id('ms_divisi', ['id' => $id], 'row');
            echo json_encode($data);
        } else {
            show_404();
        }
    }

    function hapus_divisi()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');

            $hapus = $this->customdb->process_data(
                'ms_divisi',
                ['status' => 0, 'update_at' => now(), 'user_update' => username()],
                ['id' => $id]
            );

            if ($hapus > 0) {
                $status = 1;
                $message = 'Data berhasil dihapus';
            } else {
                $status = 0;
                $message = 'Gagal menghapus data';
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        } else {
            show_404();
        }
    }

    function jabatan()
    {
        $this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css')
        );

        $footer = array(
            'script' => datatable('js'),
            'app' => 'master/jabatan.js'
        );

        $company = $this->Main_Model->list_company();

        $data = array(
            'company' => $company
        );

        $this->load->view('template/header', $header);
        $this->load->view('master/jabatan', $data);
        $this->load->view('template/footer', $footer);
    }

    function data_jabatan()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $draw = $this->input->get('draw');
            $start = $this->input->get('start');
            $length = $this->input->get('length');
            $search = $this->input->get('search');
            $order = $this->input->get('order');

            $json = $this->Master_Model->json_jabatan($draw, $start, $length, $search['value'], $order[0]['column'], $order[0]['dir']);

            echo json_encode($json);
        } else {
            show_404();
        }
    }

    function add_jabatan()
    {
        // checking auth
        $this->Auth_Model->is_login();
        // check request ajax jika bukan dari ajax show_404();
        if ($this->input->is_ajax_request()) {
            // declare all input
            $id = $this->input->post('id');
            $company = $this->input->post('company');
            $jabatan = $this->input->post('jabatan');
            $keterangan = $this->input->post('keterangan');

            // check mandatory field
            if ($company == '' || $jabatan == '') {
                $message = '';
                $status = 0;

                // jika field company kosong
                $message .= ($company == '') ? 'Field Perusahaan wajib diisi. <br>' : '';
                $message .= ($jabatan == '') ? 'Field Jabatan wajib diisi. <br>' : '';
            } else {
                // user create
                $time = ($id != '') ? 'update_at' : 'insert_at';
                $user = ($id != '') ? 'user_update' : 'user_insert';

                // buat data utk disimpan
                $data = array(
                    'id_company' => $company,
                    'jabatan' => $jabatan,
                    'keterangan' => $keterangan,
                    $time => now(),
                    $user => username()
                );

                // if id exists or not
                $condition = ($id != '') ? ['id' => $id] : [];
                // simpan action
                $simpan = $this->customdb->process_data('ms_jabatan', $data, $condition);
                // message ketika insert
                if ($id == '') {
                    if ($simpan > 0) {
                        $status = 1;
                        $message = 'Berhasil menyimpan data.';
                    } else {
                        $status = 0;
                        $message = 'Gagal menyimpan data, ulangi beberapa saat lagi.';
                    }
                } else {
                    // message ketika update
                    $status = 1;
                    $message = 'Berhasil mengupdate data.';
                }
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        } else {
            show_404();
        }
    }

    function id_jabatan()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');

            $data = $this->customdb->view_by_id('ms_jabatan', ['id' => $id], 'row');
            echo json_encode($data);
        } else {
            show_404();
        }
    }

    function hapus_jabatan()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');

            $hapus = $this->customdb->process_data(
                'ms_jabatan',
                ['status' => 0, 'update_at' => now(), 'user_update' => username()],
                ['id' => $id]
            );

            if ($hapus > 0) {
                $status = 1;
                $message = 'Data berhasil dihapus';
            } else {
                $status = 0;
                $message = 'Gagal menghapus data';
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        } else {
            show_404();
        }
    }
}

/* End of file Master.php */
/* Location: ./application/controllers/Master.php */
