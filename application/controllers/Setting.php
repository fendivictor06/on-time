<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Setting extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        $this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css')
        );

        $footer = array(
            'script' => datatable('js'),
            'app' => 'setting/setting.js'
        );

        $this->load->view('template/header', $header);
        $this->load->view('setting/setting');
        $this->load->view('template/footer', $footer);
    }

    function load_setting()
    {
        $this->Auth_Model->is_login();
        $id_company = company_id();

        $data = $this->customdb->view_by_id('tb_setting', ['id_company' => $id_company], 'row');
        $imei = isset($data->imei) ? $data->imei : 0;
        $macaddress = isset($data->macaddress) ? $data->macaddress : 0;
        $apv_cuti = isset($data->apv_cuti) ? $data->apv_cuti : '';
        $apv_ijin = isset($data->apv_ijin) ? $data->apv_ijin : '';

        $result = array(
            'imei' => $imei,
            'macaddress' => $macaddress,
            'apv_cuti' => $apv_cuti,
            'apv_ijin' => $apv_ijin
        );

        echo json_encode($result);
    }

    function simpan_setting()
    {
        $this->Auth_Model->is_login();
        $imei = $this->input->post('imei');
        $macaddress = $this->input->post('macaddress');
        $cuti = $this->input->post('cuti');
        $ijin = $this->input->post('ijin');
        $id_company = company_id();
        $user_insert = username();

        $data = [];
        if ($imei == 1) {
            $data['imei'] = $imei;
        } else {
            $data['imei'] = 0;
        }

        if ($macaddress == 1) {
            $data['macaddress'] = $macaddress;
        } else {
            $data['macaddress'] = 0;
        }

        if ($cuti != '') {
            $data['apv_cuti'] = $cuti;
        }

        if ($ijin != '') {
            $data['apv_ijin'] = $ijin;
        }

        $data['id_company'] = $id_company;

        $exists = $this->customdb->view_by_id('tb_setting', ['id_company' => $id_company], 'row');
        if (empty($exists)) {
            $data['user_insert'] = $user_insert;

            $simpan = $this->customdb->process_data('tb_setting', $data);
            if ($simpan > 0) {
                $status = 1;
                $message = 'Berhasil menyimpan data';
            } else {
                $status = 0;
                $message = 'Gagal menyimpan data';
            }
        } else {
            $data['user_update'] = $user_insert;
            $data['update_at'] = now();

            $simpan = $this->customdb->process_data('tb_setting', $data, ['id_company' => $id_company]);
            $status = 1;
            $message = 'Berhasil menyimpan data';
        }

        $result = array(
            'status' => $status,
            'message' => $message
        );

        echo json_encode($result);
    }
}

/* End of file Setting.php */
/* Location: ./application/controllers/Setting.php */
