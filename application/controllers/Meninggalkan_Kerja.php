<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Meninggalkan_Kerja extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Meninggalkan_Kerja_Model');
    }

    function index()
    {
        $this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css')
        );

        $company = $this->Main_Model->list_company();
        $data = array(
            'company' => $company
        );

        $footer = array(
            'script' => datatable('js'),
            'app' => 'meninggalkan_kerja/meninggalkan_kerja.js'
        );

        $this->load->view('template/header', $header);
        $this->load->view('meninggalkan_kerja/meninggalkan_kerja', $data);
        $this->load->view('template/footer', $footer);
    }

    function data_keluar()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $draw = $this->input->get('draw');
            $start = $this->input->get('start');
            $length = $this->input->get('length');
            $search = $this->input->get('search');
            $order = $this->input->get('order');

            $datestart = $this->input->get('datestart');
            $dateend = $this->input->get('dateend');

            $json = $this->Meninggalkan_Kerja_Model->json_keluar($draw, $start, $length, $search['value'], $order[0]['column'], $order[0]['dir'], $datestart, $dateend);

            echo json_encode($json);
        } else {
            show_404();
        }
    }
}

/* End of file Meninggalkan_Kerja.php */
/* Location: ./application/controllers/Meninggalkan_Kerja.php */
