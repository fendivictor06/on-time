<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Akses_Point extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Master_Model');
    }

    function index()
    {
        $this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css')
        );

        $company = $this->Main_Model->list_company();

        $data = array(
            'company' => $company
        );

        $footer = array(
            'script' => datatable('js'),
            'app' => 'master/akses_point.js'
        );

        $this->load->view('template/header', $header);
        $this->load->view('master/akses_point', $data);
        $this->load->view('template/footer', $footer);
    }

    function data_akses_point()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $draw = $this->input->get('draw');
            $start = $this->input->get('start');
            $length = $this->input->get('length');
            $search = $this->input->get('search');
            $order = $this->input->get('order');

            $json = $this->Master_Model->json_akses_point($draw, $start, $length, $search['value'], $order[0]['column'], $order[0]['dir']);

            echo json_encode($json);
        } else {
            show_404();
        }
    }

    function add_akses_point()
    {
        // checking auth
        $this->Auth_Model->is_login();
        // check request ajax jika bukan dari ajax show_404();
        if ($this->input->is_ajax_request()) {
            // declare all input
            $id = $this->input->post('id');
            $company = $this->input->post('company');
            $ssid = $this->input->post('ssid');
            $macaddress = $this->input->post('macaddress');
            $ippublic = $this->input->post('ippublic');

            // check mandatory field
            if ($company == '' || $ssid == '' || $macaddress == '' || $ippublic == '') {
                $message = '';
                $status = 0;

                // jika field company kosong
                $message .= ($company == '') ? 'Field Perusahaan wajib diisi. <br>' : '';
                $message .= ($ssid == '') ? 'Field SSID wajib diisi. <br>' : '';
                $message .= ($macaddress == '') ? 'Field Mac-Address wajib diisi. <br>' : '';
                $message .= ($ippublic == '') ? 'Field IP-Public wajib diisi. <br>' : '';
            } else {
                // user create
                $time = ($id != '') ? 'update_at' : 'insert_at';
                $user = ($id != '') ? 'user_update' : 'user_insert';

                // buat data utk disimpan
                $data = array(
                    'id_company' => $company,
                    'ssid' => $ssid,
                    'macaddress' => $macaddress,
                    'ippublic' => $ippublic,
                    $time => now(),
                    $user => username()
                );

                // if id exists or not
                $condition = ($id != '') ? ['id' => $id] : [];
                // simpan action
                $simpan = $this->customdb->process_data('ms_akses_point', $data, $condition);
                // message ketika insert
                if ($id == '') {
                    if ($simpan > 0) {
                        $status = 1;
                        $message = 'Berhasil menyimpan data.';
                    } else {
                        $status = 0;
                        $message = 'Gagal menyimpan data, ulangi beberapa saat lagi.';
                    }
                } else {
                    // message ketika update
                    $status = 1;
                    $message = 'Berhasil mengupdate data.';
                }
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        } else {
            show_404();
        }
    }

    function id_akses_point()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');

            $data = $this->customdb->view_by_id('ms_akses_point', ['id' => $id], 'row');
            echo json_encode($data);
        } else {
            show_404();
        }
    }

    function hapus_akses_point()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');

            $hapus = $this->customdb->process_data(
                'ms_akses_point',
                ['status' => 0, 'update_at' => now(), 'user_update' => username()],
                ['id' => $id]
            );

            if ($hapus > 0) {
                $status = 1;
                $message = 'Data berhasil dihapus';
            } else {
                $status = 0;
                $message = 'Gagal menghapus data';
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        } else {
            show_404();
        }
    }
}

/* End of file Akses_Point.php */
/* Location: ./application/controllers/Akses_Point.php */
