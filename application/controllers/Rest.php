<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Rest extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Rest_Model');
        $this->load->model('Auth_Model');
    }

    function auth()
    {
        $auth = $this->token->auth('POST', false);
        if ($auth == true) {
            $params = get_params();
            $username = isset($params['username']) ? $params['username'] : '';
            $password = isset($params['password']) ? $params['password'] : '';
            $kode_perusahaan = isset($params['kode']) ? $params['kode'] : '';
            $imei = isset($params['imei']) ? $params['imei'] : [];
            $today = date('Y-m-d');

            $response = [];
            if ($username == '' || $password == '' || $kode_perusahaan == '') {
                $status = 404;
                $message = '';

                if ($username == '') {
                    $message .= 'Masukkan Username';
                }
                if ($password == '') {
                    $message .= 'Masukkan Password';
                }
                if ($kode_perusahaan == '') {
                    $message .= 'Masukkan Kode Perusahaan';
                }
            } else {
                $login = $this->Rest_Model->auth($username, $password, $kode_perusahaan);

                # id user
                $id_user = isset($login->id) ? $login->id : '';

                # id company
                $id_company = isset($login->id_company) ? $login->id_company : '';

                # id karyawan
                $id_karyawan = isset($login->id_karyawan) ? $login->id_karyawan : '';

                # profile name
                $profile_name = isset($login->profile_name) ? $login->profile_name : '';

                $ms_company = $this->customdb->view_by_id('ms_company', ['id' => $id_company], 'row');
                $verify = isset($ms_company->verify) ? $ms_company->verify : 0;
                $mulai = isset($ms_company->mulai) ? $ms_company->mulai : '';
                $selesai = isset($ms_company->selesai) ? $ms_company->selesai : '';

                if ($login == false) {
                    $is_valid_kode_perusahaan = $this->Rest_Model->is_valid_kode_perusahaan($username, $kode_perusahaan);
                    if (empty($is_valid_kode_perusahaan)) {
                        $status = 400;
                        $message = 'Username tidak terdaftar di Perusahaan ini';
                    } else {
                        $status = 401;
                        $message = 'Username / password anda salah, silahkan cek kembali.';
                    }
                } else {
                    # do mapping imei for first time login
                    $map = $this->mapping_imei($imei, $id_karyawan, $id_company, $username);

                    # do create token here
                    $create_token = $this->token->create_token($username);

                    # update session
                    $this->Auth_Model->update_session($id_user);

                    # token
                    $token = isset($create_token['token']) ? $create_token['token'] : '';

                    if ($create_token == false) {
                        $status = 500;
                        $message = 'Terjadi kesalahan saat authentikasi';
                    } else {
                        $apv_cuti = $this->customdb->view_by_id('tb_setting', ['id_company' => $id_company, 'apv_cuti' => $id_karyawan], 'row');
                        $apv_ijin = $this->customdb->view_by_id('tb_setting', ['id_company' => $id_company, 'apv_ijin' => $id_karyawan], 'row');

                        $cuti = (!empty($apv_cuti)) ? '1' : '0';
                        $ijin = (!empty($apv_ijin)) ? '1' : '0';

                        # create array session
                        $response = array(
                            'id_user' => $id_user,
                            'username' => $username,
                            'id_company' => $id_company,
                            'id_karyawan' => $id_karyawan,
                            'profile_name' => $profile_name,
                            'token' => $token,
                            'cuti' => $cuti,
                            'ijin' => $ijin
                        );
                        
                        $status = 200;
                        $message = 'Login berhasil';
                    }
                }
            }

            print_json($status, $message, $response);
        }
    }

    function mapping_imei($imei = [], $id_karyawan = '', $id_company = '', $username = '')
    {
        if ($imei) {
            $exists = $this->customdb->view_by_id('tb_imei', ['id_karyawan' => $id_karyawan, 'id_company' => $id_company], 'row');
            if (empty($exists)) {
                for ($i = 0; $i < count($imei); $i++) {
                    $string = isset($imei[$i]) ? $imei[$i] : '';
                    if ($string != '') {
                        $data = array(
                            'id_karyawan' => $id_karyawan,
                            'id_company' => $id_company,
                            'imei' => $string,
                            'user_insert' => $username
                        );

                        $this->customdb->process_data('tb_imei', $data);
                    }
                }
            }
        }
    }

    function verify_kode()
    {
        $auth = $this->token->auth('POST', false);
        if ($auth == true) {
            $params = get_params();
            $kode_perusahaan = isset($params['kode']) ? $params['kode'] : '';

            $response = [];
            if ($kode_perusahaan == '') {
                $status = 404;
                $message = '';

                if ($kode_perusahaan == '') {
                    $message .= 'Masukkan Kode Perusahaan';
                }
            } else {
                $company = $this->customdb->view_by_id('ms_company', ['kode_perusahaan' => $kode_perusahaan], 'row');
                $nama = isset($company->company) ? $company->company : '';
                $alamat = isset($company->alamat) ? $company->alamat : '';
                $id = isset($company->id) ? $company->id : '';

                if ($company) {
                    $response = array(
                        'id' => $id,
                        'nama' => $nama,
                        'alamat' => $alamat
                    );

                    $status = 200;
                    $message = 'Data berhasil ditemukan';
                } else {
                    $status = 404;
                    $message = 'Data tidak ditemukan';
                }
            }

            print_json($status, $message, $response);
        }
    }
}

/* End of file Rest.php */
/* Location: ./application/controllers/Rest.php */
