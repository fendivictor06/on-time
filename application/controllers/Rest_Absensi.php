<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Rest_Absensi extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Rest_Absensi_Model');
    }

    function index()
    {
        $auth = $this->token->auth('POST');
        if ($auth == true) {
            $params = get_params();
            $datestart = filter_params($params, 'datestart');
            $dateend = filter_params($params, 'dateend');

            $data = $this->Rest_Absensi_Model->view_absensi($datestart, $dateend);
            $response = [];
            if ($data) {
                $status = 200;
                $message = 'Berhasil';
            } else {
                $status = 404;
                $message = 'Data tidak ditemukan';
            }

            print_json($status, $message, $data);
        }
    }
}

/* End of file Rest_Absensi.php */
/* Location: ./application/controllers/Rest_Absensi.php */
