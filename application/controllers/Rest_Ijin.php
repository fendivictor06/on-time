<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Rest_Ijin extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Rest_Ijin_Model');
    }

    function index()
    {
        $auth = $this->token->auth('POST');
        if ($auth == true) {
            $params = get_params();
            $start = filter_params($params, 'start');
            $count = filter_params($params, 'count');
            $id = filter_params($params, 'id');

            $data = $this->Rest_Ijin_Model->view_ijin($start, $count, $id);
            if ($data) {
                $status = 200;
                $message = 'Berhasil';
            } else {
                $status = 404;
                $message = 'Data tidak ditemukan';
            }

            print_json($status, $message, $data);
        }
    }

    function add_ijin()
    {
        $auth = $this->token->auth('POST');
        if ($auth == true) {
            $params = get_params();
            $id = filter_params($params, 'id');
            $tgl = filter_params($params, 'tgl');
            $jam = filter_params($params, 'jam');
            $alasan = filter_params($params, 'alasan');

            if ($tgl == '' || $jam == '' || $alasan == '') {
                $status = 404;
                $message = '';

                if ($tgl == '') {
                    $message .= 'Masukkan Tanggal. ';
                }

                if ($jam == '') {
                    $message .= 'Masukkan jam. ';
                }

                if ($alasan == '') {
                    $message .= 'Masukkan Alasan ';
                }
            } else {
                $id_company = id_company();
                $id_karyawan = id_karyawan();

                $condition = ($id != '') ? ['id' => $id] : [];
                $time = ($id != '') ? 'update_at' : 'insert_at';
                $user = ($id != '') ? 'user_update' : 'user_insert';

                $data = array(
                    'id_company' => $id_company,
                    'id_karyawan' => $id_karyawan,
                    'tgl' => $tgl,
                    'jam' => $jam,
                    'alasan' => $alasan,
                    $time => now(),
                    $user => user_id()
                );

                $simpan = $this->customdb->process_data('tb_ijin', $data, $condition);
                if ($simpan > 0) {
                    $karyawan = $this->customdb->view_by_id('ms_karyawan', ['id' => $id_karyawan], 'row');
                    $nama = isset($karyawan->nama) ? $karyawan->nama : '';

                    $title = 'Pengajuan Ijin';
                    $text = $nama.' Telah mengajukan Ijin.';
                    $fcm = apv_ijin();

                    $notif = $this->firebasenotif->notif($title, $text, $fcm, 'ACT_MAIN', [['jenis' => 'ijin']]);
                    # log notif
                    log_api([$title, $text, $fcm], $notif, '', []);

                    $status = 200;
                    $message = 'Data berhasil disimpan';
                } else {
                    $status = 500;
                    $message = 'Gagal menyimpan data';
                }
            }

            print_json($status, $message, []);
            log_api($params, $status, $message, []);
        }
    }

    function view_approval()
    {
        $auth = $this->token->auth('POST');
        if ($auth == true) {
            $params = get_params();
            $start = filter_params($params, 'start');
            $count = filter_params($params, 'count');
            $id = filter_params($params, 'id');

            $data = $this->Rest_Ijin_Model->view_approval($start, $count, $id);
            if ($data) {
                $status = 200;
                $message = 'Berhasil';
            } else {
                $status = 404;
                $message = 'Data tidak ditemukan';
            }

            print_json($status, $message, $data);
        }
    }

    function approve_ijin()
    {
        $auth = $this->token->auth('POST');
        if ($auth == true) {
            $params = get_params();
            $id = filter_params($params, 'id');
            $apv = filter_params($params, 'status');
            # status = 2 setuju
            # status = 3 tolak

            $simpan = $this->Rest_Ijin_Model->approve_ijin($id, $apv);
            if ($simpan > 0) {
                $status = 200;
                $message = 'Ijin berhasil ';

                if ($apv == 2) {
                    $message .= 'disetujui';
                } else {
                    $message .= 'ditolak';
                }
            } else {
                $status = 500;
                $message = 'Ijin gagal ';

                if ($apv == 2) {
                    $message .= 'disetujui';
                } else {
                    $message .= 'ditolak';
                }
            }

            print_json($status, $message, []);
        }
    }
}

/* End of file Rest_Ijin.php */
/* Location: ./application/controllers/Rest_Ijin.php */
