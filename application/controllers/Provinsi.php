<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Provinsi extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('Provinsi_Model');
	}

	function index()
	{
		$this->Auth_Model->is_login();
		$header = array(
			'styles' => datatable('css')
		);

		$footer = array(
			'script' => datatable('js'),
			'app' => 'provinsi/provinsi.js'
		);

		$this->load->view('template/header', $header);
		$this->load->view('provinsi/provinsi');
		$this->load->view('template/footer', $footer);
	}

	function data_provinsi()
	{
		$this->Auth_Model->is_login();
		if ($this->input->is_ajax_request()) {
			$draw = $this->input->get('draw');
			$start = $this->input->get('start');
			$length = $this->input->get('length');
			$search = $this->input->get('search');
			$order = $this->input->get('order');

			$json = $this->Provinsi_Model->json_provinsi($draw, $start, $length, $search['value'], $order[0]['column'], $order[0]['dir']);

			echo json_encode($json);
		} else {
			show_404();
		}
	}

	function add_provinsi()
	{
		// checking auth
		$this->Auth_Model->is_login();
		// check request ajax jika bukan dari ajax show_404();
		if ($this->input->is_ajax_request()) {
			// declare all input
			$id = $this->input->post('id');
			$provinsi = $this->input->post('provinsi');

			// check mandatory field
			if ($provinsi == '') {
				$message = '';
				$status = 0;	

				// jika field provinsi kosong
				$message .= ($provinsi == '') ? 'Field Provinsi wajib diisi. <br>' : '';
			} else {
				// buat data utk disimpan
				$data = array(
					'provinsi' => $provinsi
				);

				// if id exists or not
				$condition = ($id != '') ? ['id' => $id] : [];
				// simpan action
				$simpan = $this->customdb->process_data('ms_provinsi', $data, $condition);
				// message ketika insert
				if ($id == '') {
					if ($simpan > 0) {
						$status = 1;
						$message = 'Berhasil menyimpan data.';
					} else {
						$status = 0;
						$message = 'Gagal menyimpan data, ulangi beberapa saat lagi.';
					}
				} else {
					// message ketika update
					$status = 1;
					$message = 'Berhasil mengupdate data.';
				}
			}

			$result = array(
				'status' => $status,
				'message' => $message
			);

			echo json_encode($result);
		} else {
			show_404();
		}
	}

	function id_provinsi()
	{
		$this->Auth_Model->is_login();
		if ($this->input->is_ajax_request()) {
			$id = $this->input->post('id');

			$data = $this->customdb->view_by_id('ms_provinsi', ['id' => $id], 'row');
			echo json_encode($data);
		} else {
			show_404();
		}
	}

	function hapus_provinsi()
	{
		$this->Auth_Model->is_login();
		if ($this->input->is_ajax_request()) {
			$id = $this->input->post('id');

			$hapus = $this->customdb->process_data('ms_provinsi', ['status' => 0], ['id' => $id]);
			if ($hapus > 0) {
				$status = 1;
				$message = 'Data berhasil dihapus';
			} else {
				$status = 0;
				$message = 'Gagal menghapus data';
			}

			$result = array(
				'status' => $status,
				'message' => $message
			);

			echo json_encode($result);
		} else {
			show_404();
		}
	}

	function kota()
	{
		$this->Auth_Model->is_login();
		$header = array(
			'styles' => datatable('css')
		);

		$footer = array(
			'script' => datatable('js'),
			'app' => 'provinsi/kota.js'
		);

		$ms_provinsi = $this->customdb->view_by_id('ms_provinsi', ['status' => 1], 'result');
		$provinsi = [];
		if ($ms_provinsi) {
			foreach($ms_provinsi as $row) {
				$provinsi[$row->id] = $row->provinsi;
			}
		}

		$data = array(
			'provinsi' => $provinsi
		);

		$this->load->view('template/header', $header);
		$this->load->view('provinsi/kota', $data);
		$this->load->view('template/footer', $footer);
	}

	function data_kota()
	{
		$this->Auth_Model->is_login();
		if ($this->input->is_ajax_request()) {
			$draw = $this->input->get('draw');
			$start = $this->input->get('start');
			$length = $this->input->get('length');
			$search = $this->input->get('search');
			$order = $this->input->get('order');

			$json = $this->Provinsi_Model->json_kota($draw, $start, $length, $search['value'], $order[0]['column'], $order[0]['dir']);

			echo json_encode($json);
		} else {
			show_404();
		}
	}

	function add_kota()
	{
		// checking auth
		$this->Auth_Model->is_login();
		// check request ajax jika bukan dari ajax show_404();
		if ($this->input->is_ajax_request()) {
			// declare all input
			$id = $this->input->post('id');
			$provinsi = $this->input->post('provinsi');
			$kota = $this->input->post('kota');

			// check mandatory field
			if ($provinsi == '' || $kota == '') {
				$message = '';
				$status = 0;	

				// jika field provinsi kosong
				$message .= ($provinsi == '') ? 'Field Provinsi wajib diisi. <br>' : '';
				$message .= ($kota == '') ? 'Field Kota wajib diisi. <br>' : '';
			} else {
				// buat data utk disimpan
				$data = array(
					'id_provinsi' => $provinsi,
					'kota' => $kota
				);

				// if id exists or not
				$condition = ($id != '') ? ['id' => $id] : [];
				// simpan action
				$simpan = $this->customdb->process_data('ms_kota', $data, $condition);
				// message ketika insert
				if ($id == '') {
					if ($simpan > 0) {
						$status = 1;
						$message = 'Berhasil menyimpan data.';
					} else {
						$status = 0;
						$message = 'Gagal menyimpan data, ulangi beberapa saat lagi.';
					}
				} else {
					// message ketika update
					$status = 1;
					$message = 'Berhasil mengupdate data.';
				}
			}

			$result = array(
				'status' => $status,
				'message' => $message
			);

			echo json_encode($result);
		} else {
			show_404();
		}
	}

	function id_kota()
	{
		$this->Auth_Model->is_login();
		if ($this->input->is_ajax_request()) {
			$id = $this->input->post('id');

			$data = $this->customdb->view_by_id('ms_kota', ['id' => $id], 'row');
			echo json_encode($data);
		} else {
			show_404();
		}
	}

	function hapus_kota()
	{
		$this->Auth_Model->is_login();
		if ($this->input->is_ajax_request()) {
			$id = $this->input->post('id');

			$hapus = $this->customdb->process_data('ms_kota', ['status' => 0], ['id' => $id]);
			if ($hapus > 0) {
				$status = 1;
				$message = 'Data berhasil dihapus';
			} else {
				$status = 0;
				$message = 'Gagal menghapus data';
			}

			$result = array(
				'status' => $status,
				'message' => $message
			);

			echo json_encode($result);
		} else {
			show_404();
		}
	}
}

/* End of file Provinsi.php */
/* Location: ./application/controllers/Provinsi.php */ ?>