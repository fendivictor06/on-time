<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Karyawan extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Karyawan_Model');
    }

    function index()
    {
        $this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css')
        );

        $footer = array(
            'script' => datatable('js'),
            'app' => 'karyawan/karyawan.js'
        );

        $this->load->view('template/header', $header);
        $this->load->view('karyawan/karyawan');
        $this->load->view('template/footer', $footer);
    }

    function data_karyawan()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $draw = $this->input->get('draw');
            $start = $this->input->get('start');
            $length = $this->input->get('length');
            $search = $this->input->get('search');
            $order = $this->input->get('order');

            $json = $this->Karyawan_Model->json_karyawan($draw, $start, $length, $search['value'], $order[0]['column'], $order[0]['dir']);

            echo json_encode($json);
        } else {
            show_404();
        }
    }

    function form_karyawan()
    {
        $this->Auth_Model->is_login();
        $header = array(
            'title' => 'Form Karyawan'
        );


        $company = $this->Main_Model->list_company();

        # master jenis kelamin
        $ms_jenkel = $this->customdb->view_by_id('ms_jenis_kelamin', ['status' => 1], 'result');
        $jenkel = [];
        if ($ms_jenkel) {
            foreach ($ms_jenkel as $row) {
                $jenkel[$row->id] = $row->keterangan;
            }
        }

        # master status nikah
        $ms_status = $this->customdb->view_by_id('ms_status_nikah', ['status' => 1], 'result');
        $status_nikah = [];
        if ($ms_status) {
            foreach ($ms_status as $row) {
                $status_nikah[$row->id] = $row->status_nikah;
            }
        }

        # master provinsi
        $ms_provinsi = $this->customdb->view_by_id('ms_provinsi', ['status' => 1], 'result');
        $provinsi = [];
        if ($ms_provinsi) {
            foreach ($ms_provinsi as $row) {
                $provinsi[$row->id] = $row->provinsi;
            }
        }

        # agama
        $ms_agama = $this->customdb->view_by_id('ms_agama', ['status' => 1], 'result');
        $agama = [];
        if ($ms_agama) {
            foreach ($ms_agama as $row) {
                $agama[$row->id] = $row->agama;
            }
        }

        # golongan darah
        $ms_golongan_darah = $this->customdb->view_by_id('ms_golongan_darah', ['status' => 1], 'result');
        $golongan_darah = [];
        if ($ms_golongan_darah) {
            foreach ($ms_golongan_darah as $row) {
                $golongan_darah[$row->id] = $row->golongan_darah;
            }
        }

        # pendidikan
        $ms_pendidikan = $this->customdb->view_by_id('ms_pendidikan', ['status' => 1], 'result');
        $pendidikan = [];
        if ($ms_pendidikan) {
            foreach ($ms_pendidikan as $row) {
                $pendidikan[$row->id] = $row->pendidikan;
            }
        }

        $id_company = company_id();
        $ms_company = $this->customdb->view_by_id('ms_company', ['id' => $id_company], 'row');
        $flag_gaji = isset($ms_company->flag_gaji) ? $ms_company->flag_gaji : 0;

        $data = array(
            'company' => $company,
            'jenkel' => $jenkel,
            'status_nikah' => $status_nikah,
            'provinsi' => $provinsi,
            'agama' => $agama,
            'golongan_darah' => $golongan_darah,
            'pendidikan' => $pendidikan,
            'flag_gaji' => $flag_gaji
        );

        $footer = array(
            'app' => 'karyawan/form_karyawan.js'
        );

        $this->load->view('template/header', $header);
        $this->load->view('karyawan/form_karyawan', $data);
        $this->load->view('template/footer', $footer);
    }

    function form_edit($id = '')
    {
        if ($id) {
            $this->Auth_Model->is_login();
            $header = array(
                'title' => 'Form Karyawan'
            );

            $company = $this->Main_Model->list_company();

            # master jenis kelamin
            $ms_jenkel = $this->customdb->view_by_id('ms_jenis_kelamin', ['status' => 1], 'result');
            $jenkel = [];
            if ($ms_jenkel) {
                foreach ($ms_jenkel as $row) {
                    $jenkel[$row->id] = $row->keterangan;
                }
            }

            # master status nikah
            $ms_status = $this->customdb->view_by_id('ms_status_nikah', ['status' => 1], 'result');
            $status_nikah = [];
            if ($ms_status) {
                foreach ($ms_status as $row) {
                    $status_nikah[$row->id] = $row->status_nikah;
                }
            }

            # master provinsi
            $ms_provinsi = $this->customdb->view_by_id('ms_provinsi', ['status' => 1], 'result');
            $provinsi = [];
            if ($ms_provinsi) {
                foreach ($ms_provinsi as $row) {
                    $provinsi[$row->id] = $row->provinsi;
                }
            }

            # agama
            $ms_agama = $this->customdb->view_by_id('ms_agama', ['status' => 1], 'result');
            $agama = [];
            if ($ms_agama) {
                foreach ($ms_agama as $row) {
                    $agama[$row->id] = $row->agama;
                }
            }

            # golongan darah
            $ms_golongan_darah = $this->customdb->view_by_id('ms_golongan_darah', ['status' => 1], 'result');
            $golongan_darah = [];
            if ($ms_golongan_darah) {
                foreach ($ms_golongan_darah as $row) {
                    $golongan_darah[$row->id] = $row->golongan_darah;
                }
            }

            # pendidikan
            $ms_pendidikan = $this->customdb->view_by_id('ms_pendidikan', ['status' => 1], 'result');
            $pendidikan = [];
            if ($ms_pendidikan) {
                foreach ($ms_pendidikan as $row) {
                    $pendidikan[$row->id] = $row->pendidikan;
                }
            }

            $id_company = company_id();
            $level = level_user();

            $where_edit = ['id' => $id];

            if ($level != '1') {
                $where_edit['id_company'] = $id_company;
                $where_posisi['id_company'] = $id_company;
            }

            $edit = $this->customdb->view_by_id(
                'ms_karyawan',
                $where_edit,
                'row'
            );

            $user = $this->customdb->view_by_id(
                'tb_user',
                ['id_karyawan' => $id],
                'row'
            );

            $ms_company = $this->customdb->view_by_id('ms_company', ['id' => $id_company], 'row');
            $flag_gaji = isset($ms_company->flag_gaji) ? $ms_company->flag_gaji : 0;

            $username = isset($user->username) ? $user->username : '';

            if ($edit) {
                $data = array(
                    'company' => $company,
                    'jenkel' => $jenkel,
                    'status_nikah' => $status_nikah,
                    'provinsi' => $provinsi,
                    'agama' => $agama,
                    'golongan_darah' => $golongan_darah,
                    'pendidikan' => $pendidikan,
                    'username' => $username,
                    'edit' => $edit,
                    'flag_gaji' => $flag_gaji
                );

                $id_kota = isset($edit->kota) ? $edit->kota : '';
                $id_jabatan = isset($edit->id_jabatan) ? $edit->id_jabatan : '';
                $id_divisi = isset($edit->id_divisi) ? $edit->id_divisi : '';

                $footer = array(
                    'script' => '<script> 
                                    var id = "'.$id.'"; 
                                    var kota_id = "'.$id_kota.'";  
                                    var jabatan_id = "'.$id_jabatan.'";
                                    var divisi_id = "'.$id_divisi.'";
                                </script>',
                    'app' => 'karyawan/form_karyawan_edit.js'
                );

                $this->load->view('template/header', $header);
                $this->load->view('karyawan/form_karyawan', $data);
                $this->load->view('template/footer', $footer);
            } else {
                show_404();
            }
        } else {
            show_404();
        }
    }

    function kota($id_provinsi = '')
    {
        $kota = [];
        if ($this->input->is_ajax_request()) {
            $search = $this->input->get('q');
            $ms_kota = $this->Karyawan_Model->view_kota($search, $id_provinsi);

            if ($ms_kota) {
                foreach ($ms_kota as $row) {
                    $kota[] = array(
                        'id' => $row->id,
                        'text' => $row->kota
                    );
                }
            }
        }

        echo json_encode($kota);
    }

    function jabatan($id_company = '')
    {
        $jabatan = [];
        if ($this->input->is_ajax_request()) {
            $search = $this->input->get('q');
            $ms_jabatan = $this->Karyawan_Model->view_jabatan($search, $id_company);

            if ($ms_jabatan) {
                foreach ($ms_jabatan as $row) {
                    $jabatan[] = array(
                        'id' => $row->id,
                        'text' => $row->jabatan
                    );
                }
            }
        }

        echo json_encode($jabatan);
    }

    function divisi($id_company = '')
    {
        $divisi = [];
        if ($this->input->is_ajax_request()) {
            $search = $this->input->get('q');
            $ms_divisi = $this->Karyawan_Model->view_divisi($search, $id_company);

            if ($ms_divisi) {
                foreach ($ms_divisi as $row) {
                    $divisi[] = array(
                        'id' => $row->id,
                        'text' => $row->divisi
                    );
                }
            }
        }

        echo json_encode($divisi);
    }

    function add_karyawan()
    {
        $this->Auth_Model->is_login();
        $id = $this->input->post('id');
        $company = $this->input->post('company');
        $nik = $this->input->post('nik');
        $nama = $this->input->post('nama');
        $ktp = $this->input->post('ktp');
        $jenkel = $this->input->post('jenkel');
        $tempat_lahir = $this->input->post('tempat_lahir');
        $tgl_lahir = $this->input->post('tgl_lahir');
        $tgl_masuk = $this->input->post('tgl_masuk');
        $status_nikah = $this->input->post('status_nikah');
        $agama = $this->input->post('agama');
        $golongan_darah = $this->input->post('golongan_darah');
        $alamat = $this->input->post('alamat');
        $provinsi = $this->input->post('provinsi');
        $kota = $this->input->post('kota');
        $telp = $this->input->post('telp');
        $email = $this->input->post('email');
        $pendidikan = $this->input->post('pendidikan');
        $institusi = $this->input->post('institusi');
        $jurusan = $this->input->post('jurusan');
        $tahun_lulus = $this->input->post('tahun_lulus');

        $divisi = $this->input->post('divisi');
        $jabatan = $this->input->post('jabatan');
        $posisi = $this->input->post('posisi');
        $keterangan = $this->input->post('keterangan');
        $salary = $this->input->post('salary');

        $username = $this->input->post('username');

        $ms_company = $this->customdb->view_by_id('ms_company', ['id' => $company], 'row');
        $kode_perusahaan = isset($ms_company->kode_perusahaan) ? $ms_company->kode_perusahaan : '';

        # foto
        $this->load->library('upload');
        $path = path_foto();
        $file_name = 'FOTO_'.$kode_perusahaan.$nik.date('YmdHisu');
        $config = array(
            'upload_path' => $path,
            'allowed_types' => 'jpg|png',
            'file_name' => $file_name,
            'overwrite' => false,
            'max_size' => 0
        );
        $this->upload->initialize($config);

        if ($nik == '') {
            $status = 0;
            $message = '';
            if ($nik == '') {
                $message .= 'NIK Masih Kosong';
            }
        } else {
            $time = ($id != '') ? 'update_at' : 'insert_at';
            $user = ($id != '') ? 'user_update' : 'user_insert';
            $condition = ($id != '') ? ['id' => $id] : [];

            if ($id == '') {
                $nik = $kode_perusahaan.'-'.$nik;
            } else {
                $nik = $nik;
            }
            
            $nik_exists = $this->customdb->view_by_id('ms_karyawan', ['nik' => $nik, 'id_company' => $company], 'row');

            $nik_status = false;
            if ($id == '') {
                if (! empty($nik_exists)) {
                    $nik_status = true;
                } else {
                    $nik_status = false;
                }
            } else {
                $nik_status = false;
            }

            if ($nik_status == false) {
                $user_exists = $this->customdb->view_by_id('tb_user', ['id_company' => $company, 'username' => $username], 'row');
                $user_status = false;
                if ($id == '') {
                    if (! empty($user_exists)) {
                        $user_status = true;
                    } else {
                        $user_status = false;
                    }
                } else {
                    $user_status = false;
                }

                if ($user_status == false) {
                    $data = array(
                        'nik' => $nik,
                        'nama' => $nama,
                        'ktp' => $ktp,
                        'jenis_kelamin' => $jenkel,
                        'tempat_lahir' => $tempat_lahir,
                        'tgl_lahir' => convert_tgl($tgl_lahir),
                        'telp' => $telp,
                        'provinsi' => $provinsi,
                        'kota' => $kota,
                        'alamat' => $alamat,
                        'email' => $email,
                        'tgl_masuk' => convert_tgl($tgl_masuk),
                        'status_nikah' => $status_nikah,
                        'pendidikan' => $pendidikan,
                        'institusi' => $institusi,
                        'jurusan' => $jurusan,
                        'tahun_lulus' => $tahun_lulus,
                        'agama' => $agama,
                        'golongan_darah' => $golongan_darah,
                        'id_company' => $company,
                        'id_divisi' => $divisi,
                        'id_jabatan' => $jabatan,
                        'posisi' => $posisi,
                        'keterangan' => $keterangan,
                        'salary' => $salary,
                        $time => now(),
                        $user => username()
                    );

                    $upload_foto = $this->upload->do_upload('foto');
                    if ($upload_foto == true) {
                        $file = $this->upload->data();

                        $data['foto'] = $file['file_name'];
                    }

                    if ($id != '') {
                        $simpan = $this->customdb->process_data('ms_karyawan', $data, $condition);
                    } else {
                        $simpan = $this->Karyawan_Model->simpan_karyawan($data, $username);
                    }

                    if ($id == '') {
                        if ($simpan > 0) {
                            $status = 1;
                            $message = 'Data berhasil disimpan';
                        } else {
                            $status = 0;
                            $message = 'Gagal menyimpan data';
                        }
                    } else {
                        $status = 1;
                        $message = 'Data berhasil disimpan';
                    }
                } else {
                    $status = 0;
                    $message = 'username telah terdaftar';
                }
            } else {
                $status = 0;
                $message = 'Nik Sudah pernah didaftarkan, silahkan daftarkan nik lain.';
            }
        }

        $result = array(
            'status' => $status,
            'message' => $message
        );

        echo json_encode($result);
    }

    function hapus_karyawan()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');

            $hapus = $this->Karyawan_Model->hapus_karyawan($id);
            if ($hapus > 0) {
                $status = 1;
                $message = 'Data berhasil dihapus';
            } else {
                $status = 0;
                $message = 'Gagal menghapus data';
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        } else {
            show_404();
        }
    }

    function imei()
    {
        $this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css')
        );

        $company = $this->Main_Model->list_company();

        $data = array(
            'company' => $company
        );

        $footer = array(
            'script' => datatable('js'),
            'app' => 'karyawan/imei.js'
        );

        $this->load->view('template/header', $header);
        $this->load->view('karyawan/imei', $data);
        $this->load->view('template/footer', $footer);
    }

    function data_imei()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $draw = $this->input->get('draw');
            $start = $this->input->get('start');
            $length = $this->input->get('length');
            $search = $this->input->get('search');
            $order = $this->input->get('order');

            $json = $this->Karyawan_Model->json_imei($draw, $start, $length, $search['value'], $order[0]['column'], $order[0]['dir']);

            echo json_encode($json);
        } else {
            show_404();
        }
    }

    function add_imei()
    {
        // checking auth
        $this->Auth_Model->is_login();
        // check request ajax jika bukan dari ajax show_404();
        if ($this->input->is_ajax_request()) {
            // declare all input
            $id = $this->input->post('id');
            $company = $this->input->post('company');
            $karyawan = $this->input->post('karyawan');
            $imei = $this->input->post('imei');

            // check mandatory field
            if ($company == '' || $karyawan == '' || $imei == '') {
                $message = '';
                $status = 0;

                $message .= ($company == '') ? 'Field Perusahaan wajib diisi. <br>' : '';
                $message .= ($karyawan == '') ? 'Field Karyawan wajib diisi. <br>' : '';
                $message .= ($imei == '') ? 'Field Imei wajib diisi. <br>' : '';
            } else {
                // user create
                $time = ($id != '') ? 'update_at' : 'insert_at';
                $user = ($id != '') ? 'user_update' : 'user_insert';

                // buat data utk disimpan
                $data = array(
                    'id_company' => $company,
                    'id_karyawan' => $karyawan,
                    'imei' => $imei,
                    $time => now(),
                    $user => username()
                );

                // if id exists or not
                $condition = ($id != '') ? ['id' => $id] : [];
                // simpan action
                $simpan = $this->customdb->process_data('tb_imei', $data, $condition);
                // message ketika insert
                if ($id == '') {
                    if ($simpan > 0) {
                        $status = 1;
                        $message = 'Berhasil menyimpan data.';
                    } else {
                        $status = 0;
                        $message = 'Gagal menyimpan data, ulangi beberapa saat lagi.';
                    }
                } else {
                    // message ketika update
                    $status = 1;
                    $message = 'Berhasil mengupdate data.';
                }
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        } else {
            show_404();
        }
    }

    function imei_id()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');

            $data = $this->customdb->view_by_id('tb_imei', ['id' => $id], 'row');
            echo json_encode($data);
        } else {
            show_404();
        }
    }

    function hapus_imei()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');

            $hapus = $this->customdb->delete_data('tb_imei', ['id' => $id]);
            if ($hapus > 0) {
                $status = 1;
                $message = 'Data berhasil dihapus';
            } else {
                $status = 0;
                $message = 'Gagal menghapus data';
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        } else {
            show_404();
        }
    }

    function berita()
    {
        $this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css')
        );

        $footer = array(
            'script' => datatable('js'),
            'app' => 'karyawan/berita.js'
        );

        $this->load->view('template/header', $header);
        $this->load->view('karyawan/berita');
        $this->load->view('template/footer', $footer);
    }

    function data_berita()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $draw = $this->input->get('draw');
            $start = $this->input->get('start');
            $length = $this->input->get('length');
            $search = $this->input->get('search');
            $order = $this->input->get('order');

            $json = $this->Karyawan_Model->json_berita($draw, $start, $length, $search['value'], $order[0]['column'], $order[0]['dir']);

            echo json_encode($json);
        } else {
            show_404();
        }
    }

    function add_berita()
    {
        // checking auth
        $this->Auth_Model->is_login();
        // check request ajax jika bukan dari ajax show_404();
        if ($this->input->is_ajax_request()) {
            // declare all input
            $id = $this->input->post('id');
            $judul = $this->input->post('judul');
            $tgl = $this->input->post('tgl');
            $deskripsi = $this->input->post('deskripsi');

            // load library upload
            $config = array(
                'upload_path' => './assets/uploads/images/news',
                'allowed_types' => 'png|jpg|jpeg|bmp',
                'max_size' => 0,
                'file_name' => 'NEWS'.date('YmdHisu'),
                'overwrite' => false
            );
            $this->load->library('upload', $config);

            // check mandatory field
            if ($judul == '' || $tgl == '' || $deskripsi == '') {
                $message = '';
                $status = 0;

                // jika field jenis_kelamin kosong
                $message .= ($judul == '') ? 'Field Judul wajib diisi. <br>' : '';
                $message .= ($tgl == '') ? 'Field Tanggal wajib diisi. <br>' : '';
                $message .= ($deskripsi == '') ? 'Field Deskripsi wajib diisi. <br>' : '';
            } else {
                // buat data utk disimpan
                $data = array(
                    'id_company' => company_id(),
                    'judul' => $judul,
                    'tgl' => convert_tgl($tgl),
                    'teks' => $deskripsi
                );

                if ($this->upload->do_upload('foto')) {
                    $file = $this->upload->data();
                    $data['gambar'] = isset($file['file_name']) ? $file['file_name'] : '';
                }

                // if id exists or not
                $condition = ($id != '') ? ['id' => $id] : [];
                // simpan action
                $simpan = $this->customdb->process_data('tb_news', $data, $condition);
                // message ketika insert
                if ($id == '') {
                    if ($simpan > 0) {
                        $status = 1;
                        $message = 'Berhasil menyimpan data.';

                        # do send notification
                        $this->Karyawan_Model->notif_berita($judul, $deskripsi, company_id());
                    } else {
                        $status = 0;
                        $message = 'Gagal menyimpan data, ulangi beberapa saat lagi.';
                    }
                } else {
                    // message ketika update
                    $status = 1;
                    $message = 'Berhasil mengupdate data.';
                }
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        } else {
            show_404();
        }
    }

    function id_berita()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');

            $data = $this->Karyawan_Model->berita_id($id);
            echo json_encode($data);
        } else {
            show_404();
        }
    }

    function hapus_berita()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');

            $hapus = $this->customdb->delete_data('tb_news', ['id' => $id]);
            if ($hapus > 0) {
                $status = 1;
                $message = 'Data berhasil dihapus';
            } else {
                $status = 0;
                $message = 'Gagal menghapus data';
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        } else {
            show_404();
        }
    }
}

/* End of file Karyawan.php */
/* Location: ./application/controllers/Karyawan.php */
