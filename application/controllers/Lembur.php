<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Lembur extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Lembur_Model');
    }

    function index()
    {
        $this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css')
        );

        $company = $this->Main_Model->list_company();
        $data = array(
            'company' => $company
        );

        $footer = array(
            'script' => datatable('js'),
            'app' => 'lembur/lembur.js'
        );

        $this->load->view('template/header', $header);
        $this->load->view('lembur/lembur', $data);
        $this->load->view('template/footer', $footer);
    }

    function add_lembur()
    {
        // checking auth
        $this->Auth_Model->is_login();
        // check request ajax jika bukan dari ajax show_404();
        if ($this->input->is_ajax_request()) {
            // declare all input
            $id = $this->input->post('id');
            $company = $this->input->post('company');
            $karyawan = $this->input->post('karyawan');
            $mulai = $this->input->post('mulai');
            $selesai = $this->input->post('selesai');
            $nominal = $this->input->post('nominal');
            $keterangan = $this->input->post('keterangan');

            // check mandatory field
            // if ($company == '' || $karyawan == '' || $mulai == '' || $selesai == '' || $nominal == '' || $keterangan == '') {
            if ($company == '' || $karyawan == '' || $mulai == '' || $selesai == '' || $keterangan == '') {
                $message = '';
                $status = 0;

                $message .= ($company == '') ? 'Field Perusahaan wajib diisi. <br>' : '';
                $message .= ($karyawan == '') ? 'Field Karyawan wajib diisi. <br>' : '';
                $message .= ($mulai == '') ? 'Field Mulai wajib diisi. <br>' : '';
                $message .= ($selesai == '') ? 'Field Mulai wajib diisi. <br>' : '';
                // $message .= ($nominal == '') ? 'Field Nominal wajib diisi. <br>' : '';
                $message .= ($keterangan == '') ? 'Field Keterangan wajib diisi. <br>' : '';
            } else {
                $mulai = convert_datetime($mulai);
                $selesai = convert_datetime($selesai);

                # hitung jam
                $jml_jam = hitung_jam($mulai, $selesai);

                // user create
                $time = ($id != '') ? 'update_at' : 'insert_at';
                $user = ($id != '') ? 'user_update' : 'user_insert';

                // buat data utk disimpan
                $data = array(
                    'id_company' => $company,
                    'id_karyawan' => $karyawan,
                    'awal' => $mulai,
                    'akhir' => $selesai,
                    'jml_jam' => $jml_jam,
                    'nominal' => $nominal,
                    'keterangan' => $keterangan,
                    $time => now(),
                    $user => username()
                );

                // if id exists or not
                $condition = ($id != '') ? ['id' => $id] : [];
                // simpan action
                $simpan = $this->customdb->process_data('tb_lembur', $data, $condition);
                // message ketika insert
                if ($id == '') {
                    if ($simpan > 0) {
                        $status = 1;
                        $message = 'Berhasil menyimpan data.';
                    } else {
                        $status = 0;
                        $message = 'Gagal menyimpan data, ulangi beberapa saat lagi.';
                    }
                } else {
                    // message ketika update
                    $status = 1;
                    $message = 'Berhasil mengupdate data.';
                }
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        } else {
            show_404();
        }
    }

    function data_lembur()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $draw = $this->input->get('draw');
            $start = $this->input->get('start');
            $length = $this->input->get('length');
            $search = $this->input->get('search');
            $order = $this->input->get('order');

            $datestart = $this->input->get('datestart');
            $dateend = $this->input->get('dateend');

            $json = $this->Lembur_Model->json_lembur($draw, $start, $length, $search['value'], $order[0]['column'], $order[0]['dir'], $datestart, $dateend);

            echo json_encode($json);
        } else {
            show_404();
        }
    }

    function lembur_id()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');

            $data = $this->Lembur_Model->lembur_id($id);
            echo json_encode($data);
        } else {
            show_404();
        }
    }

    function hapus_lembur()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');

            $hapus = $this->customdb->delete_data('tb_lembur', ['id' => $id]);
            if ($hapus > 0) {
                $status = 1;
                $message = 'Data berhasil dihapus';
            } else {
                $status = 0;
                $message = 'Gagal menghapus data';
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        } else {
            show_404();
        }
    }
}

/* End of file Lembur.php */
/* Location: ./application/controllers/Lembur.php */
