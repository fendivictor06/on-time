<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Gaji extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Gaji_Model');
    }

    function index()
    {
        $this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css')
        );

        $id_company = company_id();
        $ms_company = $this->customdb->view_by_id('ms_company', ['id' => $id_company], 'row');
        $flag_gaji = isset($ms_company->flag_gaji) ? $ms_company->flag_gaji : 0;

        $list_bulan = list_bulan();
        $tahun = $this->Gaji_Model->list_tahun();
        $list_tahun = [];
        if ($tahun) {
            foreach ($tahun as $row) {
                $list_tahun[$row->tahun] = $row->tahun;
            }
        }

        $company = $this->Main_Model->list_company();
        $data = array(
            'company' => $company,
            'bulan' => $list_bulan,
            'tahun' => $list_tahun
        );

        $footer = array(
            'script' => datatable('js')
        );

        if ($flag_gaji == 0) {
            $footer['app'] = 'gaji/info_gaji.js';
        } else {
            $footer['app'] = 'gaji/history_gaji.js';
        }

        $this->load->view('template/header', $header);
        if ($flag_gaji == 0) {
            $this->load->view('gaji/info_gaji', $data);
        } else {
            $this->load->view('gaji/history_gaji', $data);
        }
        $this->load->view('template/footer', $footer);
    }

    function info_gaji()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $draw = $this->input->get('draw');
            $start = $this->input->get('start');
            $length = $this->input->get('length');
            $search = $this->input->get('search');
            $order = $this->input->get('order');

            $json = $this->Gaji_Model->json_info_gaji($draw, $start, $length, $search['value'], $order[0]['column'], $order[0]['dir']);

            echo json_encode($json);
        } else {
            show_404();
        }
    }

    function history_gaji()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $draw = $this->input->get('draw');
            $start = $this->input->get('start');
            $length = $this->input->get('length');
            $search = $this->input->get('search');
            $order = $this->input->get('order');

            $bulan = $this->input->get('bulan');
            $tahun = $this->input->get('tahun');

            $json = $this->Gaji_Model->json_history_gaji($draw, $start, $length, $search['value'], $order[0]['column'], $order[0]['dir'], $bulan, $tahun);

            echo json_encode($json);
        } else {
            show_404();
        }
    }
}

/* End of file Gaji.php */
/* Location: ./application/controllers/Gaji.php */
