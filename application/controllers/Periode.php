<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Periode extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        $this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css')
        );

        $footer = array(
            'script' => datatable('js'),
            'app' => 'periode/periode.js'
        );

        $this->load->view('template/header', $header);
        $this->load->view('periode/periode');
        $this->load->view('template/footer', $footer);
    }
}

/* End of file Periode.php */
/* Location: ./application/controllers/Periode.php */
