<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Log extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        $datestart = $this->input->get('datestart');
        $dateend = $this->input->get('dateend');
        $keyword = $this->input->get('keyword');

        $condition = '';

        if ($keyword != '') {
            $explode = explode(' ', $keyword);
            if ($explode) {
                $condition .= " AND (";
                for ($i = 0; $i < count($explode); $i++) {
                    $string = isset($explode[$i]) ? $explode[$i] : '';
                    if ($string != '') {
                        $condition .= " ( a.response LIKE '%$string%' OR a.request LIKE '%$string%' OR a.api LIKE '%$string%' ) ";

                        if (end($explode) != $string && end($explode) != '') {
                            $condition .= " AND ";
                        }
                    }
                }
                $condition .= ")";
            }
        }

        if ($datestart != '' && $dateend != '') {
            $condition .= " AND DATE(a.insert_at) BETWEEN '$datestart' AND '$dateend' ";
        }

        $query = $this->db->query("
            SELECT *
            FROM log_trx_api a 
            WHERE 1 = 1
            $condition
            ORDER BY a.insert_at DESC
            LIMIT 1000")->result();

        $response = [];
        if ($query) {
            $no = 1;
            foreach ($query as $row => $val) {
                $response[$row] = array(
                    'no' => $no++,
                    'api' => $val->api,
                    'request' => $val->request,
                    'response' => $val->response,
                    'header' => $val->header,
                    'timestamp' => $val->insert_at,
                    'user' => $val->user_insert
                );
            }
        }

        echo json_encode($response);
    }
}

/* End of file Log.php */
/* Location: ./application/controllers/Log.php */
