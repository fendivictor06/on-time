<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Ijin extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Ijin_Model');
    }

    function index()
    {
        $this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css')
        );

        $company = $this->Main_Model->list_company();
        $data = array(
            'company' => $company
        );

        $footer = array(
            'script' => datatable('js'),
            'app' => 'ijin/ijin.js'
        );

        $this->load->view('template/header', $header);
        $this->load->view('ijin/ijin', $data);
        $this->load->view('template/footer', $footer);
    }

    function data_ijin()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $draw = $this->input->get('draw');
            $start = $this->input->get('start');
            $length = $this->input->get('length');
            $search = $this->input->get('search');
            $order = $this->input->get('order');

            $datestart = $this->input->get('datestart');
            $dateend = $this->input->get('dateend');

            $json = $this->Ijin_Model->json_ijin($draw, $start, $length, $search['value'], $order[0]['column'], $order[0]['dir'], $datestart, $dateend);

            echo json_encode($json);
        } else {
            show_404();
        }
    }

    function hapus_ijin()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');

            $hapus = $this->customdb->delete_data('tb_ijin', ['id' => $id]);
            if ($hapus > 0) {
                $status = 1;
                $message = 'Data berhasil dihapus';
            } else {
                $status = 0;
                $message = 'Gagal menghapus data';
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        } else {
            show_404();
        }
    }
}

/* End of file Ijin.php */
/* Location: ./application/controllers/Ijin.php */
