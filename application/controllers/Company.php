<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Company extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Company_Model');
    }

    function index()
    {
        $this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css')
        );

        $arr_provinsi = [];
        $provinsi = $this->customdb->view_by_id('ms_provinsi', ['status' => 1], 'result');
        if ($provinsi) {
            foreach ($provinsi as $row) {
                $arr_provinsi[$row->id] = $row->provinsi;
            }
        }

        $data = array(
            'provinsi' => $arr_provinsi
        );

        $footer = array(
            'script' => datatable('js'),
            'app' => 'company/company.min.js'
        );

        $this->load->view('template/header', $header);
        $this->load->view('company/company', $data);
        $this->load->view('template/footer', $footer);
    }

    function data_company()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $draw = $this->input->get('draw');
            $start = $this->input->get('start');
            $length = $this->input->get('length');
            $search = $this->input->get('search');
            $order = $this->input->get('order');

            $json = $this->Company_Model->json_company($draw, $start, $length, $search['value'], $order[0]['column'], $order[0]['dir']);

            echo json_encode($json);
        } else {
            show_404();
        }
    }

    function add_company()
    {
        $login = $this->Auth_Model->is_login_ajax();
        if ($login == true) {
            $id = $this->input->post('id');
            $company = $this->input->post('company');
            $email = $this->input->post('email');
            $telpon = $this->input->post('telpon');
            $provinsi = $this->input->post('provinsi');
            $kota = $this->input->post('kota');
            $alamat = $this->input->post('alamat');
            $deskripsi = $this->input->post('deskripsi');
            $salary = $this->input->post('salary');
            $jumlah_karyawan = $this->input->post('jumlah');
            $mulai = $this->input->post('mulai');
            $selesai = $this->input->post('selesai');
            $username = $this->input->post('username');

            // load library upload
            $config = array(
                'upload_path' => './assets/uploads/images/company',
                'allowed_types' => 'png|jpg|jpeg|bmp',
                'max_size' => 0,
                'file_name' => 'COMPANY'.date('YmdHisu'),
                'overwrite' => false
            );
            $this->load->library('upload', $config);

            $this->form_validation->set_rules('company', 'Nama Perusahaan', 'required', ['required' => 'Masukkan %s']);
            $this->form_validation->set_rules('email', 'Email', 'valid_email|required', ['required' => 'Masukkan %s', 'valid_email' => 'Email anda tidak valid']);
            $this->form_validation->set_rules('telpon', 'Telepon', 'required|numeric', ['required' => 'Masukkan %s', 'numeric' => 'Inputan hanya boleh angka']);
            $this->form_validation->set_rules('provinsi', 'Provinsi', 'required', ['required' => 'Masukkan %s']);
            $this->form_validation->set_rules('kota', 'Kota', 'required', ['required' => 'Masukkan %s']);
            $this->form_validation->set_rules('alamat', 'Alamat', 'required', ['required' => 'Masukkan %s']);
            $this->form_validation->set_rules('jumlah', 'Jumlah Karyawan', 'required|numeric', ['required' => 'Masukkan %s', 'numeric' => 'Inputan hanya boleh angka']);
            $this->form_validation->set_rules('mulai', 'Tanggal Mulai', 'required', ['required' => 'Masukkan %s']);
            $this->form_validation->set_rules('selesai', 'Tanggal Selesai', 'required', ['required' => 'Masukkan %s']);

            if ($id == '') {
                $this->form_validation->set_rules('username', 'Username', 'required|is_unique[tb_user.username]', ['required' => 'Masukkan %s', 'is_unique' => 'Username sudah digunakan']);
            }

            if ($this->form_validation->run() == false) {
                $status = 0;
                $message = validation_errors();
            } else {
                $email_check = $this->email_check($email, $id);
                if ($email_check == true) {
                    $user = ($id) ? 'user_update' : 'user_insert';
                    $time = ($id) ? 'update_at' : 'insert_at';
                    $condition = ($id) ? ['id' => $id] : [];

                    $mulai = convert_tgl($mulai);
                    $selesai = convert_tgl($selesai);

                    $data = array(
                        'id_provinsi' => $provinsi,
                        'id_kota' => $kota,
                        'company' => $company,
                        'alamat' => $alamat,
                        'keterangan' => $deskripsi,
                        'email' => $email,
                        'telpon' => $telpon,
                        'flag_gaji' => $salary,
                        'jumlah_karyawan' => $jumlah_karyawan,
                        'mulai' => $mulai,
                        'selesai' => $selesai,
                        $time => now(),
                        $user => username()
                    );

                    if ($id == '') {
                        $data['kode_perusahaan'] = $this->Company_Model->kode_perusahaan();
                    }

                    if ($this->upload->do_upload('foto')) {
                        $file = $this->upload->data();
                        $data['foto'] = isset($file['file_name']) ? $file['file_name'] : '';
                    }

                    $simpan = $this->Company_Model->add_company($data, $condition, $username);
                    $status_simpan = isset($simpan['status']) ? $simpan['status'] : false;
                    $id_simpan = isset($simpan['id_company']) ? $simpan['id_company'] : '';

                    if ($id == '') {
                        if ($status_simpan == true) {
                            $status = 1;
                            $message = 'Data berhasil disimpan.';

                            # do send email
                            $this->send_mail($email, $company, $id_simpan);

                            # do copy libur
                            $this->Company_Model->copy_libur($id_simpan);
                        } else {
                            $status = 0;
                            $message = 'Data gagal disimpan.';
                        }
                    } else {
                        $status = 1;
                        $message = 'Data berhasil disimpan.';
                    }
                } else {
                    $status = 0;
                    $message = 'Email telah digunakan';
                }
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        }
    }

    function email_check($mail = '', $id = '')
    {
        $result = false;
        if ($id == '') {
            $exists = $this->customdb->view_by_id('ms_company', ['email' => $mail], 'row');
            if ($exists) {
                $result = false;
            } else {
                $result = true;
            }
        } else {
            $exists = $this->customdb->view_by_id('ms_company', ['email' => $mail, 'id' => $id], 'row');
            if ($exists) {
                $result = true;
            } else {
                $validation = $this->customdb->view_by_id('ms_company', ['email' => $mail, 'id !=' => $id], 'row');
                if ($validation) {
                    $result = false;
                } else {
                    $result = true;
                }
            }
        }

        return $result;
    }

    function id_company()
    {
        $login = $this->Auth_Model->is_login_ajax();
        if ($login == true) {
            $id = $this->input->post('id');

            $data = $this->customdb->view_by_id('ms_company', ['id' => $id], 'row');
            echo json_encode($data);
        }
    }

    function hapus_company()
    {
        $login = $this->Auth_Model->is_login_ajax();
        if ($login == true) {
            $id = $this->input->post('id');

            $hapus = $this->customdb->process_data('ms_company', ['status' => 0], ['id' => $id]);
            if ($hapus > 0) {
                $status = 1;
                $message = 'Data berhasil dihapus';
            } else {
                $status = 0;
                $message = 'Gagal menghapus data';
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        }
    }

    function kota()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $search = $this->input->post('search');
            $provinsi = $this->input->post('provinsi');

            $arr = [];
            $query = $this->Company_Model->view_kota($provinsi, $search);
            if ($query) {
                foreach ($query as $row => $val) {
                    $arr[$row] = array(
                        'id' => $val->id,
                        'kota' => $val->kota
                    );
                }
            }

            echo json_encode($arr);
        } else {
            show_404();
        }
    }

    function send_mail($to = '', $company = '', $id = '')
    {
        # create code
        $code = generate_random();
        $code = md5($code);

        # delete before insert new token
        $delete = $this->customdb->delete_data('log_verifikasi', ['id_company' => $id]);

        $log = array(
            'id_company' => $id,
            'unik' => $code,
            'user_insert' => username()
        );
        $create = $this->customdb->process_data('log_verifikasi', $log);
        $bcc = ['victor.fendi@gmedia.co.id'];
        $from = 'no-reply@ontime.co.id';

        # send email to company
        $this->load->library('Sendmail');
        $judul = 'Validasi Email On Time';
        $pesan = 'Hai, '.$company.' berikut link untuk verifikasi On Time. <br> Silahkan klik link berikut <a href="'.base_url('Validasi/perusahaan/'.$code.'/'.$id).'">klik disini</a>';
        $kirim = $this->sendmail->sending([], $from, $to, $judul, $judul, $pesan, [], $bcc);

        log_api(['to' => $to, 'company' => $company, 'id' => $id], 200, 'log-email', $kirim);
    }
}

/* End of file Company.php */
/* Location: ./application/controllers/Company.php */
