<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Rest_Gaji extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Rest_Gaji_Model');
    }

    function index()
    {
        $auth = $this->token->auth('POST');
        if ($auth == true) {
            $params = get_params();
            $tgl = filter_params($params, 'tgl');
            $bln = filter_params($params, 'bln');
            $thn = filter_params($params, 'thn');

            $id_company = id_company();
            $gaji = $this->customdb->view_by_id('ms_company', ['id' => $id_company], 'row');
            $flag_gaji = isset($gaji->flag_gaji) ? $gaji->flag_gaji : 0;

            $arr_data = [];
            $data = $this->Rest_Gaji_Model->view_payroll($tgl, $bln, $thn, $flag_gaji);
            if ($data) {
                $arr_data = $data;
                $status = 200;
                $message = 'Berhasil';
            } else {
                $status = 404;
                $message = 'Data tidak ditemukan';
            }

            print_json($status, $message, $arr_data);
        }
    }
}

/* End of file Rest_Gaji.php */
/* Location: ./application/controllers/Rest_Gaji.php */
