<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class User extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('User_Model');
    }

    function index()
    {
        $this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css')
        );

        $level = level_user();
        if ($level == 1) {
            $arr_level = array(
                '1' => 'Admin',
                '2' => 'Admin Perusahaan',
                '3' => 'Karyawan'
            );
        } elseif ($level == 2) {
            $arr_level = array(
                '3' => 'Karyawan'
            );
        } else {
            $arr_level = [];
        }

        $company = $this->Main_Model->list_company();
        $data = array(
            'company' => $company,
            'level' => $arr_level
        );

        $footer = array(
            'script' => datatable('js'),
            'app' => 'user/user.min.js'
        );

        $this->load->view('template/header', $header);
        $this->load->view('user/user', $data);
        $this->load->view('template/footer', $footer);
    }

    function data_user()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $draw = $this->input->get('draw');
            $start = $this->input->get('start');
            $length = $this->input->get('length');
            $search = $this->input->get('search');
            $order = $this->input->get('order');

            $json = $this->User_Model->json_user($draw, $start, $length, $search['value'], $order[0]['column'], $order[0]['dir']);

            echo json_encode($json);
        } else {
            show_404();
        }
    }

    function add_user()
    {
        // checking auth
        $this->Auth_Model->is_login();
        // check request ajax jika bukan dari ajax show_404();
        if ($this->input->is_ajax_request()) {
            // declare all input
            $id = $this->input->post('id');
            $level = $this->input->post('level');
            $company = $this->input->post('company');
            $karyawan = $this->input->post('karyawan');
            $profile_name = $this->input->post('profile_name');
            $username = $this->input->post('username');
            $password = $this->input->post('password');

            // check mandatory field
            if ($level == '' || $username == '' || $password == '') {
                $message = '';
                $status = 0;

                $message .= ($level == '') ? 'Field Level wajib diisi. <br>' : '';
                $message .= ($username == '') ? 'Field Username wajib diisi. <br>' : '';
                $message .= ($password == '') ? 'Field Password wajib diisi. <br>' : '';
            } else {
                $message = '';
                $check_field = true;
                if ($level == 1) {
                    if ($profile_name == '') {
                        $check_field = false;
                        $message .= 'Field Profile Name wajib diisi. <br>';
                    }

                    $company = 0;
                    $karyawan = 0;
                } elseif ($level == 2) {
                    if ($company == '' || $profile_name == '') {
                        $check_field = false;
                        $message .= ($company == '') ? 'Field Perusahaan wajib diisi. <br>' : '';
                        $message .= ($profile_name == '') ? 'Field Profile Name wajib diisi. <br>' : '';
                    }

                    $karyawan = 0;
                } else {
                    if ($company == '' || $karyawan == '') {
                        $check_field = false;
                        $message .= ($company == '') ? 'Field Perusahaan wajib diisi. <br>' : '';
                        $message .= ($karyawan == '') ? 'Field Karyawan wajib diisi. <br>' : '';
                    }

                    $ms_karyawan = $this->customdb->view_by_id('ms_karyawan', ['id' => $karyawan], 'row');
                    $profile_name = isset($ms_karyawan->nama) ? $ms_karyawan->nama : '';
                }

                $check_exists = true;
                if ($level == 1 || $level == 2) {
                    $user_exists = $this->User_Model->user_exist($username, $level);
                } else {
                    $user_exists = $this->User_Model->user_exist($username, $level, $company);
                }

                if ($id == '') {
                    if ($user_exists == true) {
                        $check_exists = false;
                    } else {
                        $check_exists = true;
                    }
                }

                if ($check_field == true) {
                    if ($check_exists == true) {
                        $simpan = $this->User_Model->add_user($level, $company, $karyawan, $username, $password, $profile_name, $id);

                        if ($simpan > 0) {
                            $status = 1;
                            $message = 'Data berhasil disimpan';
                        } else {
                            $status = 0;
                            $message = 'Gagal menyimpan data';
                        }
                    } else {
                        $status = 0;
                        $message = 'Username sudah terdaftar';
                    }
                } else {
                    $status = 0;
                    $message = $message;
                }
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        } else {
            show_404();
        }
    }

    function user_id()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');

            $data = $this->customdb->view_by_id('tb_user', ['id' => $id], 'row');
            echo json_encode($data);
        } else {
            show_404();
        }
    }

    function hapus_user()
    {
        $this->Auth_Model->is_login();
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');

            $hapus = $this->customdb->delete_data('tb_user', ['id' => $id]);
            if ($hapus > 0) {
                $status = 1;
                $message = 'Data berhasil dihapus';
            } else {
                $status = 0;
                $message = 'Gagal menghapus data';
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        } else {
            show_404();
        }
    }

    function change_password()
    {
        $this->Auth_Model->is_login();
        $header = array(
            'styles' => datatable('css'),
            'title' => 'Ganti Password'
        );

        $company = $this->Main_Model->list_company();
        $data = array(
            'company' => $company
        );

        $footer = array(
            'script' => datatable('js'),
            'app' => 'user/change_password.min.js'
        );

        $this->load->view('template/header', $header);
        $this->load->view('user/change_password', $data);
        $this->load->view('template/footer', $footer);
    }

    function ubah_password()
    {
        $login = $this->Auth_Model->is_login_ajax();
        if ($login == true) {
            $old = $this->input->post('password-lama');
            $new = $this->input->post('password-baru');
            $re_password = $this->input->post('re-password');
            $username = username();

            $this->form_validation->set_rules('password-lama', 'Password Lama', 'required', ['required' => 'Masukkan %s']);
            $this->form_validation->set_rules('password-baru', 'Password Baru', 'required', ['required' => 'Masukkan %s']);
            $this->form_validation->set_rules('re-password', 'Ulangi Password', 'required|matches[password-baru]', ['required' => 'Masukkan %s', 'matches' => 'Password tidak cocok']);

            if ($this->form_validation->run() == false) {
                $status = 0;
                $message = validation_errors();
            } else {
                $exists = $this->User_Model->check_password($username, $old);
                if (empty($exists)) {
                    $status = 0;
                    $message = 'Password anda salah';
                } else {
                    $update = $this->User_Model->change_password($username, $new, $old);

                    if ($update > 0) {
                        $status = 1;
                        $message = 'Password berhasil diganti';
                    } else {
                        $status = 0;
                        $message = 'Gagal mengubah password';
                    }
                }
            }

            $result = array(
                'status' => $status,
                'message' => $message
            );

            echo json_encode($result);
        }
    }
}

/* End of file User.php */
/* Location: ./application/controllers/User.php */
