<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Master_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function json_jenis_kelamin($draw = 1, $start = 0, $length = 0, $search = '', $column = '', $dir = '')
    {
        $start = $this->db->escape_str($start);
        $length = $this->db->escape_str($length);
        $column = $this->db->escape_str($column);
        $dir = $this->db->escape_str($dir);
        $search = $this->db->escape_str($search);

        $total_filtered = $this->total_jenis_kelamin($search);
        $data = [];
        $request = $this->view_jenis_kelamin($start, $length, $search, $column, $dir);
        if (! empty($request)) {
            $no = $start + 1;
            foreach ($request as $row) {
                $btn_edit = btn_edit($row->id);
                $btn_delete = btn_delete($row->id);

                $data[] = array(
                    $no++,
                    $row->jenis_kelamin,
                    $row->keterangan,
                    btn_group([$btn_edit, $btn_delete])
                );
            }
        }

        return response_datatable($draw, $total_filtered, $data);
    }

    function view_jenis_kelamin($start = 0, $length = 0, $search = '', $column = '', $dir = '')
    {
        $kolom = ['a.jenis_kelamin', 'a.keterangan'];
        $condition = '';
        if ($search != '') {
            $condition .= ' AND (';
            for ($i = 0; $i < count($kolom); $i++) {
                $condition .= $kolom[$i]." LIKE '%$search%' ";
                if ($kolom[$i] != end($kolom)) {
                    $condition .= ' OR ';
                }
            }
            $condition .= ')';
        }

        $kolom_order = ['1' => 'a.jenis_kelamin', '2' => 'a.keterangan'];
        $order = '';
        if ($column != '' && $dir != '') {
            $col = isset($kolom_order[$column]) ? $kolom_order[$column] : '';

            if ($col != '') {
                $order .= " ORDER BY $col $dir ";
            }
        }

        $query = $this->db->query("
			SELECT *
			FROM ms_jenis_kelamin a
			WHERE a.status = 1 
			$condition 
			$order 
			LIMIT $start, $length ")->result();

        return $query;
    }

    function total_jenis_kelamin($search = '')
    {
        $kolom = ['a.jenis_kelamin', 'a.keterangan'];
        $condition = '';
        if ($search != '') {
            $condition .= ' AND (';
            for ($i = 0; $i < count($kolom); $i++) {
                $condition .= $kolom[$i]." LIKE '%$search%' ";
                if ($kolom[$i] != end($kolom)) {
                    $condition .= ' OR ';
                }
            }
            $condition .= ')';
        }

        $query = $this->db->query("
			SELECT COUNT(*) AS jumlah
			FROM ms_jenis_kelamin a
			WHERE a.status = 1 
			$condition ")->row();

        return isset($query->jumlah) ? $query->jumlah : 0;
    }

    function json_golongan_darah($draw = 1, $start = 0, $length = 0, $search = '', $column = '', $dir = '')
    {
        $start = $this->db->escape_str($start);
        $length = $this->db->escape_str($length);
        $column = $this->db->escape_str($column);
        $dir = $this->db->escape_str($dir);
        $search = $this->db->escape_str($search);

        $total_filtered = $this->total_golongan_darah($search);
        $data = [];
        $request = $this->view_golongan_darah($start, $length, $search, $column, $dir);
        if (! empty($request)) {
            $no = $start + 1;
            foreach ($request as $row) {
                $btn_edit = btn_edit($row->id);
                $btn_delete = btn_delete($row->id);

                $data[] = array(
                    $no++,
                    $row->golongan_darah,
                    btn_group([$btn_edit, $btn_delete])
                );
            }
        }

        return response_datatable($draw, $total_filtered, $data);
    }

    function view_golongan_darah($start = 0, $length = 0, $search = '', $column = '', $dir = '')
    {
        $kolom = ['a.golongan_darah'];
        $condition = '';
        if ($search != '') {
            $condition .= ' AND (';
            for ($i = 0; $i < count($kolom); $i++) {
                $condition .= $kolom[$i]." LIKE '%$search%' ";
                if ($kolom[$i] != end($kolom)) {
                    $condition .= ' OR ';
                }
            }
            $condition .= ')';
        }

        $kolom_order = ['1' => 'a.golongan_darah'];
        $order = '';
        if ($column != '' && $dir != '') {
            $col = isset($kolom_order[$column]) ? $kolom_order[$column] : '';

            if ($col != '') {
                $order .= " ORDER BY $col $dir ";
            }
        }

        $query = $this->db->query("
			SELECT *
			FROM ms_golongan_darah a
			WHERE a.status = 1 
			$condition 
			$order 
			LIMIT $start, $length ")->result();

        return $query;
    }

    function total_golongan_darah($search = '')
    {
        $kolom = ['a.golongan_darah'];
        $condition = '';
        if ($search != '') {
            $condition .= ' AND (';
            for ($i = 0; $i < count($kolom); $i++) {
                $condition .= $kolom[$i]." LIKE '%$search%' ";
                if ($kolom[$i] != end($kolom)) {
                    $condition .= ' OR ';
                }
            }
            $condition .= ')';
        }

        $query = $this->db->query("
			SELECT COUNT(*) AS jumlah
			FROM ms_golongan_darah a
			WHERE a.status = 1 
			$condition ")->row();

        return isset($query->jumlah) ? $query->jumlah : 0;
    }

    function json_agama($draw = 1, $start = 0, $length = 0, $search = '', $column = '', $dir = '')
    {
        $start = $this->db->escape_str($start);
        $length = $this->db->escape_str($length);
        $column = $this->db->escape_str($column);
        $dir = $this->db->escape_str($dir);
        $search = $this->db->escape_str($search);

        $total_filtered = $this->total_agama($search);
        $data = [];
        $request = $this->view_agama($start, $length, $search, $column, $dir);
        if (! empty($request)) {
            $no = $start + 1;
            foreach ($request as $row) {
                $btn_edit = btn_edit($row->id);
                $btn_delete = btn_delete($row->id);

                $data[] = array(
                    $no++,
                    $row->agama,
                    btn_group([$btn_edit, $btn_delete])
                );
            }
        }

        return response_datatable($draw, $total_filtered, $data);
    }

    function view_agama($start = 0, $length = 0, $search = '', $column = '', $dir = '')
    {
        $kolom = ['a.agama'];
        $condition = '';
        if ($search != '') {
            $condition .= ' AND (';
            for ($i = 0; $i < count($kolom); $i++) {
                $condition .= $kolom[$i]." LIKE '%$search%' ";
                if ($kolom[$i] != end($kolom)) {
                    $condition .= ' OR ';
                }
            }
            $condition .= ')';
        }

        $kolom_order = ['1' => 'a.agama'];
        $order = '';
        if ($column != '' && $dir != '') {
            $col = isset($kolom_order[$column]) ? $kolom_order[$column] : '';

            if ($col != '') {
                $order .= " ORDER BY $col $dir ";
            }
        }

        $query = $this->db->query("
			SELECT *
			FROM ms_agama a
			WHERE a.status = 1 
			$condition 
			$order 
			LIMIT $start, $length ")->result();

        return $query;
    }

    function total_agama($search = '')
    {
        $kolom = ['a.agama'];
        $condition = '';
        if ($search != '') {
            $condition .= ' AND (';
            for ($i = 0; $i < count($kolom); $i++) {
                $condition .= $kolom[$i]." LIKE '%$search%' ";
                if ($kolom[$i] != end($kolom)) {
                    $condition .= ' OR ';
                }
            }
            $condition .= ')';
        }

        $query = $this->db->query("
			SELECT COUNT(*) AS jumlah
			FROM ms_agama a
			WHERE a.status = 1 
			$condition ")->row();

        return isset($query->jumlah) ? $query->jumlah : 0;
    }

    function json_status_nikah($draw = 1, $start = 0, $length = 0, $search = '', $column = '', $dir = '')
    {
        $start = $this->db->escape_str($start);
        $length = $this->db->escape_str($length);
        $column = $this->db->escape_str($column);
        $dir = $this->db->escape_str($dir);
        $search = $this->db->escape_str($search);

        $total_filtered = $this->total_status_nikah($search);
        $data = [];
        $request = $this->view_status_nikah($start, $length, $search, $column, $dir);
        if (! empty($request)) {
            $no = $start + 1;
            foreach ($request as $row) {
                $btn_edit = btn_edit($row->id);
                $btn_delete = btn_delete($row->id);

                $data[] = array(
                    $no++,
                    $row->status_nikah,
                    btn_group([$btn_edit, $btn_delete])
                );
            }
        }

        return response_datatable($draw, $total_filtered, $data);
    }

    function view_status_nikah($start = 0, $length = 0, $search = '', $column = '', $dir = '')
    {
        $kolom = ['a.status_nikah'];
        $condition = '';
        if ($search != '') {
            $condition .= ' AND (';
            for ($i = 0; $i < count($kolom); $i++) {
                $condition .= $kolom[$i]." LIKE '%$search%' ";
                if ($kolom[$i] != end($kolom)) {
                    $condition .= ' OR ';
                }
            }
            $condition .= ')';
        }

        $kolom_order = ['1' => 'a.status_nikah'];
        $order = '';
        if ($column != '' && $dir != '') {
            $col = isset($kolom_order[$column]) ? $kolom_order[$column] : '';

            if ($col != '') {
                $order .= " ORDER BY $col $dir ";
            }
        }

        $query = $this->db->query("
			SELECT *
			FROM ms_status_nikah a
			WHERE a.status = 1 
			$condition 
			$order 
			LIMIT $start, $length ")->result();

        return $query;
    }

    function total_status_nikah($search = '')
    {
        $kolom = ['a.status_nikah'];
        $condition = '';
        if ($search != '') {
            $condition .= ' AND (';
            for ($i = 0; $i < count($kolom); $i++) {
                $condition .= $kolom[$i]." LIKE '%$search%' ";
                if ($kolom[$i] != end($kolom)) {
                    $condition .= ' OR ';
                }
            }
            $condition .= ')';
        }

        $query = $this->db->query("
			SELECT COUNT(*) AS jumlah
			FROM ms_status_nikah a
			WHERE a.status = 1 
			$condition ")->row();

        return isset($query->jumlah) ? $query->jumlah : 0;
    }

    function json_pendidikan($draw = 1, $start = 0, $length = 0, $search = '', $column = '', $dir = '')
    {
        $start = $this->db->escape_str($start);
        $length = $this->db->escape_str($length);
        $column = $this->db->escape_str($column);
        $dir = $this->db->escape_str($dir);
        $search = $this->db->escape_str($search);

        $total_filtered = $this->total_pendidikan($search);
        $data = [];
        $request = $this->view_pendidikan($start, $length, $search, $column, $dir);
        if (! empty($request)) {
            $no = $start + 1;
            foreach ($request as $row) {
                $btn_edit = btn_edit($row->id);
                $btn_delete = btn_delete($row->id);

                $data[] = array(
                    $no++,
                    $row->pendidikan,
                    $row->keterangan,
                    btn_group([$btn_edit, $btn_delete])
                );
            }
        }

        return response_datatable($draw, $total_filtered, $data);
    }

    function view_pendidikan($start = 0, $length = 0, $search = '', $column = '', $dir = '')
    {
        $kolom = ['a.pendidikan', 'a.keterangan'];
        $condition = '';
        if ($search != '') {
            $condition .= ' AND (';
            for ($i = 0; $i < count($kolom); $i++) {
                $condition .= $kolom[$i]." LIKE '%$search%' ";
                if ($kolom[$i] != end($kolom)) {
                    $condition .= ' OR ';
                }
            }
            $condition .= ')';
        }

        $kolom_order = ['1' => 'a.pendidikan', '2' => 'a.keterangan'];
        $order = '';
        if ($column != '' && $dir != '') {
            $col = isset($kolom_order[$column]) ? $kolom_order[$column] : '';

            if ($col != '') {
                $order .= " ORDER BY $col $dir ";
            }
        }

        $query = $this->db->query("
			SELECT *
			FROM ms_pendidikan a
			WHERE a.status = 1 
			$condition 
			$order 
			LIMIT $start, $length ")->result();

        return $query;
    }

    function total_pendidikan($search = '')
    {
        $kolom = ['a.pendidikan', 'a.keterangan'];
        $condition = '';
        if ($search != '') {
            $condition .= ' AND (';
            for ($i = 0; $i < count($kolom); $i++) {
                $condition .= $kolom[$i]." LIKE '%$search%' ";
                if ($kolom[$i] != end($kolom)) {
                    $condition .= ' OR ';
                }
            }
            $condition .= ')';
        }

        $query = $this->db->query("
			SELECT COUNT(*) AS jumlah
			FROM ms_pendidikan a
			WHERE a.status = 1 
			$condition ")->row();

        return isset($query->jumlah) ? $query->jumlah : 0;
    }

    function json_divisi($draw = 1, $start = 0, $length = 0, $search = '', $column = '', $dir = '')
    {
        $start = $this->db->escape_str($start);
        $length = $this->db->escape_str($length);
        $column = $this->db->escape_str($column);
        $dir = $this->db->escape_str($dir);
        $search = $this->db->escape_str($search);

        $total_filtered = $this->total_divisi($search);
        $data = [];
        $request = $this->view_divisi($start, $length, $search, $column, $dir);
        if (! empty($request)) {
            $no = $start + 1;
            foreach ($request as $row) {
                $btn_edit = btn_edit($row->id);
                $btn_delete = btn_delete($row->id);

                $data[] = array(
                    $no++,
                    $row->company,
                    $row->divisi,
                    $row->keterangan,
                    btn_group([$btn_edit, $btn_delete])
                );
            }
        }

        return response_datatable($draw, $total_filtered, $data);
    }

    function view_divisi($start = 0, $length = 0, $search = '', $column = '', $dir = '')
    {
        $kolom = ['b.company', 'a.divisi', 'a.keterangan'];
        $condition = search_datatable($kolom, $search);

        $kolom_order = ['1' => 'b.company', '2' => 'a.divisi', '3' => 'a.keterangan'];
        $order = order_datatable($kolom_order, $column, $dir);

        # id company
        $company = company_id();
        if ($company != '') {
            $condition .= " AND a.id_company = '$company' ";
        }

        $query = $this->db->query("
        	SELECT a.*, b.company
        	FROM ms_divisi a
        	INNER JOIN ms_company b ON a.id_company = b.id
        	WHERE a.status = 1
			$condition 
			$order 
			LIMIT $start, $length ")->result();

        return $query;
    }

    function total_divisi($search = '')
    {
        $kolom = ['b.company', 'a.divisi', 'a.keterangan'];
        $condition = search_datatable($kolom, $search);

        # id company
        $company = company_id();
        if ($company != '') {
            $condition .= " AND a.id_company = '$company' ";
        }

        $query = $this->db->query("
        	SELECT COUNT(*) AS jumlah
        	FROM ms_divisi a
        	INNER JOIN ms_company b ON a.id_company = b.id
        	WHERE a.status = 1
			$condition ")->row();

        return isset($query->jumlah) ? $query->jumlah : 0;
    }

    function json_jabatan($draw = 1, $start = 0, $length = 0, $search = '', $column = '', $dir = '')
    {
        $start = $this->db->escape_str($start);
        $length = $this->db->escape_str($length);
        $column = $this->db->escape_str($column);
        $dir = $this->db->escape_str($dir);
        $search = $this->db->escape_str($search);

        $total_filtered = $this->total_jabatan($search);
        $data = [];
        $request = $this->view_jabatan($start, $length, $search, $column, $dir);
        if (! empty($request)) {
            $no = $start + 1;
            foreach ($request as $row) {
                $btn_edit = btn_edit($row->id);
                $btn_delete = btn_delete($row->id);

                $data[] = array(
                    $no++,
                    $row->company,
                    $row->jabatan,
                    $row->keterangan,
                    btn_group([$btn_edit, $btn_delete])
                );
            }
        }

        return response_datatable($draw, $total_filtered, $data);
    }

    function view_jabatan($start = 0, $length = 0, $search = '', $column = '', $dir = '')
    {
        $kolom = ['b.company', 'a.jabatan', 'a.keterangan'];
        $condition = search_datatable($kolom, $search);

        $kolom_order = ['1' => 'b.company', '2' => 'a.jabatan', '3' => 'a.keterangan'];
        $order = order_datatable($kolom_order, $column, $dir);

        # id company
        $company = company_id();
        if ($company != '') {
            $condition .= " AND a.id_company = '$company' ";
        }

        $query = $this->db->query("
        	SELECT a.*, b.company
        	FROM ms_jabatan a
        	INNER JOIN ms_company b ON a.id_company = b.id
        	WHERE a.status = 1
			$condition 
			$order 
			LIMIT $start, $length ")->result();

        return $query;
    }

    function total_jabatan($search = '')
    {
        $kolom = ['b.company', 'a.jabatan', 'a.keterangan'];
        $condition = search_datatable($kolom, $search);

        # id company
        $company = company_id();
        if ($company != '') {
            $condition .= " AND a.id_company = '$company' ";
        }

        $query = $this->db->query("
        	SELECT COUNT(*) AS jumlah
        	FROM ms_divisi a
        	INNER JOIN ms_jabatan b ON a.id_company = b.id
        	WHERE a.status = 1
			$condition ")->row();

        return isset($query->jumlah) ? $query->jumlah : 0;
    }

    function json_akses_point($draw = 1, $start = 0, $length = 0, $search = '', $column = '', $dir = '')
    {
        $start = $this->db->escape_str($start);
        $length = $this->db->escape_str($length);
        $column = $this->db->escape_str($column);
        $dir = $this->db->escape_str($dir);
        $search = $this->db->escape_str($search);

        $total_filtered = $this->total_akses_point($search);
        $data = [];
        $request = $this->view_akses_point($start, $length, $search, $column, $dir);
        if (! empty($request)) {
            $no = $start + 1;
            foreach ($request as $row) {
                $btn_edit = btn_edit($row->id);
                $btn_delete = btn_delete($row->id);

                $data[] = array(
                    $no++,
                    $row->company,
                    $row->ssid,
                    $row->macaddress,
                    $row->ippublic,
                    btn_group([$btn_edit, $btn_delete])
                );
            }
        }

        return response_datatable($draw, $total_filtered, $data);
    }

    function view_akses_point($start = 0, $length = 0, $search = '', $column = '', $dir = '')
    {
        $kolom = ['a.ssid', 'a.macaddress', 'a.ippublic', 'b.company'];
        $condition = '';
        if ($search != '') {
            $condition .= ' AND (';
            for ($i = 0; $i < count($kolom); $i++) {
                $condition .= $kolom[$i]." LIKE '%$search%' ";
                if ($kolom[$i] != end($kolom)) {
                    $condition .= ' OR ';
                }
            }
            $condition .= ')';
        }

        $kolom_order = ['1' => 'b.company', '2' => 'a.ssid', '3' => 'a.macaddress', '4' => 'a.ippublic'];
        $order = '';
        if ($column != '' && $dir != '') {
            $col = isset($kolom_order[$column]) ? $kolom_order[$column] : '';

            if ($col != '') {
                $order .= " ORDER BY $col $dir ";
            }
        }

        # id company
        $company = company_id();
        if ($company != '') {
            $condition .= " AND a.id_company = '$company' ";
        }

        $query = $this->db->query("
            SELECT a.*, b.company
            FROM ms_akses_point a
            LEFT JOIN ms_company b ON a.id_company = b.id
            WHERE a.status = 1 
            $condition 
            $order 
            LIMIT $start, $length ")->result();

        return $query;
    }

    function total_akses_point($search = '')
    {
        $kolom = ['a.ssid', 'a.macaddress', 'a.ippublic', 'b.company'];
        $condition = '';
        if ($search != '') {
            $condition .= ' AND (';
            for ($i = 0; $i < count($kolom); $i++) {
                $condition .= $kolom[$i]." LIKE '%$search%' ";
                if ($kolom[$i] != end($kolom)) {
                    $condition .= ' OR ';
                }
            }
            $condition .= ')';
        }

        # id company
        $company = company_id();
        if ($company != '') {
            $condition .= " AND a.id_company = '$company' ";
        }

        $query = $this->db->query("
            SELECT COUNT(*) AS jumlah
            FROM ms_akses_point a
            LEFT JOIN ms_company b ON a.id_company = b.id
            WHERE a.status = 1 
            $condition ")->row();

        return isset($query->jumlah) ? $query->jumlah : 0;
    }

    function json_lokasi($draw = 1, $start = 0, $length = 0, $search = '', $column = '', $dir = '', $company = '')
    {
        $start = $this->db->escape_str($start);
        $length = $this->db->escape_str($length);
        $column = $this->db->escape_str($column);
        $dir = $this->db->escape_str($dir);
        $search = $this->db->escape_str($search);
        $company = $this->db->escape_str($company);

        $total_filtered = $this->total_lokasi($search, $company);
        $data = [];
        $request = $this->view_lokasi($start, $length, $search, $column, $dir, $company);
        if (! empty($request)) {
            $no = $start + 1;
            foreach ($request as $row) {
                $btn_edit = btn_edit($row->id);
                $btn_delete = btn_delete($row->id);

                $lokasi = '';
                if ($row->latitude != '' && $row->longitude != '') {
                    $lokasi = ' <a href="https://www.google.com/maps/search/?api=1&query='.$row->latitude.','.$row->longitude.'" target="_blank"> '.$row->nama.' </a>';
                }

                $data[] = array(
                    $no++,
                    $row->company,
                    $lokasi,
                    $row->toleransi,
                    btn_group([$btn_edit, $btn_delete])
                );
            }
        }

        return response_datatable($draw, $total_filtered, $data);
    }

    function view_lokasi($start = 0, $length = 0, $search = '', $column = '', $dir = '', $company = '')
    {
        $kolom = ['b.company', 'a.nama', 'a.toleransi'];
        $condition = search_datatable($kolom, $search);

        $kolom_order = ['1' => 'b.company', '2' => 'a.nama', '3' => 'a.toleransi'];
        $order = order_datatable($kolom_order, $column, $dir);

        # id company
        if ($company != '') {
            $condition .= " AND a.id_company = '$company' ";
        }

        $query = $this->db->query("
            SELECT a.*, b.company
            FROM ms_lokasi a
            INNER JOIN ms_company b ON a.id_company = b.id
            WHERE a.`status` = 1
            $condition 
            $order 
            LIMIT $start, $length ")->result();

        return $query;
    }

    function total_lokasi($search = '', $company = '')
    {
        $kolom = ['b.company', 'a.nama', 'a.toleransi'];
        $condition = search_datatable($kolom, $search);

        # id company
        if ($company != '') {
            $condition .= " AND a.id_company = '$company' ";
        }

        $query = $this->db->query("
            SELECT COUNT(a.id) AS jumlah
            FROM ms_lokasi a
            INNER JOIN ms_company b ON a.id_company = b.id
            WHERE a.`status` = 1
            $condition ")->row();

        return isset($query->jumlah) ? $query->jumlah : 0;
    }
}

/* End of file Master_Model.php */
/* Location: ./application/models/Master_Model.php */
