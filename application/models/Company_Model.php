<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Company_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function view_kota($provinsi = '', $search = '')
    {
        return $this->db->query("
			SELECT *
			FROM ms_kota 
			WHERE id_provinsi = '$provinsi' 
			AND kota LIKE '%$search%'")->result();
    }

    function json_company($draw = 1, $start = 0, $length = 0, $search = '', $column = '', $dir = '')
    {
        $start = $this->db->escape_str($start);
        $length = $this->db->escape_str($length);
        $column = $this->db->escape_str($column);
        $dir = $this->db->escape_str($dir);
        $search = $this->db->escape_str($search);

        $total_filtered = $this->total_company($search);
        $data = [];
        $request = $this->view_company($start, $length, $search, $column, $dir);
        if (! empty($request)) {
            $no = $start + 1;
            foreach ($request as $row) {
                $btn_edit = btn_edit($row->id);
                $btn_delete = btn_delete($row->id);
                $images = '';
                if ($row->foto != '') {
                    $images = '<img src="'.base_url().'assets/uploads/images/company/'.$row->foto.'" width="48">';
                }

                $data[] = array(
                    $no++,
                    $row->kode_perusahaan,
                    $row->company,
                    $row->email,
                    $row->telpon,
                    $row->provinsi,
                    $row->kota,
                    $row->alamat,
                    $images,
                    $row->keterangan,
                    btn_group([$btn_edit, $btn_delete])
                );
            }
        }

        return response_datatable($draw, $total_filtered, $data);
    }

    function view_company($start = 0, $length = 0, $search = '', $column = '', $dir = '')
    {
        $kolom = ['a.company', 'a.email', 'a.telpon', 'b.provinsi', 'c.kota', 'a.alamat', 'a.keterangan', 'a.kode_perusahaan'];
        $condition = '';
        if ($search != '') {
            $condition .= ' AND (';
            for ($i = 0; $i < count($kolom); $i++) {
                $condition .= $kolom[$i]." LIKE '%$search%' ";
                if ($kolom[$i] != end($kolom)) {
                    $condition .= ' OR ';
                }
            }
            $condition .= ')';
        }

        $kolom_order = ['1' => 'a.kode_perusahaan', '2' => 'a.company', '3' => 'a.email', '4' => 'a.telpon', '5' => 'b.provinsi', '6' => 'c.kota', '7' => 'a.alamat', '9' => 'keterangan'];
        $order = '';
        if ($column != '' && $dir != '') {
            $col = isset($kolom_order[$column]) ? $kolom_order[$column] : '';

            if ($col != '') {
                $order .= " ORDER BY $col $dir ";
            }
        }

        $query = $this->db->query("
			SELECT a.*, b.`provinsi`, c.`kota`
			FROM ms_company a
			INNER JOIN ms_provinsi b ON a.`id_provinsi` = b.`id`
			INNER JOIN ms_kota c ON a.`id_kota` = c.`id`
			WHERE a.status = 1
			$condition 
			$order 
			LIMIT $start, $length ")->result();

        return $query;
    }

    function total_company($search = '')
    {
        $kolom = ['a.company', 'a.email', 'a.telpon', 'b.provinsi', 'c.kota', 'a.alamat', 'a.keterangan', 'a.kode_perusahaan'];
        $condition = '';
        if ($search != '') {
            $condition .= ' AND (';
            for ($i = 0; $i < count($kolom); $i++) {
                $condition .= $kolom[$i]." LIKE '%$search%' ";
                if ($kolom[$i] != end($kolom)) {
                    $condition .= ' OR ';
                }
            }
            $condition .= ')';
        }

        $query = $this->db->query("
			SELECT COUNT(*) AS jumlah
			FROM ms_company a
			INNER JOIN ms_provinsi b ON a.`id_provinsi` = b.`id`
			INNER JOIN ms_kota c ON a.`id_kota` = c.`id`
			WHERE a.status = 1
			$condition ")->row();

        return isset($query->jumlah) ? $query->jumlah : 0;
    }

    function kode_perusahaan()
    {
        $random_string = random_perusahaan();

        $exists = $this->db->where('kode_perusahaan', $random_string)
                    ->get('ms_company')
                    ->row();

        if (empty($exists)) {
            return $random_string;
        } else {
            return $this->kode_perusahaan();
        }
    }

    function add_company($data = [], $condition = [], $username = '')
    {
        $result = false;
        $this->db->trans_begin();
        $user_insert = username();

        if ($condition) {
            $this->db->where($condition)
                ->update('ms_company', $data);
        } else {
            $this->db->insert('ms_company', $data);
            $id_company = $this->db->insert_id();
            $company = isset($data['company']) ? $data['company'] : '';

            $password = generate_random(4);
            $key = generate_random(4);

            # do create user
            $this->db->query("
                INSERT INTO tb_user (`level`, `id_company`, `id_karyawan`, `username`, `password`, `key`, `profile_name`, `user_insert`)
                VALUES ('2', '$id_company', '', '$username', AES_ENCRYPT('$password', '$key'), '$key', '$company', '$user_insert')");
        }

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();

            $result = false;
        } else {
            $result = true;
            $this->db->trans_commit();
        }

        return ['status' => $result, 'id_company' => $id_company];
    }

    function copy_libur($id_company = '')
    {
        $username = username();
        $this->db->query("
            INSERT INTO ms_libur (`id_company`, `tgl`, `keterangan`, `user_insert`)
            (SELECT '$id_company', tgl, keterangan, '$username'
            FROM ms_libur 
            WHERE YEAR(tgl) = YEAR(NOW()))");
    }
}

/* End of file Company_Model.php */
/* Location: ./application/models/Company_Model.php */
