<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Rest_News_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function view_news($id = '')
    {
        $condition = '';
        if ($id != '') {
            $condition = " WHERE id = '$id' ";
        }

        if ($id != '') {
            return $this->db->query("
	    		SELECT *
	    		FROM tb_news a 
	    		$condition ")->row();
        } else {
            return $this->db->query("
	    		SELECT *
	    		FROM tb_news a 
	    		ORDER BY a.insert_at DESC ")->result();
        }
    }
}

/* End of file Rest_News_Model.php */
/* Location: ./application/models/Rest_News_Model.php */
