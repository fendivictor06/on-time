<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Kunjungan_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function view_lokasi($search = '')
    {
        $search = $this->db->escape_str($search);
        $company = company_id();
        $level = level_user();

        $condition = '';
        if ($level == 2) {
            $condition .= " AND a.id_company = '$company' ";
        }

        if ($search != '') {
            $condition .= " AND a.nama LIKE '%$search%' ";
        }

        return $this->db->query("
    		SELECT a.id, a.nama AS text
    		FROM ms_lokasi a 
    		WHERE a.status = 1
    		$condition ")->result();
    }

    function view_karyawan($search = '', $company = '')
    {
        $search = $this->db->escape_str($search);
        $company = $this->db->escape_str($company);
        $level = level_user();

        $condition = '';
        if ($search != '') {
            $condition .= " AND a.nama LIKE '%$search%' ";
        }

        return $this->db->query("
    		SELECT a.id, a.nama AS text
    		FROM ms_karyawan a 
    		WHERE a.tgl_resign IS NULL
    		AND a.id_company = '$company'
    		$condition 
    		ORDER BY a.nama ASC
    		LIMIT 100 ")->result();
    }

    function json_kunjungan_tgl($draw = 1, $start = 0, $length = 0, $search = '', $column = '', $dir = '', $company = '', $datestart = '', $dateend = '')
    {
        $start = $this->db->escape_str($start);
        $length = $this->db->escape_str($length);
        $column = $this->db->escape_str($column);
        $dir = $this->db->escape_str($dir);
        $search = $this->db->escape_str($search);
        $datestart = $this->db->escape_str($datestart);
        $dateend = $this->db->escape_str($dateend);
        $company = $this->db->escape_str($company);

        $datestart = convert_tgl($datestart);
        $dateend = convert_tgl($dateend);

        $total_filtered = $this->total_kunjungan_tgl($search, $company, $datestart, $dateend);
        $data = [];
        $request = $this->view_kunjungan_tgl($start, $length, $search, $column, $dir, $company, $datestart, $dateend);
        if (! empty($request)) {
            $no = $start + 1;
            foreach ($request as $row) {
                $btn_edit = btn_custom($row->id, 'edit-tgl', 'javascript:;', 'btn-warning', 'fa fa-pencil', 'Edit Data');
                $btn_delete = btn_custom($row->id, 'delete-tgl', 'javascript:;', 'btn-danger', 'fa fa-trash', 'Delete Data');

                $data[] = array(
                    $no++,
                    $row->company,
                    $row->karyawan,
                    $row->tanggal,
                    $row->lokasi,
                    $row->keterangan,
                    btn_group([$btn_edit, $btn_delete])
                );
            }
        }

        return response_datatable($draw, $total_filtered, $data);
    }

    function view_kunjungan_tgl($start = 0, $length = 0, $search = '', $column = '', $dir = '', $company = '', $datestart = '', $dateend = '')
    {
        $kolom = ['company', 'karyawan', 'tanggal', 'lokasi', 'keterangan'];
        $condition = search_datatable($kolom, $search);

        $kolom_order = ['1' => 'company', '2' => 'karyawan', '3' => 'tgl', '4' => 'lokasi', '5' => 'keterangan'];
        $order = order_datatable($kolom_order, $column, $dir);

        if ($company != '') {
            $condition .= " AND id_company = '$company' ";
        }

        if ($datestart != '' && $dateend != '') {
            $condition .= " AND tgl BETWEEN '$datestart' AND '$dateend' ";
        }

        $query = $this->db->query("
            SELECT *
			FROM kunjungan_tgl
            WHERE 1 = 1
            $condition 
            $order 
            LIMIT $start, $length ")->result();

        return $query;
    }

    function total_kunjungan_tgl($search = '', $company = '', $datestart = '', $dateend = '')
    {
        $kolom = ['company', 'karyawan', 'tanggal', 'lokasi', 'keterangan'];
        $condition = search_datatable($kolom, $search);

        if ($company != '') {
            $condition .= " AND id_company = '$company' ";
        }

        if ($datestart != '' && $dateend != '') {
            $condition .= " AND tgl BETWEEN '$datestart' AND '$dateend' ";
        }

        $query = $this->db->query("
            SELECT COUNT(id) AS jumlah
			FROM kunjungan_tgl
            WHERE 1 = 1
            $condition ")->row();

        return isset($query->jumlah) ? $query->jumlah : 0;
    }

    function json_kunjungan_hari($draw = 1, $start = 0, $length = 0, $search = '', $column = '', $dir = '', $company = '', $hari = '')
    {
        $start = $this->db->escape_str($start);
        $length = $this->db->escape_str($length);
        $column = $this->db->escape_str($column);
        $dir = $this->db->escape_str($dir);
        $search = $this->db->escape_str($search);
        $company = $this->db->escape_str($company);
        $hari = $this->db->escape_str($hari);

        $total_filtered = $this->total_kunjungan_hari($search, $company, $hari);
        $data = [];
        $request = $this->view_kunjungan_hari($start, $length, $search, $column, $dir, $company, $hari);
        if (! empty($request)) {
            $no = $start + 1;
            foreach ($request as $row) {
                $btn_edit = btn_custom($row->id, 'edit-hari', 'javascript:;', 'btn-warning', 'fa fa-pencil', 'Edit Data');
                $btn_delete = btn_custom($row->id, 'delete-hari', 'javascript:;', 'btn-danger', 'fa fa-trash', 'Delete Data');

                $data[] = array(
                    $no++,
                    $row->company,
                    $row->karyawan,
                    $row->jadwal,
                    $row->lokasi,
                    $row->keterangan,
                    btn_group([$btn_edit, $btn_delete])
                );
            }
        }

        return response_datatable($draw, $total_filtered, $data);
    }

    function view_kunjungan_hari($start = 0, $length = 0, $search = '', $column = '', $dir = '', $company = '', $hari = '')
    {
        $kolom = ['company', 'karyawan', 'jadwal', 'lokasi', 'keterangan'];
        $condition = search_datatable($kolom, $search);

        $kolom_order = ['1' => 'company', '2' => 'karyawan', '3' => 'hari', '4' => 'lokasi', '5' => 'keterangan'];
        $order = order_datatable($kolom_order, $column, $dir);

        if ($company != '') {
            $condition .= " AND id_company = '$company' ";
        }

        if ($hari != '') {
            $condition .= " AND hari = '$hari' ";
        }

        $query = $this->db->query("
            SELECT *
			FROM kunjungan_hari
            WHERE 1 = 1
            $condition 
            $order 
            LIMIT $start, $length ")->result();

        return $query;
    }

    function total_kunjungan_hari($search = '', $company = '', $hari = '')
    {
        $kolom = ['company', 'karyawan', 'jadwal', 'lokasi', 'keterangan'];
        $condition = search_datatable($kolom, $search);

        if ($company != '') {
            $condition .= " AND id_company = '$company' ";
        }

        if ($hari != '') {
            $condition .= " AND hari = '$hari' ";
        }

        $query = $this->db->query("
            SELECT COUNT(id) AS jumlah
			FROM kunjungan_hari
            WHERE 1 = 1
            $condition ")->row();

        return isset($query->jumlah) ? $query->jumlah : 0;
    }
}

/* End of file Kunjungan_Model.php */
/* Location: ./application/models/Kunjungan_Model.php */
