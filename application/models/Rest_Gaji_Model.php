<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Rest_Gaji_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function view_payroll($date = '', $bln = '', $thn = '', $flag = 0)
    {
        $id_karyawan = id_karyawan();
        if ($flag = 1) {
            if ($date != '') {
                $query = $this->db->query("
            		SELECT *
            		FROM tb_payroll a 
            		WHERE a.bulan = MONTH('$date')
            		AND a.tahun = YEAR('$date') 
                    AND a.id_karyawan = '$id_karyawan' ")->row();
            } else {
                $query = $this->db->query("
            		SELECT *
            		FROM tb_payroll a 
            		WHERE a.bulan = '$bln'
            		AND a.tahun = '$thn' 
                    AND a.id_karyawan = '$id_karyawan' ")->row();
            }
        } else {
            $query = $this->db->query("
                SELECT salary AS nominal
                FROM ms_karyawan a 
                WHERE a.id = '$id_karyawan'")->row();
        }

        return $query;
    }
}

/* End of file Rest_Gaji_Model.php */
/* Location: ./application/models/Rest_Gaji_Model.php */
