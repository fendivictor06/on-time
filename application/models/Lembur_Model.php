<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Lembur_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->helper('custom_money');
    }

    function json_lembur($draw = 1, $start = 0, $length = 0, $search = '', $column = '', $dir = '', $datestart = '', $dateend = '')
    {
        $start = $this->db->escape_str($start);
        $length = $this->db->escape_str($length);
        $column = $this->db->escape_str($column);
        $dir = $this->db->escape_str($dir);
        $search = $this->db->escape_str($search);

        $datestart = $this->db->escape_str($datestart);
        $dateend = $this->db->escape_str($dateend);

        $datestart = convert_tgl($datestart);
        $dateend = convert_tgl($dateend);

        $total_filtered = $this->total_lembur($search, $datestart, $dateend);
        $data = [];
        $request = $this->view_lembur($start, $length, $search, $column, $dir, $datestart, $dateend);
        if (! empty($request)) {
            $no = $start + 1;
            foreach ($request as $row) {
                $btn_edit = btn_edit($row->id);
                $btn_delete = btn_delete($row->id);

                $data[] = array(
                    $no++,
                    $row->company,
                    $row->nama,
                    $row->mulai,
                    $row->selesai,
                    // uang($row->nominal),
                    $row->keterangan,
                    btn_group([$btn_edit, $btn_delete])
                );
            }
        }

        return response_datatable($draw, $total_filtered, $data);
    }

    function view_lembur($start = 0, $length = 0, $search = '', $column = '', $dir = '', $datestart = '', $dateend = '')
    {
        // $kolom = ['company', 'nama', 'mulai', 'selesai', 'nominal', 'keterangan'];
        $kolom = ['company', 'nama', 'mulai', 'selesai', 'keterangan'];
        $condition = search_datatable($kolom, $search);

        // $kolom_order = ['1' => 'company', '2' => 'nama', '3' => 'mulai', '4' => 'selesai', '5' => 'nominal', '6' => 'keterangan'];
        $kolom_order = ['1' => 'company', '2' => 'nama', '3' => 'mulai', '4' => 'selesai', '5' => 'keterangan'];
        $order = order_datatable($kolom_order, $column, $dir);

        $company = company_id();
        $where = '';
        if ($company != '') {
            $where = " AND id_company = '$company' ";
        }

        if ($datestart != '' && $dateend != '') {
            $where .= " AND ((DATE(awal) BETWEEN '$datestart' AND '$dateend') OR (DATE(akhir) BETWEEN '$datestart' AND '$dateend')) ";
        }

        $query = $this->db->query("
        	SELECT *
			FROM view_lembur 
			WHERE 1 = 1
            $where
			$condition 
			$order 
			LIMIT $start, $length ")->result();

        return $query;
    }

    function total_lembur($search = '', $datestart = '', $dateend = '')
    {
        // $kolom = ['company', 'nama', 'mulai', 'selesai', 'nominal', 'keterangan'];
        $kolom = ['company', 'nama', 'mulai', 'selesai', 'keterangan'];
        $condition = search_datatable($kolom, $search);

        $company = company_id();
        $where = '';
        if ($company != '') {
            $where = " AND id_company = '$company' ";
        }

        if ($datestart != '' && $dateend != '') {
            $where .= " AND ((DATE(awal) BETWEEN '$datestart' AND '$dateend') OR (DATE(akhir) BETWEEN '$datestart' AND '$dateend')) ";
        }

        $query = $this->db->query("
        	SELECT COUNT(*) AS jumlah
			FROM view_lembur 
			WHERE 1 = 1
            $where
			$condition ")->row();

        return isset($query->jumlah) ? $query->jumlah : 0;
    }

    function lembur_id($id = '')
    {
        return $this->db->query("
            SELECT a.id, a.id_company, a.id_karyawan,
            DATE_FORMAT(a.awal, '%d/%m/%Y %H:%i') AS awal,
            DATE_FORMAT(a.akhir, '%d/%m/%Y %H:%i') AS akhir,
            a.nominal, a.keterangan
            FROM tb_lembur a 
            WHERE a.id = '$id' ")->row();
    }
}

/* End of file Lembur_Model.php */
/* Location: ./application/models/Lembur_Model.php */
