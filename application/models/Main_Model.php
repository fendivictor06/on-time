<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Main_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function view_company()
    {
        $company = company_id();

        $condition = '';
        if ($company != '') {
            $condition = " AND id = '$company' ";
        }

        $query = $this->db->query("
    		SELECT *
    		FROM ms_company a 
    		WHERE a.status = 1
    		$condition ")->result();

        return $query;
    }

    function list_company()
    {
        $data = $this->view_company();

        $result = [];
        if ($data) {
            foreach ($data as $row) {
                $result[$row->id] = $row->company;
            }
        }

        return $result;
    }

    function jumlah_perusahaan()
    {
        $query = $this->db->query("
            SELECT COUNT(id) AS jumlah 
            FROM ms_company a
            WHERE a.`status` = 1 ")->row();

        return isset($query->jumlah) ? $query->jumlah : 0;
    }

    function karyawan_aktif()
    {
        $id_company = company_id();
        $query = $this->db->query("
            SELECT COUNT(id) AS jumlah
            FROM ms_karyawan 
            WHERE id_company = '$id_company'
            AND (tgl_resign IS NULL OR tgl_resign = '')")->row();

        return isset($query->jumlah) ? $query->jumlah : 0;
    }

    function karyawan_baru()
    {
        $id_company = company_id();
        $query = $this->db->query("
            SELECT COUNT(id) AS jumlah
            FROM ms_karyawan 
            WHERE id_company = '$id_company'
            AND MONTH(tgl_masuk) = MONTH(CURDATE())")->row();

        return isset($query->jumlah) ? $query->jumlah : 0;
    }

    function karyawan_resign()
    {
        $id_company = company_id();
        $query = $this->db->query("
            SELECT COUNT(id) AS jumlah
            FROM ms_karyawan 
            WHERE id_company = '$id_company'
            AND MONTH(tgl_resign) = MONTH(CURDATE())")->row();

        return isset($query->jumlah) ? $query->jumlah : 0;
    }
}

/* End of file Main_Model.php */
/* Location: ./application/models/Main_Model.php */
