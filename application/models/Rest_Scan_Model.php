<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Rest_Scan_Model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function view_tipe_scan()
    {
        return $this->db->select('kode, keterangan')
                    ->get('ms_tipe_scan')
                    ->result();
    }

    function add_scanlog($tipe_scan = 1, $foto = '', $latitude = '', $longitude = '', $id_lokasi = '')
    {
        $id_company = id_company();
        $id_karyawan = id_karyawan();
        $user_id = user_id();

        $result = 0;
        if ($tipe_scan != '' && $id_company != '' && $id_karyawan != '' && $user_id != '') {
            $this->db->query("
    			INSERT INTO tb_scanlog(id_company, id_karyawan, id_scan, scan_date, foto, latitude, longitude, id_lokasi, insert_at, user_insert) 
    			VALUES ('$id_company', '$id_karyawan', '$tipe_scan', NOW(), '$foto', '$latitude', '$longitude', '$id_lokasi', NOW(), '$user_id')");

            $result = $this->db->insert_id();
        }

        return $result;
    }

    function view_scanlog($date = '')
    {
        $id_karyawan = id_karyawan();
        $date = $this->db->escape_str($date);

        if ($date == '') {
            $date = date('Y-m-d');
        }

        return $this->db->query("
            SELECT *
            FROM view_scanlog a
            WHERE a.`id_karyawan` = '$id_karyawan'
            AND DATE(a.scan_date) = '$date'")->result();
    }

    function validasi_imei($imei = [])
    {
        $setting_imei = setting_imei();
        $id_karyawan = id_karyawan();
        $id_company = id_company();

        $status = false;
        if ($setting_imei == 1) {
            if ($imei) {
                for ($i = 0; $i < count($imei); $i++) {
                    $im = isset($imei[$i]) ? $imei[$i] : '';
                    $tb_imei = $this->db->where('imei', $im)
                                    ->where('id_karyawan', $id_karyawan)
                                    ->where('id_company', $id_company)
                                    ->get('tb_imei')
                                    ->row();

                    if ($tb_imei) {
                        $status = true;
                    } else {
                        $status = false;
                    }
                }
            } else {
                $status = false;
            }
        } else {
            $status = true;
        }

        return $status;
    }

    function validasi_jaringan($macaddress = '', $ippublic = '')
    {
        $id_company = id_company();
        $setting_mac = setting_mac();

        if ($setting_mac == 1) {
            $ms_akses_point = $this->db->where('id_company', $id_company)
                                ->where('macaddress', $macaddress)
                                ->where('ippublic', $ippublic)
                                ->get('ms_akses_point')
                                ->row();

            if ($ms_akses_point) {
                $status = true;
            } else {
                $status = false;
            }
        } else {
            $status = true;
        }

        return $status;
    }

    function kunjungan_tgl($id_karyawan = '', $id_company = '', $tgl = '')
    {
        return $this->db->query("
            SELECT a.*, b.toleransi
            FROM ms_kunjungan a
            INNER JOIN ms_lokasi b ON a.id_lokasi = b.id
            WHERE a.`tgl` = '$tgl'
            AND a.`id_company` = '$id_company'
            AND a.`id_karyawan` = '$id_karyawan' ")->row();
    }

    function kunjungan_hari($id_karyawan = '', $id_company = '', $tgl = '')
    {
        return $this->db->query("
            SELECT a.*, b.toleransi
            FROM ms_kunjungan_hari a
            INNER JOIN ms_lokasi b ON a.id_lokasi = b.id
            WHERE a.`id_company` = '$id_company'
            AND a.`id_karyawan` = '$id_karyawan'
            AND a.`hari` = WEEKDAY('$tgl') ")->row();
    }
}

/* End of file Rest_Scan_Model.php */
/* Location: ./application/models/Rest_Scan_Model.php */
