<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Menu_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function view_menu($parent = 0)
    {
        $level = level_user();
        return $this->db->query("
			SELECT *, IFNULL(jumlah_menu.jumlah, 0) AS hitung
			FROM tb_menu a
			LEFT JOIN (
				SELECT parent, COUNT(*) AS jumlah
				FROM tb_menu
				GROUP BY parent
			) AS jumlah_menu ON a.id = jumlah_menu.parent
			WHERE a.`level` = '$level'
			AND a.parent = '$parent'
			AND a.status = 1
			ORDER BY a.urutan ASC")->result();
    }

    function parent($class = '', $method = '')
    {
        $level = level_user();

        return $this->db->where('fungsi', $class)
                ->where('method', $method)
                ->where('level', $level)
                ->get('tb_menu')
                ->row();
    }

    function create_menu($parent = 0, $level = 0)
    {
        $menu = $this->view_menu($parent);
        $class = $this->router->fetch_class();
        $method = $this->router->fetch_method();

        $this_menu = $this->parent($class, $method);
        $this_parent = isset($this_menu->parent) ? $this_menu->parent : '';

        $return = '';
        if (! empty($menu)) {
            foreach ($menu as $row) {
                $active = '';
                $expanded = '';
                if ($class == $row->fungsi && $method == $row->method) {
                    $active = 'm-menu__item--active';
                }

                $url = '#';
                if ($row->url != '') {
                    $url = base_url().$row->url;
                }

                if ($level == 0) {
                    if ($row->hitung > 0) {
                        if ($row->id == $this_parent) {
                            $expanded = 'm-menu__item--open m-menu__item--expanded';
                        }

                        $return .= '<li class="m-menu__item  m-menu__item--submenu '.$expanded.'" aria-haspopup="true"  data-menu-submenu-toggle="hover">';
                        $return .= '<a  href="'.$url.'" class="m-menu__link m-menu__toggle">
										<i class="m-menu__link-icon '.$row->icon.'"></i>
										<span class="m-menu__link-text">
											'.$row->label.'
										</span>
										<i class="m-menu__ver-arrow la la-angle-right"></i>
									</a>';
                        $return .= '<div class="m-menu__submenu">
										<span class="m-menu__arrow"></span>
										<ul class="m-menu__subnav">';
                        $return .= $this->create_menu($row->id, ($level + 1));
                        $return .= '	</ul>
									</div>';
                    } else {
                        $return .= '<li class="m-menu__item '.$active.'" aria-haspopup="true" >';
                        $return .= '<a  href="'.$url.'" class="m-menu__link ">
										<i class="m-menu__link-icon '.$row->icon.'"></i>
										<span class="m-menu__link-title">
											<span class="m-menu__link-wrap">
												<span class="m-menu__link-text">
													'.$row->label.'
												</span>
											</span>
										</span>
									</a>';
                    }

                    $return .= '</li>';
                } else {
                    $return .= '<li class="m-menu__item '.$active.'" aria-haspopup="true" >
									<a  href="'.$url.'" class="m-menu__link ">
										<i class="m-menu__link-bullet m-menu__link-bullet--dot">
											<span></span>
										</i>
										<span class="m-menu__link-text">
											'.$row->label.'
										</span>
									</a>
								</li>';
                }
            }
        }

        return $return;
        ;
    }
}

/* End of file Menu_Model.php */
/* Location: ./application/models/Menu_Model.php */
