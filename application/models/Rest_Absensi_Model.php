<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Rest_Absensi_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function view_absensi($datestart = '', $dateend = '')
    {
        $id_karyawan = id_karyawan();

        return $this->db->query("
    		SELECT *
			FROM view_absensi 
			WHERE tgl BETWEEN '$datestart' AND '$dateend' 
			AND id_karyawan = '$id_karyawan' ")->result();
    }
}

/* End of file Rest_Absensi_Model.php */
/* Location: ./application/models/Rest_Absensi_Model.php */
