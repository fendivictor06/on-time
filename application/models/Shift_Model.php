<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Shift_Model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function json_shift($draw = 1, $start = 0, $length = 0, $search = '', $column = '', $dir = '')
    {
        $start = $this->db->escape_str($start);
        $length = $this->db->escape_str($length);
        $column = $this->db->escape_str($column);
        $dir = $this->db->escape_str($dir);
        $search = $this->db->escape_str($search);

        $total_filtered = $this->total_shift($search);
        $data = [];
        $request = $this->view_shift($start, $length, $search, $column, $dir);
        if (! empty($request)) {
            $no = $start + 1;
            foreach ($request as $row) {
                $btn_edit = btn_edit($row->id);
                $btn_delete = btn_delete($row->id);

                $data[] = array(
                    $no++,
                    $row->company,
                    $row->shift,
                    $row->jam_masuk,
                    $row->mulai_istirahat,
                    $row->selesai_istirahat,
                    $row->jam_pulang,
                    $row->keterangan,
                    btn_group([$btn_edit, $btn_delete])
                );
            }
        }

        return response_datatable($draw, $total_filtered, $data);
    }

    function view_shift($start = 0, $length = 0, $search = '', $column = '', $dir = '')
    {
        $kolom = ['b.company', 'a.shift', 'a.jam_masuk', 'a.jam_pulang', 'a.keterangan', 'a.mulai_istirahat', 'a.selesai_istirahat'];
        $condition = search_datatable($kolom, $search);

        $kolom_order = ['1' => 'b.company', '2' => 'a.shift', '3' => 'a.jam_masuk', '4' => 'a.mulai_istirahat', '5' => 'a.selesai_istirahat', '6' => 'a.jam_pulang', '7' => 'a.keterangan'];
        $order = order_datatable($kolom_order, $column, $dir);

        # id company
        $company = company_id();
        if ($company != '') {
            $condition .= " AND a.id_company = '$company' ";
        }

        $query = $this->db->query("
        	SELECT a.*, b.`company`
			FROM ms_shift a
			INNER JOIN ms_company b ON a.`id_company` = b.`id`
			WHERE a.status = 1
			$condition 
			$order 
			LIMIT $start, $length ")->result();

        return $query;
    }

    function total_shift($search = '')
    {
        $kolom = ['b.company', 'a.shift', 'a.jam_masuk', 'a.jam_pulang', 'a.keterangan', 'a.mulai_istirahat', 'a.selesai_istirahat'];
        $condition = search_datatable($kolom, $search);

        # id company
        $company = company_id();
        if ($company != '') {
            $condition .= " AND a.id_company = '$company' ";
        }

        $query = $this->db->query("
        	SELECT COUNT(*) AS jumlah 
        	FROM ms_shift a
			INNER JOIN ms_company b ON a.`id_company` = b.`id`
			WHERE a.status = 1
			$condition ")->row();

        return isset($query->jumlah) ? $query->jumlah : 0;
    }

    function json_libur($draw = 1, $start = 0, $length = 0, $search = '', $column = '', $dir = '')
    {
        $start = $this->db->escape_str($start);
        $length = $this->db->escape_str($length);
        $column = $this->db->escape_str($column);
        $dir = $this->db->escape_str($dir);
        $search = $this->db->escape_str($search);

        $total_filtered = $this->total_libur($search);
        $data = [];
        $request = $this->view_libur($start, $length, $search, $column, $dir);
        if (! empty($request)) {
            $no = $start + 1;
            foreach ($request as $row) {
                $btn_edit = btn_edit($row->id);
                $btn_delete = btn_delete($row->id);

                $data[] = array(
                    $no++,
                    // $row->company,
                    tanggal($row->tgl),
                    $row->keterangan,
                    btn_group([$btn_edit, $btn_delete])
                );
            }
        }

        return response_datatable($draw, $total_filtered, $data);
    }

    function view_libur($start = 0, $length = 0, $search = '', $column = '', $dir = '')
    {
        $kolom = ['a.tgl', 'a.keterangan'];
        $condition = search_datatable($kolom, $search);

        $kolom_order = ['1' => 'a.tgl', '2' => 'a.keterangan'];
        $order = order_datatable($kolom_order, $column, $dir);

        # id company
        $company = company_id();
        if ($company != '') {
            $condition .= " AND a.id_company = '$company' ";
        }

        $query = $this->db->query("
        	SELECT a.*, b.`company`
			FROM ms_libur a
			LEFT JOIN ms_company b ON a.`id_company` = b.`id`
			WHERE 1 = 1
			$condition 
			$order 
			LIMIT $start, $length ")->result();

        return $query;
    }

    function total_libur($search = '')
    {
        $kolom = ['a.tgl', 'a.keterangan'];
        $condition = search_datatable($kolom, $search);

        # id company
        $company = company_id();
        if ($company != '') {
            $condition .= " AND a.id_company = '$company' ";
        }

        $query = $this->db->query("
        	SELECT COUNT(*) AS jumlah 
        	FROM ms_libur a
			LEFT JOIN ms_company b ON a.`id_company` = b.`id`
			WHERE 1 = 1
			$condition ")->row();

        return isset($query->jumlah) ? $query->jumlah : 0;
    }

    function json_penjadwalan($draw = 1, $start = 0, $length = 0, $search = '', $column = '', $dir = '', $datestart = '', $dateend = '', $company = '', $karyawan = '')
    {
        $start = $this->db->escape_str($start);
        $length = $this->db->escape_str($length);
        $column = $this->db->escape_str($column);
        $dir = $this->db->escape_str($dir);
        $search = $this->db->escape_str($search);

        $datestart = $this->db->escape_str($datestart);
        $dateend = $this->db->escape_str($dateend);
        $company = $this->db->escape_str($company);
        $karyawan = $this->db->escape_str($karyawan);

        $datestart = convert_tgl($datestart);
        $dateend = convert_tgl($dateend);

        $total_filtered = $this->total_penjadwalan($search, $datestart, $dateend, $company, $karyawan);
        $data = [];
        $request = $this->view_penjadwalan($start, $length, $search, $column, $dir, $datestart, $dateend, $company, $karyawan);
        if (! empty($request)) {
            $no = $start + 1;
            foreach ($request as $row) {
                $btn_edit = btn_edit($row->id);
                $btn_delete = btn_delete($row->id);

                $data[] = array(
                    $no++,
                    $row->company,
                    $row->nama,
                    tanggal($row->tgl),
                    $row->shift,
                    $row->jam_masuk,
                    $row->jam_pulang,
                    $row->keterangan,
                    btn_group([$btn_edit, $btn_delete])
                );
            }
        }

        return response_datatable($draw, $total_filtered, $data);
    }

    function view_penjadwalan($start = 0, $length = 0, $search = '', $column = '', $dir = '', $datestart = '', $dateend = '', $company = '', $karyawan = '')
    {
        $kolom = ['b.company', 'c.nama', 'a.tgl', 'a.shift', 'a.jam_masuk', 'a.jam_pulang', 'a.keterangan'];
        $condition = search_datatable($kolom, $search);

        $kolom_order = ['1' => 'b.company', '2' => 'c.nama', '3' => 'a.tgl', '4' => 'a.shift', '5' => 'a.jam_masuk', '6' => 'a.jam_pulang', '7' => 'a.keterangan'];
        $order = order_datatable($kolom_order, $column, $dir);

        if ($datestart != '' && $dateend != '') {
            $condition .= " AND a.tgl BETWEEN '$datestart' AND '$dateend' ";
        }

        if ($company != '' && $company != 'null') {
            $condition .= " AND a.id_company = '$company' ";
        }

        if ($karyawan != '' && $karyawan != 'null') {
            $condition .= " AND a.id_karyawan = '$karyawan' ";
        }

        $query = $this->db->query("
        	SELECT a.*, b.`company`, c.`nama`
			FROM tb_jadwal a
			INNER JOIN ms_company b ON a.`id_company` = b.`id`
			INNER JOIN ms_karyawan c ON c.`id` = a.`id_karyawan`
			WHERE 1 = 1
			$condition 
			$order 
			LIMIT $start, $length ")->result();

        return $query;
    }

    function total_penjadwalan($search = '', $datestart = '', $dateend = '', $company = '', $karyawan = '')
    {
        $kolom = ['b.company', 'c.nama', 'a.tgl', 'a.shift', 'a.jam_masuk', 'a.jam_pulang', 'a.keterangan'];
        $condition = search_datatable($kolom, $search);

        if ($datestart != '' && $dateend != '') {
            $condition .= " AND a.tgl BETWEEN '$datestart' AND '$dateend' ";
        }

        if ($company != '' && $company != 'null') {
            $condition .= " AND a.id_company = '$company' ";
        }

        if ($karyawan != '' && $karyawan != 'null') {
            $condition .= " AND a.id_karyawan = '$karyawan' ";
        }

        $query = $this->db->query("
        	SELECT COUNT(*) AS jumlah
			FROM tb_jadwal a
			INNER JOIN ms_company b ON a.`id_company` = b.`id`
			INNER JOIN ms_karyawan c ON c.`id` = a.`id_karyawan`
			WHERE 1 = 1
			$condition ")->row();

        return isset($query->jumlah) ? $query->jumlah : 0;
    }

    function penjadwalan_id($id = '')
    {
        return $this->db->query("
    		SELECT *, DATE_FORMAT(a.tgl, '%d/%m/%Y') AS tanggal
    		FROM tb_jadwal a 
    		WHERE a.id = '$id' ")->row();
    }

    function simpan_jadwal($string_query = '', $tgl_mulai = '', $tgl_selesai = '', $karyawan = '')
    {
        if ($string_query != '' && $tgl_mulai != '' && $tgl_selesai != '' && $karyawan != '') {
            $this->db->trans_begin();

            $this->db->query("
                DELETE FROM tb_jadwal 
                WHERE id_karyawan = '$karyawan' 
                AND tgl BETWEEN '$tgl_mulai' AND '$tgl_selesai'");

            $this->db->query("
                INSERT INTO tb_jadwal (`id_company`, `id_shift`, `id_karyawan`, `tgl`, `jam_masuk`, `jam_pulang`, `shift`, `keterangan`, `user_insert`, `mulai_istirahat`, `selesai_istirahat`) VALUES $string_query ");

            if ($this->db->trans_status() === false) {
                $this->db->trans_rollback();

                $result = 0;
            } else {
                $this->db->trans_commit();

                $result = 1;
            }
        } else {
            $result = 0;
        }

        return $result;
    }

    function salin_jadwal($id_karyawan = '', $tgl_mulai = '', $tgl_selesai = '', $target = [])
    {
        $user_insert = username();
        if ($id_karyawan != '' && $tgl_mulai != '' && $tgl_selesai != '' && ! empty($target)) {
            $this->db->trans_begin();

            for ($i = 0; $i < count($target); $i++) {
                $karyawan = isset($target[$i]) ? $target[$i] : '';

                if ($karyawan != '') {
                    $this->db->query("
                        INSERT INTO tb_jadwal (id_company, id_shift, id_karyawan, tgl, jam_masuk, jam_pulang, mulai_istirahat, selesai_istirahat, shift, keterangan, user_insert)
                        (
                        SELECT a.id_company, a.id_shift, '$karyawan', a.tgl, a.jam_masuk,
                        a.jam_pulang, a.mulai_istirahat, a.selesai_istirahat, a.shift, a.keterangan,
                        '$user_insert'
                        FROM tb_jadwal a
                        WHERE a.`id_karyawan` = '$id_karyawan'
                        AND a.`tgl` BETWEEN '$tgl_mulai' AND '$tgl_selesai')");
                }
            }

            if ($this->db->trans_status() === false) {
                $this->db->trans_rollback();

                $result = 0;
            } else {
                $this->db->trans_commit();

                $result = 1;
            }
        } else {
            $result = 0;
        }

        return $result;
    }
}

/* End of file Shift_Model.php */
/* Location: ./application/models/Shift_Model.php */
