<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Rest_Cuti_Model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function kuota_cuti($datestart = '', $dateend = '')
    {
        $id_karyawan = id_karyawan();

        $query = $this->db->query("
    		SELECT *
    		FROM ms_kuota_cuti a 
    		WHERE a.id_karyawan = '$id_karyawan'
    		AND '$datestart' BETWEEN awal AND akhir 
    		AND '$dateend' BETWEEN awal AND akhir ")->row();

        $kuota = isset($query->kuota) ? $query->kuota : 0;

        return $kuota;
    }

    function is_libur($date = '')
    {
        $id_karyawan = id_karyawan();

        $query = $this->db->query("
    		SELECT * 
    		FROM tb_jadwal a 
    		WHERE a.id_karyawan = '$id_karyawan'
    		AND a.tgl = '$date' ")->row();

        return $query;
    }

    function hitung_cuti($datestart = '', $dateend = '')
    {
        $hitung = 0;
        while (strtotime($datestart) <= strtotime($dateend)) {
            $is_libur = $this->is_libur($datestart);
            if (empty($is_libur)) {
                $hitung = $hitung + 1;
            }

            $datestart = date("Y-m-d", strtotime("+1 day", strtotime($datestart)));
        }

        return $hitung;
    }

    function tanggal_cuti($datestart = '', $dateend = '')
    {
        $tanggal = [];
        while (strtotime($datestart) <= strtotime($dateend)) {
            $is_libur = $this->is_libur($datestart);
            if (empty($is_libur)) {
                $tanggal[] = $datestart;
            }

            $datestart = date("Y-m-d", strtotime("+1 day", strtotime($datestart)));
        }

        return $tanggal;
    }

    function data_detail($date = '')
    {
        $id_karyawan = id_karyawan();
        $id_company = id_company();

        $query = $this->db->query("
    		SELECT *
			FROM detail_cuti a
			WHERE a.`id_company` = '$id_company'
			AND a.`id_karyawan` = '$id_karyawan'
			AND a.`tgl` = '$date' ")->row();

        $tanggal = isset($query->tgl) ? $query->tgl : '';

        return $tanggal;
    }

    function validasi_tanggal($datestart = '', $dateend = '')
    {
        $tanggal = [];
        while (strtotime($datestart) <= strtotime($dateend)) {
            $data_detail = $this->data_detail($datestart);
            if (! empty($data_detail)) {
                $tanggal[] = $datestart;
            }

            $datestart = date("Y-m-d", strtotime("+1 day", strtotime($datestart)));
        }

        return $tanggal;
    }

    function view_cuti($start = 0, $count = 0, $id = '')
    {
        $id_karyawan = id_karyawan();

        $limit = '';
        if ($count > 0) {
            $limit = " LIMIT $start, $count ";
        }

        $condition = '';
        if ($id != '') {
            $condition = " AND a.id = '$id' ";
        }

        return $this->db->query("
    		SELECT *
    		FROM view_cuti a 
    		WHERE a.id_karyawan = '$id_karyawan' 
            $condition
    		ORDER BY a.awal DESC
    		$limit ")->result();
    }

    function view_approval($start = 0, $count = 0, $id = '')
    {
        $id_company = id_company();
        $id_karyawan = id_karyawan();

        $apv_cuti = $this->db->where('id_company', $id_company)
                        ->where('apv_cuti', $id_karyawan)
                        ->get('tb_setting')
                        ->row();

        if (empty($apv_cuti)) {
            return [];
        } else {
            $limit = '';
            if ($count > 0) {
                $limit = " LIMIT $start, $count ";
            }

            $condition = '';
            if ($id != '') {
                $condition = " AND a.id = '$id' ";
            }

            return $this->db->query("
                SELECT *
                FROM view_cuti a 
                WHERE a.status = 1
                $condition
                ORDER BY a.awal DESC
                $limit ")->result();
        }
    }

    function approve_cuti($id = '', $status = '')
    {
        $result = 0;

        $id_company = id_company();
        $id_karyawan = id_karyawan();

        $apv_cuti = $this->db->where('id_company', $id_company)
                        ->where('apv_cuti', $id_karyawan)
                        ->get('tb_setting')
                        ->row();

        if (empty($apv_cuti)) {
            $result = 0;
        } else {
            $update = $this->db->where('id', $id)
                        ->update('tb_cuti', ['status' =>  $status]);

            $result = $this->db->affected_rows();
        }

        return $result;
    }

    function jumlah_cuti()
    {
        $id_karyawan = id_karyawan();
        $tahun = date('Y');

        $query = $this->db->query("SELECT jumlah_cuti('$id_karyawan', '$tahun') AS jumlah")->row();
        $jumlah = isset($query->jumlah) ? $query->jumlah : 0;
        $result = array(
            'jumlah' => $jumlah,
            'tahun' => $tahun
        );

        return $result;
    }
}

/* End of file Rest_Cuti_Model.php */
/* Location: ./application/models/Rest_Cuti_Model.php */
