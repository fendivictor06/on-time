<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Rest_Pulang_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function view_pulang_awal($start = 0, $count = 0, $id = '')
    {
        $id_karyawan = id_karyawan();

        $limit = '';
        if ($count > 0) {
            $limit = " LIMMIT $start, $count ";
        }

        $condition = '';
        if ($id != '') {
            $condition = " WHERE a.id = '$id' ";
        }

        return $this->db->query("
    		SELECT *
    		FROM view_pulang_awal a 
    		$condition
    		ORDER BY a.tgl DESC
    		$limit ")->result();
    }
}

/* End of file Rest_Pulang_Model.php */
/* Location: ./application/models/Rest_Pulang_Model.php */
