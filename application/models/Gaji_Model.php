<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Gaji_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function json_info_gaji($draw = 1, $start = 0, $length = 0, $search = '', $column = '', $dir = '')
    {
        $start = $this->db->escape_str($start);
        $length = $this->db->escape_str($length);
        $column = $this->db->escape_str($column);
        $dir = $this->db->escape_str($dir);
        $search = $this->db->escape_str($search);

        $total_filtered = $this->total_info_gaji($search);
        $data = [];
        $request = $this->view_info_gaji($start, $length, $search, $column, $dir);
        if (! empty($request)) {
            $no = $start + 1;
            foreach ($request as $row) {
                $data[] = array(
                    $no++,
                    $row->company,
                    $row->nama,
                    $row->divisi,
                    $row->jabatan,
                    $row->posisi,
                    uang($row->salary)
                );
            }
        }

        return response_datatable($draw, $total_filtered, $data);
    }

    function view_info_gaji($start = 0, $length = 0, $search = '', $column = '', $dir = '')
    {
        $kolom = ['a.company', 'a.nama', 'a.divisi', 'a.jabatan', 'a.posisi', 'a.salary'];
        $condition = search_datatable($kolom, $search);

        $kolom_order = ['1' => 'a.company', '2' => 'a.nama', '3' => 'a.divisi', '4' => 'a.jabatan', '5' => 'a.posisi', '6' => 'a.salary'];
        $order = order_datatable($kolom_order, $column, $dir);

        $company = company_id();
        $where = '';
        if ($company != '') {
            $where .= " AND a.id_company = '$company' ";
        }

        $query = $this->db->query("
        	SELECT *
			FROM static_payroll a
			WHERE 1 = 1
            $where
			$condition 
			$order 
			LIMIT $start, $length ")->result();

        return $query;
    }

    function total_info_gaji($search = '')
    {
        $kolom = ['a.company', 'a.nama', 'a.divisi', 'a.jabatan', 'a.posisi', 'a.salary'];
        $condition = search_datatable($kolom, $search);

        $company = company_id();
        $where = '';
        if ($company != '') {
            $where .= " AND id_company = '$company' ";
        }

        $query = $this->db->query("
        	SELECT COUNT(*) AS jumlah
			FROM static_payroll a
			WHERE 1 = 1
            $where
			$condition ")->row();

        return isset($query->jumlah) ? $query->jumlah : 0;
    }

    function list_tahun()
    {
        return $this->db->query("
    		SELECT tahun
			FROM upload_payroll a
			GROUP BY a.`tahun`
			ORDER BY a.`tahun` ASC ")->result();
    }

    function json_history_gaji($draw = 1, $start = 0, $length = 0, $search = '', $column = '', $dir = '', $bulan = '', $tahun = '')
    {
        $start = $this->db->escape_str($start);
        $length = $this->db->escape_str($length);
        $column = $this->db->escape_str($column);
        $dir = $this->db->escape_str($dir);
        $search = $this->db->escape_str($search);

        $total_filtered = $this->total_history_gaji($search, $bulan, $tahun);
        $data = [];
        $request = $this->view_history_gaji($start, $length, $search, $column, $dir, $bulan, $tahun);
        if (! empty($request)) {
            $no = $start + 1;
            foreach ($request as $row) {
                $data[] = array(
                    $no++,
                    $row->company,
                    $row->nama,
                    $row->divisi,
                    $row->jabatan,
                    $row->posisi,
                    $row->periode,
                    $row->tahun,
                    uang($row->nominal)
                );
            }
        }

        return response_datatable($draw, $total_filtered, $data);
    }

    function view_history_gaji($start = 0, $length = 0, $search = '', $column = '', $dir = '', $bulan = '', $tahun = '')
    {
        $kolom = ['a.company', 'a.nama', 'a.divisi', 'a.jabatan', 'a.posisi', 'a.periode', 'a.tahun', 'a.nominal'];
        $condition = search_datatable($kolom, $search);

        $kolom_order = ['1' => 'a.company', '2' => 'a.nama', '3' => 'a.divisi', '4' => 'a.jabatan', '5' => 'a.posisi', '6' => 'a.periode', '7' => 'a.tahun', '8' => 'a.nominal'];
        $order = order_datatable($kolom_order, $column, $dir);

        $company = company_id();
        $where = '';
        if ($company != '') {
            $where .= " AND a.id_company = '$company' ";
        }

        if ($bulan != '') {
            $condition .= " AND a.bulan = '$bulan' ";
        }

        if ($tahun != '') {
            $condition .= " AND a.tahun = '$tahun' ";
        }

        $query = $this->db->query("
        	SELECT *
			FROM upload_payroll a
			WHERE 1 = 1
            $where
			$condition 
			$order 
			LIMIT $start, $length ")->result();

        return $query;
    }

    function total_history_gaji($search = '', $bulan = '', $tahun = '')
    {
        $kolom = ['a.company', 'a.nama', 'a.divisi', 'a.jabatan', 'a.posisi', 'a.periode', 'a.tahun', 'a.nominal'];
        $condition = search_datatable($kolom, $search);

        $company = company_id();
        $where = '';
        if ($company != '') {
            $where .= " AND id_company = '$company' ";
        }

        if ($bulan != '') {
            $condition .= " AND a.bulan = '$bulan' ";
        }

        if ($tahun != '') {
            $condition .= " AND a.tahun = '$tahun' ";
        }

        $query = $this->db->query("
        	SELECT COUNT(*) AS jumlah
			FROM upload_payroll a
			WHERE 1 = 1
            $where
			$condition ")->row();

        return isset($query->jumlah) ? $query->jumlah : 0;
    }
}

/* End of file Gaji_Model.php */
/* Location: ./application/models/Gaji_Model.php */
