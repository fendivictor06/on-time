<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Provinsi_Model extends CI_Model {

	function __construct()
	{
		parent::__construct();
		
	}

	function json_provinsi($draw=1, $start=0, $length=0, $search='', $column='', $dir='')
	{
		$start = $this->db->escape_str($start);
		$length = $this->db->escape_str($length);
		$column = $this->db->escape_str($column);
		$dir = $this->db->escape_str($dir);
		$search = $this->db->escape_str($search);

		$total_filtered = $this->total_provinsi($search);
		$data = [];
		$request = $this->view_provinsi($start, $length, $search, $column, $dir);
		if (! empty($request)) {
			$no = $start + 1;
			foreach ($request as $row) {
				$btn_edit = btn_edit($row->id);
				$btn_delete = btn_delete($row->id);

				$data[] = array(
					$no++,
					$row->provinsi,
					btn_group([$btn_edit, $btn_delete])
				);
			}
		}

		return response_datatable($draw, $total_filtered, $data);
	}

	function view_provinsi($start=0, $length=0, $search='', $column='', $dir='')
	{
		$kolom = ['a.provinsi'];
		$condition = '';
		if ($search != '') {
			$condition .= ' AND (';
			for ($i = 0; $i < count($kolom); $i++) {
				$condition .= $kolom[$i]." LIKE '%$search%' ";
				if ($kolom[$i] != end($kolom)) {
					$condition .= ' OR ';
				}
			}
			$condition .= ')';
		}

		$kolom_order = ['1' => 'a.provinsi'];
		$order = '';
		if ($column != '' && $dir != '') {
			$col = isset($kolom_order[$column]) ? $kolom_order[$column] : '';

			if ($col != '') {
				$order .= " ORDER BY $col $dir ";
			}
		}

		$query = $this->db->query("
			SELECT *
			FROM ms_provinsi a
			WHERE a.status = 1 
			$condition 
			$order 
			LIMIT $start, $length ")->result();

		return $query;
	}

	function total_provinsi($search='')
	{
		$kolom = ['a.provinsi'];
		$condition = '';
		if ($search != '') {
			$condition .= ' AND (';
			for ($i = 0; $i < count($kolom); $i++) {
				$condition .= $kolom[$i]." LIKE '%$search%' ";
				if ($kolom[$i] != end($kolom)) {
					$condition .= ' OR ';
				}
			}
			$condition .= ')';
		}

		$query = $this->db->query("
			SELECT COUNT(*) AS jumlah
			FROM ms_provinsi a
			WHERE a.status = 1 
			$condition ")->row();

		return isset($query->jumlah) ? $query->jumlah : 0;
	}

	function json_kota($draw=1, $start=0, $length=0, $search='', $column='', $dir='')
	{
		$start = $this->db->escape_str($start);
		$length = $this->db->escape_str($length);
		$column = $this->db->escape_str($column);
		$dir = $this->db->escape_str($dir);
		$search = $this->db->escape_str($search);

		$total_filtered = $this->total_kota($search);
		$data = [];
		$request = $this->view_kota($start, $length, $search, $column, $dir);
		if (! empty($request)) {
			$no = $start + 1;
			foreach ($request as $row) {
				$btn_edit = btn_edit($row->id);
				$btn_delete = btn_delete($row->id);

				$data[] = array(
					$no++,
					$row->provinsi,
					$row->kota,
					btn_group([$btn_edit, $btn_delete])
				);
			}
		}

		return response_datatable($draw, $total_filtered, $data);
	}

	function view_kota($start=0, $length=0, $search='', $column='', $dir='')
	{
		$kolom = ['b.provinsi', 'a.kota'];
		$condition = '';
		if ($search != '') {
			$condition .= ' AND (';
			for ($i = 0; $i < count($kolom); $i++) {
				$condition .= $kolom[$i]." LIKE '%$search%' ";
				if ($kolom[$i] != end($kolom)) {
					$condition .= ' OR ';
				}
			}
			$condition .= ')';
		}

		$kolom_order = ['1' => 'b.provinsi', '2' => 'a.kota'];
		$order = '';
		if ($column != '' && $dir != '') {
			$col = isset($kolom_order[$column]) ? $kolom_order[$column] : '';

			if ($col != '') {
				$order .= " ORDER BY $col $dir ";
			}
		}

		$query = $this->db->query("
			SELECT a.*, b.provinsi
			FROM ms_kota a
			INNER JOIN ms_provinsi b ON b.id = a.id_provinsi
			WHERE a.status = 1 
			$condition 
			$order 
			LIMIT $start, $length ")->result();

		return $query;
	}

	function total_kota($search='')
	{
		$kolom = ['b.provinsi', 'a.kota'];
		$condition = '';
		if ($search != '') {
			$condition .= ' AND (';
			for ($i = 0; $i < count($kolom); $i++) {
				$condition .= $kolom[$i]." LIKE '%$search%' ";
				if ($kolom[$i] != end($kolom)) {
					$condition .= ' OR ';
				}
			}
			$condition .= ')';
		}

		$query = $this->db->query("
			SELECT COUNT(*) AS jumlah
			FROM ms_kota a
			INNER JOIN ms_provinsi b ON b.id = a.id_provinsi
			WHERE a.status = 1 
			$condition ")->row();

		return isset($query->jumlah) ? $query->jumlah : 0;
	}
}

/* End of file Provinsi_Model.php */
/* Location: ./application/models/Provinsi_Model.php */ ?>