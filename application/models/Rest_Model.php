<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Rest_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function is_valid_user($username = '', $password = '', $kode = '')
    {
        return $this->db->query("
			SELECT a.*
			FROM tb_user a
            INNER JOIN ms_company b ON a.id_company = b.id
			WHERE a.`username` = '$username'
			AND a.`password` = AES_ENCRYPT('$password', a.`key`)
			AND a.`status` = 1
			AND a.level = 3 
            AND b.kode_perusahaan = '$kode'")->row();
    }

    function auth($username = '', $password = '', $kode = '')
    {
        $username = $this->db->escape_str($username);
        $password = $this->db->escape_str($password);
        $kode = $this->db->escape_str($kode);

        $is_valid = $this->is_valid_user($username, $password, $kode);

        if (! empty($is_valid)) {
            return $is_valid;
        } else {
            return false;
        }
    }

    function is_valid_kode_perusahaan($username = '', $kode = '')
    {
        return $this->db->query("
            SELECT a.`username`, b.`company`
            FROM tb_user a
            INNER JOIN ms_company b ON a.`id_company` = b.`id`
            WHERE a.`username` = '$username'
            AND b.`kode_perusahaan` = '$kode'
            AND b.verify = 1
            AND CURDATE() BETWEEN b.mulai AND b.selesai ")->row();
    }
}

/* End of file Rest_Model.php */
/* Location: ./application/models/Rest_Model.php */
