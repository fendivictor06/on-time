<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Rest_Profile_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function view_profile()
    {
        $id_karyawan = id_karyawan();

        return $this->db->query("
    		SELECT *
			FROM karyawan a
			WHERE a.`id` = '$id_karyawan' ")->row();
    }
}

/* End of file Rest_Profile_Model.php */
/* Location: ./application/models/Rest_Profile_Model.php */
