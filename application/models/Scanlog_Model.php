<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Scanlog_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function json_scanlog($draw = 1, $start = 0, $length = 0, $search = '', $column = '', $dir = '', $datestart = '', $dateend = '', $company = '', $karyawan = '')
    {
        $start = $this->db->escape_str($start);
        $length = $this->db->escape_str($length);
        $column = $this->db->escape_str($column);
        $dir = $this->db->escape_str($dir);
        $search = $this->db->escape_str($search);

        $datestart = $this->db->escape_str($datestart);
        $dateend = $this->db->escape_str($dateend);
        $company = $this->db->escape_str($company);
        $karyawan = $this->db->escape_str($karyawan);

        $datestart = convert_tgl($datestart);
        $dateend = convert_tgl($dateend);

        $total_filtered = $this->total_scanlog($search, $datestart, $dateend, $company, $karyawan);
        $data = [];
        $request = $this->view_scanlog($start, $length, $search, $column, $dir, $datestart, $dateend, $company, $karyawan);
        if (! empty($request)) {
            $no = $start + 1;
            foreach ($request as $row) {
                $btn_edit = btn_edit($row->id);
                $btn_delete = btn_delete($row->id);

                $image = '';
                if ($row->foto != '') {
                    $image =
                    '<a href="'.link_scan().$row->foto.'" target="_blank">
                        <img src="'.link_scan().$row->foto.'" width="48">
                    </a>';
                }

                $lokasi = '';
                if ($row->latitude != '' && $row->longitude != '') {
                    $lokasi = ' <a href="https://www.google.com/maps/search/?api=1&query='.$row->latitude.','.$row->longitude.'" class="btn btn-info m-btn m-btn--icon m-btn--icon-only m-btn--pill" target="_blank">
                                    <i class="fa fa-location-arrow"></i>
                                </a>';
                }

                $data[] = array(
                    $no++,
                    $row->company,
                    $row->nama,
                    $row->scan_date,
                    $row->keterangan,
                    $image,
                    $lokasi
                );
            }
        }

        return response_datatable($draw, $total_filtered, $data);
    }

    function view_scanlog($start = 0, $length = 0, $search = '', $column = '', $dir = '', $datestart = '', $dateend = '', $company = '', $karyawan = '')
    {
        $kolom = ['company', 'nama', 'scan_date', 'keterangan', 'latitude', 'longitude'];
        $condition = search_datatable($kolom, $search);

        $kolom_order = ['1' => 'company', '2' => 'nama', '3' => 'scan_date', '4' => 'keterangan'];
        $order = order_datatable($kolom_order, $column, $dir);

        # id company
        $company = company_id();
        if ($company != '') {
            $condition .= " AND id_company = '$company' ";
        }

        if ($datestart != '' && $dateend != '') {
            $condition .= " AND DATE(scan_date) BETWEEN '$datestart' AND '$dateend' ";
        }

        if ($company != '' && $company != 'null') {
            $condition .= " AND id_company = '$company' ";
        }

        if ($karyawan != '' && $karyawan != 'null') {
            $condition .= " AND id_karyawan = '$karyawan' ";
        }

        $query = $this->db->query("
        	SELECT * 
        	FROM view_scanlog
			WHERE 1 = 1
			$condition 
			$order 
			LIMIT $start, $length ")->result();

        return $query;
    }

    function total_scanlog($search = '', $datestart = '', $dateend = '', $company = '', $karyawan = '')
    {
        $kolom = ['company', 'nama', 'scan_date', 'keterangan', 'latitude', 'longitude'];
        $condition = search_datatable($kolom, $search);

        # id company
        $company = company_id();
        if ($company != '') {
            $condition .= " AND id_company = '$company' ";
        }

        if ($datestart != '' && $dateend != '') {
            $condition .= " AND DATE(scan_date) BETWEEN '$datestart' AND '$dateend' ";
        }

        if ($company != '' && $company != 'null') {
            $condition .= " AND id_company = '$company' ";
        }

        if ($karyawan != '' && $karyawan != 'null') {
            $condition .= " AND id_karyawan = '$karyawan' ";
        }

        $query = $this->db->query("
        	SELECT COUNT(*) AS jumlah 
        	FROM view_scanlog 
			WHERE 1 = 1
			$condition ")->row();

        return isset($query->jumlah) ? $query->jumlah : 0;
    }
}

/* End of file Scanlog_Model.php */
/* Location: ./application/models/Scanlog_Model.php */
