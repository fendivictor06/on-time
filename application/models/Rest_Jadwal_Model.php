<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Rest_Jadwal_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function view_jadwal($datestart = '', $dateend = '', $start = 0, $count = 0)
    {
        $id_karyawan = id_karyawan();

        $limit = '';
        if ($count > 0) {
            $limit = " LIMIT $start, $count ";
        }

        return $this->db->query("
    		SELECT *
			FROM tb_jadwal a
			WHERE a.`id_karyawan` = '$id_karyawan'
			AND a.`tgl` BETWEEN '$datestart' AND '$dateend' 
			$limit ")->result();
    }
}

/* End of file Rest_Jadwal_Model.php */
/* Location: ./application/models/Rest_Jadwal_Model.php */
