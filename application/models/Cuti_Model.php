<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Cuti_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function json_cuti($draw = 1, $start = 0, $length = 0, $search = '', $column = '', $dir = '', $datestart = '', $dateend = '')
    {
        $start = $this->db->escape_str($start);
        $length = $this->db->escape_str($length);
        $column = $this->db->escape_str($column);
        $dir = $this->db->escape_str($dir);
        $search = $this->db->escape_str($search);

        $datestart = $this->db->escape_str($datestart);
        $dateend = $this->db->escape_str($dateend);

        $datestart = convert_tgl($datestart);
        $dateend = convert_tgl($dateend);

        $total_filtered = $this->total_cuti($search, $datestart, $dateend);
        $data = [];
        $request = $this->view_cuti($start, $length, $search, $column, $dir, $datestart, $dateend);
        if (! empty($request)) {
            $no = $start + 1;
            foreach ($request as $row) {
                $btn_edit = btn_edit($row->id);
                $btn_delete = btn_delete($row->id);

                $data[] = array(
                    $no++,
                    $row->company,
                    $row->nama,
                    $row->mulai,
                    $row->selesai,
                    $row->alasan,
                    $row->keterangan,
                    btn_group([$btn_delete])
                );
            }
        }

        return response_datatable($draw, $total_filtered, $data);
    }

    function view_cuti($start = 0, $length = 0, $search = '', $column = '', $dir = '', $datestart = '', $dateend = '')
    {
        $kolom = ['company', 'nama', 'awal', 'akhir', 'alasan', 'keterangan'];
        $condition = search_datatable($kolom, $search);

        $kolom_order = ['1' => 'company', '2' => 'nama', '3' => 'awal', '4' => 'akhir', '5' => 'alasan', '6' => 'keterangan'];
        $order = order_datatable($kolom_order, $column, $dir);

        $company = company_id();
        $where = '';
        if ($company != '') {
            $where .= " AND id_company = '$company' ";
        }

        if ($datestart != '' && $dateend != '') {
            $where .= " AND ((awal BETWEEN '$datestart' AND '$dateend') OR (akhir BETWEEN '$datestart' AND '$dateend')) ";
        }

        $query = $this->db->query("
        	SELECT *
			FROM view_cuti 
			WHERE 1 = 1
            $where
			$condition 
			$order 
			LIMIT $start, $length ")->result();

        return $query;
    }

    function total_cuti($search = '', $datestart = '', $dateend = '')
    {
        $kolom = ['company', 'nama', 'awal', 'akhir', 'alasan', 'keterangan'];
        $condition = search_datatable($kolom, $search);

        $company = company_id();
        $where = '';
        if ($company != '') {
            $where .= " AND id_company = '$company' ";
        }

        if ($datestart != '' && $dateend != '') {
            $where .= " AND ((awal BETWEEN '$datestart' AND '$dateend') OR (akhir BETWEEN '$datestart' AND '$dateend')) ";
        }

        $query = $this->db->query("
        	SELECT COUNT(*) AS jumlah
			FROM view_cuti 
			WHERE 1 = 1
            $where
			$condition ")->row();

        return isset($query->jumlah) ? $query->jumlah : 0;
    }
}

/* End of file Cuti_Model.php */
/* Location: ./application/models/Cuti_Model.php */
