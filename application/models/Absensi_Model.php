<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Absensi_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function view_jadwal($id_company = '', $tgl = '', $id_karyawan = '')
    {
        $condition = '';
        if ($id_company != '') {
            $condition .= " AND a.id_company = '$id_company' ";
        }

        if ($id_karyawan != '') {
            $condition .= " AND a.id = '$id_karyawan' ";
        }

        if ($tgl == '') {
            $tgl = date('Y-m-d');
        }

        return $this->db->query("
    		SELECT
			  a.`id`,
			  a.`nik`,
			  a.`nama`,
			  IFNULL(b.`tgl`, '') AS tgl,
			  IFNULL(b.`shift`, '') AS shift,
			  IFNULL(b.`jam_masuk`, '') AS jam_masuk,
			  IFNULL(b.`jam_pulang`, '') AS jam_pulang
			FROM
			  ms_karyawan a
			  LEFT JOIN
			    (SELECT
			      *
			    FROM
			      tb_jadwal
			    WHERE tgl = '$tgl'
			    GROUP BY id_karyawan,
			      tgl) AS b
			    ON b.id_karyawan = a.`id`
			WHERE 1 = 1 
			$condition ")->result();
    }

    function scan_masuk($id_karyawan = '', $tgl = '')
    {
        $query = $this->db->query("
    		CALL scan_masuk('$id_karyawan', '$tgl')");

        $result = $query->row();

        # mysql next result
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function scan_pulang($id_karyawan = '', $tgl = '')
    {
        $query = $this->db->query("
    		CALL scan_pulang('$id_karyawan', '$tgl')");

        $result = $query->row();

        # mysql next result
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function holiday($tgl = '', $id_company = '')
    {
        $condition = '';
        if ($id_company != '') {
            $condition = "AND id_company = '$id_company'";
        }

        return $this->db->query("
            SELECT *
            FROM ms_libur
            WHERE tgl = '$tgl' 
            $condition ")->row();
    }

    function rekap_absensi($datestart = '', $dateend = '', $company = '')
    {
        $datestart = convert_tgl($datestart);
        $dateend = convert_tgl($dateend);

        return $this->db->query("CALL rekap_absensi('$company', '$datestart', '$dateend')")->result();
    }

    function json_presensi($draw = 1, $start = 0, $length = 0, $search = '', $column = '', $dir = '', $company = '', $datestart = '', $dateend = '')
    {
        $start = $this->db->escape_str($start);
        $length = $this->db->escape_str($length);
        $column = $this->db->escape_str($column);
        $dir = $this->db->escape_str($dir);
        $search = $this->db->escape_str($search);
        $datestart = $this->db->escape_str($datestart);
        $dateend = $this->db->escape_str($dateend);
        $company = $this->db->escape_str($company);

        $datestart = convert_tgl($datestart);
        $dateend = convert_tgl($dateend);

        $total_filtered = $this->total_presensi($search, $company, $datestart, $dateend);
        $data = [];
        $request = $this->view_presensi($start, $length, $search, $column, $dir, $company, $datestart, $dateend);
        if (! empty($request)) {
            $no = $start + 1;
            foreach ($request as $row) {
                $btn_edit = btn_custom($row->id, 'edit', 'javascript:;', 'btn-warning', 'fa fa-pencil', 'Edit Data');
                $btn_delete = btn_custom($row->id, 'delete', 'javascript:;', 'btn-danger', 'fa fa-trash', 'Delete Data');

                $data[] = array(
                    $no++,
                    $row->company,
                    $row->nik,
                    $row->nama,
                    $row->tanggal,
                    $row->presensi,
                    $row->keterangan,
                    btn_group([$btn_edit])
                );
            }
        }

        return response_datatable($draw, $total_filtered, $data);
    }

    function view_presensi($start = 0, $length = 0, $search = '', $column = '', $dir = '', $company = '', $datestart = '', $dateend = '')
    {
        $kolom = ['company', 'nik', 'nama', 'tanggal', 'presensi', 'keterangan'];
        $condition = search_datatable($kolom, $search);

        $kolom_order = ['1' => 'company', '2' => 'nik', '3' => 'nama', '4' => 'tgl', '5' => 'presensi', '6' => 'keterangan'];
        $order = order_datatable($kolom_order, $column, $dir);

        if ($company != '') {
            $condition .= " AND id_company = '$company' ";
        }

        if ($datestart != '' && $dateend != '') {
            $condition .= " AND tgl BETWEEN '$datestart' AND '$dateend' ";
        }

        $query = $this->db->query("
            SELECT *
            FROM view_presensi a
            WHERE 1 = 1
            $condition 
            $order 
            LIMIT $start, $length ")->result();

        return $query;
    }

    function total_presensi($search = '', $company = '', $datestart = '', $dateend = '')
    {
        $kolom = ['company', 'nik', 'nama', 'tanggal', 'presensi', 'keterangan'];
        $condition = search_datatable($kolom, $search);

        if ($company != '') {
            $condition .= " AND id_company = '$company' ";
        }

        if ($datestart != '' && $dateend != '') {
            $condition .= " AND tgl BETWEEN '$datestart' AND '$dateend' ";
        }

        $query = $this->db->query("
            SELECT COUNT(a.id) AS jumlah
            FROM view_presensi a
            WHERE 1 = 1
            $condition ")->row();

        return isset($query->jumlah) ? $query->jumlah : 0;
    }

    function update_presensi($data = [], $id = '', $tgl = '', $id_karyawan = '')
    {
        $this->db->trans_begin();

        # update presensi
        $this->db->where('id', $id)
            ->update('tb_presensi', $data);

        # update flag = 1
        $this->db->where('tgl', $tgl)
            ->where('id_karyawan', $id_karyawan)
            ->update('tb_absensi', ['flag' => 1, 'user_update' => username(), 'update_at' => now()]);


        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();

            $result = false;
        } else {
            $this->db->trans_commit();

            $result = true;
        }

        return $result;
    }

    function insert_cuti($id = '', $id_karyawan = '', $id_company = '', $keterangan = '', $tgl = '')
    {
        $this->db->trans_begin();

        # insert cuti
        $cuti = array(
            'id_company' => $id_company,
            'id_karyawan' => $id_karyawan,
            'awal' => $tgl,
            'akhir' => $tgl,
            'alasan' => $keterangan,
            'status' => 2,
            'jumlah' => 1,
            'insert_at' => now(),
            'user_insert' => username()
        );

        $this->db->insert('tb_cuti', $cuti);
        $id_cuti = $this->db->insert_id();

        # insert detail cuti
        $detail = array(
            'id_company' => $id_company,
            'id_karyawan' => $id_karyawan,
            'id_cuti' => $id_cuti,
            'tgl' => $tgl,
            'insert_at' => now(),
            'user_insert' => username()
        );

        $this->db->insert('detail_cuti', $detail);

        # delete presensi
        $this->db->where('id', $id)
            ->delete('tb_presensi');

        # update flag = 1
        $this->db->where('tgl', $tgl)
            ->where('id_karyawan', $id_karyawan)
            ->update('tb_absensi', ['flag' => 1, 'user_update' => username(), 'update_at' => now()]);

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();

            $result = false;
        } else {
            $this->db->trans_commit();

            $result = true;
        }

        return $result;
    }

    function clear_presensi($id = '', $id_karyawan = '', $keterangan = '', $tgl = '')
    {
        $this->db->trans_begin();

        # delete presensi
        $this->db->where('id', $id)
            ->delete('tb_presensi');

        # update flag = 1
        $this->db->where('tgl', $tgl)
            ->where('id_karyawan', $id_karyawan)
            ->update('tb_absensi', ['flag' => 1, 'status' => 1, 'keterangan' => $keterangan, 'user_update' => username(), 'update_at' => now()]);

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();

            $result = false;
        } else {
            $this->db->trans_commit();

            $result = true;
        }

        return $result;
    }
}

/* End of file Absensi_Model.php */
/* Location: ./application/models/Absensi_Model.php */
