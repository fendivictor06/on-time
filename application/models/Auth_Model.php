<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Auth_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function is_valid_user($username = '', $password = '')
    {
        return $this->db->query("
			SELECT *
			FROM tb_user a
			WHERE a.`username` = '$username'
			AND a.`password` = AES_ENCRYPT('$password', a.`key`)
			AND a.`status` = 1
            AND a.level <> 3 ")->row();
    }

    function update_session($id = '', $remember = '')
    {
        $now = now();
        $session = md5(generate_random(4));
        $ip = $this->input->ip_address();

        $data = array(
            'session' => $session,
            'last_login' => $now,
            'last_ip' => $ip
        );

        $this->db->where('id', $id)
            ->update('tb_user', $data);

        if ($remember == 1) {
            $this->create_cookies($session);
        }

        return $session;
    }

    function create_cookies($session = '')
    {
        $name = 'hrsession';
        $expire = '8640000';
        $domain = base_url();
        
        delete_cookie($name);

        $cookie = array(
            'name' => $name,
            'value' => $session,
            'expire' => $expire
        );

        $this->input->set_cookie($cookie);
    }

    function login($username = '', $password = '', $remember = '')
    {
        $is_valid_user = $this->is_valid_user($username, $password);

        $data = [];
        if (! empty($is_valid_user)) {
            $status = true;
            $message = 'Login berhasil.';
            $data = $is_valid_user;
            $today = date('Y-m-d');

            $id = isset($data->id) ? $data->id : '';
            $username = isset($data->username) ? $data->username : '';
            $level = isset($data->level) ? $data->level : '';
            $id_company = isset($data->id_company) ? $data->id_company : '';
            $id_karyawan = isset($data->id_karyawan) ? $data->id_karyawan : '';
            $profile_name = isset($data->profile_name) ? $data->profile_name : '';

            $is_login = true;
            $ms_company = $this->db->where('id', $id_company)->get('ms_company')->row_array();
            $verify = isset($ms_company['verify']) ? $ms_company['verify'] : 0;
            $mulai = isset($ms_company['mulai']) ? $ms_company['mulai'] : '';
            $selesai = isset($ms_company['selesai']) ? $ms_company['selesai'] : '';
            if ($verify == 0 && $level == 2) {
                $is_login = false;
                $status = false;
                $message = 'Akun Anda Belum diverifikasi';
            }

            if (strtotime($selesai) < strtotime($today) && $level == 2) {
                $is_login = false;
                $status = false;
                $message = 'Akun sudah tidak berlaku';
            }

            if (strtotime($mulai) > strtotime($today) && $level == 2) {
                $is_login = false;
                $status = false;
                $message = 'Akun belum berlaku';
            }


            if ($is_login == true) {
                $session = $this->update_session($id, $remember);

                $set_session = array(
                    'username' => $username,
                    'level' => $level,
                    'id_company' => $id_company,
                    'id_karyawan' => $id_karyawan,
                    'profile_name' => $profile_name,
                    'session' => $session
                );
                
                $this->session->set_userdata($set_session);
            }
        } else {
            $status = false;
            $message = 'Username atau password salah. silahkan ulangi lagi.';
        }

        $result = array(
            'status' => $status,
            'message' => $message
        );

        return $result;
    }

    function is_valid_cookie()
    {
        $get_cookie = get_cookie('hrsession', true);
        $is_valid = $this->db->where('session', $get_cookie)
                        ->get('tb_user')
                        ->row();

        $result = (! empty($is_valid)) ? true : false;

        if ($result == true && $get_cookie != '' && $get_cookie != null) {
            $username = isset($is_valid->username) ? $is_valid->username : '';
            $level = isset($is_valid->level) ? $is_valid->level : '';
            $id_company = isset($is_valid->id_company) ? $is_valid->id_company : '';
            $id_karyawan = isset($is_valid->id_karyawan) ? $is_valid->id_karyawan : '';
            $profile_name = isset($is_valid->profile_name) ? $is_valid->profile_name : '';
            $session = $get_cookie;

            $set_session = array(
                'username' => $username,
                'level' => $level,
                'id_company' => $id_company,
                'id_karyawan' => $id_karyawan,
                'profile_name' => $profile_name,
                'session' => $session
            );
            
            $this->session->set_userdata($set_session);
        }

        return $result;
    }

    function is_valid_session()
    {
        $username = $this->session->userdata('username');
        $level = $this->session->userdata('level');
        $id_company = $this->session->userdata('id_company');
        $id_karyawan = $this->session->userdata('id_karyawan');
        $profile_name = $this->session->userdata('profile_name');
        $session = $this->session->userdata('session');

        $where = array(
            'username' => $username,
            'level' => $level,
            'id_company'  => $id_company,
            'id_karyawan' => $id_karyawan,
            'profile_name' => $profile_name,
            'session' => $session
        );

        $is_valid = $this->db->where($where)
                        ->get('tb_user')
                        ->row();

        $result = (! empty($is_valid)) ? true : false;

        return $result;
    }

    function is_login()
    {
        $valid_cooke = $this->is_valid_cookie();
        $valid_session = $this->is_valid_session();

        $cookie_status = false;
        $session_status = false;
            
        if ($valid_cooke == true) {
            $valid_session = $this->is_valid_session();
            if ($valid_session == true) {
                $cookie_status = true;
            } else {
                $cookie_status = false;
            }
        }

        if ($valid_session == true) {
            $session_status = true;
        }

        return ($cookie_status == true || $session_status == true) ? true : redirect('Login');
    }

    function is_login_ajax()
    {
        $valid_cooke = $this->is_valid_cookie();
        $valid_session = $this->is_valid_session();

        $cookie_status = false;
        $session_status = false;
        $is_ajax = false;
            
        if ($valid_cooke == true) {
            $valid_session = $this->is_valid_session();
            if ($valid_session == true) {
                $cookie_status = true;
            } else {
                $cookie_status = false;
            }
        }

        if ($valid_session == true) {
            $session_status = true;
        }

        if ($this->input->is_ajax_request() == true) {
            $is_ajax = true;
        }

        return (($cookie_status == true || $session_status == true) && $is_ajax == true) ? true : $this->output->set_status_header(401);
    }
}

/* End of file Auth_Model.php */
/* Location: ./application/models/Auth_Model.php */
