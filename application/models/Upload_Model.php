<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Upload_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function karyawan()
    {
        $id_company = company_id();
        return $this->db->query("
    		SELECT *
    		FROM ms_karyawan a 
    		WHERE a.id_company = '$id_company'
    		AND (a.tgl_resign IS NULL OR a.tgl_resign = '')")->result();
    }

    function simpan_gaji($filename = '')
    {
        ini_set('memory_limit', '-1');
        require_once(APPPATH.'libraries/PHPExcel.php');
        require_once(APPPATH.'libraries/PHPExcel/IOFactory.php');
        $file = path_gaji().$filename;
        $id_company = company_id();

        try {
            $objPHPExcel = PHPExcel_IOFactory::load($file);
        } catch (Exception $e) {
            die('Error loading file '.$e->getMessage());
        }

        $worksheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
        $numRows = count($worksheet);
        
        if ($numRows > 0) {
            for ($i = 3; $i < ($numRows + 1); $i++) {
                $nik = isset($worksheet[$i]['B']) ? $worksheet[$i]['B'] : '';
                $karyawan = $this->db->where('nik', $nik)
                                ->get('ms_karyawan')
                                ->row();
                $id_karyawan = isset($karyawan->id) ? $karyawan->id : '';

                $bulan = isset($worksheet[$i]['D']) ? $worksheet[$i]['D'] : '';
                $tahun = isset($worksheet[$i]['E']) ? $worksheet[$i]['E'] : '';
                $nominal = isset($worksheet[$i]['F']) ? $worksheet[$i]['F'] : '';
                $user_insert = username();

                $exists = $this->db->where('id_karyawan', $id_karyawan)
                            ->where('id_company', $id_company)
                            ->where('bulan', $bulan)
                            ->where('tahun', $tahun)
                            ->get('tb_payroll')
                            ->row();

                $time = (!empty($exists)) ? 'update_at' : 'insert_at';
                $user = (!empty($exists)) ? 'user_update' : 'user_insert';

                $condition = [];
                if (!empty($exists)) {
                    $condition = ['id_karyawan' => $id_karyawan, 'bulan' => $bulan, 'tahun' => $tahun];
                }

                $data = array(
                    'id_company' => $id_company,
                    'id_karyawan' => $id_karyawan,
                    'bulan' => $bulan,
                    'tahun' => $tahun,
                    'nominal' => $nominal,
                    $time => now(),
                    $user => username()
                );

                if (! empty($condition)) {
                    $this->db->where($condition)
                        ->update('tb_payroll', $data);
                } else {
                    $this->db->insert('tb_payroll', $data);
                }
            }

            $status = 1;
        } else {
            $status = 0;
        }

        return $status;
    }

    function simpan_karyawan($filename = '')
    {
        ini_set('memory_limit', '-1');
        require_once(APPPATH.'libraries/PHPExcel.php');
        require_once(APPPATH.'libraries/PHPExcel/IOFactory.php');
        $file = path_karyawan().$filename;
        $id_company = company_id();
        $ms_company = $this->db->where('id', $id_company)
                        ->get('ms_company')
                        ->row();
        $kode = isset($ms_company->kode_perusahaan) ? $ms_company->kode_perusahaan : '';

        try {
            $objPHPExcel = PHPExcel_IOFactory::load($file);
        } catch (Exception $e) {
            die('Error loading file '.$e->getMessage());
        }

        $worksheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
        $numRows = count($worksheet);
        $key = generate_random(4);
        $user_insert = username();
        
        if ($numRows > 0) {
            for ($i = 3; $i < ($numRows + 1); $i++) {
                $nik = isset($worksheet[$i]['A']) ? $worksheet[$i]['A'] : '';
                $nama = isset($worksheet[$i]['B']) ? $worksheet[$i]['B'] : '';
                $ktp = isset($worksheet[$i]['C']) ? $worksheet[$i]['C'] : '';
                $jk = isset($worksheet[$i]['D']) ? $worksheet[$i]['D'] : '';
                $tmplahir = isset($worksheet[$i]['E']) ? $worksheet[$i]['E'] : '';
                $tgllahir = isset($worksheet[$i]['F']) ? excel_tgl($worksheet[$i]['F']) : '';
                $tlp = isset($worksheet[$i]['G']) ? $worksheet[$i]['G'] : '';
                $alamat = isset($worksheet[$i]['H']) ? $worksheet[$i]['H'] : '';
                $email = isset($worksheet[$i]['I']) ? $worksheet[$i]['I'] : '';
                $tglmasuk = isset($worksheet[$i]['J']) ? excel_tgl($worksheet[$i]['J']) : '';
                $status_nikah = isset($worksheet[$i]['K']) ? $worksheet[$i]['K'] : '';
                $pendidikan = isset($worksheet[$i]['L']) ? $worksheet[$i]['L'] : '';
                // $institusi = isset($worksheet[$i]['M']) ? $worksheet[$i]['M'] : '';
                // $jurusan = isset($worksheet[$i]['N']) ? $worksheet[$i]['N'] : '';
                // $thlulus = isset($worksheet[$i]['O']) ? $worksheet[$i]['O'] : '';
                $agama = isset($worksheet[$i]['M']) ? $worksheet[$i]['M'] : '';
                $goldarah = isset($worksheet[$i]['N']) ? $worksheet[$i]['N'] : '';
                $divisi = isset($worksheet[$i]['O']) ? $worksheet[$i]['O'] : '';
                $jabatan = isset($worksheet[$i]['P']) ? $worksheet[$i]['P'] : '';
                $posisi = isset($worksheet[$i]['Q']) ? $worksheet[$i]['Q'] : '';
                $keterangan = isset($worksheet[$i]['R']) ? $worksheet[$i]['R'] : '';
                $username = isset($worksheet[$i]['S']) ? $worksheet[$i]['S'] : '';
                $gaji = isset($worksheet[$i]['T']) ? $worksheet[$i]['T'] : '';

                $nik = $kode.'-'.$nik;
                $exists = $this->db->where('nik', $nik)
                            ->get('ms_karyawan')
                            ->row();

                $exists_username = $this->db->where('username', $username)
                                        ->where('id_company', $id_company)
                                        ->get('tb_user')
                                        ->row();

                if (empty($exists) && empty($exists_username)) {
                    # kelamin
                    $ms_kelamin = $this->db->where('jenis_kelamin', $jk)
                                    ->get('ms_jenis_kelamin')
                                    ->row();
                    $jenis_kelamin = isset($ms_kelamin->id) ? $ms_kelamin->id : '';
                    # pendidikan
                    $ms_pendidikan = $this->db->where('pendidikan', $pendidikan)
                                        ->get('ms_pendidikan')
                                        ->row();
                    $id_pendidikan = isset($ms_pendidikan->id) ? $ms_pendidikan->id : '';
                    # agama
                    $ms_agama = $this->db->where('agama', $agama)
                                    ->get('ms_agama')
                                    ->row();
                    $id_agama = isset($ms_agama->id) ? $ms_agama->id : '';
                    # nikah
                    $ms_nikah = $this->db->where('status_nikah', $status_nikah)
                                    ->get('ms_status_nikah')
                                    ->row();
                    $id_status = isset($ms_nikah->id) ? $ms_nikah->id : '';
                    # golongan
                    $ms_golongan = $this->db->where('golongan_darah', $goldarah)
                                        ->get('ms_golongan_darah')
                                        ->row();
                    $id_golongan = isset($ms_golongan->id) ? $ms_golongan->id : '';
                    # divisi
                    $ms_divisi = $this->db->where('divisi', $divisi)
                                    ->where('id_company', $id_company)
                                    ->get('ms_divisi')
                                    ->row();
                    $id_divisi = isset($ms_divisi->id) ? $ms_divisi->id : '';
                    if ($id_divisi == '' && $divisi != '') {
                        $data_divisi = array(
                            'id_company' => $id_company,
                            'divisi' => $divisi,
                            'keterangan' => '',
                            'status' => 1,
                            'user_insert' => username()
                        );

                        $this->db->insert('ms_divisi', $data_divisi);
                        $id_divisi = $this->db->insert_id();
                    }
                    # jabatan
                    $ms_jabatan = $this->db->where('jabatan', $jabatan)
                                    ->where('id_company', $id_company)
                                    ->get('ms_jabatan')
                                    ->row();
                    $id_jabatan = isset($ms_jabatan->id) ? $ms_jabatan->id : '';
                    if ($id_jabatan == '' && $jabatan != '') {
                        $data_jabatan = array(
                            'id_company' => $id_company,
                            'jabatan' => $jabatan,
                            'keterangan' => '',
                            'status' => 1,
                            'user_insert' => username()
                        );

                        $this->db->insert('ms_jabatan', $data_jabatan);
                        $id_jabatan = $this->db->insert_id();
                    }

                    $data = array(
                        'nik' => $nik,
                        'nama' => $nama,
                        'ktp' => $ktp,
                        'jenis_kelamin' => $jenis_kelamin,
                        'tempat_lahir' => $tmplahir,
                        'tgl_lahir' => $tgllahir,
                        'telp' => $tlp,
                        'alamat' => $alamat,
                        'email' => $email,
                        'tgl_masuk' => $tglmasuk,
                        'status_nikah' => $id_status,
                        'pendidikan' => $id_pendidikan,
                        // 'institusi' => $institusi,
                        // 'jurusan' => $jurusan,
                        // 'tahun_lulus' => $thlulus,
                        'agama' => $id_agama,
                        'golongan_darah' => $id_golongan,
                        'id_company' => $id_company,
                        'id_divisi' => $id_divisi,
                        'id_jabatan' => $id_jabatan,
                        'posisi' => $posisi,
                        'keterangan' => $keterangan,
                        'salary' => $gaji,
                        'user_insert' => username()
                    );

                    $this->db->trans_begin();

                    $this->db->insert('ms_karyawan', $data);
                    $insert_id = $this->db->insert_id();
                    $this->db->query("
                        INSERT INTO tb_user (`level`, id_company, id_karyawan, username, `password`, `key`, `profile_name`, user_insert)
                        VALUES ('3', '$id_company', '$insert_id', '$username', AES_ENCRYPT('1234', '$key'), '$key', '$nama', '$user_insert'); ");
                    if ($this->db->trans_status() === false) {
                        $this->db->trans_rollback();
                    } else {
                        $this->db->trans_commit();
                    }
                }
            }

            $status = 1;
        } else {
            $status = 0;
        }

        return $status;
    }
}

/* End of file Upload_Model.php */
/* Location: ./application/models/Upload_Model.php */
