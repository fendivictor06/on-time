<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Karyawan_Model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function json_karyawan($draw = 1, $start = 0, $length = 0, $search = '', $column = '', $dir = '')
    {
        $start = $this->db->escape_str($start);
        $length = $this->db->escape_str($length);
        $column = $this->db->escape_str($column);
        $dir = $this->db->escape_str($dir);
        $search = $this->db->escape_str($search);

        $total_filtered = $this->total_karyawan($search);
        $data = [];
        $request = $this->view_karyawan($start, $length, $search, $column, $dir);
        if (! empty($request)) {
            $no = $start + 1;
            foreach ($request as $row) {
                $btn_edit = btn_edit($row->id);
                $btn_delete = btn_delete($row->id);

                $data[] = array(
                    $no++,
                    $row->nik,
                    $row->nama,
                    $row->ktp,
                    $row->alamat,
                    $row->tgl_masuk,
                    $row->status,
                    btn_group([$btn_edit, $btn_delete])
                );
            }
        }

        return response_datatable($draw, $total_filtered, $data);
    }

    function view_karyawan($start = 0, $length = 0, $search = '', $column = '', $dir = '')
    {
        $kolom = ['nik', 'nama', 'ktp', 'alamat', 'tgl_masuk', 'status'];
        $condition = search_datatable($kolom, $search);

        $kolom_order = ['1' => 'nik', '2' => 'nama', '3' => 'ktp', '4' => 'alamat', '5' => 'tgl_masuk', '6' => 'status'];
        $order = order_datatable($kolom_order, $column, $dir);

        # id company
        $company = company_id();
        if ($company != '') {
            $condition .= " AND id_company = '$company' ";
        }

        $query = $this->db->query("
        	SELECT * 
        	FROM (
				SELECT
                  a.id_company,
				  a.`id`,
				  a.`nik`,
				  a.`nama`,
				  a.`ktp`,
				  a.`alamat`,
				  tgl_indo (a.`tgl_masuk`, 0) AS tgl_masuk,
				  CASE
				    WHEN (a.`tgl_resign` <> '')
				    THEN 'Resign'
				    ELSE 'Active'
				  END AS `status`
				FROM
				  ms_karyawan a
			) AS karyawan 
			WHERE 1 = 1
			$condition 
			$order 
			LIMIT $start, $length ")->result();

        return $query;
    }

    function total_karyawan($search = '')
    {
        $kolom = ['nik', 'nama', 'ktp', 'alamat', 'tgl_masuk', 'status'];
        $condition = search_datatable($kolom, $search);

        # id company
        $company = company_id();
        if ($company != '') {
            $condition .= " AND id_company = '$company' ";
        }

        $query = $this->db->query("
        	SELECT COUNT(*) AS jumlah 
        	FROM (
				SELECT
                  a.id_company,
				  a.`id`,
				  a.`nik`,
				  a.`nama`,
				  a.`ktp`,
				  a.`alamat`,
				  tgl_indo (a.`tgl_masuk`, 0) AS tgl_masuk,
				  CASE
				    WHEN (a.`tgl_resign` <> '')
				    THEN 'Resign'
				    ELSE 'Active'
				  END AS `status`
				FROM
				  ms_karyawan a
			) AS karyawan 
			WHERE 1 = 1
			$condition ")->row();

        return isset($query->jumlah) ? $query->jumlah : 0;
    }

    function view_kota($search = '', $id_provinsi = '')
    {
        $condition = '';
        if ($search != '') {
            $condition = " AND a.kota LIKE '%$search%' ";
        }

        return $this->db->query("
            SELECT * 
            FROM ms_kota a 
            WHERE a.id_provinsi = '$id_provinsi'
            AND a.status = 1 
            $condition ")->result();
    }

    function view_jabatan($search = '', $id_company = '')
    {
        $condition = '';
        if ($search != '') {
            $condition = " AND a.jabatan LIKE '%$search%' ";
        }

        return $this->db->query("
            SELECT *
            FROM ms_jabatan a
            WHERE a.status = 1
            AND a.id_company = '$id_company'
            $condition ")->result();
    }

    function view_divisi($search = '', $id_company = '')
    {
        $condition = '';
        if ($search != '') {
            $condition = " AND a.divisi LIKE '%$search%' ";
        }

        return $this->db->query("
            SELECT *
            FROM ms_divisi a 
            WHERE a.status = 1
            AND a.id_company = '$id_company'
            $condition ")->result();
    }

    function hapus_karyawan($id = '')
    {
        $this->db->query("
            INSERT INTO temp_karyawan 
            (SELECT * FROM ms_karyawan WHERE id = '$id')");

        $this->db->where('id', $id)
            ->delete('ms_karyawan');

        return $this->db->affected_rows();
    }

    function json_imei($draw = 1, $start = 0, $length = 0, $search = '', $column = '', $dir = '')
    {
        $start = $this->db->escape_str($start);
        $length = $this->db->escape_str($length);
        $column = $this->db->escape_str($column);
        $dir = $this->db->escape_str($dir);
        $search = $this->db->escape_str($search);

        $total_filtered = $this->total_imei($search);
        $data = [];
        $request = $this->view_imei($start, $length, $search, $column, $dir);
        if (! empty($request)) {
            $no = $start + 1;
            foreach ($request as $row) {
                $btn_edit = btn_edit($row->id);
                $btn_delete = btn_delete($row->id);

                $data[] = array(
                    $no++,
                    $row->company,
                    $row->karyawan,
                    $row->imei,
                    btn_group([$btn_edit, $btn_delete])
                );
            }
        }

        return response_datatable($draw, $total_filtered, $data);
    }

    function view_imei($start = 0, $length = 0, $search = '', $column = '', $dir = '')
    {
        $kolom = ['c.company', 'b.nama', 'a.imei'];
        $condition = search_datatable($kolom, $search);

        $kolom_order = ['1' => 'c.company', '2' => 'b.nama', '3' => 'a.imei'];
        $order = order_datatable($kolom_order, $column, $dir);

        # id company
        $company = company_id();
        if ($company != '') {
            $condition .= " AND a.id_company = '$company' ";
        }

        $query = $this->db->query("
            SELECT a.*, b.`nama` AS karyawan, c.`company`
            FROM tb_imei a
            INNER JOIN ms_karyawan b ON a.`id_karyawan` = b.`id`
            INNER JOIN ms_company c ON c.`id` = a.`id_company`
            WHERE 1 = 1
            $condition 
            $order 
            LIMIT $start, $length ")->result();

        return $query;
    }

    function total_imei($search = '')
    {
        $kolom = ['c.company', 'b.nama', 'a.imei'];
        $condition = search_datatable($kolom, $search);

        # id company
        $company = company_id();
        if ($company != '') {
            $condition .= " AND a.id_company = '$company' ";
        }

        $query = $this->db->query("
            SELECT COUNT(a.id) AS jumlah
            FROM tb_imei a
            INNER JOIN ms_karyawan b ON a.`id_karyawan` = b.`id`
            INNER JOIN ms_company c ON c.`id` = a.`id_company`
            WHERE 1 = 1
            $condition ")->row();

        return isset($query->jumlah) ? $query->jumlah : 0;
    }

    function simpan_karyawan($data = [], $username = '')
    {
        if (! empty($data) && $username != '') {
            $user_insert = username();
            $profile_name = isset($data['nama']) ? $data['nama'] : '';
            $id_company = isset($data['id_company']) ? $data['id_company'] : '';
            $key = generate_random(4);

            $this->db->trans_begin();

            $this->db->insert('ms_karyawan', $data);
            $insert_id = $this->db->insert_id();

            $this->db->query("
                INSERT INTO tb_user (`level`, id_company, id_karyawan, username, `password`, `key`, `profile_name`, user_insert)
                VALUES ('3', '$id_company', '$insert_id', '$username', AES_ENCRYPT('1234', '$key'), '$key', '$profile_name', '$user_insert'); ");

            if ($this->db->trans_status() === false) {
                $this->db->trans_rollback();

                $result = 0;
            } else {
                $this->db->trans_commit();

                $result = $insert_id;
            }
        } else {
            $result = 0;
        }

        return $result;
    }

    function json_berita($draw = 1, $start = 0, $length = 0, $search = '', $column = '', $dir = '')
    {
        $start = $this->db->escape_str($start);
        $length = $this->db->escape_str($length);
        $column = $this->db->escape_str($column);
        $dir = $this->db->escape_str($dir);
        $search = $this->db->escape_str($search);

        $total_filtered = $this->total_berita($search);
        $data = [];
        $request = $this->view_berita($start, $length, $search, $column, $dir);
        if (! empty($request)) {
            $no = $start + 1;
            foreach ($request as $row) {
                $btn_edit = btn_edit($row->id);
                $btn_delete = btn_delete($row->id);

                $image = '';
                if ($row->gambar != '') {
                    $image = '
                    <a href="'.base_url().'assets/uploads/images/news/'.$row->gambar.'" target="_blank">
                        <img src="'.base_url().'assets/uploads/images/news/'.$row->gambar.'" width="48">
                    </a>';
                }

                $data[] = array(
                    $no++,
                    $row->judul,
                    $row->tanggal,
                    $row->teks,
                    $image,
                    btn_group([$btn_edit, $btn_delete])
                );
            }
        }

        return response_datatable($draw, $total_filtered, $data);
    }

    function view_berita($start = 0, $length = 0, $search = '', $column = '', $dir = '')
    {
        $kolom = ['nik', 'nama', 'ktp', 'alamat', 'tgl_masuk', 'status'];
        $condition = search_datatable($kolom, $search);

        $kolom_order = ['1' => 'nik', '2' => 'nama', '3' => 'ktp', '4' => 'alamat', '5' => 'tgl_masuk', '6' => 'status'];
        $order = order_datatable($kolom_order, $column, $dir);

        # id company
        $company = company_id();
        if ($company != '') {
            $condition .= " AND id_company = '$company' ";
        }

        $query = $this->db->query("
            SELECT * 
            FROM (
                SELECT *, DATE_FORMAT(a.`tgl`, '%d/%m/%Y') AS tanggal
                FROM tb_news a
                ORDER BY a.tgl DESC
            ) AS berita 
            WHERE 1 = 1
            $condition 
            $order 
            LIMIT $start, $length ")->result();

        return $query;
    }

    function total_berita($search = '')
    {
        $kolom = ['nik', 'nama', 'ktp', 'alamat', 'tgl_masuk', 'status'];
        $condition = search_datatable($kolom, $search);

        # id company
        $company = company_id();
        if ($company != '') {
            $condition .= " AND id_company = '$company' ";
        }

        $query = $this->db->query("
            SELECT COUNT(*) AS jumlah 
            FROM (
                SELECT *, DATE_FORMAT(a.`tgl`, '%d/%m/%Y') AS tanggal
                FROM tb_news a
                ORDER BY a.tgl DESC
            ) AS berita 
            WHERE 1 = 1
            $condition ")->row();

        return isset($query->jumlah) ? $query->jumlah : 0;
    }

    function berita_id($id = '')
    {
        return $this->db->query("
            SELECT *, DATE_FORMAT(a.tgl, '%d/%m/%Y') AS tanggal
            FROM tb_news a 
            WHERE a.id = '$id'")->row();
    }

    function notif_berita($judul = '', $teks = '', $id_company = '')
    {
        $karyawan = $this->db->query("
            SELECT * 
            FROM ms_karyawan a 
            WHERE a.id_company = '$id_company' ")->result();

        if ($karyawan) {
            foreach ($karyawan as $row) {
                $this->firebasenotif->notif($judul, $teks, $row->fcm_id);
            }
        }
    }
}

/* End of file Karyawan_Model.php */
/* Location: ./application/models/Karyawan_Model.php */
