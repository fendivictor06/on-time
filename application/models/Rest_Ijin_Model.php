<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Rest_Ijin_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function view_ijin($start = 0, $count = 0, $id = '')
    {
        $id_karyawan = id_karyawan();

        $limit = '';
        if ($count > 0) {
            $limit = " LIMIT $start, $count ";
        }

        $condition = '';
        if ($id != '') {
            $condition = " WHERE a.id = '$id' ";
        }

        return $this->db->query("
    		SELECT *
    		FROM view_ijin a 
    		$condition
    		ORDER BY a.tgl DESC
    		$limit ")->result();
    }

    function view_approval($start = 0, $count = 0, $id = '')
    {
        $id_company = id_company();
        $id_karyawan = id_karyawan();

        $apv_ijin = $this->db->where('id_company', $id_company)
                        ->where('apv_ijin', $id_karyawan)
                        ->get('tb_setting')
                        ->row();

        if (empty($apv_ijin)) {
            return [];
        } else {
            $limit = '';
            if ($count > 0) {
                $limit = " LIMIT $start, $count ";
            }

            $condition = '';
            if ($id != '') {
                $condition = " AND a.id = '$id' ";
            }

            return $this->db->query("
                SELECT *
                FROM view_ijin a 
                WHERE a.status = 1
                $condition
                ORDER BY a.tgl DESC
                $limit ")->result();
        }
    }

    function approve_ijin($id = '', $status = '')
    {
        $result = 0;

        $id_company = id_company();
        $id_karyawan = id_karyawan();

        $apv_ijin = $this->db->where('id_company', $id_company)
                        ->where('apv_ijin', $id_karyawan)
                        ->get('tb_setting')
                        ->row();

        if (empty($apv_ijin)) {
            $result = 0;
        } else {
            $update = $this->db->where('id', $id)
                        ->update('tb_ijin', ['status' =>  $status]);

            $result = $this->db->affected_rows();
        }

        return $result;
    }
}

/* End of file Rest_Ijin_Model.php */
/* Location: ./application/models/Rest_Ijin_Model.php */
