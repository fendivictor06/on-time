<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class User_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function json_user($draw = 1, $start = 0, $length = 0, $search = '', $column = '', $dir = '')
    {
        $start = $this->db->escape_str($start);
        $length = $this->db->escape_str($length);
        $column = $this->db->escape_str($column);
        $dir = $this->db->escape_str($dir);
        $search = $this->db->escape_str($search);

        $total_filtered = $this->total_user($search);
        $data = [];
        $request = $this->view_user($start, $length, $search, $column, $dir);
        if (! empty($request)) {
            $no = $start + 1;
            foreach ($request as $row) {
                $btn_edit = btn_edit($row->id);
                $btn_delete = btn_delete($row->id);

                $data[] = array(
                    $no++,
                    $row->level_user,
                    $row->company,
                    $row->profile_name,
                    $row->username,
                    $row->last_login,
                    btn_group([$btn_edit, $btn_delete])
                );
            }
        }

        return response_datatable($draw, $total_filtered, $data);
    }

    function view_user($start = 0, $length = 0, $search = '', $column = '', $dir = '')
    {
        $kolom = ['level_user', 'company', 'profile_name', 'username', 'last_login'];
        $condition = search_datatable($kolom, $search);

        $kolom_order = ['1' => 'level_user', '2' => 'company', '3' => 'profile_name', '4' => 'username', '5' => 'last_login'];
        $order = order_datatable($kolom_order, $column, $dir);

        $id_company = company_id();
        $where = '';
        if ($id_company != '') {
            $where = " AND a.level = 3 
                       AND a.id_company = '$id_company' ";
        }

        $query = $this->db->query("
        	SELECT * 
        	FROM (
				SELECT a.`id`, level_user(a.`level`) AS level_user,
				b.`company`, a.`profile_name`, a.`username`, tgl_indo(a.`last_login`, 1) AS last_login
				FROM tb_user a
				LEFT JOIN ms_company b ON a.`id_company` = b.`id`
				WHERE a.`status` = 1
                $where 
			) AS users 
			WHERE 1 = 1
			$condition 
			$order 
			LIMIT $start, $length ")->result();

        return $query;
    }

    function total_user($search = '')
    {
        $kolom = ['level_user', 'company', 'profile_name', 'username', 'last_login'];
        $condition = search_datatable($kolom, $search);

        $id_company = company_id();
        $where = '';
        if ($id_company != '') {
            $where = " AND a.level = 3 
                       AND a.id_company = '$id_company' ";
        }

        $query = $this->db->query("
        	SELECT COUNT(*) AS jumlah 
        	FROM (
				SELECT a.`id`, level_user(a.`level`) AS level_user,
				b.`company`, a.`profile_name`, a.`username`, tgl_indo(a.`last_login`, 1) AS last_login
				FROM tb_user a
				LEFT JOIN ms_company b ON a.`id_company` = b.`id`
				WHERE a.`status` = 1
                $where
			) AS users 
			WHERE 1 = 1
			$condition ")->row();

        return isset($query->jumlah) ? $query->jumlah : 0;
    }

    function add_user($level = '', $id_company = '', $id_karyawan = '', $username = '', $password = '', $profile_name = '', $id = '')
    {
        $level = $this->db->escape_str($level);
        $id_company = $this->db->escape_str($id_company);
        $id_karyawan = $this->db->escape_str($id_karyawan);
        $username = $this->db->escape_str($username);
        $password = $this->db->escape_str($password);
        $profile_name = $this->db->escape_str($profile_name);
        $id = $this->db->escape_str($id);

        $key = generate_random(4);
        $user_insert = username();
        $now = now();
        $result = 0;

        if ($id == '') {
            $this->db->query("
                INSERT INTO tb_user (`level`, `id_company`, `id_karyawan`, `username`, `password`, `key`, `profile_name`, `insert_at`) 
                VALUES ('$level', '$id_company', '$id_karyawan', '$username', AES_ENCRYPT('$password', '$key'), '$key', '$profile_name', '$user_insert')");

            $result = $this->db->insert_id();
        } else {
            $this->db->query("
                UPDATE tb_user SET `level` = '$level', `id_company` = '$id_company', `id_karyawan` = '$id_karyawan', `password` = AES_ENCRYPT('$password', '$key'), `key` = '$key', `profile_name` = '$profile_name', `user_update` = '$user_insert', `update_at` = '$now'
                WHERE id = '$id' ");
            $result = 1;
        }

        return $result;
    }

    function user_exist($username = '', $level = '', $id_company = '')
    {
        if ($level == 1 || $level == 2) {
            $query = $this->db->query("
                SELECT *
                FROM tb_user a
                WHERE a.username = '$username'
                AND (a.level = 1 OR a.level = 2)")->row();
        } else {
            $query = $this->db->query("
                SELECT *
                FROM tb_user a
                WHERE a.username = '$username'
                AND a.level = 3
                AND a.id_company = '$id_company'")->row();
        }

        $result = (!empty($query)) ? true : false;

        return $result;
    }

    function ms_user($id_company = '')
    {
        return $this->db->query("
            SELECT `username`, AES_DECRYPT(`password`, `key`) AS pwd
            FROM tb_user
            WHERE id_company = '$id_company'")->row();
    }

    function check_password($username = '', $password = '')
    {
        $level = level_user();
        return $this->db->query("
            SELECT *
            FROM tb_user 
            WHERE `username` = '$username'
            AND `password` = AES_ENCRYPT('$password', `key`)
            AND `level` = '$level' ")->row();
    }

    function change_password($username = '', $password = '', $old_password = '')
    {
        $level = level_user();
        $this->db->query("
            UPDATE tb_user 
            SET `password` = AES_ENCRYPT('$password', `key`),
            update_at = NOW(), user_update = '$username'
            WHERE `username` = '$username' 
            AND AES_DECRYPT(`password`, `key`) = '$old_password'
            AND `level` = '$level' ");

        return $this->db->affected_rows();
    }
}

/* End of file User_Model.php */
/* Location: ./application/models/User_Model.php */
