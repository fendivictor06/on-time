<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Rest_Lembur_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function view_lembur($datestart = '', $dateend = '')
    {
        $id_karyawan = id_karyawan();

        return $this->db->query("
    		SELECT *
    		FROM view_lembur 
    		WHERE id_karyawan = '$id_karyawan' 
            AND (DATE(awal) BETWEEN '$datestart' AND '$dateend' 
            OR DATE(akhir) BETWEEN '$datestart' AND '$dateend' )")->result();
    }

    function rekap_lembur($datestart = '', $dateend = '')
    {
        $id_karyawan = id_karyawan();

        return $this->db->query("
            SELECT SUM(nominal) AS nominal
            FROM view_lembur 
            WHERE id_karyawan = '$id_karyawan' 
            AND (DATE(awal) BETWEEN '$datestart' AND '$dateend' 
            OR DATE(akhir) BETWEEN '$datestart' AND '$dateend' )")->row();
    }
}

/* End of file Rest_Lembur_Model.php */
/* Location: ./application/models/Rest_Lembur_Model.php */
