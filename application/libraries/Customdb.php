<?php
class Customdb
{
    function view_by_id($table = '', $condition = '', $row = 'row')
    {
        $ci =& get_instance();
        if ($row == 'row') {
            if ($condition) {
                return $ci->db->where($condition)->get($table)->row();
            } else {
                return $ci->db->get($table)->row();
            }
        } else {
            if ($condition) {
                return $ci->db->where($condition)->get($table)->result();
            } else {
                return $ci->db->get($table)->result();
            }
        }
    }

    function process_data($table = '', $data = '', $condition = '')
    {
        $ci =& get_instance();
        if ($condition) {
            $ci->db->where($condition)->update($table, $data);
            return $ci->db->affected_rows();
        } else {
            $ci->db->insert($table, $data);
            return $ci->db->insert_id();
        }
    }

    function delete_data($table = '', $condition = '')
    {
        $ci =& get_instance();
        $ci->db->where($condition)->delete($table);
        return $ci->db->affected_rows();
    }

    // connect to another db
    function create_connection($hostname = '', $username = '', $password = '', $database = '')
    {
        $ci =& get_instance();
        $config['hostname'] = $hostname;
        $config['username'] = $username;
        $config['password'] = $password;
        $config['database'] = $database;
        $config['dbdriver'] = 'mysqli';
        $config['dbprefix'] = '';
        $config['pconnect'] = false;
        $config['db_debug'] = true;

        $db_new = $ci->load->database($config, true);
        return $db_new;
    }
}
