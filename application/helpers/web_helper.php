<?php
    
function datatable($param = 'css')
{
    switch ($param) {
        case 'js':
            return '<script type="text/javascript" src="'.base_url().'assets/plugins/datatables/datatables.min.js"></script>
					<script type="text/javascript" src="'.base_url().'assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js"></script>';
            break;
        
        default:
            return '<link rel="stylesheet" type="text/css" href="'.base_url().'assets/plugins/datatables/datatables.min.css">
					<link rel="stylesheet" type="text/css" href="'.base_url().'assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css">';
            break;
    }
}

function tbl_template($id = 'dataTables-example', $class = '')
{
    $template = array(
            'table_open'            => '<table class="table table-striped table-bordered table-hover table-checkable order-column '.$class.'" id="'.$id.'">',

            'thead_open'            => '<thead>',
            'thead_close'           => '</thead>',

            'heading_row_start'     => '<tr>',
            'heading_row_end'       => '</tr>',
            'heading_cell_start'    => '<th>',
            'heading_cell_end'      => '</th>',

            'tbody_open'            => '<tbody>',
            'tbody_close'           => '</tbody>',

            'row_start'             => '<tr>',
            'row_end'               => '</tr>',
            'cell_start'            => '<td>',
            'cell_end'              => '</td>',

            'row_alt_start'         => '<tr>',
            'row_alt_end'           => '</tr>',
            'cell_alt_start'        => '<td>',
            'cell_alt_end'          => '</td>',

            'table_close'           => '</table>'
    );
    
    return $template;
}

function web_title()
{
    $ci =& get_instance();
    $class = $ci->router->fetch_class();
    $method = $ci->router->fetch_method();

    $menu = $ci->Menu_Model->parent($class, $method);

    return isset($menu->label) ? $menu->label : '';
}

function response_datatable($draw = '', $total_filtered = 0, $data = [])
{
    $arr = array(
        'draw' => $draw,
        'recordsFiltered' => $total_filtered,
        'recordsTotal' => $total_filtered,
        'data' => $data
    );

    return $arr;
}

function btn_edit($id = '')
{
    return '<a href="javascript:;" data-refid="'.$id.'" class="btn btn-warning m-btn m-btn--icon m-btn--icon-only m-btn--pill update">
                <i class="fa fa-pencil"></i>
            </a>';
}

function btn_delete($id = '')
{
    return '<a href="javascript:;" data-refid="'.$id.'" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill delete">
                <i class="fa fa-trash"></i>
            </a>';
}

function btn_custom($id = '', $action = '', $url = 'javascript:;', $class = 'btn-info', $icon = 'fa fa-check', $title = '')
{
    $target = '';
    if ($url != '') {
        $target = 'target="_blank"';
    }

    return '<a href="'.$url.'" data-refid="'.$id.'" class="btn '.$class.' m-btn m-btn--icon m-btn--icon-only m-btn--pill '.$action.'" title="'.$title.'" '.$target.'>
                <i class="'.$icon.'"></i>
            </a>';
}

function btn_group($button = [])
{
    $result = '';
    if (! empty($button)) {
        $result .= '<div class="m-demo__preview m-demo__preview--btn">';
        for ($i = 0; $i < count($button); $i++) {
            $result .= $button[$i].'&nbsp;';
        }
        $result .= '</div>';
    }

    return $result;
}

function get_params()
{
    return json_decode(file_get_contents('php://input'), true);
}

function response($status = 200, $message = '', $data = [])
{
    return array(
        'response' => $data,
        'metadata' => array(
            'status' => $status,
            'message' => $message
        )
    );
}

function print_json($status = 200, $message = '', $data = [])
{
    $ci =& get_instance();
    $response = response($status, $message, $data);

    return $ci->token->print_json($response);
}

function user_id()
{
    $ci =& get_instance();
    return $ci->input->get_request_header('User-Id');
}

function id_karyawan()
{
    $ci =& get_instance();
    return $ci->input->get_request_header('Id-Karyawan');
}

function id_company()
{
    $ci =& get_instance();
    return $ci->input->get_request_header('Id-Company');
}

function id_user()
{
    $ci =& get_instance();
    return $ci->input->get_request_header('Id-User');
}

function token_auth()
{
    $ci =& get_instance();
    return $ci->input->get_request_header('Token');
}

function setting_mac()
{
    $ci =& get_instance();
    $id_company = id_company();

    $setting = $ci->customdb->view_by_id('tb_setting', ['id_company' => $id_company], 'row');
    $macaddress = isset($setting->macaddress) ? $setting->macaddress : 0;

    return $macaddress;
}

function setting_imei()
{
    $ci =& get_instance();
    $id_company = id_company();

    $setting = $ci->customdb->view_by_id('tb_setting', ['id_company' => $id_company], 'row');
    $imei = isset($setting->imei) ? $setting->imei : 0;

    return $imei;
}

function apv_cuti()
{
    $ci =& get_instance();
    $id_company = id_company();

    $setting = $ci->customdb->view_by_id('tb_setting', ['id_company' => $id_company], 'row');
    $id_karyawan = isset($setting->apv_cuti) ? $setting->apv_cuti : '';

    $karyawan = $ci->customdb->view_by_id('ms_karyawan', ['id' => $id_karyawan], 'row');
    $fcm_id = isset($karyawan->fcm_id) ? $karyawan->fcm_id : '';

    return $fcm_id;
}

function apv_ijin()
{
    $ci =& get_instance();
    $id_company = id_company();

    $setting = $ci->customdb->view_by_id('tb_setting', ['id_company' => $id_company], 'row');
    $id_karyawan = isset($setting->apv_ijin) ? $setting->apv_ijin : '';

    $karyawan = $ci->customdb->view_by_id('ms_karyawan', ['id' => $id_karyawan], 'row');
    $fcm_id = isset($karyawan->fcm_id) ? $karyawan->fcm_id : '';

    return $fcm_id;
}

function log_api($request = '', $status = '', $message = '', $data = [])
{
    $ci =& get_instance();

    $ci->load->model('Log_Model');

    $response = array(
        'status' => $status,
        'message' => $message,
        'data' => $data
    );

    $response = json_encode($response);
    $request = json_encode($request);

    $user = user_id();

    $ci->Log_Model->log($request, $response, $user);
}

function search_datatable($kolom = [], $search = '')
{
    $condition = '';
    if ($search != '') {
        $condition .= ' AND (';
        for ($i = 0; $i < count($kolom); $i++) {
            $condition .= $kolom[$i]." LIKE '%$search%' ";
            if ($kolom[$i] != end($kolom)) {
                $condition .= ' OR ';
            }
        }
        $condition .= ')';
    }

    return $condition;
}

function order_datatable($kolom_order = [], $column = '', $dir = '')
{
    $order = '';
    if ($column != '' && $dir != '') {
        $col = isset($kolom_order[$column]) ? $kolom_order[$column] : '';

        if ($col != '') {
            $order .= " ORDER BY $col $dir ";
        }
    }

    return $order;
}

function decode_image($file = '', $path = '')
{
    $decoder = base64_decode($file);
    header('Content-Type: bitmap; charset=utf-8');
    $open = fopen($path, 'wb');
    fwrite($open, $decoder);
    fclose($open);
}

function path_foto()
{
    return './assets/uploads/images/foto/';
}

function path_gaji()
{
    return './assets/uploads/gaji/';
}

function path_karyawan()
{
    return './assets/uploads/karyawan/';
}

function link_foto()
{
    return base_url('assets/uploads/images/foto/');
}

function link_scan()
{
    return base_url('assets/uploads/images/scanlog/');
}

function link_news()
{
    return base_url('assets/uploads/images/news/');
}

function filter_params($params = '', $index = '')
{
    $ci =& get_instance();
    return isset($params[$index]) ? $ci->db->escape_str($params[$index]) : '';
}

function filter_isset($params = '')
{
    return isset($params) ? $params : '';
}

function request_url($url = '', $data = [], $method = 'POST', $header = [])
{
    ini_set('memory_limit', '-1');
    ini_set('MAX_EXECUTION_TIME', '-1');
    set_time_limit(0);
    
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_CUSTOMREQUEST => $method,
        CURLOPT_POSTFIELDS => json_encode($data),
        CURLOPT_HTTPHEADER => $header,
        CURLOPT_CAINFO => dirname(__FILE__).'\cacert.pem'
    ));
 
    $response = curl_exec($curl);
    $error = curl_error($curl);

    curl_close($curl);

    if ($error) {
        return $error;
    } else {
        return $response;
    }
}
