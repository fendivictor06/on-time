<?php

function username()
{
    $ci =& get_instance();

    return $ci->session->userdata('username');
}

function profile_name()
{
    $ci =& get_instance();

    return $ci->session->userdata('profile_name');
}

function level_user()
{
    $ci =& get_instance();

    $level = $ci->session->userdata('level');

    return $level;
}

function company_id()
{
    $ci =& get_instance();
    $level = level_user();

    $company = '';
    if ($level != 1) {
        $company = $ci->session->userdata('id_company');
    }

    return $company;
}
