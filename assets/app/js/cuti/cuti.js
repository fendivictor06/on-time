var cache = sessionStorage.getItem('cuti_save_state');
if (cache) {
	cache = JSON.parse(cache);
	var start = cache.awal;
	var end = cache.akhir;
} else {
	var start = moment().format('DD/MM/YYYY');
	var end = moment().format('DD/MM/YYYY');
}

var table = $("#tb-jenis").DataTable({
	processing : true,
	serverSide : true,
	responsive :true,
	ajax : base_url+'Cuti/data_cuti?datestart='+start+'&dateend='+end,
	language : {
        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="font-size:36px;"></i><span class="sr-only"></span> '
    },
    order : [[3, 'desc']],
	columns : [
		{'orderable' : false},
		null,
		null,
		null,
		null,
		null,
		null,
		{'orderable' : false}
	]
});

$('#search').daterangepicker({
    buttonClasses: 'm-btn btn',
    applyClass: 'btn-primary',
    cancelClass: 'btn-secondary',
    startDate: start,
    endDate: end,
    locale: {
        format: 'DD/MM/YYYY'
    }
}, function (start, end){
	start = start.format('DD/MM/YYYY');
	end = end.format('DD/MM/YYYY');

	var cache = {
		awal : start,
		akhir : end
	}

	sessionStorage.setItem('cuti_save_state', JSON.stringify(cache));
});  

$("#search-btn").click(function(){
	var cache = sessionStorage.getItem('cuti_save_state');
	if (cache) {
		cache = JSON.parse(cache);
		var start = cache.awal;
		var end = cache.akhir;
	} else {
		var start = moment().format('DD/MM/YYYY');
		var end = moment().format('DD/MM/YYYY');
	}

	table.ajax.url(base_url+'Cuti/data_cuti?datestart='+start+'&dateend='+end).load();
});

$(document).on('click', '.delete', function(){
	refid = $(this).data('refid');
	swal({
	  	text: "Apakah anda yakin akan menghapus data ?",
	  	icon: "warning",
	  	buttons: true,
	  	dangerMode: true,
	})
	.then((willDelete) => {
	  	if (willDelete) {
	  		$.ajax({
	  			url : base_url+'Cuti/hapus_cuti',
	  			type : 'post',
	  			data : {
	  				'id' : refid
	  			},
	  			dataType : 'json',
	  			success : function(data) {
	  				if(data.status == 1) {
		                table.ajax.reload(null, false);
		                toastr.success(data.message);
		            } else {
		                toastr.warning(data.message);
		            }
	  			},
	  			error : function() {
	  				toastr.warning('Terjadi kesalahan saat menghapus data');
	  			}
	  		});
	  	}
	});
});