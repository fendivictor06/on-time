var table = $("#tb-status").DataTable({
	processing : true,
	serverSide : true,
	responsive :true,
	ajax : base_url+'Master/data_status_nikah',
	columns : [
		{'orderable' : false},
		null,
		{'orderable' : false}
	]
});

function clear_form() {
	$("#id").val("");
	document.getElementById('form-data').reset();
}

$("#add_new").click(function(){
	clear_form();
	$("#m_modal").modal('show');
});

$("#form-data").submit(function(event) {
	event.preventDefault();
	formData = new FormData($(this)[0]);
	$.ajax({
		url : base_url+"Master/add_status_nikah",
        type : "post",
        data : formData,
        async : false,
        cache : false,
        dataType : "json",
        contentType : false,
        processData : false,
        beforeSend : function(){
            mApp.block('#m_modal .modal-content', {
                overlayColor: '#000000',
                type: 'loader',
                state: 'success',
                size: 'lg'
            });
        },
        complete : function(){
            mApp.unblock('#m_modal .modal-content');        
        },
        success : function(data) {
            if(data.status == 1) {
                table.ajax.reload(null, false);
                clear_form();
                toastr.success(data.message);
                $("#m_modal").modal('toggle');
            } else {
                toastr.warning(data.message);
            }
        },
        error : function() {
            toastr.warning('Terjadi kesalahan saat menyimpan data');
        }
	});

	return false;
});

$(document).on('click', '.update', function(){
	refid = $(this).data('refid');
	$.ajax({
		url : base_url+"Master/id_status_nikah",
		type : "post",
		data : {
			'id' : refid
		},
		dataType : "json",
		success : function(data) {
			$("#m_modal").modal('show');
			$("#id").val(data.id);
			$("#status_nikah").val(data.status_nikah);
		},
		error : function() {
			toastr.warning('Terjadi kesalahan saat memuat data');
		}
	});
});

$(document).on('click', '.delete', function(){
	refid = $(this).data('refid');
	swal({
	  	text: "Apakah anda yakin akan menghapus data ?",
	  	icon: "warning",
	  	buttons: true,
	  	dangerMode: true,
	})
	.then((willDelete) => {
	  	if (willDelete) {
	  		$.ajax({
	  			url : base_url+'Master/hapus_status_nikah',
	  			type : 'post',
	  			data : {
	  				'id' : refid
	  			},
	  			dataType : 'json',
	  			success : function(data) {
	  				if(data.status == 1) {
		                table.ajax.reload(null, false);
		                toastr.success(data.message);
		            } else {
		                toastr.warning(data.message);
		            }
	  			},
	  			error : function() {
	  				toastr.warning('Terjadi kesalahan saat menghapus data');
	  			}
	  		});
	  	}
	});
});

