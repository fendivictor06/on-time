function clear_form() {
	$("#id").val("");
	document.getElementById('form-data').reset();
}

$("#add_new").click(function(){
	clear_form();
	removeMarker();
	
	$("#m_modal").modal('show');
});

$("#m_modal").on('show.bs.modal', function() {
	$('#company').select2({
	    placeholder: "Pilih Perusahaan"
	});
});

var map;
function initMap() {
	let location = staticLocation();
	let lat = location.lat;
	let lng = location.lng;

	let config = {
		center : new google.maps.LatLng(lat, lng),
		zoom : 12
	}

	map = new google.maps.Map(document.getElementById('google-maps'), config);
}

var markers = [];
let placeMarker = function(location) {
	var marker = new google.maps.Marker({
		position : location,
		map : map
	});

	$("#latitude").val(location.lat());
	$("#longitude").val(location.lng());

	markers.push(marker);
}

let removeMarker = function() {
	for (i = 0; i < markers.length; i++) {
		markers[i].setMap(null);
	}
}

$(document).ready(function(){
	initMap();

	google.maps.event.addListener(map, 'click', function(event){
		let location = event.latLng;

		removeMarker();
		placeMarker(location);
	});
});

var company = $("#company-search").val();
var configTable = serverSideTable('Lokasi/data_lokasi?company='+company);
configTable.columns = [
	{'orderable' : false}, 
	null, 
	null, 
	null, 
	{'orderable' : false}
];
var table = $("#tb-data").DataTable(configTable);

$("#search-btn").click(function(){
	var company = $("#company-search").val();
	table.ajax.url(base_url+'Lokasi/data_lokasi?company='+company).load();
});

$("#form-data").submit(function(event){
	event.preventDefault();
	formData = new FormData($(this)[0]);
	blockModal();
	let config = axiosDefault('post', 'Lokasi/add_lokasi', formData);
	axios(config).then(function(response){
		if (response.data.status == 1) {
			table.ajax.reload(null, false);
			clear_form();
			toastr.success(response.data.message);
			$("#m_modal").modal('toggle');
		} else {
			toastr.warning(response.data.message);
		}

		unblockModal();
	}).catch(function(error){
		catchMessage('save', error);
		unblockModal();
	});

	return false;
});

$(document).on('click', '.update', function(){
	refid = $(this).data('refid');
	let config = axiosDefault('get', 'Lokasi/lokasi_id?id='+refid);
	blockModal();
	axios(config).then(function(response){
		$("#m_modal").modal('show');
		$("#id").val(response.data.id);
		$("#company").val(response.data.id_company).trigger('change');
		$("#nama").val(response.data.nama);
		$("#latitude").val(response.data.latitude);
		$("#longitude").val(response.data.longitude);
		$("#toleransi").val(response.data.toleransi);

		let location = new google.maps.LatLng(response.data.latitude, response.data.longitude);
		placeMarker(location);
		map.setCenter(location);

		unblockModal();
	}).catch(function(error){
		catchMessage('get', error);
		unblockModal();
	});
});

$(document).on('click', '.delete', function(){
	refid = $(this).data('refid');
	swal({
	  	text: "Apakah anda yakin akan menghapus data ?",
	  	icon: "warning",
	  	buttons: true,
	  	dangerMode: true,
	})
	.then((willDelete) => {
	  	if (willDelete) {
	  		blockPage();
	  		let config = axiosDefault('get', 'Lokasi/hapus_lokasi?id='+refid);
	  		axios(config).then(function(response){
	  			if (response.data.status == 1) {
					table.ajax.reload(null, false);
					toastr.success(response.data.message);
				} else {
					toastr.warning(response.data.message);
				}

	  			unblockPage();
	  		}).catch(function(error){
	  			catchMessage('delete', error);
	  			unblockPage();
	  		});	
	  	}
	});
});