var table = $("#tb-jenis").DataTable({
	processing : true,
	serverSide : true,
	responsive :true,
	ajax : base_url+'Gaji/info_gaji',
	language : {
        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="font-size:36px;"></i><span class="sr-only"></span> '
    },
    order : [[1, 'desc']],
	columns : [
		{'orderable' : false},
		null,
		null,
		null,
		null,
		null,
		{
	        className: 'dt-body-right'
	    }
	]
});