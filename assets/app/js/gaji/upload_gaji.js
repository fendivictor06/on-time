$("#upload-gaji").submit(function(event) {
    event.preventDefault();
    formData = new FormData($(this)[0]);
    $.ajax({
        url : base_url+"Upload/add_gaji",
        type : "post",
        data : formData,
        async : false,
        cache : false,
        dataType : "json",
        contentType : false,
        processData : false,
        beforeSend : function(){
            mApp.block('.m-content', {
                overlayColor: '#000000',
                type: 'loader',
                state: 'success',
                size: 'lg'
            });
        },
        complete : function(){
            mApp.unblock('.m-content');
        },
        success : function(data) {
            if(data.status == 1) {
                toastr.success(data.message);
                document.getElementById('upload-gaji').reset();
            } else {
                toastr.warning(data.message);
            }
        },
        error : function() {
            toastr.warning('Terjadi kesalahan saat menyimpan data');
        }
    });

    return false;
});