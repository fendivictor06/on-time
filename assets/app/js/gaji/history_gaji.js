$(document).ready(function(){
	var date = new Date();
	var bulan = date.getMonth();
	var tahun = date.getFullYear();

	bulan = parseFloat(bulan + 1);

	$("#bulan").val(bulan).trigger('change');
	$("#tahun").val(tahun).trigger('change');

	var bulanx = $("#bulan").val();
	var tahunx = $("#tahun").val();

	var table = $("#tb-jenis").DataTable({
		processing : true,
		serverSide : true,
		responsive :true,
		ajax : base_url+'Gaji/history_gaji?bulan='+bulanx+'&tahun='+tahunx,
		language : {
	        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="font-size:36px;"></i><span class="sr-only"></span> '
	    },
	    order : [[1, 'desc']],
		columns : [
			{'orderable' : false},
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			{
		        className: 'dt-body-right'
		    }
		]
	});

	$("#search-btn").click(function(){
		var bulanx = $("#bulan").val();
		var tahunx = $("#tahun").val();
		table.ajax.url(base_url+'Gaji/history_gaji?bulan='+bulanx+'&tahun='+tahunx).load();
	});
});