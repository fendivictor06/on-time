var cache = sessionStorage.getItem('scanlog_save_state');
if (cache) {
	cache = JSON.parse(cache);
	var start = cache.awal;
	var end = cache.akhir;
} else {
	var start = moment().format('DD/MM/YYYY');
	var end = moment().format('DD/MM/YYYY');
}

var table = $("#tb-scanlog").DataTable({
	processing : true,
	serverSide : true,
	responsive :true,
	ajax : base_url+'Scanlog/data_scanlog?datestart='+start+'&dateend='+end,
	language : {
        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="font-size:36px;"></i><span class="sr-only"></span> '
    },
	columns : [
		{'orderable' : false},
		null,
		null,
		null,
		null,
		{'orderable' : false},
		{'orderable' : false}
	]
});

$(document).ready(function(){
	$("#company-search").val('').trigger('change');
	$("#company-search").select2({
		placeholder : "Pilih Perusahaan"
	});

	$('#search').daterangepicker({
        buttonClasses: 'm-btn btn',
        applyClass: 'btn-primary',
        cancelClass: 'btn-secondary',
        startDate: start,
        endDate: end,
        locale: {
            format: 'DD/MM/YYYY'
        }
    }, function (start, end){
    	start = start.format('DD/MM/YYYY');
    	end = end.format('DD/MM/YYYY');

    	var cache = {
    		awal : start,
    		akhir : end
    	}

    	sessionStorage.setItem('scanlog_save_state', JSON.stringify(cache));
    }); 
});

$("#search-btn").click(function(){
	var cache = sessionStorage.getItem('scanlog_save_state');
	if (cache) {
		cache = JSON.parse(cache);
		var start = cache.awal;
		var end = cache.akhir;
	} else {
		var start = moment().format('DD/MM/YYYY');
		var end = moment().format('DD/MM/YYYY');
	}

	// search parameter
	var companySearch = $("#company-search").val();
	var karyawanSearch = $("#search-karyawan").val();

	table.ajax.url(base_url+'Scanlog/data_scanlog?datestart='+start+'&dateend='+end+'&company='+companySearch+'&karyawan='+karyawanSearch).load();
});


$("#company-search").change(function(){
	var companySearch = $(this).val();

	$("#search-karyawan").val(null).trigger('change');
	ajax_select2(base_url+'Shift/ajax_karyawan/'+companySearch, '#search-karyawan', 'Pilih Karyawan');
});

var ajax_select2 = function(url, element, placeholder) {
	$(element).select2({
		placeholder: placeholder,
		allowClear: true,
		ajax: {
			url: url,
			dataType: 'json',
			delay: 250,
			data: function(params){
				return {
					q: params.term
				}
			},
			processResults: function(data) {
				var results = [];
				$.each(data, function(i, val){
					results.push({
						id: val.id,
						text: val.text
					});
				});

				return {
					results : results
				}
			}
		}
	});
}