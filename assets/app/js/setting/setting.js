$(document).ready(function(){
	load_data();

	$("#cuti").select2({
		placeholder : "Approval Cuti"
	});

	$("#ijin").select2({
		placeholder : "Approval Ijin"
	});
});

var init_select = function(id_cuti, id_ijin) {
	$("#cuti option").each(function() {
	    $(this).remove();
	});

	$("#ijin option").each(function() {
	    $(this).remove();
	});

	$.ajax({
		url : base_url+'Shift/ajax_karyawan/',
		dataType : 'json',
		success: function(data) {
			$("#cuti").attr('enabled', true);
			$.each(data, function(){
				$("#cuti").append(
					$('<option></option>').text(this.text).val(this.id)
				);
			});

			$("#ijin").attr('enabled', true);
			$.each(data, function(){
				$("#ijin").append(
					$('<option></option>').text(this.text).val(this.id)
				);
			});

			$("#cuti").val(id_cuti).trigger('change');
			$("#ijin").val(id_ijin).trigger('change');
		}
	});
}

function load_data() {
	$.ajax({
		url : base_url+'Setting/load_setting',
		dataType : 'json',
		beforeSend : function() {
			mApp.block('.m-content', {
                overlayColor: '#000000',
                type: 'loader',
                state: 'success',
                size: 'lg'
            });
		},
		complete : function() {
			mApp.unblock('.m-content');
		},
		success : function(data) {
			$("#imei").prop('checked', false);
			$("#macaddress").prop('checked', false);
			if (data.imei == 1) {
				$("#imei").prop('checked', true);
			}

			if (data.macaddress == 1) {
				$("#macaddress").prop('checked', true);
			}

			init_select(data.apv_cuti, data.apv_ijin);
		},
		error : function() {
			toastr.warning('Terjadi kesalahan saat memuat data');
		}
	});
}

function simpan() {
	var imei = 0;
	if ($("#imei").is(":checked")) {
		imei = 1;
	}

	var macaddress = 0;
	if ($("#macaddress").is(":checked")) {
		macaddress = 1;
	}

	var ijin = $("#ijin").val();
	var cuti = $("#cuti").val();

	$.ajax({
		url : base_url+'Setting/simpan_setting',
		data : {
			'imei' : imei,
			'macaddress' : macaddress,
			'ijin' : ijin,
			'cuti' : cuti
		},
		dataType : 'json',
		type : 'post',
		beforeSend : function(){
			mApp.block('.m-content', {
                overlayColor: '#000000',
                type: 'loader',
                state: 'success',
                size: 'lg'
            });
		},
		complete : function(){
			mApp.unblock('.m-content');
		},
		success : function(data) {
			if(data.status == 1) {
                toastr.success(data.message);
            } else {
                toastr.warning(data.message);
            }

            load_data();
		},
		error : function() {
			toastr.warning('Terjadi kesalahan saat menyimpan data');
		}
	})
}

$("#simpan").click(function(){
	simpan();
});