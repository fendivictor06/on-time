const axiosDefault = (method, url, data) => {
	return {
		method : method,
		url : base_url+url,
		data : data,
		responseType : 'json',
		headers : {'X-Requested-With': 'XMLHttpRequest'}
	}
};

const blockContent = () => {
	mApp.block('.m-content', {
        overlayColor: '#000000',
        type: 'loader',
        state: 'success',
        size: 'lg'
    });
}

const unblockContent = () => {
	mApp.unblock('.m-content');
}

const blockModal = () => {
	mApp.block('#m_modal .modal-content', {
        overlayColor: '#000000',
        type: 'loader',
        state: 'success',
        size: 'lg'
    });
}

const unblockModal = () => {
	mApp.unblock('#m_modal .modal-content');     
}

const blockPage = () => {
	mApp.blockPage({
        overlayColor: '#000000',
        type: 'loader',
        state: 'success',
        size: 'lg'
    });
}

const unblockPage = () => {
	mApp.unblockPage();
}

const resetForm = (form) => {
	document.getElementById(form).reset();
}

const catchMessage = (params, error) => {
	var message;
	var status_code = error.response.status;
	if (status_code == 401) {
		toastr.error('Unauthorized');

		location.reload();
	} else if (status_code == 404) {
		toastr.error('Not Found!');
	} else {
		if (params == 'save') {
			message = 'Terjadi kesalahan saat menyimpan data';
		} else if (params == 'get') {
			message = 'Terjadi kesalahan saat memuat data';
		} else {
			message = 'Terjadi kesalahan saat menghapus data';
		}

		toastr.error(message);
	}
}

const datePicker = (el) => {
	$(el).datepicker({
        todayHighlight: true,
        orientation: "bottom left",
        format: 'dd/mm/yyyy',
        autoclose : true,
        templates: {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    });
}

const tglConvert = (tgl) => {
	tgl = tgl.split('-');
	tgl = [tgl[2], tgl[1], tgl[0]].join('/');

	return tgl;
}

const staticLocation = () => {
	return {
        lat: '-6.966667',
        lng: '110.416664'
    };
}

const currentLocation = () => {
	// Try HTML5 geolocation.
    if (navigator.geolocation) {
      	navigator.geolocation.getCurrentPosition(function(position) {
        var pos = {
          	lat: position.coords.latitude,
          	lng: position.coords.longitude
        };

      	}, function() {
        	var pos = staticLocation();
      	});
    } else {
      	// Browser doesn't support Geolocation
      	var pos = staticLocation();
    }

    return pos;
}

const serverSideTable = (url) => {
	return {
		processing : true,
		serverSide : true,
		responsive :true,
		ajax : base_url+url,
		language : {
	        processing: '<i class="fa fa-refresh fa-spin" style="font-size:32px;"></i><span class="sr-only"></span>',
	        aria: {
	            sortAscending: ": activate to sort column ascending",
	            sortDescending: ": activate to sort column descending"
	        },
	        emptyTable: "Tidak ada data ditemukan",
	        info: "Showing _START_ to _END_ of _TOTAL_ entries",
	        infoEmpty: "No entries found",
	        infoFiltered: "(filtered1 from _MAX_ total entries)",
	        lengthMenu: "_MENU_ entries",
	        search: "Search:",
	        zeroRecords: "Tidak ada data ditemukan"
	    }
	};
}

const dropdownSelect = (el, url, label, minInput) => {
	$(el).select2({
		minimumInputLength: minInput,
		placeholder: label,
		allowClear: true,
		ajax: {
			url: base_url+url,
			dataType: 'json',
			delay: 250,
			data: function(params){
				return {
					q: params.term
				}
			},
			processResults: function(data) {
				var results = [];
				$.each(data, function(i, val){
					results.push({
						id: val.id,
						text: val.text
					});
				});

				return {
					results : results
				}
			}
		}
	});
}

const initSelect = (el, url, init) => {
	$(el+" option").each(function() {
	    $(this).remove();
	});

	let config = axiosDefault('post', url, {});
	axios(config).then(function(response){
		$(el).attr('enabled', true);
		$.each(response.data, function(){
			$(el).append(
				$('<option></option>').text(this.text).val(this.id)
			);
		});

		$(el).val(init).trigger('change');
	}).catch(function(error){
		console.error(error);
	});
}

const dateRangeConfig = () => {
	return {
        buttonClasses: 'm-btn btn',
        applyClass: 'btn-primary',
        cancelClass: 'btn-secondary',
        startDate: start,
        endDate: end,
        locale: {
            format: 'DD/MM/YYYY'
        }
    }
}

$(".dropdown-select2").select2({
	placeholder : "Pilih Item"
});

const sessionSetItem = (name, data) => {
	sessionStorage.setItem(name, JSON.stringify(data));
}

const sessionGetItem = (name) => {
	return sessionStorage.getItem(name);
}

const disableAlertDatatable = () => {
	$.fn.dataTable.ext.errMode = 'throw';
}