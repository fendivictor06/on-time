var table = $("#tb-jenis").DataTable({
	processing : true,
	serverSide : true,
	responsive :true,
	language : {
        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="font-size:36px;"></i><span class="sr-only"></span> '
    },
	ajax : base_url+'Karyawan/data_imei',
	columns : [
		{'orderable' : false},
		null,
		null,
		null,
		{'orderable' : false}
	]
});

function clear_form() {
	$("#id").val("");
	document.getElementById('form-data').reset();
}

$("#add_new").click(function(){
	clear_form();
	$("#m_modal").modal('show');
	init_select();
});

$("#m_modal").on('show.bs.modal', function() {
	$('#company').select2({
	    placeholder: "Pilih Perusahaan"
	});

	$('#karyawan').select2({
	    placeholder: "Pilih Karyawan"
	});
});

var init_select = function(id_karyawan) {
	var company = $("#company").val();
	var arr_karyawan = [];
	$("#karyawan option").each(function() {
	    $(this).remove();
	});

	$.ajax({
		url : base_url+'Shift/ajax_karyawan/'+company,
		dataType : 'json',
		success: function(data) {
			$("#karyawan").attr('enabled', true);
			$.each(data, function(){
				$("#karyawan").append(
					$('<option></option>').text(this.text).val(this.id)
				);
			});

			if (id_karyawan != '') {
				$("#karyawan").val(id_karyawan).trigger('change');
			}
		}
	});
}

$("#company").change(function(){
	var company = $(this).val();

	$("#karyawan").val(null).trigger('change');
	ajax_select2(base_url+'Shift/ajax_karyawan/'+company, '#karyawan', 'Pilih Karyawan');
});

var ajax_select2 = function(url, element, placeholder) {
	$(element).select2({
		placeholder: placeholder,
		allowClear: true,
		ajax: {
			url: url,
			dataType: 'json',
			delay: 250,
			data: function(params){
				return {
					q: params.term
				}
			},
			processResults: function(data) {
				var results = [];
				$.each(data, function(i, val){
					results.push({
						id: val.id,
						text: val.text
					});
				});

				return {
					results : results
				}
			}
		}
	});
}

$("#form-data").submit(function(event) {
	event.preventDefault();
	formData = new FormData($(this)[0]);
	$.ajax({
		url : base_url+"Karyawan/add_imei",
        type : "post",
        data : formData,
        async : false,
        cache : false,
        dataType : "json",
        contentType : false,
        processData : false,
        beforeSend : function(){
            mApp.block('#m_modal .modal-content', {
                overlayColor: '#000000',
                type: 'loader',
                state: 'success',
                size: 'lg'
            });
        },
        complete : function(){
            mApp.unblock('#m_modal .modal-content');        
        },
        success : function(data) {
            if(data.status == 1) {
                table.ajax.reload(null, false);
                clear_form();
                toastr.success(data.message);
                $("#m_modal").modal('toggle');
            } else {
                toastr.warning(data.message);
            }
        },
        error : function() {
            toastr.warning('Terjadi kesalahan saat menyimpan data');
        }
	});

	return false;
});

$(document).on('click', '.update', function(){
	refid = $(this).data('refid');
	$.ajax({
		url : base_url+"Karyawan/imei_id",
		type : "post",
		data : {
			'id' : refid
		},
		dataType : "json",
		success : function(data) {
			$("#m_modal").modal('show');
			$("#id").val(data.id);
			$("#company").val(data.id_company).trigger('change');

			// $("#karyawan").val(null).trigger('change');
			// ajax_select2(base_url+'Shift/ajax_karyawan/'+data.id_company, '#karyawan', 'Pilih Karyawan');

			init_select(data.id_karyawan);

			$("#karyawan").val(data.id_karyawan).trigger('change');
			$("#imei").val(data.imei);
		},
		error : function() {
			toastr.warning('Terjadi kesalahan saat memuat data');
		}
	});
});

$(document).on('click', '.delete', function(){
	refid = $(this).data('refid');
	swal({
	  	text: "Apakah anda yakin akan menghapus data ?",
	  	icon: "warning",
	  	buttons: true,
	  	dangerMode: true,
	})
	.then((willDelete) => {
	  	if (willDelete) {
	  		$.ajax({
	  			url : base_url+'Karyawan/hapus_imei',
	  			type : 'post',
	  			data : {
	  				'id' : refid
	  			},
	  			dataType : 'json',
	  			success : function(data) {
	  				if(data.status == 1) {
		                table.ajax.reload(null, false);
		                toastr.success(data.message);
		            } else {
		                toastr.warning(data.message);
		            }
	  			},
	  			error : function() {
	  				toastr.warning('Terjadi kesalahan saat menghapus data');
	  			}
	  		});
	  	}
	});
});

