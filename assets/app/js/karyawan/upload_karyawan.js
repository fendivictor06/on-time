$("#upload-karyawan").submit(function(event) {
    event.preventDefault();
    formData = new FormData($(this)[0]);
    blockContent();
    let config = axiosDefault('post', 'Upload/add_karyawan', formData);

    axios(config).then(function(response){
        if (response.data.status == 1) {
            toastr.success(response.data.message);
            resetForm('upload-karyawan');
        } else {
            toastr.warning(response.data.message);
        }

        unblockContent();
    }).catch(function(error){
        catchMessage('save', error);
        unblockContent();
    });

    return false;
});