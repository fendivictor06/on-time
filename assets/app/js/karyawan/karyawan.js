var table = $("#tb-karyawan").DataTable({
	processing : true,
	serverSide : true,
	responsive :true,
	ajax : base_url+'Karyawan/data_karyawan',
	columns : [
		{'orderable' : false},
		null,
		null,
		null,
		null,
		null,
		null,
		{'orderable' : false}
	]
});

$("#add_new").click(function(){
	window.open(base_url+'Karyawan/form_karyawan', '_blank');
});

$(document).on('click', '.update', function(){
	refid = $(this).data('refid');
	window.open(base_url+'Karyawan/form_edit/'+refid, '_blank');
});

$(document).on('click', '.delete', function(){
	refid = $(this).data('refid');
	swal({
	  	text: "Apakah anda yakin akan menghapus data ?",
	  	icon: "warning",
	  	buttons: true,
	  	dangerMode: true,
	})
	.then((willDelete) => {
	  	if (willDelete) {
	  		$.ajax({
	  			url : base_url+'Karyawan/hapus_karyawan',
	  			type : 'post',
	  			data : {
	  				'id' : refid
	  			},
	  			dataType : 'json',
	  			success : function(data) {
	  				if(data.status == 1) {
		                table.ajax.reload(null, false);
		                toastr.success(data.message);
		            } else {
		                toastr.warning(data.message);
		            }
	  			},
	  			error : function() {
	  				toastr.warning('Terjadi kesalahan saat menghapus data');
	  			}
	  		});
	  	}
	});
});