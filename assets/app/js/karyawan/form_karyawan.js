$(document).ready(function(){
	$('#company').select2({
	    placeholder: "Pilih Perusahaan"
	});

	$('#jenkel').select2({
	    placeholder: "Pilih Jenis Kelamin"
	});

	$('#status_nikah').select2({
	    placeholder: "Pilih Status Nikah"
	});

	$('#provinsi').select2({
	    placeholder: "Pilih Provinsi"
	});

	$('#agama').select2({
	    placeholder: "Pilih Agama"
	});

	$('#pendidikan').select2({
	    placeholder: "Pilih Pendidikan"
	});

	$('#golongan_darah').select2({
	    placeholder: "Pilih Golongan Darah"
	});

	$('#divisi').select2({
	    placeholder: "Pilih Divisi"
	});

	$('#jabatan').select2({
	    placeholder: "Pilih Jabatan"
	});

	$("#provinsi").val(null).trigger('change');
	$("#company").val(null).trigger('change');

	$("#provinsi").change(function(){
		var provinsi = $(this).val();
		$("#kota").val(null).trigger('change');

		$("#kota").select2({
			placeholder: "Pilih Kota",
			allowClear: true,
			ajax: {
				url: base_url+'Karyawan/kota/'+provinsi,
				dataType: 'json',
				delay: 250,
				data: function(params){
					return {
						q: params.term
					}
				},
				processResults: function(data) {
					var results = [];
					$.each(data, function(i, val){
						results.push({
							id: val.id,
							text: val.text
						});
					});

					return {
						results : results
					}
				}
			}
		});
	});

	$("#company").change(function(){
		var company = $(this).val();

		$("#jabatan").val(null).trigger('change');

		$("#jabatan").select2({
			placeholder: "Pilih Jabatan",
			allowClear: true,
			ajax: {
				url: base_url+'Karyawan/jabatan/'+company,
				dataType: 'json',
				delay: 250,
				data: function(params){
					return {
						q: params.term
					}
				},
				processResults: function(data) {
					var results = [];
					$.each(data, function(i, val){
						results.push({
							id: val.id,
							text: val.text
						});
					});

					return {
						results : results
					}
				}
			}
		});

		$("#divisi").val(null).trigger('change');

		$("#divisi").select2({
			placeholder: "Pilih Divisi",
			allowClear: true,
			ajax: {
				url: base_url+'Karyawan/divisi/'+company,
				dataType: 'json',
				delay: 250,
				data: function(params){
					return {
						q: params.term
					}
				},
				processResults: function(data) {
					var results = [];
					$.each(data, function(i, val){
						results.push({
							id: val.id,
							text: val.text
						});
					});

					return {
						results : results
					}
				}
			}
		});
	});

	$("#kota").select2({
		placeholder: "Pilih Kota"
	});

	$('#tgl_lahir, #tgl_masuk').datepicker({
        todayHighlight: true,
        orientation: "bottom left",
        format: 'dd/mm/yyyy',
        templates: {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    });

    $("#form-data").validate({
    	submitHandler: function(form) {
			formData = new FormData($(form)[0]);
			$.ajax({
				url : base_url+"Karyawan/add_karyawan",
		        type : "post",
		        data : formData,
		        async : false,
		        cache : false,
		        dataType : "json",
		        contentType : false,
		        processData : false,
		        beforeSend : function(){
		            mApp.blockPage({
		                overlayColor: '#000000',
		                state: 'primary'
		            });
		        },
		        complete : function(){
		            mApp.unblockPage();
		        },
		        success : function(data) {
		            if(data.status == 1) {
		                toastr.success(data.message);
		                window.location.href = base_url+"Karyawan/index";
		            } else {
		                toastr.warning(data.message);
		            }
		        },
		        error : function() {
		            toastr.warning('Terjadi kesalahan saat menyimpan data');
		        }
			});

			return false;
    	}
    });
});