$(document).ready(function(){
	var cache = sessionStorage.getItem('rekap_save_state');
	if (cache) {
		cache = JSON.parse(cache);
		var start = cache.awal;
		var end = cache.akhir;
	} else {
		var start = moment().format('DD/MM/YYYY');
		var end = moment().format('DD/MM/YYYY');
	}

	// search parameter
	var companySearch = $("#company-search").val();

	var table = $("#tb-jenis").DataTable({
		processing : true,
		responsive :true,
		language : {
	        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="font-size:36px;"></i><span class="sr-only"></span> '
	    },
		ajax : base_url+'Absensi/data_absensi?datestart='+start+'&dateend='+end+'&company='+companySearch,
		columns : [
			{'orderable' : false},
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			{'className' : "dt-right"}
		]
	});

	$('#search').daterangepicker({
        buttonClasses: 'm-btn btn',
        applyClass: 'btn-primary',
        cancelClass: 'btn-secondary',
        startDate: start,
        endDate: end,
        locale: {
            format: 'DD/MM/YYYY'
        }
    }, function (start, end){
    	start = start.format('DD/MM/YYYY');
    	end = end.format('DD/MM/YYYY');

    	var cache = {
    		awal : start,
    		akhir : end
    	}

    	sessionStorage.setItem('rekap_save_state', JSON.stringify(cache));
    });

	$("#company-search").select2({
		placeholder : "Pilih Perusahaan"
	});

    $("#search-btn").click(function(){
    	// search parameter
		var companySearch = $("#company-search").val();
		
    	var cache = sessionStorage.getItem('rekap_save_state');
		if (cache) {
			cache = JSON.parse(cache);
			var start = cache.awal;
			var end = cache.akhir;
		} else {
			var start = start;
			var end = end;
		}

    	table.ajax.url(base_url+'Absensi/data_absensi?datestart='+start+'&dateend='+end+'&company='+companySearch).load();
    });
});