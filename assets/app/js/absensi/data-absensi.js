var sessionName = 'data-absensi-save-state';
var sessionData = sessionGetItem(sessionName);
if (sessionData) {
	sessionData = JSON.parse(sessionData);
	var start = sessionData.awal;
	var end = sessionData.akhir;
} else {
	var start = moment().format('DD/MM/YYYY');
	var end = moment().format('DD/MM/YYYY');
}

let configDateRange = dateRangeConfig();
$('#search').daterangepicker(configDateRange, function (datestart, dateend){
	start = datestart.format('DD/MM/YYYY');
	end = dateend.format('DD/MM/YYYY');

	var cache = {
		awal : start,
		akhir : end
	}

	sessionSetItem(sessionName, cache);
});  

$("#company-search").select2();
var companySearch = $("#company-search").val();

let configTbl = serverSideTable('Absensi/data_presensi?company='+companySearch+'&datestart='+start+'&dateend='+end);
configTbl.columns = [
	{'orderable' : false}, 
	null, 
	null, 
	null, 
	null, 
	null,
	null,
	{'orderable' : false}
];
configTbl.fixedHeader = true;
var table = $("#tb-absensi").DataTable(configTbl);

$("#search-btn").click(function(){
	var sessionName = 'data-absensi-save-state';
	var sessionData = sessionGetItem(sessionName);
	if (sessionData) {
		sessionData = JSON.parse(sessionData);
		var start = sessionData.awal;
		var end = sessionData.akhir;
	} else {
		var start = moment().format('DD/MM/YYYY');
		var end = moment().format('DD/MM/YYYY');
	}

	table.ajax.url(base_url+'Absensi/data_presensi?company='+companySearch+'&datestart='+start+'&dateend='+end).load();
});

$(document).on('click', '.edit', function(){
	let id = $(this).data('refid');
	let config = axiosDefault('get', 'Absensi/absensi_id?id='+id);
	blockModal();
	axios(config).then(function(response){
		$("#m_modal").modal('toggle');
		$("#id").val(response.data.id);
		$("#id_karyawan").val(response.data.id_karyawan);
		$("#company").val(response.data.id_company).trigger('change');
		$("#nama").attr('disabled', false);
		initSelect('#nama', 'Kunjungan/data_karyawan?company='+response.data.id_company, response.data.id_karyawan);
		$("#nama").attr('disabled', true);
		$("#tgl").val(tglConvert(response.data.tgl));
		$("#absensi").val(response.data.status).trigger('change');
		$("#keterangan").val(response.data.keterangan);
		unblockModal();
	}).catch(function(error){
		unblockModal();
	});
});

$("#form-data").submit(function(event){
	event.preventDefault();
	formData = new FormData($(this)[0]);
	let config = axiosDefault('post', 'Absensi/klarifikasi', formData);
	blockModal();
	axios(config).then(function(response){
		if (response.data.status == 1) {
			table.ajax.reload(null, false);
			toastr.success(response.data.message);
			$("#m_modal").modal('toggle');
		} else {
			toastr.warning(response.data.message);
		}

		unblockModal();
	}).catch(function(error){
		catchMessage('save', error);
		unblockModal();
	});

	return false;
});