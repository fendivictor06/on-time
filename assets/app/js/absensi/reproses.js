var sessionName = 'reproses-absensi-save-state';
var sessionData = sessionGetItem(sessionName);
if (sessionData) {
	sessionData = JSON.parse(sessionData);
	var start = sessionData.awal;
	var end = sessionData.akhir;
} else {
	var start = moment().format('DD/MM/YYYY');
	var end = moment().format('DD/MM/YYYY');
}

let configDateRange = dateRangeConfig();
$('#search').daterangepicker(configDateRange, function (datestart, dateend){
	start = datestart.format('DD/MM/YYYY');
	end = dateend.format('DD/MM/YYYY');

	var cache = {
		awal : start,
		akhir : end
	}

	sessionSetItem(sessionName, cache);
});  

var company = $("#company-search").val();
initSelect('#karyawan-search', 'Kunjungan/data_karyawan?company='+company, '');

$("#company-search").change(function(){
	let company = $(this).val();
	dropdownSelect('#karyawan-search', 'Kunjungan/data_karyawan?company='+company, 'Pilih Karyawan', 0);
});

$("#form-reproses").submit(function(event){
	event.preventDefault();

	var company = $("#company-search").val();
	var karyawan = $("#karyawan-search").val();

	blockContent();
	let config = axiosDefault('get', 'Absensi/proses_absensi?company='+company+'&karyawan='+karyawan+'&datestart='+start+'&dateend='+end, {});
	axios(config).then(function(response){
		unblockContent();
		toastr.success(response.data.message);
	}).catch(function(error){
		unblockContent();
		catchMessage('save', error);
	});
});