$(document).ready(function(){
	datePicker('#mulai, #selesai');
});

function clear_form() {
	$("#id").val("");
	document.getElementById('form-data').reset();
}

$("#add_new").click(function(){
	clear_form();
	$("#provinsi").val("").trigger("change");
	$("#m_modal").modal('show');
});

$("#m_modal").on('show.bs.modal', function() {
	$('#provinsi').select2({
	    placeholder: "Pilih Provinsi"
	});
});

$("#provinsi").change(function(){
	var provinsi = $(this).val();
	$("#kota").val("").trigger("change");
	$("#kota").select2({
		ajax : {
			url : base_url+"company/kota",
			dataType : "json",
			type : "post",
			delay : 250,
			data : function(params) {
				return {
					search : params.term,
					provinsi : provinsi
				}
			},
			processResults : function(data) {
				var results = [];
				$.each(data, function(index, item) {
					results.push({
						id : item.id, 
						text : item.kota
					});
				});

				return {
					results : results
				};
			}
		},
		placeholder : "Pilih Kota"
	});
});

var table = $("#tb-perusahaan").DataTable({
	processing : true,
	serverSide : true,
	responsive :true,
	ajax : base_url+'Company/data_company',
	columns : [
		{'orderable' : false},
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		{'orderable' : false},
		null,
		{'orderable' : false}
	],
	language : {
        processing: '<i class="fa fa-refresh fa-spin" style="font-size:32px;"></i><span class="sr-only"></span>'
    }
});

$("#form-data").submit(function(event) {
	event.preventDefault();
	formData = new FormData($(this)[0]);
	blockModal();
	let config = axiosDefault('post', 'Company/add_company', formData);

	axios(config).then(function(response){
		if (response.data.status == 1) {
			table.ajax.reload(null, false);
			clear_form();
			toastr.success(response.data.message);
			$("#m_modal").modal('toggle');
		} else {
			toastr.warning(response.data.message);
		}

		unblockModal();
	}).catch(function(error){
		catchMessage('save', error);
        unblockModal();
	});

	return false;
});

function select_kota(provinsi, kota) {
	$("#kota").select2('data', null);
	$.ajax({
		url : base_url+"company/kota",
		dataType : "json",
		type : "post",
		data : {
			provinsi : provinsi
		},	
		success : function(data) {
			var results = [];
			$.each(data, function(index, item) {
				results.push({
					id : item.id, 
					text : item.kota
				});
			});

			$("#kota").select2({
				data : results
			});

			$("#kota").val(kota).trigger("change");
		}
	});
}

$(document).on('click', '.update', function(){
	refid = $(this).data('refid');
	$.ajax({
		url : base_url+"Company/id_company",
		type : "post",
		data : {
			'id' : refid
		},
		dataType : "json",
		success : function(data) {
			arr = [];
			$("#m_modal").modal('show');
			$("#id").val(data.id);
			$("#company").val(data.company);
			$("#email").val(data.email);
			$("#telpon").val(data.telpon);
			$("#alamat").val(data.alamat);
			$("#deskripsi").val(data.keterangan);
			$("#mulai").datepicker('setDate', tglConvert(data.mulai));
			$("#selesai").datepicker('setDate', tglConvert(data.selesai));
			$("#jumlah").val(data.jumlah_karyawan);
			$("#provinsi").val(data.id_provinsi).trigger("change");
			select_kota(data.id_provinsi, data.id_kota);
			$("#salary").val(data.flag_gaji).trigger('change');
		},
		error : function() {
			toastr.warning('Terjadi kesalahan saat memuat data');
		}
	});
});

$(document).on('click', '.delete', function(){
	refid = $(this).data('refid');
	swal({
	  	text: "Apakah anda yakin akan menghapus data ?",
	  	icon: "warning",
	  	buttons: true,
	  	dangerMode: true,
	})
	.then((willDelete) => {
	  	if (willDelete) {
	  		$.ajax({
	  			url : base_url+'Company/hapus_company',
	  			type : 'post',
	  			data : {
	  				'id' : refid
	  			},
	  			dataType : 'json',
	  			success : function(data) {
	  				if(data.status == 1) {
		                table.ajax.reload(null, false);
		                toastr.success(data.message);
		            } else {
		                toastr.warning(data.message);
		            }
	  			},
	  			error : function() {
	  				toastr.warning('Terjadi kesalahan saat menghapus data');
	  			}
	  		});
	  	}
	});
});