var cache = sessionStorage.getItem('pulang_save_state');
if (cache) {
	cache = JSON.parse(cache);
	var start = cache.awal;
	var end = cache.akhir;
} else {
	var start = moment().format('DD/MM/YYYY');
	var end = moment().format('DD/MM/YYYY');
}

var table = $("#tb-jenis").DataTable({
	processing : true,
	serverSide : true,
	responsive :true,
	language : {
        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="font-size:36px;"></i><span class="sr-only"></span> '
    },
	ajax : base_url+'Pulang_Awal/data_pulang?datestart='+start+'&dateend='+end,
	columns : [
		{'orderable' : false},
		null,
		null,
		null,
		null,
		null,
		null,
		{'orderable' : false}
	]
});

$('#search').daterangepicker({
    buttonClasses: 'm-btn btn',
    applyClass: 'btn-primary',
    cancelClass: 'btn-secondary',
    startDate: start,
    endDate: end,
    locale: {
        format: 'DD/MM/YYYY'
    }
}, function (start, end){
	start = start.format('DD/MM/YYYY');
	end = end.format('DD/MM/YYYY');

	var cache = {
		awal : start,
		akhir : end
	}

	sessionStorage.setItem('pulang_save_state', JSON.stringify(cache));
});  

$("#search-btn").click(function(){
	var cache = sessionStorage.getItem('pulang_save_state');
	if (cache) {
		cache = JSON.parse(cache);
		var start = cache.awal;
		var end = cache.akhir;
	} else {
		var start = moment().format('DD/MM/YYYY');
		var end = moment().format('DD/MM/YYYY');
	}

	table.ajax.url(base_url+'Pulang_Awal/data_pulang?datestart='+start+'&dateend='+end).load();
});