$("#form-data").validate({
	submitHandler : function(form) {
		formData = new FormData($(form)[0]);
		let config = axiosDefault('post', 'User/ubah_password', formData);
		blockContent();
		axios(config).then(function(response){
			if (response.data.status == 1) {
				toastr.success(response.data.message);
			} else {
				toastr.warning(response.data.message);
			}

			unblockContent();
		}).catch(function(error){
			catchMessage('save', error);
			unblockContent();
		});
		return false;
	}
});