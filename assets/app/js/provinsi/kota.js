var table = $("#tb-kota").DataTable({
	processing : true,
	serverSide : true,
	responsive :true,
	ajax : base_url+'Provinsi/data_kota',
	columns : [
		{'orderable' : false},
		null,
		null,
		{'orderable' : false}
	]
});

function clear_form() {
	$("#id").val("");
	$("#provinsi").val("").trigger("change");
	document.getElementById('form-data').reset();
}

$("#add_new").click(function(){
	clear_form();
	$("#m_modal").modal('show');
});

$("#m_modal").on('show.bs.modal', function() {
	$('#provinsi').select2({
	    placeholder: "Pilih Provinsi"
	});
});

$("#form-data").submit(function(event) {
	event.preventDefault();
	formData = new FormData($(this)[0]);
	$.ajax({
		url : base_url+"Provinsi/add_kota",
        type : "post",
        data : formData,
        async : false,
        cache : false,
        dataType : "json",
        contentType : false,
        processData : false,
        beforeSend : function(){
            mApp.block('#m_modal .modal-content', {
                overlayColor: '#000000',
                type: 'loader',
                state: 'success',
                size: 'lg'
            });
        },
        complete : function(){
            mApp.unblock('#m_modal .modal-content');        
        },
        success : function(data) {
            if(data.status == 1) {
                table.ajax.reload(null, false);
                clear_form();
                toastr.success(data.message);
                $("#m_modal").modal('toggle');
            } else {
                toastr.warning(data.message);
            }
        },
        error : function() {
            toastr.warning('Terjadi kesalahan saat menyimpan data');
        }
	});

	return false;
});

$(document).on('click', '.update', function(){
	refid = $(this).data('refid');
	$.ajax({
		url : base_url+"Provinsi/id_kota",
		type : "post",
		data : {
			'id' : refid
		},
		dataType : "json",
		success : function(data) {
			$("#m_modal").modal('show');
			$("#id").val(data.id);
			$("#provinsi").val(data.id_provinsi).trigger('change');
			$("#kota").val(data.kota)
		},
		error : function() {
			toastr.warning('Terjadi kesalahan saat memuat data');
		}
	});
});

$(document).on('click', '.delete', function(){
	refid = $(this).data('refid');
	swal({
	  	text: "Apakah anda yakin akan menghapus data ?",
	  	icon: "warning",
	  	buttons: true,
	  	dangerMode: true,
	})
	.then((willDelete) => {
	  	if (willDelete) {
	  		$.ajax({
	  			url : base_url+'Provinsi/hapus_kota',
	  			type : 'post',
	  			data : {
	  				'id' : refid
	  			},
	  			dataType : 'json',
	  			success : function(data) {
	  				if(data.status == 1) {
		                table.ajax.reload(null, false);
		                toastr.success(data.message);
		            } else {
		                toastr.warning(data.message);
		            }
	  			},
	  			error : function() {
	  				toastr.warning('Terjadi kesalahan saat menghapus data');
	  			}
	  		});
	  	}
	});
});