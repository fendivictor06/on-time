var start = moment().format('DD/MM/YYYY');
var end = moment().format('DD/MM/YYYY');

$("#add-tgl").click(function(){
	$("#modal-tgl").modal();
	
	resetForm('form-data-tgl');
	$("#lokasi-tgl").val(null).trigger('change');
	$("#nama-tgl").val(null).trigger('change');
});

$("#add-hari").click(function(){
	$("#modal-hari").modal();
	
	resetForm('form-data-hari');
	$("#lokasi-hari").val(null).trigger('change');
	$("#nama-hari").val(null).trigger('change');
	$("#hari").val(null).trigger('change');
});

$(document).ready(function(){
	$("#company-tgl").select2({
		placeholder : "Pilih Perusahaan"
	});

	$("#company-hari").select2({
		placeholder : "Pilih Perusahaan"
	});

	$("#hari").select2({
		placeholder : "Pilih Hari"
	});

	dropdownSelect('#lokasi-tgl', 'Kunjungan/data_lokasi', 'Pilih Lokasi', 0);
	datePicker('#tgl');

	var companyTgl = $("#company-tgl").val();
	dropdownSelect('#nama-tgl', 'Kunjungan/data_karyawan?company='+companyTgl, 'Pilih Karyawan', 0);

	dropdownSelect('#lokasi-hari', 'Kunjungan/data_lokasi', 'Pilih Lokasi', 0);

	var companyHari = $("#company-hari").val();
	dropdownSelect('#nama-hari', 'Kunjungan/data_karyawan?company='+companyHari, 'Pilih Karyawan', 0);
});

var companySearchTgl = $("#company-search-tgl").val();
var companySearchHari = $("#company-search-hari").val();
var hari = $("#search-hari").val();

var configTbTgl = serverSideTable('Kunjungan/data_kunjungan_tgl?company='+companySearchTgl+'&datestart='+start+'&dateend='+end);
configTbTgl.columns = [
	{'orderable' : false}, 
	null, 
	null, 
	null, 
	null, 
	null,
	{'orderable' : false}
];
var tableTgl = $("#tb-tgl").DataTable(configTbTgl);

var configTbHari = serverSideTable('Kunjungan/data_kunjungan_hari?company='+companySearchHari+'&hari='+hari);
configTbHari.columns = [
	{'orderable' : false}, 
	null, 
	null, 
	null, 
	null, 
	null,
	{'orderable' : false}
];
var tableHari = $("#tb-harian").DataTable(configTbHari);

$("#form-data-tgl").validate({
	submitHandler : function(form) {
		formData = new FormData($(form)[0]);
		let config = axiosDefault('post', 'Kunjungan/add_kunjungan_tgl', formData);
		blockModal();
		axios(config).then(function(response){
			if (response.data.status == 1) {
				toastr.success(response.data.message);
				tableTgl.ajax.reload(null, false);
				$("#modal-tgl").modal('toggle');
			} else {
				toastr.warning(response.data.message);
			}

			unblockModal();
		}).catch(function(error){
			catchMessage('save', error);
			unblockModal();
		});
		return false;
	}
});

$("#modal-tgl").on('show.bs.modal', function() {
	$("#form-data-tgl").find('input, textarea, select').change(function(){
		$(this).valid();
	});
});

$("#form-data-hari").validate({
	submitHandler : function(form) {
		formData = new FormData($(form)[0]);
		let config = axiosDefault('post', 'Kunjungan/add_kunjungan_hari', formData);
		blockModal();
		axios(config).then(function(response){
			if (response.data.status == 1) {
				toastr.success(response.data.message);
				tableHari.ajax.reload(null, false);
				$("#modal-hari").modal('toggle');
			} else {
				toastr.warning(response.data.message);
			}

			unblockModal();
		}).catch(function(error){
			catchMessage('save', error);
			unblockModal();
		});
		return false;
	}
});

$("#modal-hari").on('show.bs.modal', function() {
	$("#form-data-hari").find('input, textarea, select').change(function(){
		$(this).valid();
	});
});

$(document).on('click', '.edit-tgl', function(){
	refid = $(this).data('refid');
	let config = axiosDefault('get', 'Kunjungan/id_kunjungan_tgl?id='+refid);
	blockModal();
	axios(config).then(function(response){
		$("#modal-tgl").modal('toggle');
		$("#id-tgl").val(response.data.id);
		$("#company-tgl").val(response.data.id_company).trigger('change');
		$("#tgl").datepicker('setDate', tglConvert(response.data.tgl));
		$("#keterangan-tgl").val(response.data.keterangan);

		initSelect('#nama-tgl', 'Kunjungan/data_karyawan?company='+response.data.id_company, response.data.id_karyawan);
		initSelect('#lokasi-tgl', 'Kunjungan/data_lokasi?company='+response.data.id_company, response.data.id_lokasi);

		unblockModal();
	}).catch(function(error){
		catchMessage('get', error);
		unblockModal();
	});
});

$(document).on('click', '.edit-hari', function(){
	refid = $(this).data('refid');
	let config = axiosDefault('get', 'Kunjungan/id_kunjungan_hari?id='+refid);
	blockModal();
	axios(config).then(function(response){
		$("#modal-hari").modal('toggle');
		$("#id-hari").val(response.data.id);
		$("#lokasi-hari").val(response.data.id_lokasi).trigger('change');
		$("#hari").val(response.data.hari).trigger('change');
		$("#keterangan-hari").val(response.data.keterangan);

		initSelect('#nama-hari', 'Kunjungan/data_karyawan?company='+response.data.id_company, response.data.id_karyawan);
		initSelect('#lokasi-hari', 'Kunjungan/data_lokasi?company='+response.data.id_company, response.data.id_lokasi);

		unblockModal();
	}).catch(function(error){
		catchMessage('get', error);
		unblockModal();
	});
});

$(document).on('click', '.delete-tgl', function(){
	refid = $(this).data('refid');
	swal({
	  	text: "Apakah anda yakin akan menghapus data ?",
	  	icon: "warning",
	  	buttons: true,
	  	dangerMode: true,
	})
	.then((willDelete) => {
	  	if (willDelete) {
	  		blockPage();
	  		let config = axiosDefault('get', 'Kunjungan/hapus_tgl?id='+refid);
	  		axios(config).then(function(response){
	  			if (response.data.status == 1) {
					table.ajax.reload(null, false);
					toastr.success(response.data.message);
				} else {
					toastr.warning(response.data.message);
				}

	  			unblockPage();
	  		}).catch(function(error){
	  			catchMessage('delete', error);
	  			unblockPage();
	  		});	
	  	}
	});
});

$(document).on('click', '.delete-hari', function(){
	refid = $(this).data('refid');
	swal({
	  	text: "Apakah anda yakin akan menghapus data ?",
	  	icon: "warning",
	  	buttons: true,
	  	dangerMode: true,
	})
	.then((willDelete) => {
	  	if (willDelete) {
	  		blockPage();
	  		let config = axiosDefault('get', 'Kunjungan/hapus_hari?id='+refid);
	  		axios(config).then(function(response){
	  			if (response.data.status == 1) {
					table.ajax.reload(null, false);
					toastr.success(response.data.message);
				} else {
					toastr.warning(response.data.message);
				}

	  			unblockPage();
	  		}).catch(function(error){
	  			catchMessage('delete', error);
	  			unblockPage();
	  		});	
	  	}
	});
});


let configDateRange = dateRangeConfig();
$('#search-tgl').daterangepicker(configDateRange, function (datestart, dateend){
	start = datestart.format('DD/MM/YYYY');
	end = dateend.format('DD/MM/YYYY');
});  

$("#search-btn-tgl").click(function(){
	tableTgl.ajax.url(base_url+'Kunjungan/data_kunjungan_tgl?company='+companySearchTgl+'&datestart='+start+'&dateend='+end).load();
});

$("#search-btn-hari").click(function(){
	var hari = $("#search-hari").val();
	tableHari.ajax.url(base_url+'Kunjungan/data_kunjungan_hari?company='+companySearchHari+'&hari='+hari).load();
});