var table = $("#tb-jenis").DataTable({
    processing : true,
    serverSide : true,
    responsive :true,
    ajax : base_url+'Shift/data_libur',
    columns : [
        {'orderable' : false},
        // null,
        null,
        null,
        {'orderable' : false}
    ],
    order : [[1, 'desc']]
});

$("#m_modal").on('show.bs.modal', function() {
    $('#company').select2({
        placeholder: "Pilih Perusahaan"
    });
});

$('#tgl').datepicker({
    todayHighlight: true,
    orientation: "bottom left",
    format: 'dd/mm/yyyy',
    templates: {
        leftArrow: '<i class="la la-angle-left"></i>',
        rightArrow: '<i class="la la-angle-right"></i>'
    }
});

function clear_form() {
    $("#id").val("");
    document.getElementById('form-data').reset();
}

$("#add_new").click(function(){
    clear_form();
    $("#m_modal").modal('show');
});

$("#form-data").submit(function(event) {
    event.preventDefault();
    formData = new FormData($(this)[0]);
    $.ajax({
        url : base_url+"Shift/add_libur",
        type : "post",
        data : formData,
        async : false,
        cache : false,
        dataType : "json",
        contentType : false,
        processData : false,
        beforeSend : function(){
            mApp.block('#m_modal .modal-content', {
                overlayColor: '#000000',
                type: 'loader',
                state: 'success',
                size: 'lg'
            });
        },
        complete : function(){
            mApp.unblock('#m_modal .modal-content');        
        },
        success : function(data) {
            if(data.status == 1) {
                table.ajax.reload(null, false);
                clear_form();
                toastr.success(data.message);
                $("#m_modal").modal('toggle');
            } else {
                toastr.warning(data.message);
            }
        },
        error : function() {
            toastr.warning('Terjadi kesalahan saat menyimpan data');
        }
    });

    return false;
});

$(document).on('click', '.update', function(){
    refid = $(this).data('refid');
    $.ajax({
        url : base_url+"Shift/id_libur",
        type : "post",
        data : {
            'id' : refid
        },
        dataType : "json",
        success : function(data) {
            $("#m_modal").modal('show');
            $("#id").val(data.id);
            $("#company").val(data.id_company).trigger('change');
            $("#tgl").val(data.tgl);
            $("#keterangan").val(data.keterangan);
        },
        error : function() {
            toastr.warning('Terjadi kesalahan saat memuat data');
        }
    });
});

$(document).on('click', '.delete', function(){
    refid = $(this).data('refid');
    swal({
        text: "Apakah anda yakin akan menghapus data ?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {
            $.ajax({
                url : base_url+'Shift/hapus_libur',
                type : 'post',
                data : {
                    'id' : refid
                },
                dataType : 'json',
                success : function(data) {
                    if(data.status == 1) {
                        table.ajax.reload(null, false);
                        toastr.success(data.message);
                    } else {
                        toastr.warning(data.message);
                    }
                },
                error : function() {
                    toastr.warning('Terjadi kesalahan saat menghapus data');
                }
            });
        }
    });
});