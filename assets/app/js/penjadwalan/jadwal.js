function clear_form() {
	$("#id").val("");
	document.getElementById('form-data').reset();
}

$("#add_new").click(function(){
	clear_form();
	$("#m_modal").modal('show');
	init_select();
});

$("#copy_jadwal").click(function(){
	$("#m_copy").modal('show');
	$("#company-copy").val('').trigger('change');
	$("#mode-copy").val('');
	$("#divisi").css('display', 'none');
	$("#target").css('display', 'none');
});

$("#m_copy").on('show.bs.modal', function() {
	$('#company-copy').select2({
	    placeholder: "Pilih Perusahaan"
	});

	$('#karyawan-copy').select2({
	    placeholder: "Pilih Karyawan"
	});
});

$("#m_modal").on('show.bs.modal', function() {
	$('#company').select2({
	    placeholder: "Pilih Perusahaan"
	});

	$('#shift').select2({
	    placeholder: "Pilih Shift"
	});

	$('#karyawan').select2({
	    placeholder: "Pilih Karyawan"
	});
});

var init_select = function(id_shift, id_karyawan) {
	var company = $("#company").val();
	var companySearch = $("#company-search").val();
	var arr_shift = [];
	var arr_karyawan = [];
	var karyawan_search = [];
	$("#karyawan option").each(function() {
	    $(this).remove();
	});

	$("#shift option").each(function() {
	    $(this).remove();
	});

	$.ajax({
		url : base_url+'Shift/ajax_shift/'+company,
		dataType : 'json',
		success: function(data) {
			$("#shift").attr('enabled', true);
			$.each(data, function(){
				$("#shift").append(
					$('<option></option>').text(this.text).val(this.id)
				);
			});

			if (id_shift != '') {
				$("#shift").val(id_shift).trigger('change');
			}
		}
	});

	$.ajax({
		url : base_url+'Shift/ajax_karyawan/'+company,
		dataType : 'json',
		success: function(data) {
			$("#karyawan").attr('enabled', true);
			$.each(data, function(){
				$("#karyawan").append(
					$('<option></option>').text(this.text).val(this.id)
				);
			});

			if (id_karyawan != '') {
				$("#karyawan").val(id_karyawan).trigger('change');
			}
		}
	});
}

var cache = sessionStorage.getItem('jadwal_save_state');
if (cache) {
	cache = JSON.parse(cache);
	var start = cache.awal;
	var end = cache.akhir;
} else {
	var start = moment().format('DD/MM/YYYY');
	var end = moment().format('DD/MM/YYYY');
}

var table = $("#tb-jenis").DataTable({
	processing : true,
	serverSide : true,
	responsive :true,
	ajax : base_url+'Shift/data_penjadwalan?datestart='+start+'&dateend='+end,
	language : {
        processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="font-size:36px;"></i><span class="sr-only"></span> '
    },
	columns : [
		{'orderable' : false},
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		{'orderable' : false}
	]
});

$(document).ready(function(){
	$("#company-search").val('').trigger('change');
	$("#company-search").select2({
		placeholder : "Pilih Perusahaan"
	});

	$('#tgl_mulai, #tgl_selesai, #tgl_mulai-copy, #tgl_selesai-copy').datepicker({
	    todayHighlight: true,
	    orientation: "auto",
	    format: 'dd/mm/yyyy',
	    templates: {
	        leftArrow: '<i class="la la-angle-left"></i>',
	        rightArrow: '<i class="la la-angle-right"></i>'
	    }
	});

	$('#search').daterangepicker({
        buttonClasses: 'm-btn btn',
        applyClass: 'btn-primary',
        cancelClass: 'btn-secondary',
        startDate: start,
        endDate: end,
        locale: {
            format: 'DD/MM/YYYY'
        }
    }, function (start, end){
    	start = start.format('DD/MM/YYYY');
    	end = end.format('DD/MM/YYYY');

    	var cache = {
    		awal : start,
    		akhir : end
    	}

    	sessionStorage.setItem('jadwal_save_state', JSON.stringify(cache));
    });  
});

$("#search-btn").click(function(){
	var cache = sessionStorage.getItem('jadwal_save_state');
	if (cache) {
		cache = JSON.parse(cache);
		var start = cache.awal;
		var end = cache.akhir;
	} else {
		var start = moment().format('DD/MM/YYYY');
		var end = moment().format('DD/MM/YYYY');
	}

	// search parameter
	var companySearch = $("#company-search").val();
	var karyawanSearch = $("#search-karyawan").val();

	table.ajax.url(base_url+'Shift/data_penjadwalan?datestart='+start+'&dateend='+end+'&company='+companySearch+'&karyawan='+karyawanSearch).load();
});

$("#company").change(function(){
	var company = $(this).val();
	$("#shift").val(null).trigger('change');
	ajax_select2(base_url+'Shift/ajax_shift/'+company, '#shift', 'Pilih Shift');

	$("#karyawan").val(null).trigger('change');
	ajax_select2(base_url+'Shift/ajax_karyawan/'+company, '#karyawan', 'Pilih Karyawan');
});

$("#company-copy").change(function(){
	var company = $(this).val();

	$("#karyawan-copy").val(null).trigger('change');
	ajax_select2(base_url+'Shift/ajax_karyawan/'+company, '#karyawan-copy', 'Pilih Karyawan');

	$("#divisi-copy").val(null).trigger('change');
	ajax_select2(base_url+'Shift/ajax_divisi/'+company, '#divisi-copy', 'Pilih Divisi');

	$("#target-copy").val(null).trigger('change');
	ajax_select2(base_url+'Shift/ajax_karyawan/'+company, '#target-copy', 'Pilih Karyawan');
});

$("#mode-copy").change(function(){
	var mode = $(this).val();

	if (mode == 2) {
		$("#divisi").css('display', '');
		$("#target").css('display', 'none');
	} else {
		$("#divisi").css('display', 'none');
		$("#target").css('display', '');
	}
});

$("#company-search").change(function(){
	var companySearch = $(this).val();

	$("#search-karyawan").val(null).trigger('change');
	ajax_select2(base_url+'Shift/ajax_karyawan/'+companySearch, '#search-karyawan', 'Pilih Karyawan');
});

var ajax_select2 = function(url, element, placeholder) {
	$(element).select2({
		placeholder: placeholder,
		allowClear: true,
		ajax: {
			url: url,
			dataType: 'json',
			delay: 250,
			data: function(params){
				return {
					q: params.term
				}
			},
			processResults: function(data) {
				var results = [];
				$.each(data, function(i, val){
					results.push({
						id: val.id,
						text: val.text
					});
				});

				return {
					results : results
				}
			}
		}
	});
}

$("#form-data").submit(function(event) {
	event.preventDefault();
	formData = new FormData($(this)[0]);
	$.ajax({
		url : base_url+"Shift/add_penjadwalan",
        type : "post",
        data : formData,
        async : false,
        cache : false,
        dataType : "json",
        contentType : false,
        processData : false,
        beforeSend : function(){
            mApp.block('#m_modal .modal-content', {
                overlayColor: '#000000',
                type: 'loader',
                state: 'success',
                size: 'lg'
            });
        },
        complete : function(){
            mApp.unblock('#m_modal .modal-content');        
        },
        success : function(data) {
            if(data.status == 1) {
                table.ajax.reload(null, false);
                clear_form();
                toastr.success(data.message);
                $("#m_modal").modal('toggle');
            } else {
                toastr.warning(data.message);
            }
        },
        error : function() {
            toastr.warning('Terjadi kesalahan saat menyimpan data');
        }
	});

	return false;
});

$(document).on('click', '.update', function(){
	refid = $(this).data('refid');
	$.ajax({
		url : base_url+"Shift/id_penjadwalan",
		type : "post",
		data : {
			'id' : refid
		},
		dataType : "json",
		success : function(data) {
			var arr_shift = [];
			var arr_karyawan = [];
			$("#m_modal").modal('show');
			$("#id").val(refid);
			
			$("#tgl_mulai").val(data.tanggal);
			$("#tgl_selesai").val(data.tanggal);

			$("#company").val(data.id_company).trigger('change');
			init_select(data.id_shift, data.id_karyawan);
		},
		error : function() {
			toastr.warning('Terjadi kesalahan saat memuat data');
		}
	});
});

$(document).on('click', '.delete', function(){
	refid = $(this).data('refid');
	swal({
	  	text: "Apakah anda yakin akan menghapus data ?",
	  	icon: "warning",
	  	buttons: true,
	  	dangerMode: true,
	})
	.then((willDelete) => {
	  	if (willDelete) {
	  		$.ajax({
	  			url : base_url+'Shift/hapus_penjadwalan',
	  			type : 'post',
	  			data : {
	  				'id' : refid
	  			},
	  			dataType : 'json',
	  			success : function(data) {
	  				if(data.status == 1) {
		                table.ajax.reload(null, false);
		                toastr.success(data.message);
		            } else {
		                toastr.warning(data.message);
		            }
	  			},
	  			error : function() {
	  				toastr.warning('Terjadi kesalahan saat menghapus data');
	  			}
	  		});
	  	}
	});
});

$("#form-data-copy").submit(function(event) {
	event.preventDefault();
	formData = new FormData($(this)[0]);
	$.ajax({
		url : base_url+"Shift/copy_jadwal",
        type : "post",
        data : formData,
        async : false,
        cache : false,
        dataType : "json",
        contentType : false,
        processData : false,
        beforeSend : function(){
            mApp.block('#m_copy .modal-content', {
                overlayColor: '#000000',
                type: 'loader',
                state: 'success',
                size: 'lg'
            });
        },
        complete : function(){
            mApp.unblock('#m_copy .modal-content');        
        },
        success : function(data) {
            if(data.status == 1) {
                table.ajax.reload(null, false);
                toastr.success(data.message);
                $("#m_copy").modal('toggle');
            } else {
                toastr.warning(data.message);
            }
        },
        error : function() {
            toastr.warning('Terjadi kesalahan saat menyimpan data');
        }
	});

	return false;
});